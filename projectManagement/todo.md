# Nächstes Semester To-Do:

## Auto-Logout beim ausziehen der VR-Brille
- Sobald das Headset ausgezogen wird, soll die operation nach 10 sekunden abgebrochen, werden (Von der Operation -> Login/register menu)


## Ohne Brille die VR Brille benutzen
- Beim Start der Software eine initiale Info, dass Accessoir wie beispielsweise eine Brille ausgezogen wird, sobald man das VR-Headset aufsetzt. 


## Zoom features im Statistik Menü hinzufügen
- Im Statistiks Menu soll die Möglichkeit entstehen, dass der Graph oder das ausgeschnittene Objekt(momentan ein Bild) mit Klick auf den Zoom Button vergrößert wird.

## Anzeigen des ausgeschnittenen Objektes
- Sobald das Objekt geschnitten wurde, wird ein Mesh generiert, das im Result-Screen als auch im Statistik-Menü angezeigt wird. Aktuell liegt eine fertige Funktion bereit, die nur noch dem Mesh (als Gameobject) zugewiesen werden muss.

## VR-Tutorial für die Operation
- VR Tutorial für neue Nutzer. Beispielsweise einen Apfel schneiden

## Statistics→ Charts: Limitierung auf 10 Operationen
- Aktuell ist es so, dass der Rahmen gesprengt wird, sobald mehr als 10 Operationen am User gemacht werden. 

## Statistics → AllStatistics: Funktionalität des Details Button
- Der “Details” Button könnte alle Informationen der Operation bspw. über ein Popup anzeigen


## Export Funktionalität nur für den Admin und bestimmen der Export Formate
- Nur der Admin sollte die Möglichkeit haben, den Admin-Screen zu sehen
- Der Admin sollte sich für ein Format entscheiden dürfen, als was die Operationen exportiert werden. (Gast hat die Funktionalität schon)
- Differenzierung der User zwischen Admin und nicht-Admin


## Erstellung eines neuen Raums
- Raumdesign verbessern (realistischerer OP Raum)
- Stuhl höhenverstellbar


## Das hda-Logo ersetzen
- Logo ersetzen: statt hda Koni Trainer → Klinikum Würzburg

## Guide
- Einen ausführlichen Guide hinzufügen in dem unteranderem die medizinischen Begriffe, Measurements und Technicken erklärt werden.

## Usability der Schlinge
- Schlinge fliegt weg beim loslassen → anstrengend die ganze Zeit zu halten
- Steuerung der Schlinge intuitiver gestalten

## Hover-Over Information in Statistics-Menu hinzufügen
- Es sollen Erklärungen auftauchen, die beim Hovern mit der Maus oder VR-Ray entstehen und eine kleine Definition bieten. Bspw. der LLETZ-Score
