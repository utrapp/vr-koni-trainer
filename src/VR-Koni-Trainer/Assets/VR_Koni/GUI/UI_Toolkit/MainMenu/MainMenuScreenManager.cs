using System;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using UnityEngine.UIElements;
using VR_Koni.GUI.UI_Toolkit.Helper;
using VR_Koni.GUI.UI_Toolkit.MainMenu.Views;
using VR_Koni.GUI.UI_Toolkit.Popup;
using VR_Koni.UserData;

namespace VR_Koni.GUI.UI_Toolkit.MainMenu
{
    public class MainMenuScreenManager : UIManager
    {
        private IMainMenuView[] allViews;
        private readonly MainMenuHomeView homeView = new();
        private readonly MainMenuStatisticsView statisticsView = new();
        private readonly MainMenuGuideView guideView = new();
        private readonly MainMenuSettingsView settingsView = new();
        private readonly MainMenuTutorialsView tutorialsView = new();
        private readonly MainMenuAdminView adminView = new();

        private Button switchHomeViewButton;
        private Button switchStatisticsViewButton;
        private Button switchGuideViewButton;
        private Button switchSettingsViewButton;
        private Button switchTutorialsViewButton;
        private Button switchAdminViewButton;
        private Button userNameButton;
        private Button startVRButton, logoutButton, quitButton;
        private TextElement viewHeader;

#region LocalizedStringsEnterVR

        private readonly LocalizedString popupHeaderEnterVRLocalized = new()
        {
            TableReference = ProjectConstants.UiToolKitLocalizationTableName,
            TableEntryReference = "MainMenuScreenManagerEnterVRPopupHeader"
        };

        private readonly LocalizedString popupDescriptionEnterVRLocalized = new()
        {
            TableReference = ProjectConstants.UiToolKitLocalizationTableName,
            TableEntryReference = "MainMenuScreenManagerEnterVRPopupDescription"
        };

        private readonly LocalizedString popupLeftButtonTextEnterVRLocalized = new()
        {
            TableReference = ProjectConstants.UiToolKitLocalizationTableName,
            TableEntryReference = "MainMenuScreenManagerEnterVRPopUpLeftButtonText"
        };

        private readonly LocalizedString popupRightButtonTextEnterVRLocalized = new()
        {
            TableReference = ProjectConstants.UiToolKitLocalizationTableName,
            TableEntryReference = "MainMenuScreenManagerEnterVRPopUpRightButtonText"
        };

#endregion

#region LocalizedStringsLogOut

        private readonly LocalizedString popupHeaderLogOutLocalized = new()
        {
            TableReference = ProjectConstants.UiToolKitLocalizationTableName,
            TableEntryReference = "MainMenuScreenManagerLogOutPopupHeader"
        };

        private readonly LocalizedString popupDescriptionLogOutLocalized = new()
        {
            TableReference = ProjectConstants.UiToolKitLocalizationTableName,
            TableEntryReference = "MainMenuScreenManagerLogOutPopupDescription"
        };

        private readonly LocalizedString popupLeftButtonTextLogOutLocalized = new()
        {
            TableReference = ProjectConstants.UiToolKitLocalizationTableName,
            TableEntryReference = "MainMenuScreenManagerLogOutPopUpLeftButtonText"
        };

        private readonly LocalizedString popupRightButtonTextLogOutLocalized = new()
        {
            TableReference = ProjectConstants.UiToolKitLocalizationTableName,
            TableEntryReference = "MainMenuScreenManagerLogOutPopUpRightButtonText"
        };

#endregion

        public override void SetupVisualElements(VisualElement rootElement)
        {
            allViews = new IMainMenuView[]
            {
                homeView, statisticsView, guideView, settingsView, tutorialsView, adminView
            };

            homeView.SetupVisualElements(rootElement.Q("MainMenuHomeView"));
            statisticsView.SetupVisualElements(rootElement.Q("MainMenuStatisticsView"));
            guideView.SetupVisualElements(rootElement.Q("MainMenuGuideView"));
            settingsView.SetupVisualElements(rootElement.Q("MainMenuSettingsView"));
            tutorialsView.SetupVisualElements(rootElement.Q("MainMenuTutorialsView"));
            adminView.SetupVisualElements(rootElement.Q("MainMenuAdminView"));

            switchHomeViewButton = rootElement.Q<Button>("HomeButton");
            switchStatisticsViewButton = rootElement.Q<Button>("StatisticsButton");
            switchGuideViewButton = rootElement.Q<Button>("GuideButton");
            switchSettingsViewButton = rootElement.Q<Button>("SettingsButton");
            switchTutorialsViewButton = rootElement.Q<Button>("TutorialsButton");
            switchAdminViewButton = rootElement.Q<Button>("AdminButton");

            userNameButton = rootElement.Q<Button>("UserButton");
            userNameButton.text = DataManager.Instance?.CurrentUser?.Clearname ?? "<ERROR>";

            startVRButton = rootElement.Q<Button>("StartVRButton");
            logoutButton = rootElement.Q<Button>("LogoutButton");
            quitButton = rootElement.Q<Button>("QuitButton");

            viewHeader = rootElement.Q<TextElement>("ContentName");

            SetAdminButtonVisibility();
            SwitchToView(null, homeView);
        }

        public override void SubscribeToEvents()
        {
            switchHomeViewButton.RegisterCallback<ClickEvent, IMainMenuView>(SwitchToView, homeView);
            switchStatisticsViewButton.RegisterCallback<ClickEvent, IMainMenuView>(SwitchToView, statisticsView);
            switchGuideViewButton.RegisterCallback<ClickEvent, IMainMenuView>(SwitchToView, guideView);
            switchSettingsViewButton.RegisterCallback<ClickEvent, IMainMenuView>(SwitchToView, settingsView);
            switchTutorialsViewButton.RegisterCallback<ClickEvent, IMainMenuView>(SwitchToView, tutorialsView);
            switchAdminViewButton.RegisterCallback<ClickEvent, IMainMenuView>(SwitchToView, adminView);

            startVRButton.clicked += EnterVR;
            logoutButton.clicked += Logout;
            quitButton.clicked += ApplicationManager.AskToQuit;

            foreach (IMainMenuView view in allViews)
            {
                view.SubscribeToEvents();
            }
        }

        public override void UnsubscribeFromEvents()
        {
            switchHomeViewButton.UnregisterCallback<ClickEvent, IMainMenuView>(SwitchToView);
            switchStatisticsViewButton.UnregisterCallback<ClickEvent, IMainMenuView>(SwitchToView);
            switchGuideViewButton.UnregisterCallback<ClickEvent, IMainMenuView>(SwitchToView);
            switchSettingsViewButton.UnregisterCallback<ClickEvent, IMainMenuView>(SwitchToView);
            switchTutorialsViewButton.UnregisterCallback<ClickEvent, IMainMenuView>(SwitchToView);
            switchAdminViewButton.UnregisterCallback<ClickEvent, IMainMenuView>(SwitchToView);

            startVRButton.clicked -= EnterVR;
            logoutButton.clicked -= Logout;
            quitButton.clicked -= ApplicationManager.AskToQuit;

            foreach (IMainMenuView view in allViews)
            {
                view.UnsubscribeFromEvents();
            }
        }

        public override void ResetViewSelections()
        {
            foreach (IMainMenuView view in allViews)
            {
                view.ResetViewSelections();
            }
        }

        private void SwitchToView(ClickEvent _, IMainMenuView selectedView)
        {
            foreach (IMainMenuView view in allViews)
            {
                view.RootElement.SetIsVisible(false);
            }

            viewHeader.text = selectedView.ViewName.GetLocalizedString();
            selectedView.ResetViewSelections();
            selectedView.RootElement.SetIsVisible(true);
        }

        private void EnterVR()
        {
            //TODO: Rework to get translation per key value pair

            PopupScreenManager.Show(headerLocalized: popupHeaderEnterVRLocalized,
                                    descriptionLocalized: popupDescriptionEnterVRLocalized,
                                    leftButtonTextLocalized: popupLeftButtonTextEnterVRLocalized,
                                    actionLeft: null,
                                    rightButtonTextLocalized: popupRightButtonTextEnterVRLocalized,
                                    actionRight: () =>
                                    {
                                        UnsubscribeFromEvents(); // So no button can be pressed while loading
                                        ApplicationManager.SwitchToVROperation();
                                    });
        }

        private void Logout()
        {
            PopupScreenManager.Show(headerLocalized: popupHeaderLogOutLocalized,
                                    descriptionLocalized: popupDescriptionLogOutLocalized,
                                    leftButtonTextLocalized: popupLeftButtonTextLogOutLocalized,
                                    actionLeft: null,
                                    rightButtonTextLocalized: popupRightButtonTextLogOutLocalized,
                                    actionRight: ApplicationManager.SwitchToLoginRegister,
                                    //invert the colors because in this case, logging out is the "red" action
                                    leftButtonColor:PopupScreenManager.defaultRightButtonColor,
                                    rightButtonColor:PopupScreenManager.defaultLeftButtonColor);
        }

        /// <summary>
        /// Sets the visibility of the admin view button based on the current user's privilege.
        /// </summary>
        /// <exception cref="ArgumentNullException">
        /// Thrown when the switchAdminViewButton is null.
        /// </exception>
        private void SetAdminButtonVisibility()
        {
            if (switchAdminViewButton == null)
            {
                throw new ArgumentNullException(nameof(switchAdminViewButton),
                                                $"{nameof(switchAdminViewButton)} cannot be null.");
            }

            // Checks if User is Admin.
            // If user is admin the switchAdminViewButton is displayed otherwise hidden.
            switchAdminViewButton.SetIsVisible(DataManager.Instance?.CurrentUser?.IsAdmin() ?? false);
        }
    }
}
