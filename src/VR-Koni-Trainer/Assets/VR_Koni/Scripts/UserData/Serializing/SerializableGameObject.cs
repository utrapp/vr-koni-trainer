using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using B83.MeshTools;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;


namespace VR_Koni.UserData
{
    /// <summary>
    /// This class represents a gameobject but in a way that it can be serialized. Parameterless constructors are needed for all the classes in this file.
    /// </summary>
    [Serializable]
    public class SerializableGameObject
    {
        [FormerlySerializedAs("name")] 
        public string Name;
        
        [FormerlySerializedAs("transform")] 
        public SerializableTransform Transform;
        
        public List<ISerializableComponent> Components = new List<ISerializableComponent>();
        
        [FormerlySerializedAs("children")]
        public List<SerializableGameObject> Children = new List<SerializableGameObject>();
        
        public SerializableGameObject(GameObject gameObject)
        {
            Name = gameObject.name;
            foreach (Component component in gameObject.GetComponents<Component>())
            {
                if(component is Transform)
                    Transform=new SerializableTransform((Transform)component);
                
                else if(component is MeshFilter)
                    Components.Add(new SerializableMeshFilter((MeshFilter)component));
                
                else if (component is MeshRenderer)
                    Components.Add(new SerializableMeshRenderer((MeshRenderer)component));
            }
            
            foreach (Transform child in gameObject.transform)
            {
                Children.Add(new SerializableGameObject(child.gameObject));
            }
        }

        public SerializableGameObject()
        {
            
        }
        
        [JsonIgnore]
        public GameObject ToUnityGameObject{
            get
            {
                GameObject go = new GameObject();
                go.name = Name;
                go.transform.position = Transform.position.UnityVector;
                go.transform.rotation = Transform.rotation.UnityRotation;
                go.transform.localScale = Transform.scale.UnityVector;
                
                
                foreach (ISerializableComponent component in Components)
                {
                    if (component is SerializableMeshFilter)
                    {
                        MeshFilter meshFilter = (MeshFilter) component.GetUnityComponent();
                        MeshFilter objectMeshFilter = go.AddComponent<MeshFilter>();
                        objectMeshFilter.mesh = meshFilter.mesh;
                    }

                    if (component is SerializableMeshRenderer)
                    {
                        MeshRenderer meshRenderer = (MeshRenderer) component.GetUnityComponent();
                        MeshRenderer objectMeshRenderer = go.AddComponent<MeshRenderer>();
                        objectMeshRenderer.materials = meshRenderer.materials;

                    }
                }

                foreach (SerializableGameObject child in Children)
                {
                    GameObject childObject = child.ToUnityGameObject;
                    childObject.transform.SetParent(go.transform);
                    childObject.transform.position = child.Transform.position.UnityVector;
                    childObject.transform.rotation = child.Transform.rotation.UnityRotation;
                    childObject.transform.localScale = child.Transform.scale.UnityVector;
                }
                
                return go;
            }
        }
    }

    [Serializable]
    public class SerializableRotation
    {
        public float x;
        public float y;
        public float z;
        public float w;

        public SerializableRotation (Quaternion rotation)
        {
            x = rotation.x;
            y = rotation.y;
            z = rotation.z;
            w = rotation.w;
        }

        public SerializableRotation() {}

        [JsonIgnore]
        public Quaternion UnityRotation => new Quaternion(x,y,z,w);
    }
    
    [Serializable]
    public class SerializableVector3{
        public float x;
        public float y;
        public float z;
 
        [JsonIgnore]
        public Vector3 UnityVector => new Vector3(x, y, z);

        public SerializableVector3(Vector3 v){
            x = v.x;
            y = v.y;
            z = v.z;
        }

        public SerializableVector3()
        {
            
        }
    }

    [Serializable]
    public class SerializableTransform
    {
        public SerializableVector3 position;
        public SerializableRotation rotation;
        public SerializableVector3 scale;

        public SerializableTransform(Transform transform)
        {
            position = new SerializableVector3(transform.position);
            rotation = new SerializableRotation(transform.rotation);
            scale = new SerializableVector3(transform.localScale);
        }
        
        public SerializableTransform()
        {
            
        }
    }
    
    [Serializable]
    public class SerializableMeshRenderer : ISerializableComponent
    {
        public List<SerializableMaterial> materials = new List<SerializableMaterial>();

        public SerializableMeshRenderer(MeshRenderer meshRenderer)
        {
            foreach (Material m in meshRenderer.materials)
            {
                materials.Add(new SerializableMaterial(m));
            }
        }

        public SerializableMeshRenderer()
        {
            
        }

        public Component GetUnityComponent()
        {
            
            GameObject gameObjectTemp = new GameObject("TemporaryMeshRendererObject");
            MeshRenderer renderer = gameObjectTemp.AddComponent<MeshRenderer>();
            
            Material [] materialsArray = new Material[materials.Count];
            for (int i = 0; i < materials.Count; i++)
            {
                materialsArray [i] = materials[i].UnityMaterial;
            }

            renderer.materials = materialsArray;
            GameObject.Destroy(gameObjectTemp);
            return renderer;
        }
    }

    
        
    [Serializable]
    public class SerializableMaterial
    {
        public string materialName;
        public SerializableMaterial(Material m)
        {
            materialName = m.name.Replace(" (Instance)", "");
        }

        public SerializableMaterial()
        {
            
        }

        [JsonIgnore]
        public Material UnityMaterial => Resources.Load<Material>(materialName);
    }
    
    
    [Serializable]
    public class SerializableMeshFilter : ISerializableComponent
    {
        public SerializableMesh mesh;

        public SerializableMeshFilter(MeshFilter meshFilter)
        {
            mesh = new SerializableMesh(meshFilter.mesh);
        }

        public SerializableMeshFilter()
        {
            
        }
        
        public Component GetUnityComponent()
        {
            GameObject gameObjectTemp = new GameObject("TemporaryMeshFilterObject");
            
            MeshFilter filter = gameObjectTemp.AddComponent<MeshFilter>();
            filter.mesh = mesh.UnityMesh;
            GameObject.Destroy(gameObjectTemp);
            return filter;
        }
    }

    [Serializable]
    public class SerializableMesh
    {
        public string serializedMeshData;
        public SerializableMesh(Mesh mesh)
        {
            serializedMeshData = Convert.ToBase64String(MeshSerializer.SerializeMesh(mesh));
        }

        public SerializableMesh()
        {
            
        }

        [JsonIgnore]
        public Mesh UnityMesh
        {
            get
            {
                Mesh mesh = new Mesh();
                MeshSerializer.DeserializeMesh(Convert.FromBase64String(serializedMeshData), mesh);
                return mesh;
            }
        }
    }
    

    

    /// <summary>
    /// This Interface ensures that a class that Implements it can be converted to a Component for Unity.
    /// </summary>
    public interface ISerializableComponent
    {
        Component GetUnityComponent();
    }
}
