#nullable enable //The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.

using Mono.Data.Sqlite;
using System;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace VR_Koni.Graph
{
    /// <summary>
    /// <see cref="DatabaseExtractor{T}"/> is responsible for handling the extraction and conversion of 
    /// the compressed data stored in the SQLite file into a structured dataset like a list of tuples.
    /// Ensure that <EnableUnsafeBinaryFormatterSerialization>true</EnableUnsafeBinaryFormatterSerialization>
    /// is set in .csproj to use BinaryFormatter.
    /// </summary>
    public sealed class DatabaseExtractor<T>
    {
        /// <summary>
        /// The default query to execute for the sqlite file.
        /// </summary>
        private readonly string defaultQuery;

        /// <summary>
        /// specifies the count of tolerance till which consecutive duplicates wont be removed.
        /// When set to X for instance, it will ignore up to X consecutive duplicates but then not add
        /// the duplicates from the (X+1)th instance if it is consecutive.
        /// </summary>
        private readonly int duplicateCountTolerance;

        /// <summary>
        /// instance of <see cref="FileHandler{T}"/> to handle export and imports to and from files on the disk.
        /// </summary>
        internal readonly FileHandler<T> fileHandler;

        /// <summary>
        /// Parameterized constructor for <see cref="DatabaseExtractor{T}"/>
        /// </summary>
        /// <param name="duplicateCountTolerance"></param>
        /// <param name="query"></param>
        public DatabaseExtractor(int duplicateCountTolerance = 3, string query = @"SELECT depthData FROM surgery")
        {
            defaultQuery = query;
            this.duplicateCountTolerance = duplicateCountTolerance;
            fileHandler = new FileHandler<T>();
        }

        /// <summary>
        /// Decompresses the x64 string from the specified SQLite file and returns it as 
        /// the specified type <typeparamref name="T"/>. This method also provides an option to sanitize 
        /// the data by limiting duplicate consecutive values for which the threshold is 
        /// defined in <see cref="duplicateCountTolerance"/>
        /// </summary>
        /// <param name="sqliteFileName">The path to the SQLite file.</param>
        /// <param name="sanitizeData">A flag indicating whether to sanitize the data, taking tolerance
        /// as <see cref="duplicateCountTolerance"/>.</param>
        /// <returns>A Collection{T} representing the decompressed data.</returns>
        /// <exception cref="SqliteException"></exception>
        /// <exception cref="FileNotFoundException"></exception>
        /// <exception cref="Exception"></exception>
        [Obsolete("The method is deprecated since the recent changes(#412) of storing the values without compressing" +
                  "them into the database. Please use the method ExtractSurgeryFromDatabase instead.")]
        public ICollection<T>? DecompressDataFromSqLite(string sqliteFileName, bool sanitizeData = true)
       {
           List<T>? cuttingData = new();
           try
           {
               if (!File.Exists(sqliteFileName))
               {
                   throw new FileNotFoundException($"{nameof(DecompressDataFromSqLite)}: " +
                                                   $"File {sqliteFileName} does not exist!");
               }

               // Mono.Data.Sqlite connection string format
               string connectionString = $"URI=file:{sqliteFileName}";
               using SqliteConnection connection = new(connectionString);
               connection.Open();

               using SqliteCommand command = new(defaultQuery, connection);
               using SqliteDataReader reader = command.ExecuteReader();

               Sanitizer<T>? sanitizer = sanitizeData ? new(duplicateCountTolerance) : null;
               if (!sanitizeData && (duplicateCountTolerance > 0 && sanitizer != null))
               {
                   Debug.LogWarning($"{nameof(DecompressDataFromSqLite)}: Skipping sanitation because " +
                                    $"param {nameof(sanitizeData)} was set to {sanitizeData}.");
               }

               while (reader.Read())
               {
                   string compressedBase64 = reader.GetString(0);
                   byte[] compressedBytes = Convert.FromBase64String(compressedBase64);

                   object decompressedData = DecompressData(compressedBytes);
                   List<T> processedData = CastDataIntoType(decompressedData);

                   cuttingData.AddRange(processedData);
                   sanitizer?.SanitizeData(ref cuttingData);
               }
           }
           catch (SqliteException ex)
           {
               Debug.LogError($"SQLite Error: {ex.Message}");
               Debug.LogError(ex.StackTrace);
           }
           catch (Exception ex)
           {
               Debug.LogError($"Error: {ex.Message}");
               Debug.LogError(ex.StackTrace);
           }

           return cuttingData;
       }


        /// <summary>
        /// Decompresses the provided byte array using GZip and deserializes it 
        /// into its original object form using BinaryFormatter. This method is 
        /// central to transforming the compressed data into a usable format.
        /// </summary>
        /// <param name="compressedBytes">The byte array containing compressed data.</param>
        /// <returns>The deserialized object from the compressed data.</returns>
        [Obsolete("The method is deprecated since the recent changes(#412) of storing the values without compressing" +
        "them into the database. Please use the method ExtractSurgeryFromDatabase instead.")]
        private static object DecompressData(byte[] compressedBytes)
        {
            using MemoryStream compressedStream = new(compressedBytes);
            using MemoryStream decompressedStream = new();
            using GZipStream gzipStream = new(compressedStream, CompressionMode.Decompress);

            // Copy the decompressed data into a new stream.
            gzipStream.CopyTo(decompressedStream);
            decompressedStream.Position = 0;

#pragma warning disable SYSLIB0011
            BinaryFormatter formatter = new();
            object data = formatter.Deserialize(decompressedStream);
#pragma warning restore SYSLIB0011
            return data;
        }

        /// <summary>
        /// Processes the decompressed data and converts it to the target type if necessary.
        /// </summary>
        /// <exception cref="InvalidCastException"></exception>
        private static List<T> CastDataIntoType(object data)
        {
            //convert meter to mm
            const int DEGREE_OF_MULTIPLICATION = 1000;
            return data switch
            {
                List<Tuple<float, float>> tupleList when typeof(T) == typeof(System.Numerics.Vector2) =>
                    tupleList.Select(t => (T)(object)new System.Numerics.Vector2(t.Item1, t.Item2 * DEGREE_OF_MULTIPLICATION)).ToList(),

                List<Tuple<float, float>> tupleList when typeof(T) == typeof(Tuple<float, float>) =>
                    tupleList.Select(t => (T)(object)Tuple.Create(t.Item1, t.Item2 * DEGREE_OF_MULTIPLICATION)).ToList(),

                List<System.Numerics.Vector2> vectorList when typeof(T) == typeof(Tuple<float, float>) =>
                    vectorList.Select(v => (T)(object)Tuple.Create(v.X, v.Y * DEGREE_OF_MULTIPLICATION)).ToList(),

                List<System.Numerics.Vector2> vectorList when typeof(T) == typeof(System.Numerics.Vector2) =>
                    vectorList.Select(v => (T)(object)new System.Numerics.Vector2(v.X, v.Y * DEGREE_OF_MULTIPLICATION)).ToList(),

                _ => throw new InvalidCastException($"Cannot convert from {data.GetType()} to List<{typeof(T)}>. " +
                        "Supported types as of now are System.Numerics.Vector2 and Tuple<float, float>.")
            };
        }
        
        [Obsolete("The method is deprecated since the recent changes(#412) of storing the values without compressing" +
                  "them into the database. Please use the method ExtractSurgeryFromDatabase instead.")]
        private static List<Tuple<float, float>> DecompressDepthData(string base64Data)
        {
            try
            {
                byte[] compressedBytes = Convert.FromBase64String(base64Data);

                using MemoryStream compressedStream = new(compressedBytes);
                using MemoryStream decompressedStream = new();
                using GZipStream gzipStream = new(compressedStream, CompressionMode.Decompress);

                gzipStream.CopyTo(decompressedStream);
                decompressedStream.Position = 0;

#pragma warning disable SYSLIB0011 // BinaryFormatter is deprecated but still usable
                BinaryFormatter formatter = new();
                ICollection<Tuple<float,float>> data = (List<Tuple<float, float>>)formatter.Deserialize(decompressedStream);
#pragma warning restore SYSLIB0011
                
                //convert from mm to m
                data = data
                       .Skip(1)
                       .Select(t => Tuple.Create(t.Item1, t.Item2 * 1000))
                       .ToList();
                DataPoints<Tuple<float,float>>.StartXFromZero(ref data);
                return data.ToList();
            }
            catch (Exception ex)
            {
                Debug.LogError($"Decompression failed: {ex.Message}\n{ex.StackTrace}");
                throw;
            }
        }
    }

    /// <summary>
    /// Extension methods for <see cref="DatabaseExtractor{T}"/> reside here.
    /// </summary>
    public static class DataBaseExtractorExtensions
    {
        /// <summary>
        /// writes the JSON content to the specified path <paramref name="outputFilePath"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="databaseExtractor"></param>
        /// <param name="data"></param>
        /// <param name="outputFilePath"></param>
        public static void WriteDataToJsonFile<T>(this DatabaseExtractor<T> databaseExtractor, ICollection<T>? data, string outputFilePath = "output.json")
        {
            if (data == null)
            {
                return;
            }
            databaseExtractor.fileHandler.WriteDataToJsonFile(data, outputFilePath);
        }

        /// <summary>
        /// writes the CSV content to the specified path <paramref name="outputFilePath"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="databaseExtractor"></param>
        /// <param name="data"></param>
        /// <param name="outputFilePath"></param>
        public static void WriteDataToCsvFile<T>(this DatabaseExtractor<T> databaseExtractor, ICollection<T>? data, string outputFilePath = "output.csv")
        {
            if (data == null)
            {
                return;
            }
            databaseExtractor.fileHandler.WriteDataToCsvFile(data, outputFilePath);

        }

        /// <summary>
        /// reads in the CSV contents from the specified path <paramref name="inputFilePath"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="databaseExtractor"></param>
        /// <param name="inputFilePath"></param>
        /// <param name="delimiter"></param>
        /// <returns></returns>
        /// <exception cref="FileNotFoundException"></exception>
        public static List<T> ReadFromCsvFile<T>(this DatabaseExtractor<T> databaseExtractor, string inputFilePath, char delimiter = ',')
        {
            if (!File.Exists(inputFilePath))
            {
                throw new FileNotFoundException($"{nameof(ReadFromCsvFile)}: File {inputFilePath} does not exist!");
            }

            return databaseExtractor.fileHandler.ReadDataFromCsvFile(inputFilePath, delimiter);
        }
    }
}
