using System;
using UnityEngine;
using UnityEngine.UI;
using VR_Koni.GUI.HeightAdjustment;

namespace VR_Koni.GUI.Results
{
    public class ResultMenu : FadingMenu
    {
        [SerializeField] private ResultVictoryMenu resultsVictory;
        [SerializeField] private ResultLooseMenu resultsLoose;

        [SerializeField] private Button buttonRepeatOperation;
        [SerializeField] private Button buttonBackToMenu;

        private void Start()
        {
            HideMenu(true);
            buttonRepeatOperation.onClick.AddListener(GameManager.RestartOperation);
            buttonBackToMenu.onClick.AddListener(ApplicationManager.SwitchToMainMenu);
        }

        private void OnEnable() => GameManager.OnGameStateChanged += OnGameStateChanged;
        private void OnDisable() => GameManager.OnGameStateChanged -= OnGameStateChanged;

        private void OnGameStateChanged(GameManager.GameState newState)
        {
            if (newState is GameManager.GameState.Victory or GameManager.GameState.Loose)
            {
                ShowMenu();
                ShowSubMenu(newState);
                HeightAdjustmentMenu.Hide();
            }
            else if (newState is GameManager.GameState.Operation)
                HideMenu();
        }

        private void ShowSubMenu(GameManager.GameState newState)
        {
            switch (newState)
            {
                case GameManager.GameState.Victory:
                    resultsLoose.HideMenu(true);
                    resultsVictory.ShowMenu();
                    return;
                case GameManager.GameState.Loose:
                    resultsVictory.HideMenu(true);
                    resultsLoose.ShowMenu();
                    return;
                default:
                    throw new ArgumentException($"{nameof(newState)} was not of type Victory or Loose");
            }
        }
    }
}
