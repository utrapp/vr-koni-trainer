# VR-Coni-Trainer

![Build Status](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/badges/main/pipeline.svg)

Our vision is to develop a high-quality, low-cost, resource-saving and easy-to-use simulator for training the basic surgical skills of a [dysplasia](https://en.wikipedia.org/wiki/Dysplasia) consultation. The project was initiated by Anne Cathrine Quenzer and Matthias Kiesel of the [University Women's Hospital](https://www.ukw.de/startseite/) in Würzburg , Germany.

**For prospective gynecologists who want to practice [conization](https://www.konisation.at/), VR-Coni-Trainer is a virtual training opportunity with gamification elements.**

Currently, trainee medical assistants watch the surgery several times and then perform it on the patient under supervision. However, both watching and supervision are limited because the observation window is very narrow. Conization is a simple operation in itself. However, possible risks are considerable (infertility, injuries to the intestine, ...).

Example-Video: 
https://www.medical-videos.com/de/leep-conization-with-monopolar-electrodes/

Don't hesitate to visit the [Wiki](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/wikis/home) of this project to gain more information.

## Requirements

A good overview can be found in the initial [concept](ux/Konzept_KoniTrainerVR.pdf) (in German).
In the simulation we aim for a depth of the cone (i.e. the tissue removed by electric wire loop) of 8-10mm. This is a scientifically confirmed depth, even for women who still wish to have children.
The wire loop in the [video of the operation](https://www.youtube-nocookie.com/embed/8xSp0QuC7XU?start=128) is passed through the tissue of the cervix too slowly overall. The electric current changes the tissue if it is in contact with it for too long, making it more difficult to examine the tissue in the pathology department. It is important to detect mistakes like this and bring them to the trainees' attention.

In any case, the depth of the cone should be displayed after the surgery has been performed (see below for more details).

Training should be able to be done multiple times and gamification elements like scores should be used to increase the UX.

### Game-over criteria

- Touching the speculum or vagina with the activated (i.e., energized) wire loop.
- Activated wire loop remains in the tissue of the cervix for longer than 8 seconds (this way we want to avoid that participants guide the wire loop too slowly and thus cause the beforementioned problem).
- Activated wire loop is deactivated while it is still in the tissue of the cervix.
  (The wire loop is activated by pressing and holding a button on the loop's holding handle; if the button is released, the current flow is interrupted. This could be mimicked with one of the buttons on the joystick/controller of the VR goggles).
- Opening of the cervical canal is not part of the removed cone (the opening of the cervical canal always has to be removed as well).

It would be desirable to display the reason for a game over directly in the field of vision so that the participant knows immediately why game over has occurred.

In addition, the results should be displayed after the operation and also be saved and analyzed later. This way, one could visualize one's own learning success.
The following should be displayed:

- Maximum depth of the removed cone, relative to the entrance to the cervical canal.
- Volume of the cone, e.g. cubic centimeters or similar.
- If this can be recorded or displayed, a 3D representation, i.e., a 3D image of the removed cone.
- An indicator whether the white area was completely removed or no (the cervix should have a white "blob" around the opening of the cervical canal entrance. This white area corresponds to precancerous lesions, which should be removed completely).

## Getting started

Carefully read the prepared documents in the projectManagement folder. For familiarization with the essential tools: [Training.md](projectManagement/training.md).

Instructions for setting-up the recommended **toolchain** and some other necessary **setup-steps** can be found in the corresponding [Wiki-Entries](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/wikis/home/Setup).

## Folder structure

- projectManagement: Everything relevant for the self-organization of the team, esp. rules for using gitlab etc.
- learning: Simplified projects for learning/testing.
- src: Code of the application.
- ux: Persona, moodboard, blender models.

## Discord

Discord Server to organize yourselves: https://discord.gg/HtaXhrRf
