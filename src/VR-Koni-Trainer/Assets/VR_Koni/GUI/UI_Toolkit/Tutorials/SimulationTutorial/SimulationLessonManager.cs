using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using VR_Koni.Tutorials;


namespace VR_Koni.GUI.UI_Toolkit.Tutorials.SimulationTutorial
{
    public class SimulationLessonManager: MonoBehaviour
    {
        public Tutorial3StepManager stepManager;
        [SerializeField] List<GameObject> fruits = new List<GameObject>();
        public GameObject meshElectroSnare;
        public GameObject popup;

        void Start()
        {
            Debug.Log("Start");
            SetPlayerVariantInMainSceneInactive(true);
            SetCurrentLessonIndexFromPrefs();
        }

        private void OnEnable()
        {
            Debug.Log("OnEnable");
            SetCurrentLessonIndexFromPrefs();
        }

        //TODO: This should be done on scene change in ApplicationsManager
        private void SetPlayerVariantInMainSceneInactive(bool isInactive)
        {
            if (SceneManager.GetSceneByName("VR_MainScene").isLoaded)
            {
                foreach (GameObject rootGameObject in SceneManager.GetSceneByName("VR_MainScene").GetRootGameObjects())
                {
                    if (rootGameObject.name == "PlayerVariant")
                    {
                        rootGameObject.SetActive(!isInactive);
                        break;
                    }
                }
            }
        }

        public void OnEndTutorialButtonClick()
        {
            popup.SetActive(true);
        }

        public void EndSimulationTutorial()
        {
            SetPlayerVariantInMainSceneInactive(false);
            string currentSceneName = SceneManager.GetActiveScene().name;
            PlayerPrefs.SetString("PreviousScene", currentSceneName);
            ApplicationManager.SwitchToTutorials();
        }

        private void SetCurrentLessonIndexFromPrefs()
        {
            if (PlayerPrefs.HasKey("lesson"))
            {
                int lessonNumber = PlayerPrefs.GetInt("lesson") - 1; // Convert to 0-based index;
                Debug.Log("Lesson number: " + lessonNumber);
                stepManager.setM_CurrentLessonIndex(lessonNumber);
                if (lessonNumber == 3)
                {
                    foreach (GameObject fruit in fruits)
                    {
                        fruit.SetActive(true);
                    }
                }
                else if (lessonNumber == 1)
                {
                    if (meshElectroSnare)
                    {
                        MaterialChanger materialChanger = meshElectroSnare.GetComponent<MaterialChanger>();
                        materialChanger.ChangeMaterial();
                    }
                    else
                    {
                        Debug.LogError("meshElectroSnare is null");

                    }
                }
            }
            else
            {
                stepManager.setM_CurrentLessonIndex(0);
            }
        }
    }


}
