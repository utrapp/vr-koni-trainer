using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Tutorial3StepManager : MonoBehaviour
{

    [SerializeField]
    private List<List<GameObject>> m_StepList = new List<List<GameObject>>
    {
        new List<GameObject>(),
        new List<GameObject>(),
        new List<GameObject>(),
        new List<GameObject>(),
        new List<GameObject>(),
    };

    private int currentLessonIndex;
    private int m_CurrentTaskIndex = 0;

    public int getM_CurrentStepIndex()
    {
        return currentLessonIndex;
    }
    
    public void setM_CurrentLessonIndex(int newValue)
    { 
        Debug.Log($"Setting m_CurrentLessonIndex from {currentLessonIndex} to {newValue}"); 
        currentLessonIndex = newValue;
    }

    void Start()
    {
        m_StepList[0].Add(GameObject.Find("UI/PositionUI/Lesson1/Task1.1")); 
        m_StepList[1].Add(GameObject.Find("UI/PositionUI/Lesson2/Task2.1")); 
        m_StepList[1].Add(GameObject.Find("UI/PositionUI/Lesson2/Task2.2")); 
        m_StepList[1].Add(GameObject.Find("UI/PositionUI/Lesson2/Task2.3")); 
        m_StepList[1].Add(GameObject.Find("UI/PositionUI/Lesson2/Task2.4")); 
        m_StepList[2].Add(GameObject.Find("UI/PositionUI/Lesson3/Task3.1")); 
        //m_StepList.Add(GameObject.Find("UI/PositionUI/Lesson3/Task3.2")); 
        m_StepList[3].Add(GameObject.Find("UI/PositionUI/Lesson4/Task4.1")); 
        m_StepList[3].Add(GameObject.Find("UI/PositionUI/Lesson4/Task4.2")); 
        m_StepList[4].Add(GameObject.Find("UI/PositionUI/Lesson5/Task5.1")); 
        m_StepList[currentLessonIndex][0].SetActive(true);
    }
    
    void InitializeTutorialObjects(int test)
    {
        switch (test)
        {
            case 0: m_StepList[0].Add(GameObject.Find("UI/PositionUI/Lesson1/Task1.1")); break;
            case 2: m_StepList[1].Add(GameObject.Find("UI/PositionUI/Lesson2/Task2.1")); break;
            case 3: m_StepList[1].Add(GameObject.Find("UI/PositionUI/Lesson2/Task2.2")); break;
            case 4: m_StepList[1].Add(GameObject.Find("UI/PositionUI/Lesson2/Task2.3")); break;
            case 5: m_StepList[1].Add(GameObject.Find("UI/PositionUI/Lesson2/Task2.4")); break;
            case 6: m_StepList[2].Add(GameObject.Find("UI/PositionUI/Lesson3/Task3.1")); break;
            case 7: m_StepList[3].Add(GameObject.Find("UI/PositionUI/Lesson4/Task4.1")); break;
            case 8: m_StepList[3].Add(GameObject.Find("UI/PositionUI/Lesson4/Task4.2")); break;
            case 9: m_StepList[4].Add(GameObject.Find("UI/PositionUI/Lesson5/Task5.1")); break;
        }
    }

    public void Next()
    {
        /* Wieder aktivieren, falls es zu uninitialized errors kommt
        if (m_StepList[currentLessonIndex] == null || m_StepList[currentLessonIndex + 1] == null)
        {
            Debug.Log("Reinitializing in next");
            //InitializeTutorialObjects(m_CurrentLessonIndex);
        }
        */
        m_StepList[currentLessonIndex][m_CurrentTaskIndex].SetActive(false);
        m_CurrentTaskIndex++;
        if(m_CurrentTaskIndex > m_StepList[currentLessonIndex].Count -1)
        {
            currentLessonIndex++;
            m_CurrentTaskIndex = 0;
        }
        m_StepList[currentLessonIndex][m_CurrentTaskIndex].SetActive(true);
    }

    public void Previous()
    {
        /* /* Wieder aktivieren, falls es zu uninitialized errors kommt
        if (m_StepList[currentLessonIndex] == null)
        {
            Debug.Log("Reinitializing in previous");
            //InitializeTutorialObjects(m_CurrentLessonIndex);
        }
        */
        m_StepList[currentLessonIndex][m_CurrentTaskIndex].SetActive(false);
        m_CurrentTaskIndex--;
        m_StepList[currentLessonIndex][m_CurrentTaskIndex].SetActive(true);
    }
}
