using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace VR_Koni.GUI.UI_Toolkit_Components.PopupWindow
{
    public class BasePopupWindow : VisualElement
    {
        [UnityEngine.Scripting.Preserve]
        public new class UxmlFactory : UxmlFactory<BasePopupWindow> { }

        private const string styleResource = "PopupWindowStyleSheet";
        private const string ussPopup = "popup_window";
        private const string ussPopupContainer = "popup_container";
        private const string ussHorizontalContainer = "horizontal_container";
        private const string ussVerticalContainer = "vertical_container";
        private const string ussPopupTextContainer = "popup_text_container";
        private const string ussPopupHeader = "popup_header";
        private const string ussPopupDescription = "popup_description";
        private const string ussPopupButtonLeft = "button-left";
        private const string ussPopupButtonRight = "button-right";

        private Label headerText;
        private Label descriptionText;
        private Button buttonLeft;
        private Button buttonRight;

        public event Action OnButtonLeftClicked;
        public event Action OnButtonRightClicked;
        public BasePopupWindow()
        {
            styleSheets.Add(Resources.Load<StyleSheet>(styleResource));
            AddToClassList(ussPopupContainer);

            VisualElement window = new VisualElement();
            window.AddToClassList(ussPopup);
            hierarchy.Add(window);

            // Text Section
            VisualElement verticalContainer = new VisualElement();
            verticalContainer.AddToClassList(ussVerticalContainer);
            window.Add(verticalContainer);

            VisualElement textContainer = new VisualElement();
            textContainer.AddToClassList(ussPopupTextContainer);

            headerText = new Label("Header");
            headerText.AddToClassList(ussPopupHeader);
            textContainer.Add(headerText);

            descriptionText = new Label("Description");
            descriptionText.AddToClassList(ussPopupDescription);
            textContainer.Add(descriptionText);

            verticalContainer.Add(textContainer);

            // Button Section
            VisualElement horizontalContainer = new VisualElement();
            horizontalContainer.AddToClassList(ussHorizontalContainer);
            verticalContainer.Add(horizontalContainer);

            buttonLeft = new Button();
            buttonLeft.text = "Left";
            buttonLeft.AddToClassList(ussPopupButtonLeft);
            horizontalContainer.Add(buttonLeft);

            buttonRight = new Button();
            buttonRight.text = "Right";
            buttonRight.AddToClassList(ussPopupButtonRight);
            horizontalContainer.Add(buttonRight);

            buttonLeft.clicked += () => OnButtonLeftClicked?.Invoke();
            buttonRight.clicked += () => OnButtonRightClicked?.Invoke();
        }

        public void SetHeaderText(string text)
        {
            headerText.text = text;
        }

        public void SetDescriptionText(string text)
        {
            descriptionText.text = text;
        }

        public void SetLeftButtonAction(string buttonText, Action action)
        {
            buttonLeft.clicked -= OnButtonLeftClicked;
            OnButtonLeftClicked = action;
            buttonLeft.clicked += OnButtonLeftClicked;
            buttonLeft.text = buttonText;
        }

        public void SetRightButtonAction(string buttonText, Action action)
        {
            buttonRight.clicked -= OnButtonRightClicked;
            OnButtonRightClicked = action;
            buttonRight.clicked += OnButtonRightClicked;
            buttonRight.text = buttonText;
        }

    }
}
