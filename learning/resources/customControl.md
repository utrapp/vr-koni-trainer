# Custom Controls in UI Toolkit

•	https://docs.unity3d.com/Manual/UIE-custom-controls.html
•	https://docs.unity3d.com/Manual/UIE-create-custom-style-custom-control.html 
•	https://github.com/Unity-Technologies/ui-toolkit-manual-code-examples/blob/2023.2/pie-chart/PieChart.cs 
•	https://github.com/Unity-Technologies/ui-toolkit-manual-code-examples/blob/2023.2/radial-progress/RadialProgress.cs
•	Chartlib of Android (Java): https://github.com/PhilJay/MPAndroidChart/blob/master/MPChartLib/src/main/java/com/github/mikephil/charting/renderer/AxisRenderer.java 

## Custom Attributes and Localization
Be careful, if you do not use localization, the attribute casing is different, e.g. 
```xml
<VR_Koni.GUI.UI_Toolkit_Components.Charts.LineChart name="ExampleLineChart" title="Demo" x-axis-label="time"
```
For 
```cs
[UxmlAttrib`te] public string Title { get => titleLabel.text; set => titleLabel.text = value; }
[UxmlAttribute] public string XAxisLabel { get; set; } = "X Axis";
```
And within the bindin`for localization use
```xml
<localize:LocalizedString property="Title" table="UIToolkitStringTableCollection" entry="keyTitle" />
            <localize:LocalizedString property="XAxisLabel" table="UIToolkitStringTableCollection" entry="keyXAxisLabel" />
            <localize:LocalizedString property="YAxisLabel" table="UIToolkitStringTableCollection" entry="keyYAxisLabel" />
```
And each localizable attribute needs the additional annotation *CreateProperty*.

Make sure, you use either localization bindiing XOR the corresponding uxml attribute in your UXML file.


## Drawing and Padding
- contentRect takes margin and padding into account, i.e. returns the actual available rect
- MeshGenerationContext is for drawing of the complete visualelement, i.e. contentRect.Height+padding+margin
- Binding Forces redrawing and somehow moves the chart element 20pixel to the Bottom -- seems to be a bug in Unity


## Examole Code 
Provide examples and e.g. test it in the scene ComponentTest.
