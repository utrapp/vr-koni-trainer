using UnityEngine;

// ReSharper disable Unity.PreferAddressByIdToGraphicsParams

namespace VR_Koni.Kubus
{
    /// <summary>
    /// This script is only used to store the original texture at the beginning of the game and restore it with an
    /// backup at the end of the game
    /// </summary>
    public class KubusRenderer : MonoBehaviour
    {
        [SerializeField]
        private int textureWidth = 2048;
        [SerializeField]
        private int textureHeight = 2048;
        [SerializeField]
        private RenderTextureFormat textureFormat = RenderTextureFormat.ARGB32;

        private RenderTexture renderTexture;

        private void Start()
        {
            if (!TryGetComponent(out Renderer rend) || rend.sharedMaterial == null)
                return;

            renderTexture = new RenderTexture(textureWidth, textureHeight, 0, textureFormat,
                                              RenderTextureReadWrite.Linear)
            {
                enableRandomWrite = true // Enable the UAV flag
            };

            rend.sharedMaterial.SetTexture("_SecondTex", renderTexture);
        }

        private void OnDisable()
        {
            renderTexture.Release();
        }
    }
}
