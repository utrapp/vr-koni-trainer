using System.Collections;
using EditorCools;
using UnityEngine;
using UnityEngine.SceneManagement;
using VR_Koni.ElectroSnare;
using VR_Koni.UserData;

namespace VR_Koni.Debugging
{
    public class DebugSurgery : MonoBehaviour
    {
        public float duration = 6f;
        public bool useDebugUser = true;
        private ElectroSnareVR snare;
        private readonly float timeStep = 60;
        private int steps;
        private int currentStep;
        private Vector3 startPoint;
        private Vector3 middlePoint;
        private Vector3 endPoint;
        private Rigidbody snareRigidbody;

        [Button]
        public void StartOperation()
        {
            StartCoroutine(DebugSurgeryRoutine());
        }

        private void Start()
        {
            LoginUser();
            StartCoroutine(LoadScenes());
            DataManager.OnSurgeryEnded += HandleSurgeryEnded;
            ElectroSnareVR.onCuttingLengthChange += HandleCuttingDepthChange;
        }

        void OnDestroy()
        {
            DataManager.OnSurgeryEnded -= HandleSurgeryEnded;
            ElectroSnareVR.onCuttingLengthChange -= HandleCuttingDepthChange;
        }

        private void LoginUser()
        {
            if (useDebugUser)
            {
                DataManager.Instance.LogInDebugUser();
            }
            else
            {
                DataManager.Instance.LogInGuest();
            }
        }
        private IEnumerator LoadScenes()
        {
            yield return SceneManager.LoadSceneAsync("VR_MainScene", LoadSceneMode.Additive); //loaded to show the resulting cut out
            yield return SceneManager.LoadSceneAsync("VR_OperationRoom", LoadSceneMode.Additive);
        }
        private void HandleCuttingDepthChange(float depth)
        {
            Debug.Log($"{nameof(HandleCuttingDepthChange)}: Cutting depth: {depth:F4}");
        }
        private void HandleSurgeryEnded(Surgery surgery)
        {
            Debug.Log(surgery.ContactTime);
            Debug.Log("Surgery ended");
        }
        // ReSharper disable Unity.PerformanceAnalysis
        private IEnumerator DebugSurgeryRoutine()
        {

            GameObject snapPositionSnareOp = GameObject.Find("SnapPositionSnareOP"); 
            Vector3 snareSnapStartPosition = snapPositionSnareOp.transform.position;
            ElectroSnareVR[] snares = FindObjectsOfType<ElectroSnareVR>();
            if (snares.Length==0)
            {
                Debug.LogError("No snare found");
                yield break;
            }
            snare = snares[0];
            snareRigidbody = snare.GetComponent<Rigidbody>();
            PrepareSnareForDebug();
            
            snareRigidbody.transform.position = snareSnapStartPosition;
            // ReSharper disable once Unity.InefficientPropertyAccess -- is ok in this case (UT)
            snareRigidbody.transform.rotation = snapPositionSnareOp.transform.rotation;
            startPoint = snareSnapStartPosition;
            middlePoint = snareSnapStartPosition + new Vector3(0.011f, -0.003f, 0.01f);
            endPoint = snareSnapStartPosition + new Vector3(-0.003f, 0.0032f, 0.0154f);
            steps = Mathf.RoundToInt(duration * 60);
            currentStep = 0;

            InvokeRepeating(nameof(SurgeryMove), 0f, 1 / timeStep);

            yield return null;
        }

        // ReSharper disable Unity.PerformanceAnalysis
        private void SurgeryMove()
        {
            if (currentStep <= steps)
            {
                float t = (float)currentStep / steps;

                // Interpolate 
                Vector3 a = Vector3.Lerp(startPoint, middlePoint, t);
                Vector3 b = Vector3.Lerp(middlePoint, endPoint, t);
                Vector3 position = Vector3.Lerp(a, b, t);

                // Move the snare to the new position
                snareRigidbody.transform.position = position;
                
                currentStep++;
            }
            else
            {
                CancelInvoke(nameof(SurgeryMove));
                ResetSnareToNormal();
                Debug.Log("Debug Surgery ended");
            }
        }
        private void PrepareSnareForDebug()
        {
            // Make the snare controllable by this script
            snare.GetComponent<MoveToPoint>().enabled = false;
            // ignore physics
            // ToDo: for the simulation of failure surgeries it might be necessary to enable physics
            snareRigidbody.isKinematic = true;
            snare.SetDebugWireState(true);
        }
        private void ResetSnareToNormal()
        {
            snare.SetDebugWireState(false);
            snareRigidbody.isKinematic = false;   
            snare.GetComponent<MoveToPoint>().enabled = true;
        }
    }
}
