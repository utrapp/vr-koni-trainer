using System;
using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using UnityEngine.SceneManagement;
using VR_Koni.GUI;
using VR_Koni.GUI.UI_Toolkit.Popup;
using VR_Koni.UserData;

namespace VR_Koni
{
    public class ApplicationManager : MonoBehaviour
    {
        private static ApplicationManager instance;

        private void Awake()
        {
            if (instance)
            {
                DestroyImmediate(gameObject);
                return;
            }

            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        public static void SwitchToLoginRegister()
        {
            DataManager.Instance.LogOutUser();
            Destroy(GameObject.Find("PlayerVariant")); // Remove duplicate PlayerVariant
            instance.StartCoroutine(SceneHelper.LoadScene("UI_LoginRegister", LoadSceneMode.Single));
        }

        public static void SwitchToMainMenu() =>
            instance.StartCoroutine(SceneHelper.LoadScene("UI_MainMenu", LoadSceneMode.Single));

        public static void SwitchToTutorials() =>
            instance.StartCoroutine(SceneHelper.LoadScene("UI_Tutorials", LoadSceneMode.Single));

        public static void SwitchToVROperation()
        {
            instance.StartCoroutine(LoadScenes());
            return;

            IEnumerator LoadScenes()
            {
                yield return SceneHelper.LoadScene("VR_MainScene", mode: LoadSceneMode.Single);
                yield return SceneHelper.LoadScene("VR_OperationRoom");
            }
        }

        public static void SwitchToVRTutorial()
        {
            throw new NotImplementedException();
            //instance.StartCoroutine(LoadScenes());
            //return;

            // IEnumerator LoadScenes()
            // {
            //     yield return SceneHelper.LoadScene("VR_MainScene", mode: LoadSceneMode.Single);
            //     yield return SceneHelper.LoadScene("<TBF>");
            // }
        }

        public static void StartIEnumerator(IEnumerator coroutine) => instance.StartCoroutine(coroutine);

        public static void AskToQuit()
        {
            LocalizedString popupHeaderQuitLocalized = new()
            {
                TableReference = ProjectConstants.UiToolKitLocalizationTableName,
                TableEntryReference = "MainMenuScreenLogOutPopupHeaderText"
            };

            LocalizedString popupDescriptionQuitLocalized = new()
            {
                TableReference = ProjectConstants.UiToolKitLocalizationTableName,
                TableEntryReference = "MainMenuScreenLogOutPopupDescriptionText"
            };

            LocalizedString popupLeftButtonTextQuitLocalized = new()
            {
                TableReference = ProjectConstants.UiToolKitLocalizationTableName,
                TableEntryReference = "MainMenuScreenLogOutPopupLeftButtonText"
            };

            LocalizedString popupRightButtonTextQuitLocalized = new()
            {
                TableReference = ProjectConstants.UiToolKitLocalizationTableName,
                TableEntryReference = "MainMenuScreenLogOutPopupRightButtonText"
            };

            PopupScreenManager.Show(headerLocalized: popupHeaderQuitLocalized,
                                    descriptionLocalized: popupDescriptionQuitLocalized,
                                    leftButtonTextLocalized: popupLeftButtonTextQuitLocalized,
                                    actionLeft: null,
                                    rightButtonTextLocalized: popupRightButtonTextQuitLocalized,
                                    actionRight: () =>
                                    {
                                        DataManager.Instance.Shutdown();
#if UNITY_EDITOR
                                        //If the application is running in the Unity Editor, exit play mode.
                                        EditorApplication.isPlaying = false;
#else
                                            //If the application is running as a standalone build, quit the application.
                                            Application.Quit();
#endif
                                    }
                                   );
        }
    }
}
