# Usability Test Tasks - Inexperienced User

## Thinking Aloud

[Link to the Thinking Aloud file](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/blob/DesignDocumentation-%23140-SAE/ux/usabillity_tests/2024/thinking_aloud.md)

## Tasks

### Pre-Menu

#### Task 1

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und möchten einen Account für den KONI TRAINER anlegen. Wie gehen Sie vor?

**Inputs**

- Sind die geforderten Eingaben für Sie klar?
- Ist der Registrierungsprozess für Sie angenehm und einfach gestaltet?

#### Task 2

**Scenario**
Sie möchten sich jetzt in Ihren Account einloggen. Sie tippen das falsche Passwort ein.

- Ist die Anzeige für Sie klar?
- Wie gehen Sie danach vor?
- Können Sie nun versuchen, sich erneut anzumelden, aber diesmal mit den richtigen Informationen

**Inputs**

- Sind die verschiedenen Anzeigen und Eingabefelder für Sie klar?
- Ist der Login-Prozess für Sie angenehm und einfach gestaltet?

### Menu

#### Task 3

**Scenario**
Nachdem Sie sich eingeloggt haben, sehen Sie sich das Hauptmenü an. Können Sie grob erklären, was Sie sehen?

**Inputs**

- Sind die verschiedenen Tasten für Sie übersichtlich?
- Ist die Schrift im Allgemeinen gut lesbar?
- Wie finden Sie die Navigierbarkeit des Menüs?

#### Task 4

**Scenario**
// make simpler
Stellen Sie sich vor, Sie sind angehende Ärztin und haben noch nie eine Konisation durchgeführt. Sie benutzen den Koni Trainer zum ersten mal. Sie wissen nicht wie die Steuerung funktioniert und wollen sich nun mit ihr vertraut machen. Wie gehen Sie vor, um sich mit dem Verfahren vertraut zu machen?

**Inputs**

- Fühlen Sie sich nun gut mit dem Verfahren vertraut und gut vorbereitet um die Controller auch im Spiel zu benutzen?
- Ist die Darstellung der Informationen für Sie hilfreich?
- Ist die Erklärung für Sie ausreichend? Würden Sie sich weitere Informationen wünschen? Wenn ja, welche?
- Könnte man die Steuerung Ihrer meinung nach noch besser Erklären/Darstellen?

#### Task 5

**Scenario**
Bevor Sie das erste Training starten, möchten Sie die Lautstärke im Spiel auf ihre Präferenzen anpassen. Wie gehen Sie dabei vor?

**Inputs**

- Waren die Einstelungen für Sie gut zu erreichen?
- Sind alle nötigen Einstellungen vorhanden?

#### Task 6

**Scenario**
Jetzt wollen wir ihre erstes Training starten. Wie gehen Sie dabei vor?

**Inputs**
// training

- War der Weg bis in das Training klar?
- Waren alle Anweisungen gut verständlich?

### Im VR-Modus

#### Task 7

**Scenario**
Das ist ihr erstes Training. Sie möchten die Benutzung grob testen und die Schlinge im Raum nehmen und testen. Wie gehen sie dabei vor?

**Tasks**

- Greifen Sie die Schlinge und schalten Sie sie an und wieder aus
- Legen Sie die Schlinge wieder hin
- Drehen und Kippen Sie die Schlinge

**Inputs**

- War die Benutzung der Schlinge für Sie leicht?
- Konnten Sie die Schlinge gut greifen?
- Konnten Sie sich noch an die Steuerung erinnern?
- Wie empfinden Sie das akustische Feedback?

#### Task 8

**Scenario**
Sie wollen vor der Trainings-OP das zu entfernende Gewebe erstmal genauer anschauen. Wie gehen Sie dabei vor?

**Inputs**

- Ist die Lupe einfach für Sie zu finden
- War die Benutzung der vergrößerten Ansicht (Lupe) für Sie angenehm?
- Ist das auszuschneidende Gewebe für Sie gut erkenntlich?
- Verlassen Sie die vergößerte Ansicht

#### Task 9

**Scenario**
Sie möchten nun Ihre erste Trainings-OP durchführen, wie gehen Sie dabei vor?

**Inputs**

- Wie ist ihr erster Eindruck nach der OP?
- Waren alle nötigen Informationen während der OP vorhanden?
- Würden Sie sich weitere Informationen während der OP wünschen?
- Wie empfinden Sie das akustische Feedback? Sind die Bedeutung davon klar zu verstehen
- Wie fühlt sich die Handhabung und Steuerung währen der OP für Sie an?

#### Task 10

**Scenario**
Sie wollen nun eine weitere OP durchführen (repeat Funktion)

**Inputs**

- War es leicht für Sie eine weiter OP zu wiederholen?

#### Task 11

**Scenario**
Sie wollen sich direkt nach dem Training Ihren Ergebnissen anschauen. Wie gehen Sie dabei vor?

**Inputs**

- Sind alle dargestellten Werte für Sie klar? Und auch klar erkennbar (gut zu lesen etc.)
- Wie finden Sie die Navigation zwischen den Bildschirmen?
- Fehlen Werte?
- Sind die angegebenen Werte für Sie angenehm dargestellt? (3D Ausschnitt der Konisation)

#### Task 12

**Scenario**
Sie möchten nun die Entwicklung ihrer Ergebnisse über die letzte Zeit ansehen.
Wie gehen Sie dabei vor?

**Inputs**

- Sind alle dargestellten Werte für Sie klar? Und auch klar erkennbar (gut zu lesen etc.)
- Sind die angegebenen Werte für Sie angenehm dargestellt? (Graphen)

### Back to Menu

#### Task 13

**Scenario**
Jetzt möchten Sie sich Ihre Statistiken nochmal ansehen, aber am normalen Computerbildschirm. Wie gehen Sie dabei vor?

**Inputs**

- Ist der Übergang zu den Statisteken klar?

### Task 14

**Scenario**
Sie haben endlich alles abgeschlossen was Sie am Koni Trainer machen wollten. Sie wollen das Programm nun beenden. Wie gehen Sie vor?

**Inputs**

- Ist es Ihnen leicht/schwer gefallen das Programm zu beenden?
- War die Aktion hinter den Buttons die, die Sie erwartet haben?
