using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

namespace VR_Koni.GUI.UI_Toolkit.Components.Charts
{
    [UxmlElement]
    public partial class PieChart : Chart
    {
        private readonly List<PieDataSet> dataSets = new(); // Holds all the data entries
        private const float LINE_WIDTH = 2.0f; // Width for segment outlines
        private const int PADDING = 20; // Padding around the pie chart
        
        //Extract hardcoded values into constants or properties
        private const float LABEL_RADIUS_OFFSET = 0.75f;
        public int LabelFontSize { get; set; } = 14;
        public Color LabelColor { get; set; } = Color.black;
        public TextAnchor LabelAlignment { get; set; } = TextAnchor.MiddleCenter;

        /// <summary>
        /// Constructor for the PieChart
        /// </summary>
        public PieChart()
        {
            Canvas.generateVisualContent += DrawPieChart; // Trigger rendering when visual content updates
        }

        /// <summary>
        /// Adds a data entry to the pie chart.
        /// </summary>
        /// <param name="label">Label for the data entry</param>
        /// <param name="value">Numerical value for the data entry</param>
        /// <param name="color">Color for the data entry</param>
        public void AddData(string label, float value, Color color)
        {
            if (value <= 0)
            {
                Debug.LogWarning($"Value for {label} must be greater than 0.");
                return;
            }

            base.AddData(label, color); // Add to legend
            dataSets.Add(new PieDataSet(label, value, color));
            Canvas.MarkDirtyRepaint(); // Trigger a redraw
        }

        /// <summary>
        /// Draw the pie chart in the canvas.
        /// </summary>
        /// <param name="context">Mesh generation context</param>
        private void DrawPieChart(MeshGenerationContext context)
        {
            if (dataSets.Count == 0) return;
            
            float totalValue = GetTotalValue();
            Rect rect = Canvas.contentRect;
            Vector2 center = GetCenter(rect);
            float radius = GetRadius(rect);
            

            float startAngle = 0f;
            foreach (var dataSet in dataSets)
            {
                float sweepAngle = CalculateSweepAngle(dataSet.Value, totalValue);
                DrawPieSegment(context, center, radius, startAngle, sweepAngle, dataSet.Color);
                
                float midAngle = startAngle + sweepAngle / 2;
                //DrawLabel(context, center, radius, midAngle, dataSet.Label, dataSet.Color);
                startAngle += sweepAngle;
            }
        }
        private Vector2 GetCenter(Rect rect)
        {
            return new Vector2(rect.x + rect.width / 2, rect.y + rect.height / 2);
        }
        private float GetRadius(Rect rect)
        {
            return Mathf.Min(rect.width, rect.height) / 2 - PADDING;
        }
        private float CalculateSweepAngle(float value, float totalValue)
        {
            return value / totalValue * 360f;
        }

        /// <summary>
        /// Draws an individual pie chart segment.
        /// </summary>
        /// <param name="context">Mesh generation context</param>
        /// <param name="center">Center of the pie chart</param>
        /// <param name="radius">Radius of the pie chart</param>
        /// <param name="startAngle">Starting angle of the segment</param>
        /// <param name="sweepAngle">Sweep angle of the segment</param>
        /// <param name="color">Color of the segment</param>
        private void DrawPieSegment(MeshGenerationContext context, Vector2 center, float radius, float startAngle, float sweepAngle, Color color)
        {
            Painter2D painter = context.painter2D;
            painter.fillColor = color;
            painter.strokeColor = ChartColor; // Optional outline color
            painter.lineWidth = LINE_WIDTH;

            painter.BeginPath();
            painter.MoveTo(center); // Start at the center
            for (float angle = startAngle; angle <= startAngle + sweepAngle; angle += 1f)
            {
                float rad = Mathf.Deg2Rad * angle;
                float x = center.x + Mathf.Cos(rad) * radius;
                float y = center.y + Mathf.Sin(rad) * radius;
                painter.LineTo(new Vector2(x, y));
            }
            painter.ClosePath();
            painter.Fill();
        }

        private void DrawLabel(MeshGenerationContext context, Vector2 center, float radius, float angle, string text,
                               Color color)
        {
            //convert angle to radians
            float rad = Mathf.Deg2Rad * angle;
            
            //calculate position of label
            float labelRadius = radius + LABEL_RADIUS_OFFSET;
            float x = center.x + Mathf.Cos(rad) * (labelRadius /2);
            float y = center.y + Mathf.Sin(rad) * (labelRadius /2);

            var label = new TextElement()
            {
                text = text,
                style =
                {
                    color = LabelColor,
                    fontSize = LabelFontSize,
                    unityTextAlign = LabelAlignment,
                    position = Position.Absolute,
                    left = x,
                    top = y
                }
            };
            Canvas.Add(label);
        }

        /// <summary>
        /// Calculates the total value of all data entries.
        /// </summary>
        /// <returns>Total value</returns>
        private float GetTotalValue()
        {
            return dataSets.Sum(dataSet => dataSet.Value);
        }
        

        /// <summary>
        /// Data structure to store pie chart data entries.
        /// </summary>
        private class PieDataSet : DataEntry
        {
            public float Value;

            public PieDataSet(string label, float value, Color color) : base(label, color)
            {
                Value = value;
            }
        }
        
    }
}
