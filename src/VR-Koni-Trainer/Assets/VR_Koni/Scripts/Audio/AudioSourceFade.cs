using EditorCools;
using UnityEngine;

namespace VR_Koni.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioSourceFade : MonoBehaviour
    {
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private float maxVolume;

        public float FadeTime;

        private int fadeMode;
        private float currentFade;

        public bool IsPlaying => audioSource.isPlaying;

        private void Reset()
        {
            audioSource = GetComponent<AudioSource>();
            maxVolume = audioSource.volume;
        }

        private void Update()
        {
            if (fadeMode == 0)
                return;

            currentFade += Time.deltaTime * fadeMode * FadeTime;
            audioSource.volume = currentFade * maxVolume;

            if (currentFade is > 1f or < 0f)
            {
                if (fadeMode == -1)
                    audioSource.Stop();

                fadeMode = 0;
                currentFade = Mathf.Clamp01(currentFade);
            }
        }

        [Button]
        public void Play()
        {
            fadeMode = 1;
            if (!audioSource.isPlaying)
                audioSource.Play();
        }

        [Button]
        public void Stop()
        {
            fadeMode = -1;
        }
    }
}
