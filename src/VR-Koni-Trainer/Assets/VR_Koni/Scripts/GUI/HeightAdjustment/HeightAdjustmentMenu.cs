namespace VR_Koni.GUI.HeightAdjustment
{
    public class HeightAdjustmentMenu : FadingMenu
    {
        private static HeightAdjustmentMenu instance;

        private void Awake() => instance = this;

        private void Start() => ShowMenu();

        public static void Show() => instance.ShowMenu();
        public static void Hide(bool skipFading = false) => instance.HideMenu(skipFading);
    }
}
