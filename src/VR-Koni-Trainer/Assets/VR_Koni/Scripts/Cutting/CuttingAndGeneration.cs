using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;
using UnityEngine;
using Game.Utils.Math;
using Game.Utils.Triangulation;
using UnityEngine.Serialization;
using VR_Koni.ElectroSnare;

using Unity.Profiling;
using System.Globalization;
using VR_Koni;

/// <summary>
/// This script is used to cut the cervix and generate two new objects.
/// The current procedure works like this:
/// TODO:
/// </summary>


public class ListWrapper<T>
{
    public List<T> value { get; set; }
}


public class CuttingAndGeneration : MonoBehaviour
{
    // Event invoked after Surgery is successfully finished
    public delegate void Victory(GameManager.GameState newState);
    public static event Victory OnVictory;
    
    public delegate void Loose(GameManager.GameState newState);
    public static event Loose OnLoose;

    public delegate void CutoutGenerated(GameObject cutOutObject);

    public static event CutoutGenerated OnCutoutGenerated;
    
    [FormerlySerializedAs("elektroschlingeVR")] public ElectroSnareVR electroSnareVR;
    public ComputeShader dilationShader;
    public ComputeShader erosionShader;
    public ComputeShader splittingShader;

    public ComputeShader drawLines;
    
    public ComputeShader countingPixels;
    private ComputeBuffer greenPixelsBuffer;
    
    private bool hotWire;
    private bool cutting;

    private struct CutoutSegment
    {
        public Vector2 LeftUV;
        public Vector2 RightUV;

        public RaycastHit LeftHit;
        public RaycastHit RightHit;
        
        // needed for possible sorting of cuttingSegments
        public Vector2 MiddlePoint()
        {
            return (LeftUV + RightUV) * 0.5f;
        }
        
        public List<Vector3> LeftSegmentedVertices;
        public List<Vector3> RightSegmentedVertices;
        
        // needed for analyzing
        [FormerlySerializedAs("timestamp")] public float Timestamp;
    }

    private struct UVCutoutSegment
    {
        public List<Vector2> LeftSegmentedVertices;
        public List<Vector2> RightSegmentedVertices;
    }
    
    private List<CutoutSegment> cutoutSegments;

    [FormerlySerializedAs("CutAccuracy")]
    [Header("Cut Accuracy")]
    [Tooltip("Defines the Accuracy of the cuts. The higher the lower the amount of cuts")]
    [SerializeField]
    public int cutAccuracy = 1;

    private bool cuttingDebug;
    private bool endCutting;
    private bool raycastFireOn;

    private int dilationKernelIndex;
    private int splittingKernelIndex;
    private int drawLinesKernelIndex;
    
    // counting pixel
    int texWidth = 2048;
    int texHeight = 2048;
    
    private static event Action<bool> onCutting;

    
    // ===== Meshes =====
    //public GameObject separatedMeshPrefab; // Prefab for the separated meshes
    public GameObject cancerCellsObject;
    public GameObject cervixObject;

    [SerializeField]
    public GameObject cancerCutoutPreparation;

    [SerializeReference] public GameObject objectToDestroy;

    //[SerializeField] public SteamVR_ActionSet actionToSet;
    
    // ===== Profiling =====
    private bool mainCoroutineFinished = false;
    static readonly ProfilerMarker s_PreparePerfMarker = new ProfilerMarker("MySystem.Cutting");
    
    // ===== Helper Methods =====
    
    
    List<Vector2> SortCCW(List<Vector2> vertices)
    {
        List<Vector2> verticesCopy = new List<Vector2>(vertices);

        // Find the centroid of the points
        Vector2 centroid = Vector2.zero;
        foreach (Vector2 vertex in verticesCopy)
        {
            centroid += vertex;
        }
        centroid /= verticesCopy.Count;

        // Sort the vertices based on polar angle
        verticesCopy.Sort((a, b) =>
        {
            float angleA = Mathf.Atan2(a.y - centroid.y, a.x - centroid.x);
            float angleB = Mathf.Atan2(b.y - centroid.y, b.x - centroid.x);
            if (angleA < angleB)
                return -1;
            if (angleA > angleB)
                return 1;
            return 0;
        });

        return verticesCopy;
    }
    
    
    private bool IsConvex(List<Vector2> polygon)
    {
        const float epsilon = 1e-5f;
        
        int n = polygon.Count;

        if (n < 3)
        {
            // A polygon with less than 3 vertices is not considered convex or concave
            return false;
        }

        bool gotNegative = false;
        bool gotPositive = false;

        for (int i = 0; i < n; i++)
        {
            Vector2 p0 = polygon[i];
            Vector2 p1 = polygon[(i + 1) % n];
            Vector2 p2 = polygon[(i + 2) % n];

            float crossProduct = CrossProduct(p0, p1, p2);

            if (crossProduct < -epsilon)
            {
                gotNegative = true;
            }
            else if (crossProduct > epsilon)
            {
                gotPositive = true;
            }

            if (gotNegative && gotPositive)
            {
                // The polygon is concave if both negative and positive cross products are encountered
                return false;
            }
        }
        // If both negative and positive cross products are not encountered, the polygon is convex
        return true;
    }
    
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="p0"></param>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    /// <returns></returns>
    private float CrossProduct(Vector2 p0, Vector2 p1, Vector2 p2)
    {
        // Calculate the cross product of the vectors (p1 - p0) and (p2 - p0)
        return (p1.x - p0.x) * (p2.y - p0.y) - (p1.y - p0.y) * (p2.x - p0.x);
    }

     
    // sort list 
    int CompareByYCoordinate(Vector2 a, Vector2 b)
    {
        if (a.y < b.y)
            return -1;
        if (a.y > b.y)
            return 1;
  
        return 0;
    }

    
    Vector2 UVToPixel(Vector2 uv, int textureWidth, int textureHeight)
    {
        float pixelX = Mathf.Clamp01(uv.x) * (textureWidth - 1);
        float pixelY = Mathf.Clamp01(uv.y) * (textureHeight - 1);

        return new Vector2(pixelX, pixelY);
    }
    
    
    
    private bool CouldFormTriangle(List<Vector2> toTriangulate)
    {
        if (toTriangulate.Count < 3)
            return false;
        
        // check if any vector combination could form a triangle with an area < 0
        int count = toTriangulate.Count;

        for (int i = 0; i < count - 2; i++)
        {
            for (int j = i + 1; j < count - 1; j++)
            {
                for (int k = j + 1; k < count; k++)
                {
                    float area = TriangleArea(toTriangulate[i], toTriangulate[j], toTriangulate[k]);

                    if (area > 0)
                        return true;  // At least one combination forms a triangle with an area larger than 0
                }
            }
        }

        return false;  // No combination forms a triangle with an area larger than 0
    }
    
    private float TriangleArea(Vector2 p1, Vector2 p2, Vector2 p3)
    {
        // Calculate the area of the triangle formed by three points
        return Mathf.Abs((p1.x * (p2.y - p3.y) + p2.x * (p3.y - p1.y) + p3.x * (p1.y - p2.y)) / 2f);
    }
    
    
    public List<int> ConvertToCwOrder(List<int> ccwTriangles)
    {
        List<int> cwTriangles = new List<int>();

        for (int i = 0; i < ccwTriangles.Count; i += 3)
        {
            // Reverse the order of vertices in each triangle
            cwTriangles.Add(ccwTriangles[i + 2]);
            cwTriangles.Add(ccwTriangles[i + 1]);
            cwTriangles.Add(ccwTriangles[i]);
        }

        return cwTriangles;
    }
    
    /// <summary>
    /// Checks if a point p is on the line formed by the points a and b
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="p"></param>
    /// <returns></returns>
    private bool IsOnLine(Vector2 a, Vector2 b, Vector2 p)
    {
        float crossProduct = CrossProduct(a, b, p);
        
        // Check if the cross product is very close to 0 (considering floating-point precision)
        return Mathf.Approximately(crossProduct, 0f);
    }

    
    /// <summary>
    /// Initialize p as leftmost point.
    /// Do following while we don’t come back to the first (or leftmost) point.
    /// The next point q is the point, such that the triplet (p, q, r) is counter clockwise for any other point r.
    /// To find this, we simply initialize q as next point, then we traverse through all points.
    /// For any point i, if i is more counter clockwise, i.e., orientation(p, i, q) is counter clockwise, then we update q as i.
    /// Our final value of q is going to be the most counter clockwise point.
    /// next[p] = q (Store q as next of p in the output convex hull).
    /// p = q (Set p as q for next iteration).
    /// </summary>
    /// <param name="points"></param>
    /// <param name="numberOfPoints"></param>
    List<Vector2> GiftwrappingAlgorithm(List<Vector2> points, int numberOfPoints)
    {
        if (numberOfPoints < 3) return null;
        
        // hull list
        List<Vector2> hull = new List<Vector2>();
        
        // lowest y
        int startingPointIndex = 0;
        
        // lowest point on y (p in literature)
        int p = startingPointIndex;
        int q = startingPointIndex;
        
        do
        {
            // add current to hulls
            hull.Add(points[p]);
            
            // init next point
            q = (p + 1) % numberOfPoints;
            
            // go through all points
            for (int i = 0; i < numberOfPoints; i++)
            {
                if (IsCCW(points[p], points[q], points[i]) == 2)
                    q = i;
            }

            // set q as next point for p
            p = q;

        } while (p != startingPointIndex);

        return hull;
    }

    /// <summary>
    /// 0 -> collinear
    /// 1 -> clockwise
    /// 2 -> counterclockwise
    /// </summary>
    /// <param name="p"></param>
    /// <param name="q"></param>
    /// <param name="r"></param>
    /// <returns></returns>
    private static int IsCCW(Vector2 p, Vector2 q, Vector2 r)
    {
        float val = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);
        
        // collinear 
        if (val == 0) 
            return 0; 
        
        // clock or counter-clock wise 
        return (val > 0)? 1: 2; 
    }
    
    bool IsPointInsideTriangle(Vector2 point, Vector2 uv0, Vector2 uv1, Vector2 uv2)
    {
        const float epsilon = 0.0001f;
        
        // Calculate barycentric coordinates
        float denominator = (uv1.y - uv2.y) * (uv0.x - uv2.x) + (uv2.x - uv1.x) * (uv0.y - uv2.y);
        float alpha = ((uv1.y - uv2.y) * (point.x - uv2.x) + (uv2.x - uv1.x) * (point.y - uv2.y)) / denominator;
        float beta = ((uv2.y - uv0.y) * (point.x - uv2.x) + (uv0.x - uv2.x) * (point.y - uv2.y)) / denominator;
        float gamma = 1 - alpha - beta;

        // Check if the point is inside the triangle (including edges)
        return alpha >= -epsilon && beta >= -epsilon && gamma >= -epsilon;
    }
    
    Vector3 CalcNormal(Vector3 normal1, Vector3 normal2, float t)
    {
        return Vector3.Lerp(normal1, normal2, t).normalized;
    }
    
    
    bool IsMultipleOfThree(int count)
    {
        return count % 3 == 0;
    }
    
    
    bool IsVertexInHashSet(Vector2 vertex, HashSet<Vector2> vertexSet)
    {
        var vector2EqualityComparer = new Vector2EqualityComparer();
        return vertexSet.Contains(vertex, vector2EqualityComparer);
    }
    
    bool IsVertexInHashSet(Vector3 vertex, HashSet<Vector3> vertexSet)
    {
        var vector3EqualityComparer = new Vector3EqualityComparer();
        return vertexSet.Contains(vertex, vector3EqualityComparer);
    }
    
    class Vector2EqualityComparer : IEqualityComparer<Vector2>
    {
        public bool Equals(Vector2 v1, Vector2 v2)
        {
            // Compare each component with tolerance
            return Mathf.Approximately(v1.x, v2.x) &&
                   Mathf.Approximately(v1.y, v2.y);
        }

        public int GetHashCode(Vector2 v)
        {
            return v.GetHashCode();
        }
    }
    
    // Vector3EqualityComparer class to handle comparisons with tolerance
    class Vector3EqualityComparer : IEqualityComparer<Vector3>
    {
        public bool Equals(Vector3 v1, Vector3 v2)
        {
            // Compare each component with tolerance
            return Mathf.Approximately(v1.x, v2.x) &&
                   Mathf.Approximately(v1.y, v2.y) &&
                   Mathf.Approximately(v1.z, v2.z);
        }

        public int GetHashCode(Vector3 v)
        {
            return v.GetHashCode();
        }
    }

    
    
    // ===== EventHandler =====
    private void HandleWireStateChange(bool state)
    {
        hotWire = state;
    }

    private void HandleCuttingStateChange(ElectroSnareVR.CuttingState state)
    {
        cutting = state == ElectroSnareVR.CuttingState.HealthyTissue;
    }

    private void HandleRayCastFire(bool fire)
    {
        raycastFireOn = fire;
    }
    
    // ===== Unity =====
    
    private void Awake()
    {
        cutoutSegments = new List<CutoutSegment>();
    }

    // Start is called before the first frame update
    void Start()
    {
        // subscribe to local event
        onCutting += StartMainCuttingAndGeneration;
        
        electroSnareVR = GetComponent<ElectroSnareVR>();
        
        // subscribe to wire state event
        ElectroSnareVR.onWireStateChange += HandleWireStateChange;
        ElectroSnareVR.onChangingState += HandleCuttingStateChange;
        ElectroSnareVR.onRayCastFire += HandleRayCastFire;
        
        // set shader kernels
        dilationKernelIndex = dilationShader.FindKernel("CSMain");
        splittingKernelIndex = splittingShader.FindKernel("CSMain");
        drawLinesKernelIndex = drawLines.FindKernel("CSMain");

        endCutting = false;
        cuttingDebug = false;
    }

    // Update is called once per frame
    // only event in update to prevent costly method call in update loop
    void Update()
    {
        // wire cuts, touches the cervix and a raycast hits
        if (hotWire && cutting && raycastFireOn)
        {
            if (endCutting)
            {
                StartCoroutine(SamplePositions());
            }

            // if the wire didnt cut before, set endCutting true. 
            
            if (!endCutting)
            {
                // First Time Cutting
                endCutting = true;
            }
            
        }
        else if(!hotWire && endCutting) // wire is cold but endCutting was set to true -> user ended cutting and disabled the wire
        {
            endCutting = false;
            onCutting?.Invoke(endCutting);// fire event that endCutting is true -> now the splicing can begin
        }
    }

   /// <summary>
   /// Disable Event handles
   /// </summary>
    private void OnDisable()
    {
        ElectroSnareVR.onWireStateChange -= HandleWireStateChange;

        ElectroSnareVR.onChangingState -= HandleCuttingStateChange;

        onCutting -= StartMainCuttingAndGeneration;
    }
    
    // ===== Mesh Cutting =====

    /// <summary>
    /// Samples the current ray cas positions inside the object to cut and the two current hit points
    /// </summary>
    /// <returns></returns>
    IEnumerator SamplePositions()
    {
        // add raycastpositions to the vertices list. But only when they are inside the mesh. 
        // From to to bottom -> Left/Right->1->2->3->Top
        // Check which raycast has a hit, -> sample all positions under this.
        // index +1 because the one that hits is not inside the mesh. Instead we need the hit Transform location 
        
        Vector3 leftHitVertex = electroSnareVR.currentLeftHit.point;
        Vector3 rightHitVertex = electroSnareVR.currentRightHit.point;

        CutoutSegment cutoutSegment = new CutoutSegment();
        
        // create Timestamp
        cutoutSegment.Timestamp = Time.realtimeSinceStartup;
        
        cutoutSegment.LeftSegmentedVertices = new List<Vector3>();
        cutoutSegment.RightSegmentedVertices = new List<Vector3>();
        
        cutoutSegment.LeftSegmentedVertices.Add(leftHitVertex);
        cutoutSegment.LeftUV = electroSnareVR.currentLeftHit.textureCoord;

        cutoutSegment.RightSegmentedVertices.Add(rightHitVertex);
        cutoutSegment.RightUV = electroSnareVR.currentRightHit.textureCoord;

        cutoutSegment.LeftHit = electroSnareVR.currentLeftHit;
        cutoutSegment.RightHit = electroSnareVR.currentRightHit;
      
        int leftIndex = electroSnareVR.currentLeftHitIndex;
        int rightIndex = electroSnareVR.currentRightHitIndex;
        
        // add every raycast position under the mesh (hit position)
        
        // lowest point at highest index. 
        if (leftIndex + 1 < electroSnareVR.leftRayCastList.Count)
        {
            for (int i = leftIndex+1; i < electroSnareVR.leftRayCastList.Count; i++ )
            {
                Vector3 vertex = electroSnareVR.leftRayCastList[i].position;
                cutoutSegment.LeftSegmentedVertices.Add(vertex);
            } 
        }
        
        if (rightIndex + 1 < electroSnareVR.rightRayCastList.Count)
        {
            for (int i = rightIndex+1; i < electroSnareVR.rightRayCastList.Count; i++ )
            {
                Vector3 vertex = electroSnareVR.rightRayCastList[i].position;
                cutoutSegment.RightSegmentedVertices.Add(vertex);
            } 
        }
        cutoutSegments.Add(cutoutSegment);
        
        yield return null;
    }


    // Start routine after event call onCutting
    private void StartMainCuttingAndGeneration(bool cuttingInput)
    {
        s_PreparePerfMarker.Begin();
        float startTime = Time.realtimeSinceStartup;

        StartCoroutine(MainCuttingAndGeneration());
        
        StartCoroutine(MeasureCoroutineTime(startTime));
        
        s_PreparePerfMarker.End();
    }
    
    
    /// <summary>
    /// Main method for cutting and generating
    /// </summary>
    /// <returns></returns>
    IEnumerator  MainCuttingAndGeneration()
    {
        //Debug.Log("End of Cutting. This is where the fun begins");
        
        // determine the two sides of the sliced object. 
        // for that, dispatch a compute shader that determines all uv coordinates and their corresponding vertices
        // "inside" the polygon drawn by the raycasts on the renderTexture.
        
        // TODO: correct hardcoded tex size
        // get second tex from cervix material. 
        dilationShader.SetTexture(dilationKernelIndex, "Result", electroSnareVR.textureToColor);
        
        // dispatch shader
        
        // Calculate the dispatch size based on the texture dimensions
        int numThreadsX = Mathf.CeilToInt(2048 / 8.0f);
        int numThreadsY = Mathf.CeilToInt(2048 / 8.0f);
        
        // create sorted segments - performance problems?
        List<CutoutSegment> adaptedSegments = CreateSortedSegments();
  
        List<Vector2> hull = new List<Vector2>();
        
        // leftUV forward, rightUV backward
        foreach (var segment in adaptedSegments)
        {
            Vector2 pixelCoords = UVToPixel(segment.LeftUV, texWidth, texHeight);
            hull.Add(pixelCoords );
        }
        
        // rightUV backward
        for (int i = adaptedSegments.Count - 1; i >= 0; i--)
        {
            var segment = adaptedSegments[i];
            Vector2 pixelCoords = UVToPixel(segment.RightUV, texWidth, texHeight);
            hull.Add(pixelCoords);
        }
        
        
        // draw lines of created convex hull
        
        // transform vector2 to float2
        List<float2> listToBuffer = new List<float2>();
        foreach (var vector in hull)
        {
            float2 f = new float2(vector.x, vector.y);
            listToBuffer.Add(f);
        }
        
        // send listToBuffer to Shader and draw hull
        ComputeBuffer computeBuffer = new ComputeBuffer(listToBuffer.Count, sizeof(float) * 2); // Each float2 is 2 floats
        computeBuffer.SetData(listToBuffer.ToArray()); // Convert the List<float2> to an array and set it on the buffer
        
        // Set InputBuffer Count
        drawLines.SetInt("pixelBufferLength",listToBuffer.Count);
        // Set Buffer Kernel
        drawLines.SetBuffer(drawLinesKernelIndex, "PixelBuffer", computeBuffer);
        // Set inplut texture
        drawLines.SetTexture(drawLinesKernelIndex, "Result", electroSnareVR.textureToColor);
        // Dispatch Compute Shader
        drawLines.Dispatch(drawLinesKernelIndex, numThreadsX, numThreadsY,1);
        
        
        // initialize the two game objects. At this point, they are empty but with a meshfilter and empty mesh
        InitNewGameObjects();

        // list with all affected "edge triangles" with the corresponding vertex indices

        //HashSet<>
        List<Vector2> hullVertices = new List<Vector2>();
        HashSet<Vector2> hullVerticesHashSet = new HashSet<Vector2>(hullVertices, new Vector2EqualityComparer());
        
        List<Vector2> intersectionVertices = new List<Vector2>();
        HashSet<Vector2> intersectionVerticesHashSet = new HashSet<Vector2>(intersectionVertices, new Vector2EqualityComparer());
        

        List<(int, int)> edgeTriangleVertexIndicesPairList = new List<(int, int)>();
        List<List<int>> sortedEdges = new List<List<int>>();
        
        
        // Creates and Inserts new vertices into the surface mesh part
        BuildMeshes
            (hull, 
                adaptedSegments, 
                out List<Vector3> vertices, 
                out List<Vector3> normals, 
                out List<Vector2> uvs, 
                out List<int> triangles, 
                out List<Vector3> additionalVertices, 
                out List<int> additionalVerticesIndices,
                ref edgeTriangleVertexIndicesPairList, 
                hullVerticesHashSet,
                ref sortedEdges
                );
        
        // fill inside of polygon with color
        splittingShader.SetInt("texWidth", 2048);
        splittingShader.SetInt("texHeight", 2048);
        splittingShader.SetInt("pixelBufferLength",listToBuffer.Count);
        splittingShader.SetBuffer(splittingKernelIndex, "PixelBuffer", computeBuffer);
        splittingShader.SetTexture(splittingKernelIndex, "Result", electroSnareVR.textureToColor);
        splittingShader.Dispatch(splittingKernelIndex, numThreadsX, numThreadsY, 1);
        computeBuffer.Release();

        
        // generates the triangles and separates the two cut halves
        GenerateMeshes
            (
                ref vertices, 
                ref normals, 
                ref uvs, 
                ref triangles, 
                ref additionalVertices,
                ref edgeTriangleVertexIndicesPairList,
                ref sortedEdges
                );

        // TODO: calc middlepoint of cancer mesh vertices
        // temp point:
        Vector3 tempMiddlePoint = new Vector3(0.8598f, 1.2258f, 0.036f);

        // generates and triangulates the cutout surface
        GenerateCutout(tempMiddlePoint, adaptedSegments);
        
        GameObject.Destroy(objectToDestroy);
        
        mainCoroutineFinished = true;
        yield return null;
    }
    
    /// <summary>
    /// Init game objects. CancerCellsObject is set to a prepared object already in Unity because of the
    /// interactive script behaviour
    /// </summary>
    void InitNewGameObjects()
    {
        cancerCellsObject = cancerCutoutPreparation; 
        
        cervixObject = new GameObject("cervixObject");
        MeshFilter cervixMeshFilter = cervixObject.AddComponent<MeshFilter>();
        cervixMeshFilter.mesh = new Mesh();
    }
    
    /// <summary>
    /// Generates new vertices/UVs and normals from the ray cast hit and from the intersections between
    /// existing triangle edges and connections between the new vertices. Adds all into the same mesh
    /// </summary>
    /// <param name="hull"></param>
    /// <param name="cutoutSegmentsInput"></param>
    /// <param name="vertices"></param>
    /// <param name="normals"></param>
    /// <param name="uvs"></param>
    /// <param name="triangles"></param>
    /// <param name="additionalVertices"></param>
    /// <param name="additionalVerticesIndices"></param>
    /// <param name="edgeTriangleVertexIndicesPairList"></param>
    /// <param name="hullVerticesHashSet"></param>
    /// <param name="sortedEdges"></param>
    void BuildMeshes
    (
        List<Vector2> hull, 
        List<CutoutSegment> cutoutSegmentsInput, 
        out List<Vector3> vertices, 
        out List<Vector3> normals, 
        out List<Vector2> uvs, 
        out List<int> triangles, 
        out List<Vector3> additionalVertices,
        out List<int> additionalVerticesIndices,
        ref List<(int, int)> edgeTriangleVertexIndicesPairList,
        HashSet<Vector2> hullVerticesHashSet,
        ref List<List<int>> sortedEdges
    )
    {
        // copy information to not modify the original
        vertices = electroSnareVR.canvasMesh.vertices.ToList();
        normals = electroSnareVR.canvasMesh.normals.ToList();
        uvs = electroSnareVR.canvasMesh.uv.ToList();
        triangles = electroSnareVR.canvasMesh.triangles.ToList();
        
        // information to store the additional vertices that must be added to both meshes
        //additionalVertices = new List<int>();
        
        // Inserts all new mesh information into the "original" mesh
        InsertNewMeshInformation
        (
            hull, 
            cutoutSegmentsInput, 
            ref vertices, 
            ref normals, 
            ref uvs, 
            triangles, 
            out List<Vector3> convertedAdditionalVertices,
            out List<int> additionalVerticesIndicesOutput,
            ref edgeTriangleVertexIndicesPairList, 
            hullVerticesHashSet,
            ref sortedEdges
        );

        additionalVertices = convertedAdditionalVertices;
        additionalVerticesIndices = additionalVerticesIndicesOutput;
    }

    
    /// <summary>
    /// Copies original mesh information, inserts new vertices and triangles, returns the modified mesh information
    /// We have technically three new information blocks:
    /// 1: The vertices and triangles from the points creating the seam of the cut.
    /// 2: The vertices and triangles from the intersection of the cut with existing triangles
    /// 3: The new vertices generated by the 3d nature of the cut, where the vertices are generated by the position of
    /// the raycast origins of the electrosnare
    /// </summary>
    void InsertNewMeshInformation
    (
        List<Vector2> hull, 
        List<CutoutSegment> cutoutSegmentsInput, 
        ref List<Vector3> vertices, 
        ref List<Vector3> normals, 
        ref List<Vector2> uvs, 
        List<int> triangles, 
        out List<Vector3> additionalVertices,
        out List<int> additionalVerticesIndices,
        ref List<(int, int)> edgeTriangleVertexIndicesPairList,
        HashSet<Vector2> hullVerticesHashSet,
        ref List<List<int>> sortedEdges
    )
    {
        List<int> tempAdditionalVertices = new List<int>();

        // insert the mesh information created by the seam
        // after this, the created vertices are inserted into the original mesh but in the order of creation, starting on the left side
        // and then iterating the right side backwards. Thus the order is some sort of circle
        
        // TODO: remove unnecessary edgeTriangleVertexIndicesPairList
        List<(int, int)> tempIToRemove = new List<(int, int)>();
        InsertHullInformation(cutoutSegmentsInput, vertices, normals, uvs, tempAdditionalVertices, tempIToRemove, hullVerticesHashSet);
        
        
        //int k = hull.Count;
        // Create a new list and add the last N elements from the original list
        //List<Vector3> newList = new List<Vector3>(vertices.GetRange(vertices.Count - k, k));
        
        //DebugVertices(newList, Color.green);
        
        // insert the mesh information created by the intersections
        //InsertIntersectionInformationV2(newOffsetCounter, hull, cutoutSegmentsInput, vertices, normals, uvs, triangles, Tvertices, Tnormals, Tuvs, additionalVertices, edgeTriangleVertexIndicesPairList, intersectionVerticesHashSet);
        List<(int, int)> updatedEdgeTriangleVertexIndicesPairList;
        List<List<int>> sortedEdgesOutput;

        List<int> additionalVertices2 = new List<int>();

        
        
        InsertIntersectionInformationV2(hull, vertices, normals, uvs, triangles, ref additionalVertices2, out updatedEdgeTriangleVertexIndicesPairList, out sortedEdgesOutput);
        edgeTriangleVertexIndicesPairList = updatedEdgeTriangleVertexIndicesPairList;
        
        //
        additionalVerticesIndices = additionalVertices2;
        
        // fill add vertices
        additionalVertices = new List<Vector3>();
        foreach (var index in additionalVertices2)
        {
            additionalVertices.Add(vertices[index]);
        }

        sortedEdges = sortedEdgesOutput;
    }
    
    
    /// <summary>
    /// inserts the mesh information created by the seam
    /// </summary>
    /// <param name="cutoutSegmentsInput"></param>
    /// <param name="vertices"></param>
    /// <param name="normals"></param>
    /// <param name="uvs"></param>
    /// <param name="additionalVertices"></param>
    /// <param name="edgeTriangleVertexIndicesPairList"></param>
    /// <param name="hullVerticesHashSet"></param>
    void InsertHullInformation
    (
        List<CutoutSegment> cutoutSegmentsInput, 
        List<Vector3> vertices, 
        List<Vector3> normals, 
        List<Vector2> uvs, 
        List<int> additionalVertices, 
        List<(int, int)> edgeTriangleVertexIndicesPairList,
        HashSet<Vector2> hullVerticesHashSet
    )
    {
        // create temp triangleList for safe deletion
        List<(int, Vector3)> trianglesToInsertTo = new List<(int, Vector3)>();
        
        // Create a HashSet to efficiently check for duplicate vertices
        HashSet<Vector3> vertexSet = new HashSet<Vector3>(vertices, new Vector3EqualityComparer());
        
        // iterate through every Edge point. Currently this is the result of the Giftwrapping Algorithm but it could 
        // later be every point forming a line of every drawn pixel in the correct order for more accurate results

        
        // first left, then backwards right
        foreach (var segment in cutoutSegmentsInput)
        {
            InsertSingleSegmentInfo(segment.LeftHit, vertexSet, trianglesToInsertTo, edgeTriangleVertexIndicesPairList, vertices, normals, uvs, additionalVertices, hullVerticesHashSet);
        }
        
        // rightUV backward
        for (int i = cutoutSegmentsInput.Count - 1; i >= 0; i--)
        {
            var segment = cutoutSegmentsInput[i];
            InsertSingleSegmentInfo(segment.RightHit, vertexSet, trianglesToInsertTo, edgeTriangleVertexIndicesPairList, vertices, normals, uvs, additionalVertices, hullVerticesHashSet);
        }
    }
    
    
    /// <summary>
    /// Iterate through hull, find ALL! intersections between hull edges and triangles. remember intersection with triangle Indices
    /// </summary>
    /// <param name="hull"></param>
    /// <param name="vertices"></param>
    /// <param name="normals"></param>
    /// <param name="uvs"></param>
    /// <param name="triangles"></param>
    /// <param name="additionalVertices"></param>
    /// <param name="updatedEdgeTriangleVertexIndicesPairList"></param>
    /// <param name="edgeSorting"></param>
    void InsertIntersectionInformationV2
    (
        List<Vector2> hull,
        List<Vector3> vertices, 
        List<Vector3> normals,
        List<Vector2> uvs, 
        List<int> triangles,
        ref List<int> additionalVertices, 
        out List<(int, int)> updatedEdgeTriangleVertexIndicesPairList,
        out List<List<int>> edgeSorting
    )
    {
        // keep track of intended indices
        int currentNewVertexIndex = vertices.Count;

        Vector3 newVertexToAdd0 = new Vector3();
        Vector3 newNormalToAdd0 = new Vector3();

        Vector3 newVertexToAdd1 = new Vector3();
        Vector3 newNormalToAdd1 = new Vector3();

        Vector3 newVertexToAdd2 = new Vector3();
        Vector3 newNormalToAdd2 = new Vector3();
        
        // Create HashSet for efficient duplicate checking
        HashSet<Vector3> tvertexSet = new HashSet<Vector3>(vertices, new Vector3EqualityComparer());

        // sample all found intersections along the two hullpoints
        // sort found intersections along the vector
        // only then insert vertices?

        List<int> hullMapping = new List<int>();
        for (int i = vertices.Count-hull.Count(); i < vertices.Count; i++)
        {
            hullMapping.Add(i);
        }

        List<int> edgeMapping = hullMapping;

       
        //List<List<Vector2>> edges = new List<List<Vector2>>();
        List<List<int>> indexEdges = new List<List<int>>();

        // iterate every hull edge
        for (int hullIndex = 0; hullIndex < hull.Count; hullIndex++)
        {
            int startIndex = hullIndex;
            int endIndex = (hullIndex + 1) % hull.Count;

            int realStartIndex = hullMapping[startIndex];
            int realEndIndex = hullMapping[endIndex];

            int currentIndexToChoose = realEndIndex;

            if (realStartIndex >= realEndIndex)
                currentIndexToChoose = realStartIndex + 1;

            currentNewVertexIndex = currentIndexToChoose;

            

            // get hull edge in Pixel Coordinates
            Vector2 startPoint = hull[hullIndex];
            Vector2 endPoint = hull[(hullIndex + 1) % hull.Count];

            // convert to UV coordinates
            Vector2 startUV = new Vector2(startPoint.x / (2048 - 1), startPoint.y / (2048 - 1));
            Vector2 endUV = new Vector2(endPoint.x / (2048 - 1), endPoint.y / (2048 - 1));


            List<Vector2> uvsForTriangles = new List<Vector2>();
            List<int> indexForTriangle = new List<int>();

            // iterate through every triangle edge
            // should always include two triangles
            for (int j = 0; j < triangles.Count; j += 3)
            {
                // get triangleIndex based on startingVertex. 
                int currentTriangleIndex = j / 3;

                // find vertices of original triangle
                int vertex0Index = triangles[j];
                int vertex1Index = triangles[j + 1];
                int vertex2Index = triangles[j + 2];

                // Get the UV coordinates of the vertices
                Vector2 uv0 = uvs[vertex0Index];
                Vector2 uv1 = uvs[vertex1Index];
                Vector2 uv2 = uvs[vertex2Index];

                // get vertices
                Vector3 v0 = vertices[vertex0Index];
                Vector3 v1 = vertices[vertex1Index];
                Vector3 v2 = vertices[vertex2Index];

                // get normals
                Vector3 n0 = normals[vertex0Index];
                Vector3 n1 = normals[vertex1Index];
                Vector3 n2 = normals[vertex2Index];

                // find ALL intersections between hull edge and triangle edge. Between 1 - 3 intersections.
                // remember which edge: 
                // v0-v1, v0-v2, v1-2
                FindNewIntersectingVertexPositionsV2(uv0, uv1, uv0, uv2, uv1, uv2, startUV, endUV,
                    out Vector2 inP0, out Vector2 inP1, out Vector2 inP2);

                // V0-V1
                if (inP0 != Vector2.zero)
                {
                    generateVertex(inP0, ref newVertexToAdd0, ref newNormalToAdd0, uv0, uv1, v0, v1,
                        n0, n1);

                    // Check if the vertex is already in the Tvertices
                    if (!IsVertexInHashSet(newVertexToAdd0, tvertexSet))
                    {
                        vertices.Insert(currentNewVertexIndex, newVertexToAdd0);
                        normals.Insert(currentNewVertexIndex, newNormalToAdd0);
                        uvs.Insert(currentNewVertexIndex, inP0);

                        uvsForTriangles.Add(inP0);
                        
                        indexForTriangle.Add(currentNewVertexIndex);

                        tvertexSet.Add(newVertexToAdd0);

                        // adjust hullmappings
                        int index = edgeMapping.IndexOf(currentNewVertexIndex);

                        if (index != -1)
                        {
                            edgeMapping = edgeMapping.Select((value, i) => i >= index ? value + 1 : value).ToList();

                            // insert new 
                            edgeMapping.Insert(index, currentNewVertexIndex);
                        }
                        else
                        {
                            edgeMapping.Add(currentNewVertexIndex);
                        }

                        // change values of map:
                        hullMapping = hullMapping.Select((value, i) => i >= endIndex ? value + 1 : value).ToList();

                        currentNewVertexIndex++;
                    }
                }

                if (inP1 != Vector2.zero)
                {
                    generateVertex(inP1, ref newVertexToAdd1, ref newNormalToAdd1, uv0, uv2, v0, v2,
                        n0, n2);

                    // Check if the vertex is already in the Tvertices with a small error margin
                    if (!IsVertexInHashSet(newVertexToAdd1, tvertexSet))
                    {
                        vertices.Insert(currentNewVertexIndex, newVertexToAdd1);
                        normals.Insert(currentNewVertexIndex, newNormalToAdd1);
                        uvs.Insert(currentNewVertexIndex, inP1);
                        
                        uvsForTriangles.Add(inP1);
                        indexForTriangle.Add(currentNewVertexIndex);

                        tvertexSet.Add(newVertexToAdd1);
                        
                        // adjust hullmappings
                        int index = edgeMapping.IndexOf(currentNewVertexIndex);

                        if (index != -1)
                        {
                            edgeMapping = edgeMapping.Select((value, i) => i >= index ? value + 1 : value).ToList();

                            // insert new 
                            edgeMapping.Insert(index, currentNewVertexIndex);
                        }
                        else
                        {
                            edgeMapping.Add(currentNewVertexIndex);
                        }

                        // change values of map:
                        hullMapping = hullMapping.Select((value, i) => i >= endIndex ? value + 1 : value).ToList();

                        currentNewVertexIndex++;
                    }
                }

                if (inP2 != Vector2.zero)
                {
                    generateVertex(inP2, ref newVertexToAdd2, ref newNormalToAdd2, uv1, uv2, v1, v2,
                        n1, n2);

                    // Check if the vertex is already in the Tvertices with a small error margin
                    if (!IsVertexInHashSet(newVertexToAdd2, tvertexSet))
                    {
                        vertices.Insert(currentNewVertexIndex, newVertexToAdd2);
                        normals.Insert(currentNewVertexIndex, newNormalToAdd2);
                        uvs.Insert(currentNewVertexIndex, inP2);
                        
                        uvsForTriangles.Add(inP2);
                        indexForTriangle.Add(currentNewVertexIndex);

                        tvertexSet.Add(newVertexToAdd2);
                      
                        // adjust hullmappings
                        int index = edgeMapping.IndexOf(currentNewVertexIndex);

                        if (index != -1)
                        {
                            edgeMapping = edgeMapping.Select((value, i) => i >= index ? value + 1 : value).ToList();

                            // insert new 
                            edgeMapping.Insert(index, currentNewVertexIndex);
                        }
                        else
                        {
                            edgeMapping.Add(currentNewVertexIndex);
                        }

                        // change values of map:
                        //hullMapping[endIndex] = currentNewVertexIndex + 1;
                        hullMapping = hullMapping.Select((value, i) => i >= endIndex ? value + 1 : value).ToList();

                        currentNewVertexIndex++;
                    }
                }
            }
            
            Vector2 resultStartUV;
            Vector2 resultEndUV;
                
            
            if (realStartIndex >= realEndIndex)
            {
                resultStartUV = uvs[realStartIndex];
                resultEndUV = uvs[realEndIndex];
                 
                indexForTriangle.Insert(0, realStartIndex);
                indexForTriangle.Add(realEndIndex);
            }
            else
            {
                resultStartUV = uvs[realStartIndex];
                resultEndUV = uvs[currentNewVertexIndex];
                
                indexForTriangle.Insert(0, realStartIndex);
                indexForTriangle.Add(currentNewVertexIndex);
            }
            
            //resultStartUV = uvs[realStartIndex];
            //resultEndUV = uvs[currentNewVertexIndex];
            
            uvsForTriangles.Insert(0, resultStartUV);
            uvsForTriangles.Add(resultEndUV);
            
            //edges.Add(uvsForTriangles);
            indexEdges.Add(indexForTriangle);
            
        }

        // make copy to sort
        List<List<int>> indexEdgesCopy = indexEdges;

        List<Vector3> verticesC = new List<Vector3>(vertices);
        List<Vector3> normalsC = new List<Vector3>(normals);
        List<Vector2> uvsC = new List<Vector2>(uvs);

        
        // till here everything looks ok. Edges has every vertex in vertices but is not sorted

        List<List<int>> indexEdgesCopy2 = new List<List<int>>();
        // what do to: sort edge after uv order to start
        foreach (var indexEdge in indexEdgesCopy)
        {
            // get uvs of edge
            List<Vector2> uvsToSort = indexEdge.Select(index => uvsC[index]).ToList();
            
            // sort uvs
            int firstIndex = 0;
            
            // Get the sorting order based on the distance to the first element
            List<int> sortingOrder = Enumerable.Range(0, uvsToSort.Count)
                .OrderBy(index => Vector2.Distance(uvsToSort[index], uvsToSort[firstIndex]))
                .ToList();
            
            // Apply the same sorting order to indexEdge
            List<int> indexEdgeC = sortingOrder.Select(index => indexEdge[index]).ToList();
            
            indexEdgesCopy2.Add(indexEdgeC);
        }
        
        // indexEdgesCopy2 now has ALL hull vertices in the correct order
        // replace values with values of indexEdgesCopy2 in correct order
        // delete at vertices.Count -hull.Count, then just iterate and add
  
        HashSet<int> uniqueIndices = new HashSet<int>();
        List<int> newIndices = new List<int>();

        foreach (var listE in indexEdgesCopy2)
        {
            foreach (var index in listE)
            {
                if (uniqueIndices.Add(index)) // If the index is added to the HashSet (i.e., it's unique)
                {
                    newIndices.Add(index); // Add it to the list
                }
            }
        }
        
    
        List<int> updatedAdditionalVertices = new List<int>();
        int indexCounter = (verticesC.Count - newIndices.Count);
        foreach (var index in newIndices)
        {
            updatedAdditionalVertices.Add(indexCounter);
            
            vertices[indexCounter] = verticesC[index];
            normals[indexCounter] = normalsC[index];
            uvs[indexCounter] = uvsC[index];
            indexCounter++;
        }

        // now, the values are updated to the sorted way.
        // create additional vertices
        // and triangle pairing
        updatedEdgeTriangleVertexIndicesPairList = new List<(int, int)>();
        
        // iterate over all triangles, find out of completeAdditionalVertices the 
        for (int j = 0; j < triangles.Count; j += 3)
        {
            // get triangleIndex based on startingVertex. 
            int currentTriangleIndex = j / 3;

            // find vertices of original triangle
            int vertex0Index = triangles[j];
            int vertex1Index = triangles[j + 1];
            int vertex2Index = triangles[j + 2];

            // Get the UV coordinates of the vertices
            Vector2 uv0 = uvs[vertex0Index];
            Vector2 uv1 = uvs[vertex1Index];
            Vector2 uv2 = uvs[vertex2Index];
            
            foreach (var additionalIndex in updatedAdditionalVertices)
            {
                Vector2 additionalUV = uvs[additionalIndex];
                
                // if additionalUV is On the edges/inside the triangle, add currentTriangleIndex to list
                // Check if additionalUV is inside the current triangle
                if (IsPointInsideTriangle(additionalUV, uv0, uv1, uv2))
                {
                    // additionalUV is inside the current triangle, add currentTriangleIndex to the list
                    updatedEdgeTriangleVertexIndicesPairList.Add((currentTriangleIndex, additionalIndex));
                }
            }
        }

        additionalVertices = updatedAdditionalVertices;
        edgeSorting = indexEdgesCopy2;
    }
    
    
    void InsertSingleSegmentInfo
    (
        RaycastHit hit, 
        HashSet<Vector3> vertexSet, 
        List<(int, Vector3)> trianglesToInsertTo, 
        List<(int, int)> edgeTriangleVertexIndicesPairList, 
        List<Vector3> vertices, 
        List<Vector3> normals, 
        List<Vector2> uvs, 
        List<int> additionalVertices,
        HashSet<Vector2> hullVerticesHashSet 
    )
    {
        // generate vertex at correct position
        Vector3 vertex = electroSnareVR.cervixTransform.InverseTransformPoint(hit.point);
                
        // copy normal (could potentially not be perfectly accurate but good enough. For maybe better results triangulate with other normals )
        Vector3 normal = hit.normal;
                
        // get UV coordinate of hullPixel

        Vector2 hullUV = hit.textureCoord; //new Vector2(hullPixel.x / (2048 - 1), hullPixel.y / (2048 - 1));

        // get triangle index
        int triangleIndex = hit.triangleIndex;
                
        // Check if the vertex is already in the vertices
        if (!IsVertexInHashSet(vertex, vertexSet))
        {
            // Add the original triangle index to the list of triangles to delete
            trianglesToInsertTo.Add((triangleIndex, vertex));
                
            int newIndex = vertices.Count;
            edgeTriangleVertexIndicesPairList.Add((triangleIndex, newIndex));
                
            vertices.Add(vertex);
            normals.Add(normal);
            uvs.Add(hullUV);

            // list for checking which vertices are new
            additionalVertices.Add(newIndex);
                    
            // Add the vertex to the HashSet
            vertexSet.Add(vertex);
            
            // uv coord to heshset
            hullVerticesHashSet.Add(hullUV);
        }
    }
    
    
    void GenerateMeshes
    (
        ref List<Vector3> vertices, 
        ref List<Vector3> normals, 
        ref List<Vector2> uvs, 
        ref List<int> triangles, 
        ref List<Vector3> additionalVertices,
        ref List<(int, int)> edgeTriangleVertexIndicesPairList,
        ref List<List<int>> sortedEdges
    )
    {
        List<Vector3> cancerVertices = new List<Vector3>();
        List<Vector3> cancerNormals = new List<Vector3>();
        List<Vector2> cancerUVs = new List<Vector2>();
        List<int> cancerTriangles = new List<int>();

        List<int> vertexIndicesCancerMesh = new List<int>();


        List<Vector3> cervixVertices = new List<Vector3>();
        List<Vector3> cervixNormals = new List<Vector3>();
        List<Vector2> cervixUVs = new List<Vector2>();
        List<int> cervixTriangles = new List<int>();

        List<int> vertexIndicesCervixMesh = new List<int>();

        // ===== Divide =====
        
        // Ensure the RenderTexture is active
        RenderTexture.active = electroSnareVR.textureToColor;

        // Create a new Texture2D to store the pixels
        Texture2D texture = new Texture2D(2048, 2048);

        // Read pixels from the RenderTexture into the Texture2D
        texture.ReadPixels(new Rect(0, 0, 2048, 2048), 0, 0);
        texture.Apply(); // Apply changes to the Texture2D

        // Reset the active RenderTexture
        RenderTexture.active = null;

        // store the indices of the original order in the two lists.. necessary for later accessing the correct indices
        int i = 0;
        
        // Now, 'texture' contains the pixel data from the RenderTexture
        
        // iterate over every uv in the BuildMesh
        foreach (var uv in uvs)
        {
            Color pixelColor = texture.GetPixelBilinear(uv.x, uv.y);
            if (pixelColor == Color.red)
            {
                // add corresponding vertex to the list
                cancerVertices.Add(vertices[i]);
                cancerNormals.Add(normals[i]);
                cancerUVs.Add(uvs[i]);
                
                vertexIndicesCancerMesh.Add(i);
                
                // also to the other colour if in additional Vertices
                if (additionalVertices.Contains(vertices[i]))
                {
                    cervixVertices.Add(vertices[i]);
                    cervixNormals.Add(normals[i]);
                    cervixUVs.Add(uvs[i]);
                    
                    vertexIndicesCervixMesh.Add(i);
                }
            }
            else
            {
                // add corresponding vertex to the list
                cervixVertices.Add(vertices[i]);
                cervixNormals.Add(normals[i]);
                cervixUVs.Add(uvs[i]);
                
                vertexIndicesCervixMesh.Add(i);
                
                // also to the other colour if in additional Vertices
                if (additionalVertices.Contains(vertices[i]))
                {
                    cancerVertices.Add(vertices[i]);
                    cancerNormals.Add(normals[i]);
                    cancerUVs.Add(uvs[i]);
                    
                    vertexIndicesCancerMesh.Add(i);
                }
            }
            i++;
        }


        for (int j = 0; j < triangles.Count; j += 3)
        {
            int triangleIndex = j / 3;

            // first check that the triangle is not part of any hitpoints or intersections
            if (!edgeTriangleVertexIndicesPairList.Any(pair => pair.Item1 == triangleIndex))
            {
                int vertexIndex0 = triangles[j];
                int vertexIndex1 = triangles[j + 1];
                int vertexIndex2 = triangles[j + 2];

                // Check if all three vertices of the triangle belong to the cancer or cervix mesh
                if (vertexIndicesCancerMesh.Contains(vertexIndex0) && vertexIndicesCancerMesh.Contains(vertexIndex1) && vertexIndicesCancerMesh.Contains(vertexIndex2))
                {
                    cancerTriangles.Add(vertexIndicesCancerMesh.IndexOf(vertexIndex0));
                    cancerTriangles.Add(vertexIndicesCancerMesh.IndexOf(vertexIndex1));
                    cancerTriangles.Add(vertexIndicesCancerMesh.IndexOf(vertexIndex2));
                }
                else if (vertexIndicesCervixMesh.Contains(vertexIndex0) && vertexIndicesCervixMesh.Contains(vertexIndex1) && vertexIndicesCervixMesh.Contains(vertexIndex2))
                {
                    cervixTriangles.Add(vertexIndicesCervixMesh.IndexOf(vertexIndex0));
                    cervixTriangles.Add(vertexIndicesCervixMesh.IndexOf(vertexIndex1));
                    cervixTriangles.Add(vertexIndicesCervixMesh.IndexOf(vertexIndex2));
                }
            }
        }
        // Triangulation of new triangles
        
        CircleTriangleCreationUV
            (
                ref edgeTriangleVertexIndicesPairList,
                ref uvs, 
                triangles, 
                ref cancerUVs, 
                cancerTriangles, 
                cervixUVs, 
                cervixTriangles,
                ref sortedEdges,
                vertices
            );
            

        // Set components
        Mesh cancerMesh = cancerCellsObject.GetComponent<MeshFilter>().mesh;
        Mesh cervixMesh = cervixObject.GetComponent<MeshFilter>().mesh;
        
        cancerMesh.vertices = cancerVertices.ToArray();
        cancerMesh.normals = cancerNormals.ToArray();
        cancerMesh.uv = cancerUVs.ToArray();
        cancerMesh.triangles = cancerTriangles.ToArray();
        
        cervixMesh.vertices = cervixVertices.ToArray();
        cervixMesh.normals = cervixNormals.ToArray();
        cervixMesh.uv = cervixUVs.ToArray();
        cervixMesh.triangles = cervixTriangles.ToArray();
        
        // Update the mesh
        cancerMesh.RecalculateNormals();
        cancerMesh.RecalculateBounds();
        
        cervixMesh.RecalculateNormals();
        cervixMesh.RecalculateBounds();

        Transform cervixTransform = electroSnareVR.cervixTransform;
        
        Vector3 pos = cervixTransform.position;
        Quaternion qua = cervixTransform.rotation;
        Vector3 scale = cervixTransform.localScale;

        cancerCellsObject.transform.position = pos;
        cancerCellsObject.transform.rotation = qua;
        cancerCellsObject.transform.localScale = scale;

        cervixObject.transform.position = pos;
        cervixObject.transform.rotation = qua;
        cervixObject.transform.localScale = scale;
        
        
       // Load the material from the specified path
        Material existingMaterial = Resources.Load<Material>("Preset_drawn_Trible");
        MeshRenderer cmR = cancerCellsObject.GetComponent<MeshRenderer>();
        cmR.material = new Material(existingMaterial);
        
        // set new material 
        MeshRenderer cxR = cervixObject.AddComponent<MeshRenderer>();
        cxR.material = new Material(existingMaterial);
    }
    
    
    void CircleTriangleCreationUV
    (
        ref List<(int, int)> edgeTriangleVertexIndicesPairList,
        ref List<Vector2> uvs,
        List<int> triangles,
        ref List<Vector2> cancerUVs, 
        List<int> cancerTriangles, 
        List<Vector2> cervixUVs, 
        List<int> cervixTriangles,
        ref List<List<int>> sortedEdges,
        List<Vector3> vertices
    )
    {
        //DebugVertices(vertices, Color.yellow );

        // Group the items by triangleIndex
        var groupedPairs = edgeTriangleVertexIndicesPairList.GroupBy(pair => pair.Item1);

        // iterate through every triangle index in edgeTriangleVertexIndicesPairList (combination of new and destroyed triangles)
        foreach (var group in groupedPairs)
        {
            int triangleIndex = group.Key; // This is the common triangleIndex shared by the pairs in this group.
            
            // get all vertices for this deleted triangle
            // first, the original ones
            List<int> cancerPolygonIndices = new List<int>();
            List<int> cancerPolygonEdgeIndices = new List<int>();
            
            List<int> origCancerPolygonIndices = new List<int>();
            
            List<int> cervixPolygonIndices = new List<int>();
            List<int> cervixPolygonEdgeIndices = new List<int>();
            
            List<int> origCervixPolygonIndices = new List<int>();

            // get original vertices from the triangle index
            int startIndex = triangleIndex * 3; // Each triangle uses 3 consecutive vertices
            
            int vertexIndex0 = triangles[startIndex];
            int vertexIndex1 = triangles[startIndex + 1];
            int vertexIndex2 = triangles[startIndex + 2];

            Vector2 v0 = uvs[vertexIndex0];
            Vector2 v1 = uvs[vertexIndex1];
            Vector2 v2 = uvs[vertexIndex2];
            
            // check if original three vertices are inside the cancer or cervix mesh
            if (cancerUVs.Contains(v0))
            {
                cancerPolygonIndices.Add(vertexIndex0);
                origCancerPolygonIndices.Add(vertexIndex0);
            }
            
            if (cervixUVs.Contains(v0))
            {
                cervixPolygonIndices.Add(vertexIndex0);
                origCervixPolygonIndices.Add(vertexIndex0);
            }
            
            if (cancerUVs.Contains(v1))
            {
                cancerPolygonIndices.Add(vertexIndex1);
                origCancerPolygonIndices.Add(vertexIndex1);
            }
            
            if(cervixUVs.Contains(v1))
            {
                cervixPolygonIndices.Add(vertexIndex1);
                origCervixPolygonIndices.Add(vertexIndex1);
            }
            
            if (cancerUVs.Contains(v2))
            {
                cancerPolygonIndices.Add(vertexIndex2);
                origCancerPolygonIndices.Add(vertexIndex2);
            }
            
            if(cervixUVs.Contains(v2))
            {
                cervixPolygonIndices.Add(vertexIndex2);
                origCervixPolygonIndices.Add(vertexIndex2);
            }
            
            // now get the additional vertices for this triangle
            foreach (var pair in group)
            {
                // first, take original vertexIndex and take original vertex
                int vertexIndex = pair.Item2; // This is the vertexIndex from the pair.
                
                Vector2 uv = uvs[vertexIndex];

                // now, search for the "new" vertexIndex in the two seperated meshes
                if (cancerUVs.Contains(uv))
                {
                    cancerPolygonIndices.Add(vertexIndex);
                    cancerPolygonEdgeIndices.Add(vertexIndex);
                }
                if (cervixUVs.Contains(uv))
                {
                    cervixPolygonIndices.Add(vertexIndex);
                    cervixPolygonEdgeIndices.Add(vertexIndex);
                    
                }
            }


            if (cancerPolygonIndices.Count >= 3)// && origCancerPolygonIndices.Count < 3)
            {
                List<int> completePolygon = SortPolygonIndices(ref origCancerPolygonIndices, ref cancerPolygonEdgeIndices, sortedEdges,uvs);

                /*
                CreateSingleDelaunayTriangleUV(
                    true,
                    ref completePolygon,
                    ref origCancerPolygonIndices,
                    ref cancerPolygonEdgeIndices, 
                    ref vertices , 
                    ref uvs,
                    ref cancerUVs, 
                    counter, 
                    out List<int> cancerTrianglesToAdd, 
                    ref hullVerticesHashSet, 
                    ref intersectionVerticesHashSet);
                */

                ListWrapper<int> listWrapper = new ListWrapper<int>();
                listWrapper.value = new List<int>();
                
                
                StartCoroutine(CreateSingleDelaunayTriangleUVCoroutine(true,
                    completePolygon,
                    origCancerPolygonIndices,
                    cancerPolygonEdgeIndices,
                    uvs,
                    cancerUVs,
                    listWrapper,
                    vertices
                    )
                        );
                
                cancerTriangles.AddRange(listWrapper.value);
            }

            
            
            if (cervixPolygonIndices.Count >= 3) //&& origCervixPolygonIndices.Count < 3)
            {
                List<int> completePolygon = SortPolygonIndices(ref origCervixPolygonIndices, ref cervixPolygonEdgeIndices, sortedEdges,uvs);
                
                /*
                CreateSingleDelaunayTriangleUV(
                    false,
                    ref completePolygon,
                    ref origCervixPolygonIndices,
                    ref cervixPolygonEdgeIndices, 
                    ref vertices , 
                    ref uvs,
                    ref cervixUVs, 
                    counter, 
                    out List<int> cervixTrianglesToAdd,
                    ref hullVerticesHashSet, 
                    ref intersectionVerticesHashSet);
                    */
            
                //cervixTriangles.AddRange(cervixTrianglesToAdd);
               
                ListWrapper<int> listWrapper = new ListWrapper<int>();
                listWrapper.value = new List<int>();
                
                
                StartCoroutine(CreateSingleDelaunayTriangleUVCoroutine(false,
                    completePolygon,
                    origCervixPolygonIndices,
                    cervixPolygonEdgeIndices,
                    uvs,
                    cervixUVs,
                    listWrapper,
                    vertices
                    )
                );
                
                cervixTriangles.AddRange(listWrapper.value);
            }
            
        }
    }
    
    
    private IEnumerator  CreateSingleDelaunayTriangleUVCoroutine
    (
        bool isCancer,
        List<int> polygonIndices,
        List<int> origCancerPolygonIndices,
        List<int> polygonEdgeIndices,
        List<Vector2> uvs,
        List<Vector2> seperatedMeshUvs,
        ListWrapper<int> listWrapper,
        List<Vector3> vertices)
    {
        if (!isCancer)
        {
            polygonIndices.Reverse();
            polygonEdgeIndices.Reverse();
        }

        List<int> trianglesToAdd = new List<int>();

        // create the polygon and the edge
        List<Vector2> polygonUVs = new List<Vector2>();
        foreach (var index in polygonIndices)
        {
            polygonUVs.Add(uvs[index]);
        }

        List<Vector2> polygonEdgeUVs = new List<Vector2>();
        foreach (var index in polygonEdgeIndices)
        {
            polygonEdgeUVs.Add(uvs[index]);
        }

        

        // Delaunay
        List<Triangle2D> tmOutputTriangles = new List<Triangle2D>();
        DelaunayTriangulation tmTriangulation = new DelaunayTriangulation();

        //bool intersecting = IsIntersecting(polygonEdgeUVs);polygonUVs
        bool intersecting = IsIntersecting(polygonUVs);
        
        trianglesToAdd = new List<int>();
        
        if (origCancerPolygonIndices.Count == 3)
        {
            
            bool t = IsThreePointCase(polygonEdgeIndices, origCancerPolygonIndices, uvs);
            
            if (t)
            {
                HandleThreePointCase(polygonEdgeIndices, origCancerPolygonIndices, uvs, out List<List<Vector2>> constraintsList);

                List<List<Vector2>> convConstraintsList = new List<List<Vector2>>();

                foreach (var conL in constraintsList)
                {
                    List<Vector2> convList = new List<Vector2>();
                    foreach (var v in conL)
                    {
                        Vector2 textureSize = new Vector2(2048f, 2048f);

                        Vector2 textureCoordinate = v * textureSize;
                        convList.Add(textureCoordinate);
                    }
                    convConstraintsList.Add(convList);
                }

                tmTriangulation.Triangulate(polygonUVs);
                //tmTriangulation.Triangulate(polygonUVs, 0.0f, convConstraintsList);
                tmTriangulation.GetTrianglesDiscardingHoles(tmOutputTriangles);
            }
            else
            {
                if (!IsConvex(polygonUVs) && !intersecting)
                {
            
                    List<Vector2> sortedPolygonCopy = new List<Vector2>(polygonUVs);
                    sortedPolygonCopy.Sort(CompareByYCoordinate);
            
                    List<Vector2> h = GiftwrappingAlgorithm(sortedPolygonCopy, sortedPolygonCopy.Count);
            
                    ExpandHull(ref h, polygonEdgeUVs);
                    
                    CompareHullsV2(h, polygonEdgeUVs, out List<List<Vector2>> constraints);

                    tmTriangulation.Triangulate(polygonUVs);
                    //tmTriangulation.Triangulate(polygonUVs, 0.0f, constraints);
                    tmTriangulation.GetTrianglesDiscardingHoles(tmOutputTriangles);
                }
                else
                {
                    tmTriangulation.Triangulate(polygonUVs);
                    tmTriangulation.GetTrianglesDiscardingHoles(tmOutputTriangles);
                }
            }
            
        }
        else if (!IsConvex(polygonUVs) && !intersecting)
        {
            // create the polygon and the edge
            /*
            List<Vector3> t = new List<Vector3>();
            foreach (var index in polygonIndices)
            {
                t.Add(vertices[index]);
            }
            DebugVertices(t, Color.magenta);
            */
            
            // create the polygon and the edge
            /*
            List<Vector3> s = new List<Vector3>();
            foreach (var index in polygonEdgeIndices)
            {
                s.Add(vertices[index]);
            }
            DebugVertices(s, Color.green);
            */
            
            List<Vector2> sortedPolygonCopy = new List<Vector2>(polygonUVs);
            sortedPolygonCopy.Sort(CompareByYCoordinate);
            
            List<Vector2> h = GiftwrappingAlgorithm(sortedPolygonCopy, sortedPolygonCopy.Count);
            
            ExpandHull(ref h, polygonEdgeUVs);
            
            CompareHullsV2(h, polygonEdgeUVs, out List<List<Vector2>> constraints);

            tmTriangulation.Triangulate(polygonUVs);
            //tmTriangulation.Triangulate(polygonUVs, 0.0f, constraints);
            tmTriangulation.GetTrianglesDiscardingHoles(tmOutputTriangles);
        }
        else
        {
            tmTriangulation.Triangulate(polygonUVs);
            tmTriangulation.GetTrianglesDiscardingHoles(tmOutputTriangles);
        }
        
        Mesh mesh = CreateMeshFromTriangles(tmOutputTriangles);
        
        List<Vector3> delaunayVertices= mesh.vertices.ToList();
        List<int> delaunayIndices = mesh.triangles.ToList();
        
        // reduce duplicates and get unique lists
        ConvertDelaunayResult(delaunayIndices, delaunayVertices, out List<Vector3> convertedVertices, out List<int> convertedIndices);
       
        // remap indices to input. Now, projectedIndices has the indices mapped to the unique UV input.
        MapDelaunayIndicesV2(convertedVertices, convertedIndices,
            polygonUVs, out List<int> uniqueIndices);
            
        //MapDelaunayIndices(convertedVertices, convertedIndices,
        //polygonUVs, out List<int> uniqueIndices);
        
        // last step: map unique UV input to original indices, access the corresponding vertices
        trianglesToAdd = new List<int>();
        
        // iterate over indices
        foreach (var uniqueIndex in uniqueIndices)
        {
            if (uniqueIndex != -1)
            {
                Vector2 uv = polygonUVs[uniqueIndex];

                trianglesToAdd.Add(seperatedMeshUvs.IndexOf(uv));
            }
        }

        listWrapper.value = trianglesToAdd;
        
        yield return null;
    }
    

    /// <summary>
    /// 
    /// </summary>
    /// <param name="delaunayIndices"></param>
    /// <param name="delaunayVertices"></param>
    /// <param name="convertedVertices"></param>
    /// <param name="convertedIndices"></param>
    private void ConvertDelaunayResult(List<int> delaunayIndices, List<Vector3> delaunayVertices, out List<Vector3> convertedVertices, out List<int> convertedIndices)
    {
        Dictionary<int, int> indexDictionary = new Dictionary<int, int>();

        convertedVertices = new List<Vector3>();

        int i = 0;
        foreach (var delaunayVertex in delaunayVertices)
        {
            if (!convertedVertices.Contains(delaunayVertex))
            {
                convertedVertices.Add(delaunayVertex);
                indexDictionary.Add(i, convertedVertices.Count -1);
            }
            else
                indexDictionary.Add(i, convertedVertices.IndexOf(delaunayVertex));
            
            i++;
        }
        
        // newVertexList now contains only unique vertices (should be the same as the original)
        // indexDirectory maps the delaunay Vertices indices to the new newVertexList indices
        // create a new indices list with the help of the indexDirectory

        convertedIndices = new List<int>();

        foreach (var delaunayIndex in delaunayIndices)
        {
            if (indexDictionary.TryGetValue(delaunayIndex, out var value))
            {
                convertedIndices.Add(value);
            }
        }
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="convertedVertices"></param>
    /// <param name="convertedIndices"></param>
    /// <param name="projectedVertices"></param>
    /// <param name="projectedIndices"></param>
    void MapDelaunayIndicesV2(List<Vector3> convertedVertices, List<int> convertedIndices, List<Vector2> projectedVertices, out List<int> projectedIndices)
    {
        projectedIndices = new List<int>();

        // Create a dictionary to map 2D representations of vertices to their corresponding indices in projectedVertices
        Dictionary<Vector2, int> vertexIndexMapping = new Dictionary<Vector2, int>();

        for (int i = 0; i < projectedVertices.Count; i++)
        {
            // Use the 2D representation (ignoring the Z coordinate) as the key
            vertexIndexMapping[projectedVertices[i]] = i;
        }

        foreach (var index in convertedIndices)
        {
            // Use the 2D representation of the converted vertex to find the corresponding index in projectedVertices
            Vector2 v = new Vector2(convertedVertices[index].x, convertedVertices[index].y);
            
            //Vector2 textureSize = new Vector2(2048f, 2048f);
            //Vector2 u = v / textureSize;
            
            if (vertexIndexMapping.TryGetValue(v, out int projectedIndex))
            {
                projectedIndices.Add(projectedIndex);
            }
            else
            {
                // Handle the case where the mapping is not found (e.g., vertex is missing in projectedVertices)
                projectedIndices.Add(-1);
            }
        }
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="convertedVertices"></param>
    /// <param name="convertedIndices"></param>
    /// <param name="projectedVertices"></param>
    /// <param name="projectedIndices"></param>
    void MapCutoutDelaunayIndices(List<Vector3> convertedVertices, List<int> convertedIndices, List<Vector2> projectedVertices, out List<int> projectedIndices)
    {
        projectedIndices = new List<int>();

        // Create a dictionary to map 2D representations of vertices to their corresponding indices in projectedVertices
        Dictionary<Vector2, int> vertexIndexMapping = new Dictionary<Vector2, int>();

        for (int i = 0; i < projectedVertices.Count; i++)
        {
            // Use the 2D representation (ignoring the Z coordinate) as the key
            vertexIndexMapping[projectedVertices[i]] = i;
        }

        foreach (var index in convertedIndices)
        {
            // Use the 2D representation of the converted vertex to find the corresponding index in projectedVertices
            Vector2 v = new Vector2(convertedVertices[index].x, convertedVertices[index].y);

            if (vertexIndexMapping.TryGetValue(v, out int projectedIndex))
            {
                projectedIndices.Add(projectedIndex);
            }
            else
            {
                // Handle the case where the mapping is not found (e.g., vertex is missing in projectedVertices)
                projectedIndices.Add(-1);
            }
        }
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="triangles"></param>
    /// <returns></returns>
    private Mesh CreateMeshFromTriangles(List<Triangle2D> triangles)
    {
        List<Vector3> vertices = new List<Vector3>(triangles.Count * 3);
        List<int> indices = new List<int>(triangles.Count * 3);

        for (int i = 0; i < triangles.Count; ++i)
        {
            vertices.Add(triangles[i].p0);
            vertices.Add(triangles[i].p1);
            vertices.Add(triangles[i].p2);
            indices.Add(i * 3 + 2); // Changes order
            indices.Add(i * 3 + 1);
            indices.Add(i * 3);
        }

        Mesh mesh = new Mesh();
        mesh.subMeshCount = 1;
        mesh.SetVertices(vertices);
        mesh.SetIndices(indices, MeshTopology.Triangles, 0);
        return mesh;
    }

    private List<CutoutSegment> CreateSortedSegments()
    {
        int counter = cutAccuracy;
        List<CutoutSegment> adaptedSegments = new List<CutoutSegment>();

        for (int i = 0; i < cutoutSegments.Count - (1 + counter); i += counter)
        {
            adaptedSegments.Add(cutoutSegments[i]);
        }
        
        adaptedSegments.Add(cutoutSegments[cutoutSegments.Count -1]);

        return adaptedSegments;
    }
    
    
    void GenerateCutout(Vector3 middlePoint, List<CutoutSegment> cutoutSegmentsInput)
    {
        List<Vector3> vertices = new List<Vector3>();
        List<Vector3> normals = new List<Vector3>();
        List<Vector2> uvs = new List<Vector2>();
        List<int> triangles = new List<int>();
        
  
        // to generate the cutout, take the first and last element and iterate all CutAccuracy
        // number of segments.
        // build triangle segment with segment+1
        
        // create normals and uvs
        int currentSegment = 0;

       
        float uSegmentationStep = 1.0f / cutoutSegmentsInput.Count;


        List<UVCutoutSegment> uvSegments = new List<UVCutoutSegment>();
        

        foreach (var segment in cutoutSegmentsInput)
        {
            UVCutoutSegment uvS = new UVCutoutSegment();
            uvS.LeftSegmentedVertices = new List<Vector2>();
            uvS.RightSegmentedVertices = new List<Vector2>();

            // add vertices
            float vertexStepSize = 1.0f / (segment.LeftSegmentedVertices.Count + segment.RightSegmentedVertices.Count);

            // uvs are calculated the following way: UV coordinates range from 0.0 to 1.1.
            // every segment is a vertical "strip" along the uv space. 
            // divide the x coordinate by the number of hullSegments. every uv of the same segment has the 
            // same x coordiante. 
            // divide the y coordinate by the number of vertices in the segment. 
            // Calculate UVs
            
            int currentLeftVertex = 0;
            foreach (var vertex in segment.LeftSegmentedVertices )
            {
                vertices.Add(vertex);
               
               // normal is normalized vector from vertex to middle point
               Vector3 normal = (vertex - middlePoint).normalized;
               normals.Add(normal);
               
               // u = stepsize * currentSegment
               float u = uSegmentationStep * currentSegment;
               
               // v = vertexStepSize * currentVertex
               float v = vertexStepSize * currentLeftVertex;

               Vector2 newUV = new Vector2(u, v);

               uvs.Add(newUV);
               
               uvS.LeftSegmentedVertices.Add(newUV);
               
               currentLeftVertex++;
            }
            
            // start from highest to lowest
            int currentRightVertex = currentLeftVertex + segment.RightSegmentedVertices.Count;
            foreach (var vertex in segment.RightSegmentedVertices )
            {
                vertices.Add(vertex);
               
                // normal is normalized vector from vertex to middle point
                Vector3 normal = (vertex - middlePoint).normalized;
                normals.Add(normal);
               
                // uvs are calculated the following way: UV coordinates range from 0.0 to 1.1.
                // every segment is a vertical "strip" along the uv space. 
                // divide the x coordinate by the number of hullSegments. every uv of the same segment has the 
                // same x coordinate. 
                // divide the y coordinate by the number of vertices in the segment. 
                // Calculate UVs

                // u = stepsize * currentSegment
                float u = uSegmentationStep * currentSegment;
               
                // v = vertexStepSize * currentVertex
                float v = vertexStepSize * currentRightVertex;
               
                Vector2 newUV = new Vector2(u, v);
               
                uvs.Add(newUV);

                uvS.RightSegmentedVertices.Add(newUV);
                
                currentRightVertex--;
            }

           currentSegment++;
           uvSegments.Add(uvS);
        }
        
        
        for (int i = 0; i < cutoutSegmentsInput.Count - 1; i++)
        {
            CutoutSegment current = cutoutSegmentsInput[i];
            CutoutSegment next = cutoutSegmentsInput[i+1];

            // find all uvs 
            List<Vector2> leftUVList = new List<Vector2>();
            List<Vector2> rightUVList = new List<Vector2>();

            
            foreach (var vertex in current.LeftSegmentedVertices)
            {
                Vector2 uv = uvs[vertices.IndexOf(vertex)];
                leftUVList.Add(uv);
            }
            
            foreach (var vertex in next.LeftSegmentedVertices)
            {
                Vector2 uv = uvs[vertices.IndexOf(vertex)];
                leftUVList.Add(uv);
            }
            
            CreateSingleDelaunayTriangleUVCutout(uvs, leftUVList, out List<int> leftTrianglesToAdd);
            
            if(IsMultipleOfThree(leftTrianglesToAdd.Count))
                triangles.AddRange(leftTrianglesToAdd);
            
            
            foreach (var vertex in current.RightSegmentedVertices)
            {
                Vector2 uv = uvs[vertices.IndexOf(vertex)];
                rightUVList.Add(uv);
            }
            
            foreach (var vertex in next.RightSegmentedVertices)
            {
                Vector2 uv = uvs[vertices.IndexOf(vertex)];
                rightUVList.Add(uv);
            }

            CreateSingleDelaunayTriangleUVCutout(uvs, rightUVList, out List<int> rightTrianglesToAdd);
            
            if(IsMultipleOfThree(rightTrianglesToAdd.Count))
                triangles.AddRange(rightTrianglesToAdd);
        }

        // create triangles for the first and last segment to "close" the hull
        
        CreateSideHullTrianglesV2(uvSegments, uvs, out List<int> firstTrianglesToAdd);
        triangles.AddRange(firstTrianglesToAdd);
    
        
        // create two gameobjects
        GameObject cancerCutout = new GameObject("cancerCutout");
        MeshFilter cancerCutoutMeshFilter = cancerCutout.AddComponent<MeshFilter>();
        cancerCutoutMeshFilter.mesh = new Mesh();

        cancerCutoutMeshFilter.mesh.SetVertices(vertices);
        cancerCutoutMeshFilter.mesh.SetNormals(normals);
        cancerCutoutMeshFilter.mesh.uv = uvs.ToArray();
        cancerCutoutMeshFilter.mesh.triangles = triangles.ToArray();
        
        cancerCutoutMeshFilter.mesh.RecalculateNormals();
        
        
        // make triangles cw of cervixCutout
        List<int> cervixTriangles = ConvertToCwOrder(triangles);
        
        GameObject cervixCutout = new GameObject("cervixCutout");
        MeshFilter cervixCutoutMeshFilter = cervixCutout.AddComponent<MeshFilter>();
        cervixCutoutMeshFilter.mesh = new Mesh();
        
        // normals for the cancerMesh must be reversed
        List<Vector3> cutoutMeshCervixNormals = new List<Vector3>();
        foreach (var normal in normals)
        {
            cutoutMeshCervixNormals.Add(normal * (-1));
        }
        
        cervixCutoutMeshFilter.mesh.SetVertices(vertices);
        cervixCutoutMeshFilter.mesh.SetNormals(cutoutMeshCervixNormals);
        cervixCutoutMeshFilter.mesh.uv = uvs.ToArray();
        cervixCutoutMeshFilter.mesh.triangles = cervixTriangles.ToArray();
        
        cervixCutoutMeshFilter.mesh.RecalculateNormals();
        
        
        
        // set new material
        // Load the material from the specified path
        Material existingMaterial = Resources.Load<Material>("CutoutMaterial");
        
        MeshRenderer mrCa = cancerCutout.AddComponent<MeshRenderer>();
        Material m = existingMaterial; //new Material(Shader.Find("Standard"));
        //m.color = Color.grey;
        mrCa.material = m;//existingMaterial; //m;
        
        MeshRenderer mrCe = cervixCutout.AddComponent<MeshRenderer>();
        mrCe.material = m;//existingMaterial; //m;
        
        // set as child
        cancerCutout.transform.SetParent(cancerCellsObject.transform);
        cervixCutout.transform.SetParent(cervixObject.transform);

        if (OnCutoutGenerated != null)
            OnCutoutGenerated(cancerCellsObject);

        if (OnVictory != null)
            OnVictory(GameManager.GameState.Victory);
    }
    
    
    
    private void CreateSideHullTrianglesV2(List<UVCutoutSegment> uvSegments, List<Vector2> uvs,
        out List<int> firstTrianglesToAdd)
    {
        
        firstTrianglesToAdd = new List<int>();
        List<Vector2> leftToTriangulate = new List<Vector2>();
        
        // for the "front" of the cutout
        foreach (var segment in uvSegments)
        {
            // Check if there are at least 3 UV points forming a triangle with an area larger than 0
            if (!CouldFormTriangle(leftToTriangulate))
            {
                // add to vertices to Triangulate
                foreach (var vertex in segment.LeftSegmentedVertices)
                {
                    leftToTriangulate.Add(vertex);
                }
                foreach (var vertex in segment.RightSegmentedVertices)
                {
                    leftToTriangulate.Add(vertex);
                }
            }
            else
            {
                break;
            }
        }
        
        // triangulate over uvs
        CreateSingleDelaunayTriangleUVCutoutV2(leftToTriangulate, uvs, out List<int> leftTrianglesToAdd);
        List<int> tl = new List<int>(leftTrianglesToAdd);
        
        firstTrianglesToAdd.AddRange(tl);
        tl.Reverse();
        firstTrianglesToAdd.AddRange(tl);

        // for the "back" of the hull
        List<Vector2> rightToTriangulate = new List<Vector2>();
        
        for (int i = uvSegments.Count-1; i >= 0; i--)
        {
            var segment = uvSegments[i];
            
            // Check if there are at least 3 UV points forming a triangle with an area larger than 0
            if (!CouldFormTriangle(rightToTriangulate))
            {
                // add to vertices to Triangulate
                foreach (var vertex in segment.LeftSegmentedVertices)
                {
                    rightToTriangulate.Add(vertex);
                }
                foreach (var vertex in segment.RightSegmentedVertices)
                {
                    rightToTriangulate.Add(vertex);
                }
            }
            else
            {
                break;
            }
        }

        // triangulate over uvs
        CreateSingleDelaunayTriangleUVCutoutV2(rightToTriangulate, uvs, out List<int> rightTrianglesToAdd);
        List<int> tr = new List<int>(rightTrianglesToAdd);
        
        firstTrianglesToAdd.AddRange(tr);
        tr.Reverse();
        firstTrianglesToAdd.AddRange(tr);
        
    }
    

    /// <summary>
    /// Sort Polygon list
    /// </summary>
    /// <param name="polygonIndices"></param>
    /// <param name="polygonEdges"></param>
    /// <param name="sortedEdges"></param>
    /// <param name="uvs"></param>
    private List<int> SortPolygonIndices(ref List<int> polygonIndices, ref List<int> polygonEdges, List<List<int>> sortedEdges, List<Vector2> uvs)
    {
        // Check if there is at least one value in sortedEdges[0] and another in sortedEdges[^1]
        bool needsSpecialSorting = polygonEdges.Any(index => sortedEdges[0].Contains(index)) && 
                                   polygonEdges.Any(index => sortedEdges[^1].Contains(index));
        
        if ( !needsSpecialSorting)
        {
            polygonEdges.Sort();
        }
        else
        {
            List<int> sE = new List<int>(sortedEdges[^1]);
            sE.RemoveAt(sE.Count-1);
            sE.AddRange(sortedEdges[0]);

            // Filter out indices not present in PolygonIndices
            sE = sE.Where(polygonEdges.Contains).ToList();

            // Sort PolygonIndices in the order they appear in the combinedStartEnd list
            polygonEdges.Sort((a, b) =>
            {
                int indexA = sE.IndexOf(a);
                int indexB = sE.IndexOf(b);
                return indexA.CompareTo(indexB);
            });
        }
        
        // Sort Original dependent on edge
        if (polygonIndices.Count > 1)
        {
            Vector2 firstVector = uvs[polygonEdges[0]];
            Vector2 lastVector = uvs[polygonEdges[^1]];
                
            // direction vector
            Vector2 directionVector = lastVector - firstVector;
            directionVector.Normalize();
                
            // sort uvs of PolygonOriginalIndices along direction vector
            polygonIndices.Sort((index1, index2) =>
            {
                Vector2 uv1 = uvs[index1];
                Vector2 uv2 = uvs[index2];

                // Compare projections onto the direction vector
                float dotProduct1 = Vector2.Dot(uv1 - firstVector, directionVector);
                float dotProduct2 = Vector2.Dot(uv2 - firstVector, directionVector);

                return dotProduct1.CompareTo(dotProduct2);
            });
                
            // reverse ordering
            polygonIndices.Reverse();

            List<int> completePolygon = new List<int>(polygonIndices);
            completePolygon.AddRange(polygonEdges);

            return completePolygon;
        }
        else
        {
            List<int> completePolygon = new List<int>(polygonIndices);
            completePolygon.AddRange(polygonEdges);

            return completePolygon;
        }
    }
    
    
    private void CreateSingleDelaunayTriangleUVCutoutV2(List<Vector2> polygonUVs, List<Vector2> allUVs,
        out List<int> trianglesToAdd)
    {
        // sort
        List<Vector2> sortedUVs = SortCCW(polygonUVs);

        List<Triangle2D> tmOutputTriangles = new List<Triangle2D>();
        DelaunayTriangulation tmTriangulation = new DelaunayTriangulation();
        
        tmTriangulation.Triangulate(sortedUVs);
        tmTriangulation.GetTrianglesDiscardingHoles(tmOutputTriangles);
        
        Mesh mesh = CreateMeshFromTriangles(tmOutputTriangles);
        
        List<Vector3> delaunayVertices= mesh.vertices.ToList();
        List<int> delaunayIndices = mesh.triangles.ToList();

        // reduce duplicates and get unique lists
        ConvertDelaunayResult(delaunayIndices, delaunayVertices, out List<Vector3> convertedVertices, out List<int> convertedIndices);
        
        // remap indices to input. Now, projectedIndices has the indices mapped to the unique UV input.
        MapCutoutDelaunayIndices(convertedVertices, convertedIndices,
            sortedUVs, out List<int> uniqueIndices);
        
        // last step: map unique UV input to original indices, access the corresponding vertices
        trianglesToAdd = new List<int>();
        
        // iterate over indices
        foreach (var uniqueIndex in uniqueIndices)
        {
            Vector2 uv = polygonUVs[uniqueIndex];

            int index = allUVs.IndexOf(uv);
            if(index != -1)
                trianglesToAdd.Add(allUVs.IndexOf(uv));
        }
    }
    
    
    private void CreateSingleDelaunayTriangleUVCutout(List<Vector2> allUVs,List<Vector2> origUVs, out List<int> trianglesToAdd)
    {
        // filter out duplicates of origUVs, only triangulate with uniques
        List<Vector2> uniqueUVs = new List<Vector2>();
        HashSet<Vector2> uvSet = new HashSet<Vector2>(uniqueUVs, new Vector2EqualityComparer());

        trianglesToAdd = new List<int>();
        
        foreach (var uv in origUVs)
        {
            // Check if the vertex is already in the vertices
            if (!IsVertexInHashSet(uv, uvSet))
            {
                uniqueUVs.Add(uv);
                uvSet.Add(uv);
            }
        } 
        
        // throw error if uniques are less then three
        if (uniqueUVs.Count >= 3)
        {
            // sort vertices
            List<Vector2> sortedUVs = SortCCW(uniqueUVs);

            List<Triangle2D> tmOutputTriangles = new List<Triangle2D>();
            DelaunayTriangulation tmTriangulation = new DelaunayTriangulation();
        
            tmTriangulation.Triangulate(sortedUVs);
            tmTriangulation.GetTrianglesDiscardingHoles(tmOutputTriangles);
        
            Mesh mesh = CreateMeshFromTriangles(tmOutputTriangles);
        
            List<Vector3> delaunayVertices= mesh.vertices.ToList();
            List<int> delaunayIndices = mesh.triangles.ToList();

            // reduce duplicates and get unique lists
            ConvertDelaunayResult(delaunayIndices, delaunayVertices, out List<Vector3> convertedVertices, out List<int> convertedIndices);
        
            
            foreach (var index in convertedIndices)
            {
                Vector3 v = convertedVertices[index];
                Vector2 v2 = new Vector2(v.x, v.y);
            
                float tolerance = 0.001f;
                int closestIndex = FindClosestUVIndex(v2, origUVs, tolerance);
            
                trianglesToAdd.Add(closestIndex);
            }
        
        
            // not index of instead use approx
            foreach (var index in convertedIndices)
            {
                Vector3 v3 = convertedVertices[index];
                Vector2 v = new Vector2(v3.x, v3.y);

                //trianglesToAdd.Add(allUVs.IndexOf(v));
                float tolerance = 0.001f;
            
                // Find the closest UV coordinate in allUVs based on proximity
                int closestIndex = FindClosestUVIndex(v, allUVs, tolerance);

                trianglesToAdd.Add(closestIndex);
            }
        }
        else
        {
            Debug.LogWarning("Less then 3 unique UV coordinates");
        }
    }
    
    // Function to find the index of the closest UV coordinate within a tolerance
    private int FindClosestUVIndex(Vector2 targetUV, List<Vector2> uvList, float tolerance)
    {
        for (int i = 0; i < uvList.Count; i++)
        {
            if (Vector2.Distance(targetUV, uvList[i]) < tolerance)
            {
                return i;
            }
        }
        return -1; // If no close match is found within the tolerance
    }
    
    

    bool IsIntersecting(List<Vector2> polygon)
    {
        List<(int, int)> vertexPairs = new List<(int, int)>();

        // Iterate over pairs of vertices and store their indices
        
        for (int i = 0; i < polygon.Count; i++)
        {
            int nextIndex = (i + 1) % polygon.Count; // Circular iteration
            vertexPairs.Add((i, nextIndex));
        }
     
        // Iterate over the stored edge pairs
        foreach (var edgePair1 in vertexPairs)
        {
            int indexA1 = edgePair1.Item1;
            int indexB1 = edgePair1.Item2;
   
            Vector2 startA = polygon[indexA1];
            Vector2 endA = polygon[indexB1];
   
            foreach (var edgePair2 in vertexPairs)
            {
                int indexA2 = edgePair2.Item1;
                int indexB2 = edgePair2.Item2;
   
                Vector2 startB = polygon[indexA2];
                Vector2 endB = polygon[indexB2];
   
                // Check for intersection
               
                Vector2 intersection = CalcIntersectionPoint(startA, endA, startB, endB);

                if (intersection != Vector2.zero)
                {
                    
                    if (((Vector2.Distance(endA, intersection) < 0.00001 ) &&
                         (Vector2.Distance(startB, intersection) < 0.00001 )))
                        return false;
                    
                        return true;
                }
            }
        }
   
        // No intersections found
        return false;
    }
    
/// <summary>
   /// Checks if the polygon has three orig vertices and is cut through the middle
   /// </summary>
   /// <param name="edgeIndices"></param>
   /// <param name="origIndices"></param>
   /// <param name="uvs"></param>
   /// <returns></returns>
   private bool IsThreePointCase(List<int> edgeIndices, List<int> origIndices, List<Vector2> uvs)
   {
       List<(int, int)> edges = new List<(int, int)>();
       
       for (int i = 0; i < edgeIndices.Count - 1; i++)
       {
           edges.Add( new(edgeIndices[i], edgeIndices[i+1]));
       }
       
       List<Vector2> originalsToSort = new List<Vector2>();
       originalsToSort.Add(uvs[origIndices[0]]);
       originalsToSort.Add(uvs[origIndices[1]]);
       originalsToSort.Add(uvs[origIndices[2]]);
       SortCCW(originalsToSort);

       List<(int, int)> triangleEdges = new List<(int, int)>();
       for (int i = 0; i < origIndices.Count - 1; i++)
       {
           triangleEdges.Add( new(origIndices[i], origIndices[i+1]));
       }

       // Create a set to store unique triangleEdges
       HashSet<int> uniqueTriangleEdges = new HashSet<int>();

       // iterate over all edge lines
       foreach (var edgeSide in edges)
       {
           // sample all intersectingPoints of the current edge
           int triangleEdgeCounter = 0;
           foreach (var triangleEdge in triangleEdges)
           {
               Vector2 intersection = CalcIntersectionPoint(uvs[triangleEdge.Item1], uvs[triangleEdge.Item2],
                   uvs[edgeSide.Item1], uvs[edgeSide.Item2]);

               if (intersection != Vector2.zero)
               {
                   // Add the triangleEdge to the set to keep track of unique triangleEdges
                   if(uniqueTriangleEdges.Contains(triangleEdgeCounter)) 
                       uniqueTriangleEdges.Add(triangleEdgeCounter);
               }

               triangleEdgeCounter++;
           }
       }
       
       // Now uniqueTriangleEdges contains the indices of unique triangleEdges in intersectingPoints
       int numberOfUniqueTriangleEdges = uniqueTriangleEdges.Count;

       if (numberOfUniqueTriangleEdges > 1)
           return true;
       
        return false;
   }


   /// <summary>
   /// Creates the constraints for a ThreePointCase
   /// </summary>
   /// <param name="edgeIndices"></param>
   /// <param name="origIndices"></param>
   /// <param name="uvs"></param>
   /// <param name="constraintsList"></param>
   private void HandleThreePointCase(List<int> edgeIndices, List<int> origIndices, List<Vector2> uvs, out List<List<Vector2>> constraintsList)
   {
       // in OrigIndices, only the three original uvs are mapped
       // EdgeIndices are the edges in the correct order.
       // For a triangle to have 3 points, only 2 sides are being cut.
       // The intersections also always go in and out
       // Constraint: All Edge Indices that are included in one "in and out" - cut
       
       constraintsList = new List<List<Vector2>>();

       // Algorithm. Form Edges for the edges, 0-1, 1-2, 2-3, etc.. not circular!
       // Form edges for the given EdgeIndices
       List<(int, int)> edges = new List<(int, int)>();
       
       for (int i = 0; i < edgeIndices.Count - 1; i++)
       {
           edges.Add( new(edgeIndices[i], edgeIndices[i+1]));
       }
       
       // Define the three sides of the triangle OrigIndices[0]-OrigIndices[1], OrigIndices[1]-OrigIndices[2], OrigIndices[2]-OrigIndices[0], 
       // Choose the creation in a CCW order
       List<Vector2> originalsToSort = new List<Vector2>();
       originalsToSort.Add(uvs[origIndices[0]]);
       originalsToSort.Add(uvs[origIndices[1]]);
       originalsToSort.Add(uvs[origIndices[2]]);
       SortCCW(originalsToSort);

       List<(int, int)> triangleEdges = new List<(int, int)>();
       for (int i = 0; i < origIndices.Count - 1; i++)
       {
           triangleEdges.Add( new(origIndices[i], origIndices[i+1]));
       }
       
       // Now iterate over the edges and check for intersections with edges. As soon as the same triangle side is 
       // intersected for the second time, all EdgeIndices in previous and the current edge are in one constraint list
       // Start a new constraint list and begin the counting from 0. If a same triangle edge is intersected a second time again,
       // dont include the EdgeIndices from the previous constraint in the new constraint list

       List<Vector2> currentConstraintList = new List<Vector2>();

       var firstVisitedEdge = -1;
       int edgeStart = 0;
       int edgeEnd = edgeStart +1;
       
       // iterate over all edge lines
       foreach (var edgeSide in edges)
       {
           List<(Vector2, int)> intersectingPoints = new List<(Vector2, int)>();
           
           // sample all intersectingPoints of the current edge
           int triangleEdgeCounter = 0;
           foreach (var triangleEdge in triangleEdges)
           {
               Vector2 intersection = CalcIntersectionPoint(uvs[triangleEdge.Item1], uvs[triangleEdge.Item2], uvs[edgeSide.Item1], uvs[edgeSide.Item2]);
               
               if (intersection != Vector2.zero)
               {
                   // create pair with intersection and corresponding triangle edge
                   intersectingPoints.Add(new (intersection, triangleEdgeCounter));
               }

               triangleEdgeCounter++;
           }

           
           // we found an intersecting point. Take the one closer to the starting point of the edge
           if (intersectingPoints.Count > 0)
           {
               int closestTriangleEdgeIndex = -1;

               // we already found an intersection edge. Check if any found par has the same edge. If so, take that point
               if (firstVisitedEdge != -1)
               {
                   foreach (var pointEdgePair in intersectingPoints)
                   {
                       if (pointEdgePair.Item2 == firstVisitedEdge)
                           closestTriangleEdgeIndex = pointEdgePair.Item2;
                   }
               }
               else if (firstVisitedEdge == -1 || closestTriangleEdgeIndex == -1)
               { 
                   // sort all intersecting Points after how close they are to the start point of the edgeSide
                  // take the Item2 (triangleEdgeIndex) that has the closest vertex
                  float minDistance = float.MaxValue;
                  
                  foreach (var pointEdgePair in intersectingPoints)
                  {
                      float distance = Vector2.Distance(uvs[edgeSide.Item1], pointEdgePair.Item1);
                  
                      // Check if the current distance is smaller than the minimum distance
                      if (distance < minDistance)
                      {
                          minDistance = distance;
                          closestTriangleEdgeIndex = pointEdgePair.Item2;
                      }
                  }
               }

               // now we found the closest intersection point and the corresponding edge
               // check if the edge was already visited again
               if (firstVisitedEdge == -1)
                   firstVisitedEdge = closestTriangleEdgeIndex;
               else if(firstVisitedEdge == closestTriangleEdgeIndex)
               {
                   // add all indices of the current and prev indices to a constraint list ...
                   List<int> constraintIndices = new List<int>();
                   for (int i = edgeStart; i < edges.Count; i++)
                   {
                       var indexA = edges[i].Item1;
                       var indexB = edges[i].Item2;
                       
                       if(!constraintIndices.Contains(indexA))
                           constraintIndices.Add(indexA);
                       
                       if(!constraintIndices.Contains(indexB))
                           constraintIndices.Add(indexB);
                   }
                   
                   // create constraint list
                   foreach (var conIndex in constraintIndices)
                   {
                       currentConstraintList.Add(uvs[conIndex]);
                   }
                   
                   // add current constraint list to constraintsList
                   constraintsList.Add(new List<Vector2>(currentConstraintList));
                   
                   // Resets
                   firstVisitedEdge = -1;
                   currentConstraintList.Clear();

                   edgeStart = edgeEnd;
               }
           }
           // needed?
           if(edgeEnd < edges.Count-1)
            edgeEnd++;
       }
   }
   
   /// <summary>
   /// 
   /// </summary>
   /// <param name="h"></param>
   /// <param name="outerEdge"></param>
   private void ExpandHull(ref List<Vector2> h, List<Vector2> outerEdge)
   {
       // iterate over the outer Edge indices and check if any of those that are not 
       // included in h are on an edge in h
       // if so, insert it

       for (int k = 0; k < outerEdge.Count; k++)
       {
           Vector2 edgeVector = outerEdge[k];
           
           if (!h.Contains(edgeVector))
           {
               // iterate every hull edge
               for (int i = 0; i < h.Count; i++)
               {
                   int startIndex = i;
                   int endIndex = (i + 1) % h.Count;
                   
                   Vector2 currentValue = h[startIndex];
                   Vector2 nextValue = h[endIndex];
                   
                   // check if edgeVector is on the line of current and next
                   if (IsOnLine(currentValue, nextValue, edgeVector))
                   {
                       // if so, insert between them the edgeVector
                       h.Insert(endIndex, edgeVector);
                       break;
                   }
               }
           }
       }
   }


   /// <summary>
   /// 
   /// </summary>
   /// <param name="convexGiftWrappingHull"></param>
   /// <param name="orderedEdgeAndIntersectionVertices"></param>
   /// <param name="constraints"></param>
   private void CompareHullsV2
   (
       List<Vector2> convexGiftWrappingHull, 
       List<Vector2> orderedEdgeAndIntersectionVertices,
       out List<List<Vector2>> constraints
   )
   {
       constraints = new List<List<Vector2>>();
       
       // init current Constraints and prev Vertex
       List<Vector2> currentConstraintList = new List<Vector2>();
        
       Vector2 prevVertex = orderedEdgeAndIntersectionVertices[0];
       bool constraintFound = false;
       
       for (int iteratorCounter = 0; iteratorCounter < orderedEdgeAndIntersectionVertices.Count;)
       {
           Vector2 currentVertex = orderedEdgeAndIntersectionVertices[iteratorCounter];

           if (!convexGiftWrappingHull.Contains(currentVertex))
           {
               // vertex not in hull but until now no constraint found
               if (!constraintFound)
               {
                   // add prev vertex first, set constraintFound to true
                   currentConstraintList.Add(prevVertex);
                   constraintFound = true;
               }
               // vertex not in hull and constraint found. just add the current vertex
               // to the list
               currentConstraintList.Add(currentVertex);
               prevVertex = currentVertex;
               iteratorCounter++;
           }
           else
           {
               // vertex in hull but constraint found
               if (constraintFound)
               {
                   // add current to list, set it to prev
                   currentConstraintList.Add(currentVertex);
                   constraints.Add((new List<Vector2>(currentConstraintList)));
                   
                   currentConstraintList.Clear();
                   
                   constraintFound = false;
                   prevVertex = currentVertex;
                   iteratorCounter++;
               }
               else
               {
                   // vertex in hull and no constraint found - do nothing
                   prevVertex = currentVertex;
                   iteratorCounter++;
               }
           }
       }
   }
   


    /// <summary>
    /// 
    /// </summary>
    /// <param name="intersection"></param>
    /// <param name="intersection3D"></param>
    /// <param name="normal"></param>
    /// <param name="uvp1"></param>
    /// <param name="uvp2"></param>
    /// <param name="vertex1"></param>
    /// <param name="vertex2"></param>
    /// <param name="normal1"></param>
    /// <param name="normal2"></param>
    void generateVertex(Vector2 intersection, ref Vector3 intersection3D, ref Vector3 normal,Vector2 uvp1, Vector2 uvp2, Vector3 vertex1, Vector3 vertex2, Vector3 normal1, Vector3 normal2)
    {
        // Calculate the direction vector in UV space
        Vector2 uvDirection = uvp2 - uvp1;

        // Calculate the parameter t based on UV space
        float t = Vector2.Dot(intersection - uvp1, uvDirection) / Vector2.Dot(uvDirection, uvDirection);

        // Interpolate the 3D vertex using the parameter t
        intersection3D = Vector3.Lerp(vertex1, vertex2, t);
        normal = CalcNormal(normal1, normal2, t);
    }

    
    
    
    
    void FindNewIntersectingVertexPositionsV2(Vector2 ts1, Vector2 te1, Vector2 ts2, Vector2 te2, Vector2 ts3, Vector2 te3, Vector2 edgeS, Vector2 edgeE, out Vector2 inP1, out Vector2 inP2, out Vector2 inP3)
    {
        // return Vector2.zero if no intersection point found
        inP1 = CalcIntersectionPoint(ts1, te1, edgeS, edgeE);

        inP2 = CalcIntersectionPoint(ts2, te2, edgeS, edgeE);

        inP3 = CalcIntersectionPoint(ts3, te3, edgeS, edgeE);
    }
    
    
    /// <summary>
    /// calculates the intersection point between two lines in 2D
    /// </summary>
    /// <param name="s1"></param>
    /// <param name="e1"></param>
    /// <param name="s2"></param>
    /// <param name="e2"></param>
    /// <returns></returns>
    Vector2 CalcIntersectionPoint(Vector2 s1, Vector2 e1, Vector2 s2, Vector2 e2)
    {
        // direction vectors
        Vector2 dir1 = (e1 - s1);
        Vector2 dir2 = (e2 - s2);

        // determinante
        float det = dir1.x * dir2.y - dir1.y * dir2.x;

        // if lines are parallel (0)
        if (Mathf.Approximately(det, 0f))
        {
            // no intersection
            return Vector2.zero;
        }

        // Calculate t values for each line segment
        float t1 = ((s2.x - s1.x) * dir2.y - (s2.y - s1.y) * dir2.x) / det;
        float t2 = ((s2.x - s1.x) * dir1.y - (s2.y - s1.y) * dir1.x) / det;

        // Check if the intersection point is within both line segments with a small epsilon
        float epsilon = 0.0001f; 
        if (t1 >= -epsilon && t1 <= 1f + epsilon && t2 >= -epsilon && t2 <= 1f + epsilon)
        {
            // Calculate and return the intersection point
            Vector2 intersection = s1 + t1 * dir1;
            return intersection;
        }

        // no intersection
        return Vector2.zero;
    }

    // ===== Debugging =====
    
    // Time measurement coroutine
    private IEnumerator MeasureCoroutineTime(float startTime)
    {
        while (!mainCoroutineFinished)
        {
            yield return null; // Wait for the next frame
        }

        float elapsedTime = (Time.realtimeSinceStartup - startTime) * 1000f; // Convert to milliseconds
        Debug.Log("Coroutine running for " + elapsedTime + " milliseconds");
    }
    

    void DebugVertices(List<Vector3> newVertices, Color colour)
    {
        float sphereScale = 0.0001f;
        
        // Create a sphere mesh for the small spheres
        GameObject spherePrefab = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Destroy(spherePrefab.GetComponent<Collider>()); // Remove the collider

        // Apply the blue material to the sphere
        Renderer sphereRenderer = spherePrefab.GetComponent<Renderer>();
        
        Material newM = new Material(Shader.Find("Standard"));
        newM.color = colour;
       
        sphereRenderer.material = newM;
        
        foreach (Vector3 vertexPosition in newVertices)
        {
            // Instantiate a small sphere at the vertex position
            //GameObject sphere = Instantiate(spherePrefab); //Instantiate(spherePrefab, electroSnareVR.cervixTransform.TransformPoint(vertexPosition), Quaternion.identity);//Instantiate(spherePrefab); //Instantiate(spherePrefab, electroSnareVR.cervixTransform.TransformPoint(vertexPosition), Quaternion.identity);
            GameObject sphere = Instantiate(spherePrefab, electroSnareVR.cervixTransform.TransformPoint(vertexPosition), Quaternion.identity);
            
            sphere.transform.position = electroSnareVR.cervixTransform.TransformPoint(vertexPosition);
            //sphere.transform.position = vertexPosition;
            
            // Set the scale of the sphere
            sphere.transform.localScale = new Vector3(sphereScale, sphereScale, sphereScale);
            //sphere.transform.localScale = new Vector3(sphereScale, sphereScale, sphereScale);
        }
    }


    void DebugTriangles(List<Vector3> triangles)
    {
        float sphereScale = 0.0001f;
        
        // Create a sphere mesh for the small spheres
        GameObject spherePrefab = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Destroy(spherePrefab.GetComponent<Collider>()); // Remove the collider
            
        // Set the sphere prefab to inactive
        //spherePrefab.SetActive(false);
        
        // Apply the blue material to the sphere
        Renderer sphereRenderer = spherePrefab.GetComponent<Renderer>();
        
        Material newM = new Material(Shader.Find("Standard"));
        newM.color = Color.magenta;
       
        sphereRenderer.material = newM;
        
        foreach (Vector3 vertexPosition in triangles)
        {
            // Instantiate a small sphere at the vertex position
            GameObject sphere = Instantiate(spherePrefab, electroSnareVR.cervixTransform.TransformPoint(vertexPosition), Quaternion.identity);
            
            sphere.transform.localScale = new Vector3(sphereScale, sphereScale, sphereScale);

        }
    }
}
