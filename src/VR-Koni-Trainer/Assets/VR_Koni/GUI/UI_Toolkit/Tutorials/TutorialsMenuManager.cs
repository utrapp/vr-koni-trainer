using UnityEngine;
using UnityEngine.UIElements;
using VR_Koni;

public class TutorialsMenuManager : MonoBehaviour
{
    private UIDocument uiDoc;
    private VisualElement rootElement;
    private Button startInVRTutorialButton;
    private Button simulationTutorialButton;
    private Button statisticsTutorialButton;
    private Button mainMenuButton;
    private TabMenuManger tabMenuManger;

    private void OnEnable()
    {
        uiDoc = GetComponent<UIDocument>();
        rootElement = uiDoc.rootVisualElement;

        startInVRTutorialButton = InitialiseButton("StartInVRButton", OnStartInVRButtonClick);
        simulationTutorialButton = InitialiseButton("SimulationButton", OnSimulationButtonClick);
        statisticsTutorialButton = InitialiseButton("StatisticsButton", OnStatisticsButtonClick);
        mainMenuButton = InitialiseButton("MainMenuButton", OnMainMenuButtonClick);

        tabMenuManger = new TabMenuManger();

        if (PlayerPrefs.HasKey("PreviousScene"))
        {
            string previousSceneName = PlayerPrefs.GetString("PreviousScene");
            PlayerPrefs.DeleteKey("PreviousScene");

            switch (previousSceneName)
            {
                case "Tutorials_Start_In_VR_Scene":
                    ShowStartInVRTutorialOverview();
                    break;
                case "Tutorials_Simulation_Scene":
                    ShowSimulationTutorialOverview();
                    break;
                case "Tutorials_Statistics_Scene":
                    ShowStatisticsTutorialOverview();
                    break;
                default:
                    Debug.LogError("Unknown previous scene name.");
                    break;
            }
        }
        else
        {
            ShowStartInVRTutorialOverview();
        }
    }

    private void OnDisable()
    {
        startInVRTutorialButton.UnregisterCallback<ClickEvent>(OnStartInVRButtonClick);
        simulationTutorialButton.UnregisterCallback<ClickEvent>(OnSimulationButtonClick);
        statisticsTutorialButton.UnregisterCallback<ClickEvent>(OnStatisticsButtonClick);
        mainMenuButton.UnregisterCallback<ClickEvent>(OnMainMenuButtonClick);
        UnregisterButton("LessonsButton", evt => tabMenuManger.OnLessonsButtonClick(evt));
        UnregisterButton("LearningObjectivesButton", evt => tabMenuManger.OnLearningObjectivesButtonClick(evt));
    }

    private void OnStartInVRButtonClick(ClickEvent evt) => ShowStartInVRTutorialOverview();
    private void OnSimulationButtonClick(ClickEvent evt) => ShowSimulationTutorialOverview();
    private void OnStatisticsButtonClick(ClickEvent evt) => ShowStatisticsTutorialOverview();
    private void OnMainMenuButtonClick(ClickEvent evt) => ApplicationManager.SwitchToMainMenu();

    private void ShowStartInVRTutorialOverview()
    {
        rootElement.Q("StartInVRMainPanelTemplate").style.display = DisplayStyle.Flex;
        rootElement.Q("SimulationMainPanelTemplate").style.display = DisplayStyle.None;
        rootElement.Q("StatisticsMainPanelTemplate").style.display = DisplayStyle.None;
        startInVRTutorialButton.AddToClassList("selected");
        simulationTutorialButton.RemoveFromClassList("selected");
        statisticsTutorialButton.RemoveFromClassList("selected");
        tabMenuManger.SetActiveMainPanelTemplate(rootElement.Q("StartInVRMainPanelTemplate"));
    }

    private void ShowSimulationTutorialOverview()
    {
        rootElement.Q("StartInVRMainPanelTemplate").style.display = DisplayStyle.None;
        rootElement.Q("SimulationMainPanelTemplate").style.display = DisplayStyle.Flex;
        rootElement.Q("StatisticsMainPanelTemplate").style.display = DisplayStyle.None;
        startInVRTutorialButton.RemoveFromClassList("selected");
        simulationTutorialButton.AddToClassList("selected");
        statisticsTutorialButton.RemoveFromClassList("selected");
        tabMenuManger.SetActiveMainPanelTemplate(rootElement.Q("SimulationMainPanelTemplate"));
    }

    private void ShowStatisticsTutorialOverview()
    {
        rootElement.Q("StartInVRMainPanelTemplate").style.display = DisplayStyle.None;
        rootElement.Q("SimulationMainPanelTemplate").style.display = DisplayStyle.None;
        rootElement.Q("StatisticsMainPanelTemplate").style.display = DisplayStyle.Flex;
        startInVRTutorialButton.RemoveFromClassList("selected");
        simulationTutorialButton.RemoveFromClassList("selected");
        statisticsTutorialButton.AddToClassList("selected");
        tabMenuManger.SetActiveMainPanelTemplate(rootElement.Q("StatisticsMainPanelTemplate"));
    }

    private Button InitialiseButton(string buttonName, EventCallback<ClickEvent> callback)
    {
        Button button = rootElement.Q(buttonName) as Button;
        button?.RegisterCallback<ClickEvent>(callback);
        if (button == null)
        {
            Debug.LogError($"{buttonName} not found");
        }

        return button;
    }

    private void UnregisterButton(string buttonName, EventCallback<ClickEvent> callback)
    {
        Button button = rootElement.Q(buttonName) as Button;
        button?.UnregisterCallback<ClickEvent>(callback);
        if (button == null)
        {
            Debug.LogError($"{buttonName} not found");
        }
    }
}
