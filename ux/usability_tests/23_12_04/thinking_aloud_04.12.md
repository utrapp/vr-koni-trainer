# Thinking Aloud

## Einleitung:

Herzlich willkommen zum Usability-Test für das Menü unserer VR-Anwendung für den VR KONI TRAINER. Wir freuen uns, dass Sie an diesem Usability Test teilnehmen und uns dabei helfen, die Benutzerfreundlichkeit unseres Menüs zu verbessern. Ihr Feedback ist uns sehr wichtig.

Bevor wir beginnen, möchten wir betonen, dass Ihr ehrliches und spontanes Feedback entscheidend für die Verbesserung der Anwendung ist. Während des Tests ermutigen wir Sie, laut darüber nachzudenken, was Sie tun und welche Gedanken Ihnen durch den Kopf gehen. Teilen Sie uns bitte Ihre Eindrücke, Meinungen und jegliche Herausforderungen mit, die Sie während der Aufgaben erleben. Ihr "Thinking Aloud" hilft uns, Ihre Interaktionen besser zu verstehen.

Wir testen heute ausschließlich das Menü der Anwendung. Bitte konzentrieren Sie sich darauf, wie gut Sie sich im Menü zurechtfinden und wie intuitiv die einzelnen Funktionen für Sie sind.

Also, denken Sie daran, sich während der Aufgaben frei zu äußern und uns einen Einblick in Ihre Gedanken zu geben. Vielen Dank für Ihre Teilnahme an unserem Usability-Test. Ihr offenes Feedback ist für uns von unschätzbarem Wert.

## Tasks:

### Task 1 - Register

**Research question:**
Gibt es irgendwelche Probleme beim Registrierungsprozess?

**Tasks & Scenario:**
Stellen Sie sich vor, Sie möchten ein Konto erstellen. Wie gehen Sie dabei vor?

1. Wechseln Sie zum Registrierungsfenster
2. Füllen Sie die leeren Felder aus
3. Klicken Sie auf "Register"

### Task 2 - Guide

**Research question:**
Ist die Steuerung klar und leicht zu erreichen? Sind weitere Guide-Komponenten erwünscht?

**Tasks & Scenario:**
Stellen Sie sich vor, Sie benutzen das Programm zum ersten Mal und wissen nicht, wie die Steuerung funktioniert.

1. Starten Sie vom Home-Screen aus und navigieren Sie zur Steuerung
2. In das Menü navigieren und Guide anklicken

### Task 3 - Settings

**Research question:**
Ist die Einstellungsseite leicht zu finden? Sind die Einstellungsoptionen verständlich und intuitiv? Sind die Schriften lesbar, und ist es einfach, mit den Einstellungen zu interagieren?

**Tasks & Scenario:**
Stellen Sie sich vor, Sie möchten die Sprache des Programms auf Deutsch ändern. Wie gehen Sie dabei vor?

1. Suchen Sie den Button "Settings" und klicken Sie darauf
2. Ändern Sie die Sprache auf Deutsch

### Task 4 - Training Starten

**Research question:**
Ist der Weg von den Einstellungen hin zum Training zu starten intuitiv? Ist es intuitiv, dass man dafür erst wieder auf den Home-Screen muss? Sollte es eventuell von überall aus möglich sein, ein Training zu starten?

**Tasks & Scenario:**
Sie wollen nun ein Training starten und haben gerade etwas in den Einstellungen verändert.

1. Starten Sie ein Training

### Task 5 - Training Beenden

**Research question:**
Ist der Exit Button gut zu finden?

**Tasks & Scenario:**
Sie wollen nun ein gestartetes Training beenden.

1. Beenden Sie das laufende Training

### Task 6 - Statistik ansehen

**Research question:**
Ist der Aufbau der Statistik-Seite gut gegliedert? Sind alle Informationen vorhanden? Werden alle Informationen ansprechend angezeigt?

**Tasks & Scenario:**
Sie wollen sich nun die Ergebnisse dieses Versuches anschauen.

1. Lesen Sie sich die Statistiken durch

### Task 7 - Ausschnitt ansehen

**Research question:**
Ist der Aufbau der Statistik-Seite gut gegliedert? Sind alle Informationen vorhanden? Werden alle Informationen ansprechend angezeigt?

**Tasks & Scenario:**
Sie wollen sich nun den ausgeschnittenen Bereich ansehen.

1. Sehen Sie sich den eben ausgeschnittenen Bereich an
2. Anklicken der Ausschnitt Vorschau

### Task 8 - Ältere Statistiken

**Research question:**
Ist die Navigation zu den älteren Statistiken klar? Ist dem Nutzer klar, dass man einen Tabelleneintrag anklicken kann? Sind die Ergebnisse der Statistiken verständlich und intuitiv?

**Tasks & Scenario:**
Sie haben das Training gerade beendet, bekommen Ihr Ergebnis angezeigt und möchten es mit einem vorherigen Versuch vergleichen.

1. Schauen Sie sich die Statistik des letzten Versuchs an

### Task 9 - Entwicklung ansehen

**Research question:**
Ist dem Nutzer klar, wie er zum ‘Development-Screen’ gelangt? Sind alle gewünschten Statistiken zur Entwicklung vorhanden? Sind die Statistiken klar?

**Tasks & Scenario:**
Sie wollen nun, nachdem Sie sich Ihre Statistiken zu einzelnen Spielen angesehen haben, herausfinden, wie sich Ihr Training über die Zeit entwickelt hat.

1. Sehen Sie sich Ihre Training Entwicklung an

### Task 10 - Programm beenden

**Research question:**
Ist dem Nutzer klar, wie er das Spiel verlassen kann? Ist es klar, dass man ausgeloggt wird, wenn man den Exit-Button drückt? Ist dem Nutzer klar, dass der Exit Button das Spiel beendet?

**Tasks & Scenario:**
Sie haben nun alles erledigt und wollen das Programm beenden.

1. Beenden Sie das Programm

## Abschlussfragen:

1. Wie würden Sie die Gesamtnavigation im Prototyp bewerten?
2. Gibt es bestimmte Punkte, die Ihnen besonders leicht oder schwer gefallen sind?
3. Hatten Sie Schwierigkeiten bei der Ausführung einer der Aufgaben? Wenn ja, welche?
4. Haben Sie Vorschläge zur Verbesserung der Benutzerfreundlichkeit des Prototyps?
5. Welche zusätzlichen Funktionen oder Informationen würden Sie sich im Prototyp wünschen?
