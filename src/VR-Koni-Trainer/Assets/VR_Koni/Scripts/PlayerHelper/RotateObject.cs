using UnityEngine;

namespace VR_Koni.PlayerHelper
{
    public class RotateObject : MonoBehaviour
    {
        [SerializeField] private MeshFilter meshFilter;
        private const float ROTATION_SPEED = 300f;

        private void Update()
        {
            if (Input.GetMouseButton(0))
            {
                float mouseX = Input.GetAxis("Mouse X") * Time.deltaTime;
                float mouseY = Input.GetAxis("Mouse Y") * Time.deltaTime;

                transform.Rotate(Vector3.up, mouseX * ROTATION_SPEED, Space.World);
                transform.Rotate(Vector3.right, mouseY * ROTATION_SPEED, Space.Self);
            }
        }

        public void SetMesh(Mesh cutMesh)
        {
            //meshFilter.mesh = cutMesh; TODO When meshes set correctly toggle comment
        }
    }
}
