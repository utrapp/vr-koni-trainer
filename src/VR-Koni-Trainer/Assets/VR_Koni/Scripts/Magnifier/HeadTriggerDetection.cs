
using UnityEngine;
namespace VR_Koni.Magnifier
{
    public class HeadTriggerDetection : MonoBehaviour
    {
        [SerializeField] private Camera playerCamera;
        [SerializeField] private Camera colposcopeCamera;
        
        private void Start()
        {
            colposcopeCamera.enabled = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.name == "HeadCollider")
            {
                SwitchToColposcopeView();
            }
        }
        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.name == "HeadCollider")
            {
                SwitchToMainView();
            }
        }
        
        private void SwitchToColposcopeView()
        {
            colposcopeCamera.GetComponent<ZoomAdjustment>()?.PrepareForSwitch();
            colposcopeCamera.enabled = true;
            playerCamera.enabled = false;
        }
        
        private void SwitchToMainView()  
        {
            playerCamera.enabled = true;
            colposcopeCamera.enabled = false;
        }
    }
}
