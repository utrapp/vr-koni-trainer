using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using VR_Koni;
using VR_Koni.GUI;

public class StartInVRLessonsManager : MonoBehaviour
{
    [SerializeField] private UIDocument uiDoc;
    private VisualElement rootElement;
    private Button prevLessonButton;
    private Button nextLessonButton;
    private Button prevTaskButton;
    private Button nextTaskButton;
    private Button endTutorialButton;
    private Button lessonsOverviewButton;
    private List<VisualElement> lessons;
    private List<List<VisualElement>> tasks;
    private int currentLessonIndex;
    private int currentTaskIndex;

    private void OnEnable()
    {
        uiDoc = GetComponent<UIDocument>();
        rootElement = uiDoc.rootVisualElement;

        // Initialise the buttons and the lists of lessons and tasks
        prevLessonButton = InitialiseButton("PrevLessonButton", OnPrevLessonButtonClick);
        nextLessonButton = InitialiseButton("NextLessonButton", OnNextLessonButtonClick);
        prevTaskButton = InitialiseButton("PrevTaskButton", OnPrevTaskButtonClick);
        nextTaskButton = InitialiseButton("NextTaskButton", OnNextTaskButtonClick);
        endTutorialButton = InitialiseButton("EndTutorialButton", OnEndTutorialButtonClick);
        lessonsOverviewButton = InitialiseButton("LessonsOverviewButton", OnLessonsOverviewButtonClick);

        lessons = new List<VisualElement>
        {
            rootElement.Q("Lesson1"),
            rootElement.Q("Lesson2"),
            rootElement.Q("Lesson3"),
            rootElement.Q("Lesson4"),
            rootElement.Q("Lesson5"),
            rootElement.Q("Lesson6")
        };
        tasks = new List<List<VisualElement>>();
        for (int i = 0; i < lessons.Count; i++)
        {
            tasks.Add(new List<VisualElement>());
            int j = 1;
            while (true)
            {
                var task = lessons[i].Q($"Task{j}");
                if (task == null)
                {
                    break;
                }
                tasks[i].Add(task);
                j++;
            }
        }

        SetCurrentLessonIndexFromPrefs();
        currentTaskIndex = 0;
        ShowCurrentLesson();
        ShowCurrentTask();
    }

    private void OnDisable()
    {
        // Deregister the ClickEvent callbacks for the buttons
        prevLessonButton.UnregisterCallback<ClickEvent>(OnPrevLessonButtonClick);
        nextLessonButton.UnregisterCallback<ClickEvent>(OnNextLessonButtonClick);
        prevTaskButton.UnregisterCallback<ClickEvent>(OnPrevTaskButtonClick);
        nextTaskButton.UnregisterCallback<ClickEvent>(OnNextTaskButtonClick);
        endTutorialButton.UnregisterCallback<ClickEvent>(OnEndTutorialButtonClick);
        lessonsOverviewButton.UnregisterCallback<ClickEvent>(OnLessonsOverviewButtonClick);
    }

    private void SetCurrentLessonIndexFromPrefs()
    {
        if (PlayerPrefs.HasKey("lesson"))
        {
            int lessonNumber = PlayerPrefs.GetInt("lesson") - 1; // Convert to 0-based index;
            currentLessonIndex = lessonNumber;
        }
        else
        {
            currentLessonIndex = 0;
        }
    }

    private void OnPrevLessonButtonClick(ClickEvent evt)
    {
        // Hide the current task
        tasks[currentLessonIndex][currentTaskIndex].style.display = DisplayStyle.None;
        // Hide the current lesson
        lessons[currentLessonIndex].style.display = DisplayStyle.None;

        // Switch to the last task of the previous lesson
        currentLessonIndex--;
        currentTaskIndex = tasks[currentLessonIndex].Count - 1;

        ShowCurrentLesson();
        ShowCurrentTask();
    }

    private void OnNextLessonButtonClick(ClickEvent evt)
    {
        // Hide the current task
        tasks[currentLessonIndex][currentTaskIndex].style.display = DisplayStyle.None;
        // Hide the current lesson
        lessons[currentLessonIndex].style.display = DisplayStyle.None;

        // Switch to the first task of the next lesson
        currentLessonIndex++;
        currentTaskIndex = 0;

        ShowCurrentLesson();
        ShowCurrentTask();
    }

    private void OnPrevTaskButtonClick(ClickEvent evt)
    {
        // Hide the current task
        tasks[currentLessonIndex][currentTaskIndex].style.display = DisplayStyle.None;

        // Switch to the previous task
        currentTaskIndex--;

        ShowCurrentTask();
    }

    private void OnNextTaskButtonClick(ClickEvent evt)
    {
        // Hide the current task
        tasks[currentLessonIndex][currentTaskIndex].style.display = DisplayStyle.None;

        // Switch to the next task
        currentTaskIndex++;

        ShowCurrentTask();
    }

    private void OnEndTutorialButtonClick(ClickEvent evt)
    {
        string currentSceneName = SceneManager.GetActiveScene().name;
        PlayerPrefs.SetString("PreviousScene", currentSceneName);
        ApplicationManager.SwitchToTutorials();
    }

    private void ShowCurrentLesson()
    {
        // Show the current lesson
        lessons[currentLessonIndex].style.display = DisplayStyle.Flex;
    }

    private void ShowCurrentTask()
    {
        // Show the current task
        tasks[currentLessonIndex][currentTaskIndex].style.display = DisplayStyle.Flex;

        // Update the visibility of the buttons
        prevLessonButton.style.display = (currentLessonIndex > 0 && currentTaskIndex == 0) ? DisplayStyle.Flex : DisplayStyle.None;
        nextLessonButton.style.display = (currentLessonIndex < lessons.Count - 1 && currentTaskIndex == tasks[currentLessonIndex].Count -1) ? DisplayStyle.Flex : DisplayStyle.None;
        if (currentLessonIndex == 0 && currentTaskIndex == 0)
        {
            prevTaskButton.style.display = DisplayStyle.Flex;
            prevTaskButton.style.visibility = Visibility.Hidden;
        }
        else
        {
            prevTaskButton.style.display = (currentTaskIndex > 0) ? DisplayStyle.Flex : DisplayStyle.None;
            prevTaskButton.style.visibility = Visibility.Visible;
        }
        nextTaskButton.style.display = (currentTaskIndex < tasks[currentLessonIndex].Count - 1) ? DisplayStyle.Flex : DisplayStyle.None;
        endTutorialButton.style.display = (currentLessonIndex == lessons.Count - 1 && currentTaskIndex == tasks[currentLessonIndex].Count - 1) ? DisplayStyle.Flex : DisplayStyle.None;
    }

    private void OnLessonsOverviewButtonClick(ClickEvent evt)
    {
        string currentSceneName = SceneManager.GetActiveScene().name;
        PlayerPrefs.SetString("PreviousScene", currentSceneName);
        ApplicationManager.SwitchToTutorials();
    }

    private Button InitialiseButton(string buttonName, EventCallback<ClickEvent> callback)
    {
        Button button = rootElement.Q(buttonName) as Button;
        button?.RegisterCallback<ClickEvent>(callback);
        if (button == null)
        {
            Debug.LogError($"{buttonName} not found");
        }

        return button;
    }
}
