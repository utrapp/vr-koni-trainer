using UnityEngine.Localization;
using UnityEngine.UIElements;
using VR_Koni.GUI.UI_Toolkit.Helper;
using VR_Koni.GUI.UI_Toolkit.MainMenu.Views.StatisticsViews;
using VR_Koni.UserData;

namespace VR_Koni.GUI.UI_Toolkit.MainMenu.Views
{
    public class MainMenuStatisticsView : IMainMenuView
    {
        public LocalizedString ViewName => new LocalizedString { TableReference =  ProjectConstants.UiToolKitLocalizationTableName, TableEntryReference = "MainMenuStatisticsViewName" };
        public VisualElement RootElement { get; private set; }

        private IMainMenuView[] allSubViews;
        private readonly MainMenuStatisticsSurgeryDetailsSubView surgeryDetailsSubView = new();
        private readonly MainMenuStatisticsAllTriesSubView allTriesSubView = new();
        private readonly MainMenuStatisticsGraphsSubView graphsSubView = new();
        private Button lastSelectedStatisticsViewButton;
        private Button surgeryDetailsViewButton;
        private Button allTriesViewButton;
        private Button graphsViewButton;

        public void SetupVisualElements(VisualElement rootElement)
        {
            RootElement = rootElement;
            allSubViews = new IMainMenuView[]
            {
                surgeryDetailsSubView, allTriesSubView, graphsSubView
            };

            surgeryDetailsSubView.SetupVisualElements(rootElement.Q("MainMenuStatisticsSurgeryDetailsSubView"));
            allTriesSubView.SetupVisualElements(rootElement.Q("MainMenuStatisticsAllTriesSubView"));
            graphsSubView.SetupVisualElements(rootElement.Q("MainMenuStatisticsGraphsSubView"));

            surgeryDetailsViewButton = rootElement.Q<Button>("SurgeryDetails");
            allTriesViewButton = rootElement.Q<Button>("AllTriesViewButton");
            graphsViewButton = rootElement.Q<Button>("GraphsViewButton");
            
            SwitchToView(null, surgeryDetailsSubView);
        }

        public void SubscribeToEvents()
        {
            surgeryDetailsViewButton.RegisterCallback<ClickEvent, IMainMenuView>(SwitchToView, surgeryDetailsSubView);
            allTriesViewButton.RegisterCallback<ClickEvent, IMainMenuView>(SwitchToView, allTriesSubView);
            graphsViewButton.RegisterCallback<ClickEvent, IMainMenuView>(SwitchToView, graphsSubView);

            allTriesSubView.OnDetailsRequested += SwitchToSurgeryDetailsWithSurgery;
            foreach (IMainMenuView view in allSubViews)
            {
                view.SubscribeToEvents();
            }
        }

        public void UnsubscribeFromEvents()
        {
            surgeryDetailsViewButton.UnregisterCallback<ClickEvent, IMainMenuView>(SwitchToView);
            allTriesViewButton.UnregisterCallback<ClickEvent, IMainMenuView>(SwitchToView);
            graphsViewButton.UnregisterCallback<ClickEvent, IMainMenuView>(SwitchToView);

            allTriesSubView.OnDetailsRequested -= SwitchToSurgeryDetailsWithSurgery;
            foreach (IMainMenuView view in allSubViews)
            {
                view.UnsubscribeFromEvents();
            }
        }

        public void ResetViewSelections()
        {
            foreach (IMainMenuView view in allSubViews)
            {
                view.ResetViewSelections();
            }
        }

        private void SwitchToView(ClickEvent _, IMainMenuView selectedView)
        {
            foreach (IMainMenuView view in allSubViews)
            {
                view.RootElement.SetIsVisible(false);
            }

            selectedView.ResetViewSelections();
            selectedView.RootElement.SetIsVisible(true);
            
            AddClassToSelectedButton(
                                     selectedView == surgeryDetailsSubView ? surgeryDetailsViewButton :
                                     selectedView == allTriesSubView ? allTriesViewButton :
                                     selectedView == graphsSubView ? graphsViewButton :
                                     null
                                     );
        }
        
        /// <summary>
        /// Adds the "selected" class to the provided button and removes it from the previously selected button.
        /// </summary>
        /// <param name="selectedButton">The button to which the "selected" class will be added.</param>
        private void AddClassToSelectedButton(Button selectedButton)
        {
            lastSelectedStatisticsViewButton?.RemoveFromClassList("selected");
            selectedButton?.AddToClassList("selected");
            lastSelectedStatisticsViewButton = selectedButton;
        }

        private void SwitchToSurgeryDetailsWithSurgery(Surgery surgery)
        {
            foreach (IMainMenuView view in allSubViews)
            {
                view.RootElement.SetIsVisible(false);
            }
            surgeryDetailsSubView.ResetViewSelections();
            surgeryDetailsSubView.DisplaySelectedSurgery(surgery);
            surgeryDetailsSubView.RootElement.SetIsVisible(true);
            AddClassToSelectedButton(surgeryDetailsViewButton);
        }
    }
}
