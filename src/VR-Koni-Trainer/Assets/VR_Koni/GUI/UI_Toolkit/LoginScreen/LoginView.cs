using System;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using UnityEngine.UIElements;
using VR_Koni.GUI.UI_Toolkit.Helper;
using VR_Koni.GUI.UI_Toolkit.Popup;
using VR_Koni.UserData;
using VR_Koni.GUI;

namespace VR_Koni.GUI.UI_Toolkit.LoginScreen
{
    public class LoginView : IUIView
    {
        public VisualElement RootElement { get; private set; }

        private Button loginButton, guestLoginButton, togglePasswordVisibilityButton;
        private TextField usernameTextField, passwordTextField;
        private Label errorLabel;
        private Label linkToRegisterScreenLabel;

#region  LocalizationStrings
        private readonly LocalizedString usernamePlaceholder = new()
        {
            TableReference = ProjectConstants.UiToolKitLocalizationTableName, 
            TableEntryReference = "UsernamePlaceholder" 
        };
        
        private readonly LocalizedString passwordPlaceholder = new()
        {
            TableReference = ProjectConstants.UiToolKitLocalizationTableName, 
            TableEntryReference = "PasswordPlaceholder" 
        };
        
        private readonly LocalizedString emptyFieldsError = new()
        {
            TableReference = ProjectConstants.UiToolKitLocalizationTableName, 
            TableEntryReference = "EmptyFieldsError" 
        };
        
        private readonly LocalizedString loginErrorInvalidCredentials = new()
        {
            TableReference = ProjectConstants.UiToolKitLocalizationTableName, 
            TableEntryReference = "LoginErrorInvalidCredentials"
        };
        
        private readonly LocalizedString popupHeaderLocalized = new()
        {
            TableReference = ProjectConstants.UiToolKitLocalizationTableName,
            TableEntryReference = "LoginViewPopupHeaderText"
        };
        private readonly  LocalizedString popupDescriptionLocalized= new()
        {
            TableReference = ProjectConstants.UiToolKitLocalizationTableName,
            TableEntryReference = "LoginViewPopupDescriptionText"
        };
        private readonly LocalizedString popupLeftButtonText= new()
        {
            TableReference = ProjectConstants.UiToolKitLocalizationTableName,
            TableEntryReference = "LoginViewPopupLeftButtonText"
        };
        private readonly LocalizedString popupRightButtonText = new()
        {
            TableReference = ProjectConstants.UiToolKitLocalizationTableName,
            TableEntryReference = "LoginViewPopupRightButtonText"
        };
#endregion
        public void SetupVisualElements(VisualElement rootElement)
        {
            RootElement = rootElement;

            loginButton = rootElement.Q<Button>("LoginButton");
            guestLoginButton = rootElement.Q<Button>("GuestLoginButton");
            usernameTextField = rootElement.Q<TextField>("UsernameTextField");
            passwordTextField = rootElement.Q<TextField>("PasswordTextField");
            errorLabel = rootElement.Q<Label>("ErrorLabel");
            togglePasswordVisibilityButton = rootElement.Q<Button>("TogglePasswordVisibilityButton");
            linkToRegisterScreenLabel = rootElement.Q<Label>("LinkToRegisterScreenLabel");
            ResetViewSelections();
        }

        public void SubscribeToEvents()
        {
            loginButton.clicked += Login;
            guestLoginButton.clicked += GuestLogin;
            togglePasswordVisibilityButton.clicked += TogglePasswordVisibility;
            
            // Register focus events for placeholders.
            usernameTextField.RegisterCallback<FocusInEvent>(OnUsernameFocusIn);
            usernameTextField.RegisterCallback<FocusOutEvent>(OnUsernameFocusOut);
            passwordTextField.RegisterCallback<FocusInEvent>(OnPasswordFocusIn);
            passwordTextField.RegisterCallback<FocusOutEvent>(OnPasswordFocusOut);
            
            // Register click event for label
            linkToRegisterScreenLabel.RegisterCallback<ClickEvent>(ShowRegisterScreen);
            
            // Subscribe to locale change
            LocalizationSettings.SelectedLocaleChanged += OnLocaleChanged;
        }

        public void UnsubscribeFromEvents()
        {
            loginButton.clicked -= Login;
            guestLoginButton.clicked -= GuestLogin;
            togglePasswordVisibilityButton.clicked -= TogglePasswordVisibility;
            
            // Unregister focus events for placeholders.
            usernameTextField.UnregisterCallback<FocusInEvent>(OnUsernameFocusIn);
            usernameTextField.UnregisterCallback<FocusOutEvent>(OnUsernameFocusOut);
            passwordTextField.UnregisterCallback<FocusInEvent>(OnPasswordFocusIn);
            passwordTextField.UnregisterCallback<FocusOutEvent>(OnPasswordFocusOut);
            
            // Unregister Click events for label
            linkToRegisterScreenLabel.UnregisterCallback<ClickEvent>(ShowRegisterScreen);
            
            // Subscribe to locale change
            LocalizationSettings.SelectedLocaleChanged -= OnLocaleChanged;
        }

        public void ResetViewSelections()
        {
            UpdatePlaceholders();
            passwordTextField.SetPasswordVisibility(togglePasswordVisibilityButton, true);
            errorLabel.SetIsVisible(false);
        }
        
        /// <summary>
        /// Updates the placeholder text in the username and password text fields.
        /// Retrieves the localized placeholder values for the username and password fields 
        /// and sets them as the text field values to reflect the current language settings.
        /// </summary>
        private  void UpdatePlaceholders()
        {
            usernameTextField.value =  usernamePlaceholder.GetLocalizedString();
            passwordTextField.value =  passwordPlaceholder.GetLocalizedString();
        }
        
        private void OnLocaleChanged(Locale newLocale)
        {
            // Update placeholders when the locale changes
            UpdatePlaceholders();
        }

        private void TogglePasswordVisibility() => passwordTextField.TogglePasswordVisibility(togglePasswordVisibilityButton);

        private void GuestLogin()
        {
            //TODO: Rework to get translation per key value pair
            PopupScreenManager.Show(headerLocalized:popupHeaderLocalized,
                                    descriptionLocalized:popupDescriptionLocalized,
                                    rightButtonTextLocalized:popupLeftButtonText, 
                                    actionRight:() =>
                                    {
                                        LoginRegisterScreenManager.LoginScreenSelected?.Invoke(false);
                                    },
                                    leftButtonTextLocalized:popupRightButtonText,
                                    actionLeft:() =>
                                    {
                                        DataManager.Instance.LogInGuest();
                                        ApplicationManager.SwitchToMainMenu();
                                    });
        }

        private void Login()
        {
            errorLabel.SetIsVisible(false);

            if (string.Equals(passwordTextField.value, passwordPlaceholder.GetLocalizedString()) || string.Equals(usernameTextField.value, usernamePlaceholder.GetLocalizedString()))
            {
                errorLabel.text = emptyFieldsError.GetLocalizedString();
                errorLabel.SetIsVisible(true);
                return;
            }

            if (!DataManager.Instance.LogInUser(usernameTextField.value, passwordTextField.value))
            {
                errorLabel.text = loginErrorInvalidCredentials.GetLocalizedString();
                errorLabel.SetIsVisible(true);
                return;
            }

            ApplicationManager.SwitchToMainMenu();
        }
        
        // Methods to handle focus events.
        private void OnUsernameFocusIn(FocusInEvent evt) => ClearUsernamePlaceholder(usernameTextField, usernamePlaceholder.GetLocalizedString());
        private void OnUsernameFocusOut(FocusOutEvent evt) => RestoreUsernamePlaceholder(usernameTextField, usernamePlaceholder.GetLocalizedString());
        private void OnPasswordFocusIn(FocusInEvent evt) => ClearPasswordPlaceholder(passwordTextField, passwordPlaceholder.GetLocalizedString());
        private void OnPasswordFocusOut(FocusOutEvent evt) => RestorePasswordPlaceholder(passwordTextField, passwordPlaceholder.GetLocalizedString());

        // Clears username-placeholder.
        private void ClearUsernamePlaceholder(TextField textField, string placeholder)
        {
            ClearPlaceholder(textField, placeholder);
        }
        
        // Clears password-placeholder and toggles its visibility.
        private void ClearPasswordPlaceholder(TextField textField, string placeholder)
        {
            ClearPlaceholder(textField, placeholder);
            if (IsTextFieldEmptyOrPlaceholder(textField, placeholder))
            {
                TogglePasswordVisibility();
            }
        }

        // Restores username-placeholder.
        private void RestoreUsernamePlaceholder(TextField textField, string placeholder)
        {
            RestorePlaceholder(textField, placeholder);
        }
        
        // Restores password-placeholder and toggles its visibility.
        private void RestorePasswordPlaceholder(TextField textField, string placeholder)
        {
            RestorePlaceholder(textField, placeholder);
            if (IsTextFieldEmptyOrPlaceholder(textField, placeholder))
            {
                TogglePasswordVisibility();
            }
        }

        // Restores the placeholder when there is no input.
        private void RestorePlaceholder(TextField textField, string placeholder)
        {
            if (string.IsNullOrEmpty(textField.value))
            {
                textField.value = placeholder;
            }
        }
        
        // Clears placeholder to take input.
        private void ClearPlaceholder(TextField textField, string placeholder)
        {
            if (string.Equals(textField.value, placeholder, StringComparison.OrdinalIgnoreCase))
            {
                textField.value = string.Empty;
            }
        }

        // Handles the event to display the Register screen.
        private void ShowRegisterScreen(ClickEvent evt)
        {
            LoginRegisterScreenManager.LoginScreenSelected?.Invoke(false);
        }
        
        /// <summary>
        /// Checks if the text field is either empty or contains the specified placeholder text.
        /// </summary>
        /// <param name="textField">The text field to check.</param>
        /// <param name="placeholder">The placeholder text to compare against.</param>
        /// <returns>Returns true if the text field is empty or contains the placeholder text; otherwise, false.</returns>
        private bool IsTextFieldEmptyOrPlaceholder(TextField textField, string placeholder)
        {
            return string.Equals(textField.value, placeholder, StringComparison.OrdinalIgnoreCase) || string.IsNullOrEmpty(textField.value);
        }
    }
}
