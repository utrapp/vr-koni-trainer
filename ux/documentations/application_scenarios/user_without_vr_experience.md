## Nutzer ohne VR Erfahrung ##
- Wie benutze ich die Brille
- Wie benutze ich die Controller
- Wie navigiere ich mich im Spiel?
- Wie benutze ich Raycast oder andere Steuerung
- Es fällt mir immer noch schwer mich zu Navigieren ich bräuchte mehr Übung
- Okay, besser, was ist das Ziel des Spiels?
- Wie funktioniert die Steuerung bezüglich des Spiels
- Wie kann ich innerhalb des Spiels zur Navigation zurück
- Ich habe nun fertig gespielt
- Wo sehe ich mein Ergebnis?
- Wie ergibt sich das Ergebnis?
- Wie gut ist mein Ergebnis?

### 1. Nutzer setzt Headset auf ###
1.1 Muss sich orientieren etc.

1.2 Muss Hände finden / lernen wie man Sachen auswählt

1.3 Muss Menü finden

### 2. Menü Optionen: A) Start / B) Tutorial ### 
2.1 "Tutorial" wird ausgewählt
- Nutzer wählt vielleicht falsche Option aus
- muss lernen wie man zurückgeht (braucht Hinweis)

2.2 Nutzer führt Tutorial durch:
- sieht wo Werkzeug/ Lupe/ etc. sind
- muss lernen wie man die Schlinge an/aus macht
- muss lernen wie Operation funktioniert
- Abstand zu Operationstisch/ Lupe

2.3 Nutzer wählt "Start" aus
- will erste Operation durchführen

### 3. Simulation ###
3.1 Führt Erste "Operation" durch
- mithilfe von Tutorial

3.2 Sieht Statistiken nach Operation
- weiß vielleicht nicht was Statistiken bedeuten

3.3 Nutzer beendet Simulation/ Schließt Fenster mit Statistike