using System;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using UnityEngine.UIElements;
using VR_Koni.GUI.UI_Toolkit.Helper;
using VR_Koni.UserData;

namespace VR_Koni.GUI.UI_Toolkit.LoginScreen
{
    public class RegisterView : IUIView
    {
        public VisualElement RootElement { get; private set; }

        private Button registerButton, togglePasswordVisibilityButton, togglePasswordRepeatVisibilityButton;
        private TextField usernameTextField, passwordTextField, passwordRepeatTextField;
        private Label errorLabel;
        
        private Label  linkToLoginScreenLabel;
        
        private readonly LocalizedString usernamePlaceholder = new LocalizedString { TableReference = ProjectConstants.UiToolKitLocalizationTableName, TableEntryReference = "UsernamePlaceholder" };
        private readonly LocalizedString passwordPlaceholder = new LocalizedString { TableReference = ProjectConstants.UiToolKitLocalizationTableName, TableEntryReference = "PasswordPlaceholder" };
        private readonly LocalizedString passwordRepeatPlaceholder = new LocalizedString { TableReference =  ProjectConstants.UiToolKitLocalizationTableName, TableEntryReference = "PasswordRepeatPlaceholder" };
        
        private readonly LocalizedString emptyFieldsError = new LocalizedString { TableReference =  ProjectConstants.UiToolKitLocalizationTableName, TableEntryReference = "EmptyFieldsError" };
        private readonly LocalizedString passwordNotMatchingError = new LocalizedString { TableReference =  ProjectConstants.UiToolKitLocalizationTableName, TableEntryReference = "RegisterErrorPasswordNotMatching" };
        private readonly LocalizedString userAlreadyExistsError = new LocalizedString { TableReference =  ProjectConstants.UiToolKitLocalizationTableName, TableEntryReference = "RegisterErrorUserAlreadyExists" };

        public void SetupVisualElements(VisualElement rootElement)
        {
            RootElement = rootElement;

            registerButton = rootElement.Q<Button>("RegisterButton");
            usernameTextField = rootElement.Q<TextField>("UsernameTextField");
            passwordTextField = rootElement.Q<TextField>("PasswordTextField");
            passwordRepeatTextField = rootElement.Q<TextField>("PasswordRepeatTextField");
            errorLabel = rootElement.Q<Label>("ErrorLabel");
            togglePasswordVisibilityButton = rootElement.Q<Button>("TogglePasswordVisibilityButton");
            togglePasswordRepeatVisibilityButton = rootElement.Q<Button>("TogglePasswordRepeatVisibilityButton");
            linkToLoginScreenLabel = rootElement.Q<Label>("LinkToLoginScreenLabel");
            ResetViewSelections();
        }

        public void SubscribeToEvents()
        {
            registerButton.clicked += Register;
            togglePasswordVisibilityButton.clicked += TogglePasswordVisibility;
            togglePasswordRepeatVisibilityButton.clicked += TogglePasswordRepeatVisibility;
            
            //Register focus events for placeholders
            usernameTextField.RegisterCallback<FocusInEvent>(OnUsernameFocusIn);
            usernameTextField.RegisterCallback<FocusOutEvent>(OnUsernameFocusOut);
            passwordTextField.RegisterCallback<FocusInEvent>(OnPasswordFocusIn);
            passwordTextField.RegisterCallback<FocusOutEvent>(OnPasswordFocusOut);
            passwordRepeatTextField.RegisterCallback<FocusInEvent>(OnPasswordRepeatFocusIn);
            passwordRepeatTextField.RegisterCallback<FocusOutEvent>(OnPasswordRepeatFocusOut);
            
            //Register Click event for Label
            linkToLoginScreenLabel.RegisterCallback<ClickEvent>(ShowLoginScreen);
            
            // Subscribe to locale change
            LocalizationSettings.SelectedLocaleChanged += OnLocaleChanged;
        }

        public void UnsubscribeFromEvents()
        {
            registerButton.clicked -= Register;
            togglePasswordVisibilityButton.clicked -= TogglePasswordVisibility;
            togglePasswordRepeatVisibilityButton.clicked -= TogglePasswordRepeatVisibility;
            
            //Unregister focus events for placeholders
            usernameTextField.UnregisterCallback<FocusInEvent>(OnUsernameFocusIn);
            usernameTextField.UnregisterCallback<FocusOutEvent>(OnUsernameFocusOut);
            passwordTextField.UnregisterCallback<FocusInEvent>(OnPasswordFocusIn);
            passwordTextField.UnregisterCallback<FocusOutEvent>(OnPasswordFocusOut);
            passwordRepeatTextField.UnregisterCallback<FocusInEvent>(OnPasswordRepeatFocusIn);
            passwordRepeatTextField.UnregisterCallback<FocusOutEvent>(OnPasswordRepeatFocusOut);
            
            //Unregister Click event for label
            linkToLoginScreenLabel.UnregisterCallback<ClickEvent>(ShowLoginScreen);
            
            // Subscribe to locale change
            LocalizationSettings.SelectedLocaleChanged -= OnLocaleChanged;
        }

        public void ResetViewSelections()
        {
            UpdatePlaceholders();
            passwordTextField.SetPasswordVisibility(togglePasswordVisibilityButton, true);
            passwordRepeatTextField.SetPasswordVisibility(togglePasswordRepeatVisibilityButton, true);
        }

        /// <summary>
        /// Updates the placeholder text in the username and password text fields.
        /// Retrieves the localized placeholder values for the username, password and passwordRepeat fields 
        /// and sets them as the text field values to reflect the current language settings.
        /// </summary>
        private  void UpdatePlaceholders()
        {
            usernameTextField.value =  usernamePlaceholder.GetLocalizedString();
            passwordTextField.value =  passwordPlaceholder.GetLocalizedString();
            passwordRepeatTextField.value =  passwordRepeatPlaceholder.GetLocalizedString();
        }
        
        private void OnLocaleChanged(Locale newLocale)
        {
            // Update placeholders when the locale changes
            UpdatePlaceholders();
        }
        
        private void TogglePasswordVisibility() => passwordTextField.TogglePasswordVisibility(togglePasswordVisibilityButton);
        private void TogglePasswordRepeatVisibility() => passwordRepeatTextField.TogglePasswordVisibility(togglePasswordRepeatVisibilityButton);

        private void Register()
        {
            if (string.Equals(passwordTextField.value, passwordPlaceholder.GetLocalizedString()) || string.Equals(usernameTextField.value, usernamePlaceholder.GetLocalizedString()))
            {
                errorLabel.text = emptyFieldsError.GetLocalizedString();
                errorLabel.SetIsVisible(true);
                return;
            }

            if (!passwordTextField.value.Equals(passwordRepeatTextField.value, StringComparison.CurrentCulture))
            {
                errorLabel.text = passwordNotMatchingError.GetLocalizedString();
                errorLabel.SetIsVisible(true);
                return;
            }

            if (!DataManager.Instance.CreateUser(usernameTextField.value, passwordTextField.value))
            {
                errorLabel.text = userAlreadyExistsError.GetLocalizedString();
                errorLabel.SetIsVisible(true);
                return;
            }

            if (!DataManager.Instance.LogInUser(usernameTextField.value, passwordTextField.value))
            {
                errorLabel.text = "Contact Darmstadt";
                errorLabel.SetIsVisible(true);
                return;
            }
            
             //TODO: feedback ui user created
             ApplicationManager.SwitchToMainMenu();
        }
        
        //Methods to handle focus events
        //TODO: check if it is necessary in unity 6
        private void OnUsernameFocusIn(FocusInEvent evt) => ClearUsernamePlaceholder(usernameTextField, usernamePlaceholder.GetLocalizedString());
        private void OnUsernameFocusOut(FocusOutEvent evt) => RestoreUsernamePlaceholder(usernameTextField, usernamePlaceholder.GetLocalizedString());
        private void OnPasswordFocusIn(FocusInEvent evt) => ClearPasswordPlaceholder(passwordTextField, passwordPlaceholder.GetLocalizedString());
        private void OnPasswordFocusOut(FocusOutEvent evt) => RestorePasswordPlaceholder(passwordTextField, passwordPlaceholder.GetLocalizedString());
        private void OnPasswordRepeatFocusIn(FocusInEvent evt) => ClearPasswordRepeatPlaceholder(passwordRepeatTextField, passwordRepeatPlaceholder.GetLocalizedString());
        private void OnPasswordRepeatFocusOut(FocusOutEvent evt) => RestorePasswordRepeatPlaceholder(passwordRepeatTextField, passwordRepeatPlaceholder.GetLocalizedString());
        
        // Clears the username-placeholder.
        private void ClearUsernamePlaceholder(TextField textField, string placeholder)
        {
            ClearPlaceholder(textField, placeholder);
        }
        
        // Clears the password-placeholder and toggles its visibility.
        private void ClearPasswordPlaceholder(TextField textField, string placeholder)
        {
            ClearPlaceholder(textField, placeholder);
            if (IsTextFieldEmptyOrPlaceholder(textField, placeholder))
            {
                TogglePasswordVisibility();
            }
        }
        
        // Clears the password-repeat-placeholder and toggles its visibility.
        private void ClearPasswordRepeatPlaceholder(TextField textField, string placeholder)
        {
            ClearPlaceholder(textField, placeholder);
            if (IsTextFieldEmptyOrPlaceholder(textField, placeholder))
            {
                TogglePasswordRepeatVisibility();
            }
        }

        // Restores the username-placeholder.
        private void RestoreUsernamePlaceholder(TextField textField, string placeholder)
        {
            RestorePlaceholder(textField, placeholder);
        }
        
        // Restores the password-placeholder and toggles its visibility.
        private void RestorePasswordPlaceholder(TextField textField, string placeholder)
        {
            RestorePlaceholder(textField, placeholder);
            if (IsTextFieldEmptyOrPlaceholder(textField, placeholder))
            {
                TogglePasswordVisibility();
            }
        }
        
        // Restores the password-repeat-placeholder and toggles its visibility.
        private void RestorePasswordRepeatPlaceholder(TextField textField, string placeholder)
        {
            RestorePlaceholder(textField, placeholder);
            if (IsTextFieldEmptyOrPlaceholder(textField, placeholder))
            {
                TogglePasswordRepeatVisibility();
            }
        }

        // Restores the placeholder if there is no input.
        private void RestorePlaceholder(TextField textField, string placeholder)
        {
            if (string.IsNullOrEmpty(textField.value))
            {
                textField.value = placeholder;
            }
        }
        
        // Clears the placeholder to take input.
        private void ClearPlaceholder(TextField textField, string placeholder)
        {
            if (string.Equals(textField.value, placeholder, StringComparison.OrdinalIgnoreCase))
            {
                textField.value = string.Empty;
            }
        }

        /// Handles the event to display the Login screen.
        private void ShowLoginScreen(ClickEvent evt)
        {
            LoginRegisterScreenManager.LoginScreenSelected?.Invoke(true);
        }
        
        /// <summary>
        /// Checks if the text field is either empty or contains the specified placeholder text.
        /// </summary>
        /// <param name="textField">The text field to check.</param>
        /// <param name="placeholder">The placeholder text to compare against.</param>
        /// <returns>Returns true if the text field is empty or contains the placeholder text; otherwise, false.</returns>
        private bool IsTextFieldEmptyOrPlaceholder(TextField textField, string placeholder)
        {
            return string.Equals(textField.value, placeholder, StringComparison.OrdinalIgnoreCase) || string.IsNullOrEmpty(textField.value);
        }
    }
    
}
