using UnityEngine.UIElements;

namespace VR_Koni.GUI.UI_Toolkit.Popup
{
    /// <summary>
    /// <see cref="TextOverflowHandler"/> handles the text overflow in a <see cref="Label"/> within a
    /// <see cref="VisualElement"/> container.
    /// </summary>
    public static class TextOverflowHandler
    {
        private const float SAFETY_MARGIN = 0.9f;

        /// <summary>
        /// Adjusts the text in a <see cref="Label"/> to fit within a <see cref="VisualElement"/> container.
        /// </summary>
        /// <param name="label">The Label to adjust.</param>
        /// <param name="container">The VisualElement container.</param>
        public static void AdjustTextToContainer(Label label, VisualElement container)
        {
            if (label == null || container == null) return;

            label.style.whiteSpace = WhiteSpace.Normal;
            label.style.flexWrap = Wrap.Wrap;

            container.RegisterCallback<GeometryChangedEvent>(evt => { UpdateTextWrapping(label, container); });

            //update the text initially after adjusting the text to the container size
            UpdateTextWrapping(label, container);
        }

        /// <summary>
        /// Updates the text wrapping in a Label to fit within a VisualElement container.
        /// </summary>
        /// <param name="label">The Label to update.</param>
        /// <param name="container">The VisualElement container.</param>
        private static void UpdateTextWrapping(Label label, VisualElement container)
        {
            IResolvedStyle containerStyle = container.resolvedStyle;
            IResolvedStyle labelStyle = label.resolvedStyle;

            float availableWidth = containerStyle.width
                                   - containerStyle.paddingLeft
                                   - containerStyle.paddingRight
                                   - labelStyle.marginLeft
                                   - labelStyle.marginRight;

            float availableHeight = containerStyle.height
                                    - containerStyle.paddingTop
                                    - containerStyle.paddingBottom
                                    - labelStyle.marginTop
                                    - labelStyle.marginBottom;

            float safeWidth = availableWidth * SAFETY_MARGIN;
            label.style.maxWidth = safeWidth;
            float labelHeight = label.contentRect.height;

            //if the label's height exceeds the available height, reduce the font size until it fits
            if (labelHeight > availableHeight)
            {
                float currentFontSize = labelStyle.fontSize;
                while (labelHeight > availableHeight && currentFontSize > 10) //min font size of 10
                {
                    currentFontSize--;
                    label.style.fontSize = currentFontSize;

                    label.schedule.Execute(() => { labelHeight = label.contentRect.height; });
                }
            }
        }
    }
}
