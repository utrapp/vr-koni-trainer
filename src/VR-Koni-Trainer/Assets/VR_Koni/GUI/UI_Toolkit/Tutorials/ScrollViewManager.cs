using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using VR_Koni;
using VR_Koni.GUI;
using VR_Koni.GUI.UI_Toolkit_Components.PopupWindow;

public class ScrollViewManager
{
    private VisualElement rootElement;

    public void SetRootElement(VisualElement newRootElement)
    {
        rootElement = newRootElement;
        RegisterCardClickEvents();
    }

    private void RegisterCardClickEvents()
    {
        ScrollView scrollView = rootElement.Q<ScrollView>();
        VisualElement content = scrollView.Q("Content");
        foreach (VisualElement card in content.Children())
        {
            card.RegisterCallback<ClickEvent>(evt => OnCardClick(evt, card));
        }
    }

    private void OnCardClick(ClickEvent evt, VisualElement card)
    {
        string lessonName =  card.name;
        int lessonNumber = int.Parse(lessonName.Replace("Lesson", ""));
        PlayerPrefs.SetInt("lesson", lessonNumber);

        string sceneName = "";
        switch (rootElement.name)
        {
            case "StartInVRMainPanelTemplate":
                sceneName = "Tutorials_Start_In_VR_Scene";
                break;
            case "SimulationMainPanelTemplate":
                sceneName = "Tutorials_Simulation_Scene";
                break;
            case "StatisticsMainPanelTemplate":
                sceneName = "Tutorials_Statistics_Scene";
                break;
        }

        if (sceneName == "Tutorials_Simulation_Scene")
        {
            BasePopupWindow popup = new BasePopupWindow();
            VisualElement rootVisualTreeElement = rootElement.panel.visualTree;
            rootVisualTreeElement.Add(popup);
            popup.SetHeaderText("Wechsel in VR-Umgebung");
            popup.SetDescriptionText("Bitte bestätigen Sie den Wechsel und setzen Sie anschließend die VR Brille auf.");
            popup.SetRightButtonAction("Abbrechen", () => rootVisualTreeElement.Remove(popup));
            popup.SetLeftButtonAction("Bestätigen", () => ApplicationManager.StartIEnumerator(SceneHelper.LoadScene(sceneName, LoadSceneMode.Single)));
        }
        else
        {
            ApplicationManager.StartIEnumerator(SceneHelper.LoadScene(sceneName, LoadSceneMode.Single));
        }

    }
}
