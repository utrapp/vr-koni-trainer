using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Linq;
using JetBrains.Annotations;
using Mono.Data.Sqlite;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using VR_Koni.ElectroSnare;
using VR_Koni.UserData.Export;

namespace VR_Koni.UserData
{
    /// <summary>
    /// Class <c>DataManager</c> acts as an interface between the backend (namespace UserData) and the ui-integration.
    /// To ensure that there's only one instance in each scene of the application, it's implemented as singleton.
    /// </summary>
    public class DataManager
    {
        private static DataManager instance;

        public static DataManager Instance
        {
            get
            {
                if (instance == null)
                    SetupInstance();
                return instance;
            }
        }

        [CanBeNull]
        public User CurrentUser { get; private set; }

        private float operationStartTime;
        private float operationEndTime;
        private float contactTime;
        private double volume;
        private GameObject cancerCutout;

        private const string DB_BACKUP_PATH = "_backup";

        private readonly List<Tuple<float, float>> cuttingData = new();

        public delegate bool ExportDelegate(List<Surgery> surgeries, string filename);

        public delegate void SurgeryEnded(Surgery s);

        public static event SurgeryEnded OnSurgeryEnded;
        public ExportDelegate ExportStrategy { get; private set; }

        /// <summary>
        /// Constructor <c>DataManager</c> of DataManager is private to disable instantiating.
        /// </summary>
        private DataManager()
        {
            ExportStrategy = ExportExcel.Export;
        }

        /// <summary>
        /// <c>SetupInstance</c> Instantiate a new instance into the scene.
        /// </summary>
        private static void SetupInstance()
        {
            if (instance == null)
            {
                instance = new DataManager();
                //if the koni.sqlite file is missing, then instantiate the database structure using crebas.sql
                if (!File.Exists(GetDbPath()))
                {
                    Debug.LogWarning($"File {GetDbPath()} not found!");
                    string crebas = Resources.Load<TextAsset>("crebas.sql").text;
                    IDbConnection con = instance.OpenConnection();
                    ExecuteQuery(con, crebas);
                    instance.CloseConnection(con);
                    SaveBackup();
                }
            }
        }

        private static string GetDbPath(bool withDbName = true)
        {
            if (withDbName)
            {
                return System.IO.Path.Combine(Application.persistentDataPath, "koni.sqlite");
            }

            return Application.persistentDataPath;
        }

        // TODO: think of a better solution for this mess
        private IDbConnection OpenConnection(string dbPath = "")
        {
            if (string.IsNullOrEmpty(dbPath))
            {
                dbPath = GetDbPath();
            }

            // Create a database connection string
            string dbUri = $"URI=file:{dbPath}";
            IDbConnection dbConnection = new SqliteConnection(dbUri);
            dbConnection.Open();
            return dbConnection;
        }

        private void CloseConnection(IDbConnection con)
        {
            con.Close();
        }

        private static IDataReader ExecuteQuery(IDbConnection con, string query)
        {
            IDbCommand command = con.CreateCommand();
            command.CommandText = query;
            IDataReader dataReader = command.ExecuteReader();

            return dataReader;
        }


        /// <summary>
        /// <c>CreateUser</c> creates a new User into the DataBase.
        /// Returns <see langword="false"/> if the username is already in use.
        /// </summary>
        /// <param name="username">Username of the new User</param>
        /// <param name="password">Password (unencrypted) of the new User</param>
        /// <returns><see langword="true "/> if a new User is created, <see langword="false"/> if the new User can't be created,
        /// either due to a DataBase failure or an invalid input.</returns>
        public bool CreateUser(string username, string password)
        {
            bool creationSuccess = false;

            try
            {
                IDbConnection con = OpenConnection();

                const string USER_INSERT_QUERY = @"
            INSERT INTO user (username, password, privilege)
            VALUES (@username, @password, @privilege);
        ";

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = USER_INSERT_QUERY;
                    cmd.Parameters.Add(CreateParameter(cmd, "@username", new SecureString(username)));
                    cmd.Parameters.Add(CreateParameter(cmd, "@password", new SecureString(password)));
                    cmd.Parameters.Add(CreateParameter(cmd, "@privilege", (int)User.UserPrivileges.Participant));

                    cmd.ExecuteNonQuery();
                }

                const string PREF_INSERT_QUERY = @"
            INSERT INTO user_preferences (username, language, volume)
            VALUES (@username, @language, @volume);
        ";

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = PREF_INSERT_QUERY;
                    cmd.Parameters.Add(CreateParameter(cmd, "@username", new SecureString(username)));
                    cmd.Parameters.Add(CreateParameter(cmd, "@language", "en"));
                    cmd.Parameters.Add(CreateParameter(cmd, "@volume", 1.0));

                    cmd.ExecuteNonQuery();
                }

                CloseConnection(con);
                creationSuccess = true;

                Debug.Log($"User {username} with default preferences has been created.");
            }
            catch (SqliteException exp)
            {
                Debug.LogWarning($"Failed to create user {username}: {exp.Message}");
            }

            return creationSuccess;
        }

        /// <summary>
        /// <c>LogInUser</c> fetches an existing User from the DataBase.
        /// If the username or the password aren't correct, <c>LogInUser</c> returns false.
        /// </summary>
        /// <param name="username">Username of the User</param>
        /// <param name="password">Password (not encrypted) of the User</param>
        /// <returns>If the username and password (encrypted) match to a DataBase entry, true is returned.
        /// If the User can't be retrieved from the DataBase, false is returned</returns>
        public bool LogInUser(string username, string password)
        {
            CurrentUser = null;
            bool loginSuccess = false;

            IDbConnection con = OpenConnection();

            try
            {
                IDataReader userResult = ExecuteQuery(con, $@"
                    SELECT password, privilege, has_seen_overlay
                    FROM user
                    WHERE user.username = '{new SecureString(username)}';
                ");

                if (userResult.Read())
                {
                    SecureString hashedPw = new(userResult.GetString(0), true);
                    int privilege = userResult.GetInt16(1);
                    bool hasSeenOverlay = userResult.GetBoolean(2);
                    loginSuccess = hashedPw.CheckString(password);

                    if (loginSuccess)
                    {
                        IDataReader preferencesResult = ExecuteQuery(con, $@"
                            SELECT language, volume
                            FROM user_preferences
                            WHERE user_preferences.username = '{new SecureString(username)}';
                        ");

                        string preferredLanguage = "en";
                        float preferredVolume = 1.0f;
                        if (preferencesResult.Read())
                        {
                            preferredLanguage = preferencesResult.GetString(0);
                            preferredVolume = preferencesResult.GetFloat(1);
                        }
                        else
                        {
                            Debug.LogWarning($"Preferences not found for user {username}. Using defaults.");
                        }
                        UserPreferences preferences= new UserPreferences(preferredVolume, preferredLanguage);
                        CurrentUser = new User(username, password, privilege, hasSeenOverlay: hasSeenOverlay, userPreference: preferences);
                        FetchUserSurgeries();
                        UpdatePreferencesInGame(preferences);
                    }
                }

                if (loginSuccess)
                {
                    Debug.Log($"Login of user {username} succeeded");
                }
                else
                {
                    Debug.LogWarning($"Login of user {username} failed");
                }
            }
            catch (Exception ex)
            {
                Debug.LogError($"Error during login: {ex.Message}");
                loginSuccess = false;
            }
            finally
            {
                CloseConnection(con);
            }

            return loginSuccess;
        }
        
        /// <summary>
        /// Updates in-game user preferences, including language and volume settings.
        /// </summary>
        /// <param name="preferences">The user preferences containing language and volume settings.</param>
        private void UpdatePreferencesInGame(UserPreferences preferences)
        {
            Locale currentLocale = Locale.CreateLocale(preferences.PreferredLanguage);
            LocalizationSettings.SelectedLocale = currentLocale;

            AudioListener.volume = (float)preferences.PreferredVolume;
        }

        private List<Surgery> FetchSurgeriesWithoutCuttingData(string query, bool useDBName = false)
        {
            List<Surgery> surgeryList = new();

            IDbConnection con = OpenConnection();
            IDataReader result = ExecuteQuery(con, query);

            while (result.Read())
            {
//query = "username, depth, lletz, contacttime, volume, operationtime, operationdate, numofcuts, meshdata, imagepath, surgeryid"
                string name = useDBName ? result.GetString(0) : CurrentUser?.Clearname;
                surgeryList.Add(new Surgery(username: name,
                                            depth: result.GetFloat(1),
                                            lletz: result.GetInt32(2),
                                            contactTime: result.GetFloat(3),
                                            volume: result.GetFloat(4),
                                            operationTime: result.GetFloat(5),
                                            date: DateTime.ParseExact(result.GetString(6), "dd.MM.yyyy HH:mm:ss",
                                                                      CultureInfo.InvariantCulture),
                                            numOfCuts: result.GetInt16(7),
                                            serializedGameObject: result.GetString(8),
                                            imagePaths: result.GetString(9).Split(','),
                                            surgeryId: result.GetInt16(10)
                                           ));
            }

            CloseConnection(con);
            return surgeryList;
        }

        /// <summary>
        /// gets the surgeries from the database but nests it in a List of List of Surgery for better visualization
        /// and usage during the excel export.
        /// Main difference from <see cref="GetAllSurgeries"/> is that we leverage LINQ to return a nested list and also
        /// use <see cref="Surgery.SQLDepthHeader"/> to extract the surgery data including the
        /// <see cref="Surgery.CuttingData"/>.
        /// </summary>
        /// <returns></returns>
        [CanBeNull]
        public List<List<Surgery>> GetSurgeriesGroupedByUser(List<Surgery>? externalSurgeries = null)
        {
            //fetch surgeries using SQLDepthHeader to include depth-related data
            List<Surgery> surgeries = externalSurgeries ?? FetchSurgeriesWithoutCuttingData($@"
                SELECT {Surgery.SQLDepthHeader()}, surgeryid
                FROM surgery
                ORDER BY username;", true);

            return surgeries?.GroupBy(surgery => surgery.Username)
                            .Select(group => group.ToList())
                            .ToList();
        }

        private bool FetchUserSurgeries()
        {
            if (CurrentUser == null)
                return false;

            List<Surgery> surgeries = FetchSurgeriesWithoutCuttingData($@"
                SELECT {Surgery.SQLHeader()}, surgeryid
                FROM surgery
                WHERE surgery.username = '{CurrentUser.Clearname}'
                ORDER BY operationdate ASC;
            ");

            CurrentUser.Surgeries.Clear();

            foreach (Surgery s in surgeries)
            {
                CurrentUser.AddSurgery(s);
            }

            CurrentUser.Surgeries = CurrentUser.Surgeries.OrderBy(x => x.Date).ToList();

            return true;
        }

        /// <summary>
        /// <c>LogInGuest</c> enters the simulator with a guest-User.
        /// </summary>
        public void LogInGuest()
        {
            //TODO: check if username is in DataBase. If so, check that password is valid. If valid return User, if invalid return null
            if (CurrentUser != null && CurrentUser.IsGuest()) return;
            CurrentUser = User.CreateGuest();
            UpdatePreferencesInGame(CurrentUser?.Preferences);
        }

        /// <summary>
        /// Creates a debug user if it doesn't exist and logs in with it.
        /// Username: debug
        /// Passsword: debug
        /// </summary>
        public void LogInDebugUser()
        {
            const string DEBUG_USER_NAME = "debug";
            if (CurrentUser != null && CurrentUser.Clearname.Equals(DEBUG_USER_NAME))
            {
                return;
            }

            if (!LogInUser(DEBUG_USER_NAME, DEBUG_USER_NAME))
            {
                CreateUser(DEBUG_USER_NAME, DEBUG_USER_NAME);
            }

            LogInUser(DEBUG_USER_NAME, DEBUG_USER_NAME);
        }

        /// <summary>
        /// <c>LogOutUser</c> logs out the currentUser and writes it's (new) data into the Database.
        /// The data that needs to be written is detected using the "needsUpdate"-flag.
        /// </summary>
        public void LogOutUser()
        {
            WriteCurrentUser();
            if (CurrentUser != null && CurrentUser.IsGuest())
            {
                CurrentUser.Surgeries.Clear();
            }

            CurrentUser = null;
        }

        /// <summary>
        /// <c>Shutdown</c> is used to gracefully shutdown the system.
        /// The data of the actual data is datad into the Database.
        /// The data that needs to be written is detected using the "needsUpdate"-flag.
        /// </summary>
        public void Shutdown()
        {
            LogOutUser();
        }

        /// <summary>
        /// <see cref="WriteCurrentUser"/> writes new data of the User into the DataBase.
        /// This includes all Surgeries that the user did.
        /// The data that needs to be written is detected using the <see cref="Surgery.NeedsUpdate"/> flag.
        /// </summary>
        /// <returns><see langword="true"/> if update succeeded, <see langword="false"/> otherwise.</returns>
        /// <exception cref="SqliteException"></exception>
        public bool WriteCurrentUser()
        {
            if (CurrentUser == null
                || CurrentUser.Privileges == User.UserPrivileges.Guest
                || !CurrentUser.NeedSurgeryUpdate())
            {
                Debug.Log($"{nameof(WriteCurrentUser)}: Skipping insertion of data for the current user " +
                          $"to the DB User Status: {CurrentUser?.Privileges} with {nameof(CurrentUser.NeedSurgeryUpdate)}: " +
                          $"{CurrentUser?.NeedSurgeryUpdate()}");
                return false;
            }
            
            //if both succeeded, return true else return false indicating smth didnt go as planned
            return 
                WriteSurgeries() 
                && 
                WriteOrUpdateUserPreferences();
        }

        /// <summary>
        /// Writes surgeries that require updates to the database.
        /// </summary>
        /// <returns><see langword="true"/> if surgeries were written successfully, <see langword="false"/> otherwise.</returns>
        private bool WriteSurgeries()
        {
            // Write surgeries to the database
            foreach (Surgery surgery in CurrentUser.Surgeries.Where(x => x.NeedsUpdate))
            {
                IDbConnection con = OpenConnection();
                try
                {
                    const string QUERY = @"
            INSERT INTO surgery 
                (username, 
                 operationdate,
                 depth, lletz, 
                 contacttime, 
                 operationtime,
                 volume, 
                 numofcuts,
                 depthdata, 
                 meshdata, 
                 imagePath)
            VALUES
                (@username, 
                 @operationdate, 
                 @depth, @lletz,
                 @contacttime,
                 @operationtime,
                 @volume,
                 @numofcuts,
                 @depthdata,
                 @meshdata,
                 @imagePath)";

                    using IDbCommand cmd = con.CreateCommand();
                    cmd.CommandText = QUERY;

                    // Add parameters using the GetSqlParameters method
                    ICollection<IDbDataParameter> parameters = surgery.GetSqlParameters();
                    foreach (IDbDataParameter param in parameters)
                    {
                        cmd.Parameters.Add(param);
                    }

                    cmd.ExecuteNonQuery();
                    // Since we updated the surgery, it no longer needs to be updated
                    surgery.NeedsUpdate = false;
                }
                catch (Exception ex) when (ex is SqliteException)
                {
                    Debug.LogError(ex.Message);
                    return false;
                }
                finally
                {
                    CloseConnection(con);
                }
            }
            Debug.Log($"{nameof(WriteSurgeries)}: Successfully wrote all surgery records for the user.");
            return true;
        }

        /// <summary>
        /// <see cref="WriteOrUpdateUserPreferences"/> writes or updates the user preferences in the database.
        /// If the preference exists, it updates the existing preferences using UPDATE query, else
        /// it creates a new preference for this user
        /// </summary>
        /// <returns><see langword="true"/> if preferences were written or updated successfully, <see langword="false"/> otherwise.</returns>
        public bool WriteOrUpdateUserPreferences()
        {
            try
            {
                IDbConnection con = OpenConnection();

                //checks if preferences exist
                const string CHECK_QUERY = "SELECT COUNT(*) FROM user_preferences WHERE username = @username";
                using (IDbCommand checkCmd = con.CreateCommand())
                {
                    checkCmd.CommandText = CHECK_QUERY;
                    checkCmd.Parameters.Add(CreateParameter(checkCmd, "@username", CurrentUser.Username));

                    long count = (long)checkCmd.ExecuteScalar();
                    
                    //if pref existed, update it
                    if (count > 0)
                    {
                        const string UPDATE_QUERY = @"
                UPDATE user_preferences
                SET language = @language,
                    volume = @volume
                WHERE username = @username";

                        using IDbCommand updateCmd = con.CreateCommand();
                        updateCmd.CommandText = UPDATE_QUERY;
                        updateCmd.Parameters.Add(CreateParameter(updateCmd, "@username", CurrentUser.Username));
                        updateCmd.Parameters.Add(CreateParameter(updateCmd, "@language",
                                                                 CurrentUser.Preferences.PreferredLanguage));
                        updateCmd.Parameters.Add(CreateParameter(updateCmd, "@volume",
                                                                 CurrentUser.Preferences.PreferredVolume));
                        updateCmd.ExecuteNonQuery();

                        Debug.Log($"{nameof(WriteOrUpdateUserPreferences)}: Preferences for " +
                                  $"user {CurrentUser.Clearname} ({CurrentUser.Username}) updated successfully.");
                    }
                    //else add a new pref for this user
                    else
                    {
                        const string INSERT_QUERY = @"
                INSERT INTO user_preferences (username, language, volume)
                VALUES (@username, @language, @volume)";

                        using IDbCommand insertCmd = con.CreateCommand();
                        insertCmd.CommandText = INSERT_QUERY;
                        insertCmd.Parameters.Add(CreateParameter(insertCmd, "@username", CurrentUser.Username));
                        insertCmd.Parameters.Add(CreateParameter(insertCmd, "@language",
                                                                 CurrentUser.Preferences.PreferredLanguage));
                        insertCmd.Parameters.Add(CreateParameter(insertCmd, "@volume",
                                                                 CurrentUser.Preferences.PreferredVolume));
                        insertCmd.ExecuteNonQuery();

                        Debug.Log($"{nameof(WriteOrUpdateUserPreferences)}: Preferences for " +
                                  $"user {CurrentUser.Clearname} ({CurrentUser.Username}) inserted successfully.");
                    }
                }
            }
            catch (Exception ex) when (ex is SqliteException)
            {
                Debug.LogError($"{nameof(WriteOrUpdateUserPreferences)}: Error updating or inserting " +
                               $"preferences for user {CurrentUser.Username} ({CurrentUser.Username}): {ex.Message}");
                return false;
            }
            
            Debug.Log($"{nameof(WriteOrUpdateUserPreferences)} Successfully updated all user preferences.");
            return true;
        }


        private IDbDataParameter CreateParameter(IDbCommand cmd, string name, object value)
        {
            IDbDataParameter param = cmd.CreateParameter();
            param.ParameterName = name;
            param.Value = value ?? DBNull.Value;
            return param;
        }


        public string FetchCuttingData(int surgeryId)
        {
            IDbConnection con = OpenConnection();
            IDataReader result = ExecuteQuery(con, $@"
                SELECT depthdata
                FROM surgery
                WHERE surgery.surgeryid = {surgeryId};
            ");

            return result.Read() ? result.GetString(0) : string.Empty;
        }

        /// <summary>
        /// <c>GetAllUsers</c> get all Users from the DataBase. This functionality is only available for Users with
        /// <see cref="User.UserPrivileges.Admin"/> privileges.
        /// </summary>
        /// <returns>Returns List of Users. If none is found, the returned List is empty.
        /// Calling the function without having admin-privileges returns null</returns>
        [CanBeNull]
        public List<User> GetAllUsers()
        {
            if (CurrentUser?.Privileges != User.UserPrivileges.Admin)
            {
                Debug.Log($"{nameof(GetAllUsers)}: You are not an admin, hence cannot access all users data. Privilege:{CurrentUser?.Privileges}");
                return null;
            }

            //TODO: get all Users from the DataBase
            List<User> userList = new();

            IDbConnection con = OpenConnection();

            IDataReader result = ExecuteQuery(con, $@"
                SELECT username privilage
                FROM user;
            ");

            while (result.Read())
            {
                User user = new(result.GetString(0), "****", result.GetInt16(0));
                userList.Add(user);
            }

            CloseConnection(con);

            return userList;
        }

        /// <summary>
        /// <c>GetAllSurgeries</c> get all Surgeries from the DataBase. This functionality is only available for Users with admin-privileges!
        /// </summary>
        /// <returns>Returns List of Surgeries. If none is found, the returned List is empty.
        /// Calling the function without having admin-privileges returns null</returns>
        [CanBeNull]
        public List<Surgery> GetAllSurgeries()
        {
            return CurrentUser?.Privileges == User.UserPrivileges.Admin
                ? FetchSurgeriesWithoutCuttingData($@"
                SELECT {Surgery.SQLHeader()}, surgeryid
                FROM surgery
                ORDER BY username;")
                : null;
        }

        public List<int> GetLastLletz(int amount)
        {
            List<int> lletzList = new();

            IDbConnection con = OpenConnection();
            IDataReader result = ExecuteQuery(con, $@"
            SELECT lletz
            FROM surgery
            WHERE surgery.username = '{CurrentUser.Username}';
            ORDER BY surgery.operationdate DESC
            LIMIT {amount};
        ");

            while (result.Read())
            {
                lletzList.Add(result.GetInt16(0));
            }

            CloseConnection(con);
            return lletzList;
        }


        /// <summary>
        /// <c>ExportUserStats</c> exports stats (surgeries) that are stored in the DataBase and belong to a certain user
        /// into a file.
        /// </summary>
        /// <param name="user">User that surgeries should be exported</param>
        /// <param name="path">Path of the desired output-file</param>
        /// <returns>true if the export succeed, false if not</returns>
        public bool ExportUserStats(User user, string path)
        {
            //TODO: export all Surgeries as Excel
            Debug.Log($"ExportUserStats({user.Username}, {path})");

            throw new NotImplementedException();
        }

        /// <summary>
        /// <c>ExportAllStats</c> exports all stats (surgeries) that are stored in the DataBase into a file. This functionality is only available for Users with admin-privileges!
        /// </summary>
        /// <param name="path">Path of the desired output-file</param>
        /// <returns>true if the export succeed, false if not</returns>
        public bool ExportAllStats(string path = "surgeries")
        {
            if (CurrentUser?.Privileges != User.UserPrivileges.Admin)
                return false;

            List<Surgery> surgeries = GetAllSurgeries();

            //Export as Excel
            ExportStrategy = ExportExcel.Export;
            ExportStrategy(surgeries, path);

            //Export as .csv
            ExportStrategy = ExportCSV.Export;
            ExportStrategy(surgeries, path);

            Debug.Log($"ExportAllStats({path})");

            return true;
        }

        public void OperationReset()
        {
            cuttingData.Clear();
            operationStartTime = 0f;
            operationEndTime = 0f;
            Timer.OnTimeChange -= ReceiveCuttingTime;
            ElectroSnareVR.onVolumeChange -= ReceiveVolume;
            GameManager.OnGameStateChanged -= UpdateGameState;
            ElectroSnareVR.onCuttingLengthChange -= ReceiveCuttingDepth;
        }

        public void OperationStart()
        {
            if (operationStartTime == 0f)
            {
                operationStartTime = Time.time;
                Debug.Log("Operation Start");
                Timer.OnTimeChange += ReceiveCuttingTime;
                ElectroSnareVR.onVolumeChange += ReceiveVolume;
                GameManager.OnGameStateChanged += UpdateGameState;
                ElectroSnareVR.onCuttingLengthChange += ReceiveCuttingDepth;
                CuttingAndGeneration.OnCutoutGenerated += ReceiveCancerCutout;
            }
        }

        private void UpdateGameState(GameManager.GameState newState)
        {
            if (newState is GameManager.GameState.Victory or GameManager.GameState.Loose)
            {
                OperationEnd();
                GameManager.OnGameStateChanged -= UpdateGameState;
            }
        }

        public void UpdateHasSeenOverlay()
        {
            if (CurrentUser == null) return;

            IDbConnection con = OpenConnection();
            ExecuteQuery(con, $@"
                UPDATE user
                SET has_seen_overlay = 1
                WHERE username = '{CurrentUser.Username}';
            ");
            CloseConnection(con);

            CurrentUser.HasSeenOverlay = true;
        }


        private void ReceiveCancerCutout(GameObject cutout)
        {
            cancerCutout = cutout;
        }

        private void ReceiveCuttingTime(float cuttingTime)
        {
            contactTime = cuttingTime;
        }

        private void ReceiveVolume(double newVolume)
        {
            volume = newVolume;
        }

        private void ReceiveCuttingDepth(float depth)
        {
            if (depth != 0 || cuttingData.Any())
                cuttingData.Add(new Tuple<float, float>(Time.time, depth));
        }

        public void OperationEnd()
        {
            operationEndTime = Time.time;

            float maxDepth = cuttingData.Max(x => x.Item2);

            Surgery s = new(CurrentUser,
                            operationEndTime - operationStartTime,
                            contactTime,
                            maxDepth * 1000f,
                            volume,
                            cuttingData,
                            GameObjectSerializer.SerializeGameObject(cancerCutout)
                           );

            CurrentUser.AddSurgery(s);
            if (CurrentUser.IsAdmin() || CurrentUser.IsUser())
            {
                WriteCurrentUser();
                FetchUserSurgeries();
            }

            OnSurgeryEnded(s);

            OperationReset();
        }

        public static void SaveBackup()
        {
            string backupPath = GetBackupPath();
            Directory.CreateDirectory(backupPath);

            string target =
                Path.Combine(backupPath,
                             @$"{DateTime.Now.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)}.sqlite");

            File.Copy(GetDbPath(), target, true);
            List<string> files = GetBackupFiles();
            if (files.Count > 10)
            {
                files.Sort();
                File.Delete(files[0]);
            }
        }

        private static string GetBackupPath()
        {
            return Path.Combine(GetDbPath(false), DB_BACKUP_PATH);
        }

        public bool LoadBackup(string filepath)
        {
            bool success;

            try
            {
                File.Copy(filepath, "koni.sqlite", true);
                success = true;
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }

        public static List<string> GetBackupFiles()
        {
            return Directory.GetFiles(GetBackupPath()).ToList();
        }
    }
}
