using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace VR_Koni.UserData
{
    /// <summary>
    /// Class <c>User</c> models a user that can train some surgeries in the simulator.
    /// </summary>
    [Serializable]
    public class User
    {
        public enum UserPrivileges
        {
            Guest,
            Participant,
            Admin,
        }

        public UserPrivileges Privileges { get; private set; }

        public string Clearname { get; private set; }

        public SecureString Username { get; private set; }

        public SecureString UserPassword { get; private set; }

        public List<Surgery> Surgeries;

        public bool HasSeenOverlay { get; set; }

        public bool NeedsUpdate;

        /// <summary>
        /// Gets or sets the user preferences.
        /// </summary>
        public UserPreferences Preferences { get; set; }

        /// <summary>
        /// <c>User</c> constructs an instance of the class User, could either be used manual or by creating an instance from the DB.
        /// </summary>
        /// <param name="clearname">Username.</param>
        /// <param name="clearUserPassword">Password of the User.</param>
        /// <param name="privilege">UserPrivileges of the User. Admin or normal User</param>
        /// <param name="surgeries">List of all Surgeries by the User.
        /// If the Surgeries don't exist at the moment of creation, use the default argument</param>
        /// <param name="needsUpdate">Shows that local (unity) data isn't synced with the DataBase.
        /// <param name="userPreference"> denotes the preferences of this user</param>
        /// <param name="hasSeenOverlay"> determines if the user has seen the overlay</param>
        /// If created manually, use the default argument.</param>
        public User(
            string clearname,
            string clearUserPassword,
            int privilege,
            List<Surgery> surgeries = null,
            bool needsUpdate = true,
            bool hasSeenOverlay = false,
            [CanBeNull] UserPreferences userPreference = null
        )
        {
            Privileges = (UserPrivileges)privilege;
            Username = new SecureString(clearname);
            Clearname = clearname;
            UserPassword = new SecureString(clearUserPassword);
            Surgeries = surgeries ?? new List<Surgery>(); //if surgeries is null instantiate new one
            NeedsUpdate = needsUpdate;
            HasSeenOverlay = hasSeenOverlay;
            Preferences = userPreference ?? new UserPreferences();
        }


        /// <summary>
        /// <c>Guest</c> creates a new User with Guest-role.
        /// </summary>
        public static User CreateGuest()
        {
            UserPreferences guestPreferences = new UserPreferences(1.0f, "en");
            return new User(
                            "Guest",
                            "Guest",
                            (int)UserPrivileges.Guest,
                            userPreference: guestPreferences);
        }

        /// <summary>
        /// <c>AddSurgery</c> adds a new Surgery to the surgeries of the User.
        /// </summary>
        /// <param name="surgery">Surgery that needs to be added.</param>
        public void AddSurgery(Surgery surgery)
        {
            //TODO: encrypted username
            if (Privileges != UserPrivileges.Guest)
            {
                surgery.Username = Clearname;
                Surgeries.Add(surgery);
            }
        }

        public bool NeedSurgeryUpdate()
        {
            return Surgeries.Count(x => x.NeedsUpdate) > 0;
        }

        public Surgery GetLastSurgery()
        {
            //TODO: order by date to last
            return Surgeries.Last();
        }

        public bool IsGuest() => Privileges == UserPrivileges.Guest;

        public bool IsAdmin() => Privileges == UserPrivileges.Admin;

        public bool IsUser() => Privileges == UserPrivileges.Participant;
    }
}
