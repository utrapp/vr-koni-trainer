# Usability Test Tasks - Experienced User

## Thinking Aloud

[Link to the Thinking Aloud file](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/blob/DesignDocumentation-%23140-SAE/ux/usabillity_tests/2024/thinking_aloud.md)

## Tasks

### Pre-Menu

#### Task 1

**Scenario**
Als erfahrener Benutzer von KONI-Trainer möchten Sie sich erst in Ihren Account einloggen und geben versehentlich das falsche Passwort ein.

- Sind die geforderten Eingaben für Sie klar, und können Sie sie schnell ausfüllen?
- Ist die Anzeige für Sie klar, und wie schnell können Sie das Problem beheben?
- Welche Funktionen nutzen Sie, um den Login-Prozess zu beschleunigen?

**Inputs**

- Sind die verschiedenen Anzeigen und Eingabefelder für Sie klar und intuitiv?
- Wie effizient empfinden Sie den Login-Prozess im Vergleich zu anderen Anwendungen?
- Können Sie nun versuchen, sich erneut anzumelden, aber diesmal mit den richtigen Informationen

### Menu

#### Task 2

**Scenario**
Nach dem Login sehen Sie sich das Hauptmenü an. Können Sie das Menü schnell durchschauen und erklären, was Sie sehen?

**Inputs**

- Wie effizient finden Sie die verschiedenen Funktionen im Hauptmenü?
- Ist die Schriftgröße und -art weiterhin angemessen?
- Welche Verbesserungsvorschläge haben Sie für die Navigierbarkeit des Menüs?

#### Task 3

**Scenario**
Als erfahrener Benutzer, der bereits Erfahrung mit Konisation hat, nutzen Sie den Koni Trainer erneut. Wie machen Sie sich mit der Steuerung vertraut?

**Inputs**

- Fühlen Sie sich bereits mit der Steuerung vertraut, oder gibt es Bereiche, die Sie nochmals überprüfen möchten?
- Sind die bereitgestellten Informationen für Sie ausreichend, oder wünschen Sie sich zusätzliche Details?
- Haben Sie Vorschläge zur Verbesserung der Steuerungsdarstellung oder -erklärung?

#### Task 4

**Scenario**
Vor dem Start des ersten Trainings möchten Sie die Lautstärke anpassen. Wie gehen Sie dabei vor?

**Inputs**

- Waren die Einstellungen für Sie intuitiv und schnell zugänglich?
- Gibt es weitere Anpassungen, die Sie in dieser Phase vornehmen möchten?

#### Task 5

**Scenario**
Sie möchten jetzt Ihr erstes Training starten. Wie gehen Sie dabei vor?

**Inputs**

- War der Weg bis zum Start des Trainings effizient und gut nachvollziehbar?
- Gab es Anweisungen, die Sie bereits kennen, oder haben Sie noch zusätzliche Anmerkungen?

### Im VR-Modus

#### Task 6

**Scenario**
Während Ihres ersten Trainings möchten Sie die Schlinge im Raum testen. Wie gehen Sie dabei vor?

**Tasks**

- Demonstrieren Sie das Ein- und Ausschalten der Schlinge.
- Zeigen Sie das Ablegen der Schlinge.
- Führen Sie Dreh- und Kippbewegungen mit der Schlinge aus.

**Inputs**

- Wie leicht fiel Ihnen die Bedienung der Schlinge?
- Konnten Sie die Schlinge ohne Probleme greifen?
- Wie empfinden Sie das akustische Feedback im Vergleich zu Ihrer Erfahrung?

#### Task 7

**Scenario**
Vor der Trainings-OP möchten Sie das zu entfernende Gewebe genauer betrachten. Wie gehen Sie dabei vor?

**Inputs**

- War die Lupe leicht für Sie zu finden und zu nutzen?
- Erfüllte die vergrößerte Ansicht (Lupe) Ihre Erwartungen?
- Konnten Sie das zu entfernende Gewebe klar erkennen?
- Beenden Sie die vergrößerte Ansicht.

#### Task 8

**Scenario**
Sie möchten nun Ihre erste Trainings-OP durchführen. Wie gehen Sie dabei vor?

**Inputs**

- Wie ist Ihr Eindruck nach der OP?
- Gab es während der OP alle notwendigen Informationen?
- Möchten Sie zusätzliche Informationen während der OP erhalten?
- Wie empfinden Sie das akustische Feedback? Sind die Bedeutungen klar verständlich?
- Wie fühlt sich die Handhabung und Steuerung während der OP für Sie an?

#### Task 9

**Scenario**
Sie möchten nun eine weitere OP durchführen (repeat Funktion).

**Inputs**

- War es einfach für Sie, eine weitere OP zu wiederholen?

#### Task 10

**Scenario**
Nach dem Training möchten Sie Ihre Ergebnisse überprüfen. Wie gehen Sie dabei vor?

**Inputs**

- Sind alle angezeigten Werte klar und gut erkennbar?
- Wie erleben Sie die Navigation zwischen den Bildschirmen?
- Gibt es zusätzliche Daten, die Sie gerne sehen würden?
- Wie empfinden Sie die Darstellung der Konisation im 3D-Schnitt?

#### Task 11

**Scenario**
Sie möchten nun die Entwicklung Ihrer Ergebnisse über die letzten Trainingseinheiten ansehen. Wie gehen Sie dabei vor?

**Inputs**

- Sind alle dargestellten Werte für Sie klar und leicht verständlich?
- Wie finden Sie die Darstellung der Werte im Verlauf (Graphen)?

### Back to Menu

#### Task 12

**Scenario**
Jetzt möchten Sie sich Ihre Statistiken nochmal ansehen, aber am normalen Computerbildschirm. Wie gehen Sie dabei vor?

**Inputs**

- Ist der Übergang zu den Statisteken klar?

#### Task 13

**Scenario**
Sie möchten nun Ihre OP-Statistiken exportieren und einer separaten Datei herunterladen und anzeigen. Wie würden Sie vorgehen?

**Inputs**

- Ist der Übergang zur Exportfunktion klar?
- wissen Sie, wie Sie die erzeugte Datei finden?

nachdem Sie die Datei gefunden haben: öffnen Sie sie und sehen Sie sie sich an.

- können Sie kurz erklären, was die Daten bedeuten

### Task 14

**Scenario**
Sie haben endlich alles abgeschlossen was Sie am Koni Trainer machen wollten. Sie wollen das Programm nun beenden. Wie gehen Sie vor?

**Inputs**

- Ist es Ihnen leicht/schwer gefallen das Programm zu beenden?
- War die Aktion hinter den Buttons die, die Sie erwartet haben?
