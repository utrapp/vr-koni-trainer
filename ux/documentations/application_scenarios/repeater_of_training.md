## Wiederholer des Trainings ## 
### Akteur: ###
Wiederholer des Trainings und gleichzeitig angehender Mediziner

### Kurze Zusammenfassung: ###
Der Benutzer übernimmt die Rolle eines angehenden Mediziners, der bereits grundlegende Erfahrungen mit dem VR-Simulator gesammelt hat. Er probiert nun erneut die VR-Simulation aus.

### Vorbedingungen: ###
Er hat die Sumulation schonmal verwendet und weiß im besten Fall wie es funktioniert.

### Hauptverlauf: ###
Der Akteur fängt die Simulation an. Er nimmt die Elektroschlinge, schaltet sie an und schaut
sich an wo die Krebszellen sind. Er schaut um die Lupe herum wo genau sich die Portio
befindet um besser abschätzen zu können wo er gleich das Gewebe entfernen muss. Er
fängt daraufhin an langsam die Krebszellen zu entfernen. Wenn er fertig ist macht er die
Elektroschlinge aus und legt sie wieder zurück.

### Alternativer Verlauf: ###
...Er fängt an langsam die Krebszellen zu entfernen, aber schneidet zu tief rein sodass ein Game Over kommt. Er beschwert sich, dass man wegen der Lupe nicht gut abschätzen kann wie weit weg die Portio ist.

### Ergebnisse: ###
Der Benutzer möchte keine Ergebnisse während der Ausführung. 
Die beschriftungen würden ihn nur ablenken und in der Realität würde es sie auch nicht geben. 
Er möchte lieber nachdem er fertig ist die Ergebnisse anschauen können (wenn er möchte). 
Er möchte Ergebnisse nach dem LLETZ Score. 

Postbedingungen (Die Bedingungen oder den Zustand, in dem sich das System nach Abschluss des Szenarios befindet):
Nach Abschluss des Szenarios soll es sich immernoch in der Simulation befinden. 
Er möchte eine Möglichkeit die Simulation neu zu starten oder zu verlassen. Davor möchte er
irgendwo nachschauen wie sein Score war ohne die Simulation verlassen zu müssen.

<br><br>

### Shaw: ###
Sign up & log in <br>
The user starts by turning the application on <br>
The first page that comes up in the signing up page, the user has the ability to either click <br>
“sign up” or “log in” <br>
if “sign up” is pressed: <br>
Necessary data to be filled (Username, password, name, age, ...) <br>
Warning if data field is missing <br>
Then back to the previous page (log in & sign up) <br>
if “log in” is pressed: → normal logging in process: username and password to be filled when logged in → Main Menu

### Main Menu ###
The user sees the menu with the buttons: <br>
Start <br>
Tutorial <br>
stats <br>
Two smaller buttons (maybe in the corner): <br>
Settings <br>
Sign up <br> <br>
Tutorial <br>
Ideally the user chooses the Tutorial first <br> <br>
Tutorial process <br>
Tutorial is over → options <br>
Start directly <br>
back to main menu <br>
repeat tutorial → <br> <br>
Simulation (start) <br>
Simulation starts: