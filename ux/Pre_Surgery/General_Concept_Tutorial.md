# Generelles Konzept für das Tutorial

## Figma Prototyp
https://www.figma.com/design/eNovki0y8W8YwoTsCeA1JH/TutorialPrototype?node-id=91-3663
Der Prototyp war in erster Linie dazu gedacht, gruppenintern eine Ausgangsbasis für die Implementierung und die ersten Usertests darzustellen. Deshalb wurde er während des Semesters nicht weiter an den Projektstand angepasst oder in den Prototypen aus dem letzten Semester integriert. Auch wurden die Texte, die versehentlich überschrieben wurden, durch Änderung an der entsprechenden Komponente, nicht wieder hergestellt.

## Lektionen
Einteilung anhand der Folien in https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/blob/dev/ux/tutorial_concept_and_guide/VR-Koni%20Trainer_Konzept_interaktives_Tutorial.pdf?ref_type=heads

Lektion 1: Erklärung des Menüs
* Umsetzung als Overlay über das existierende Menü 
* wird aufgerufen wenn sich ein User neu registriert hat und sich das erste mal anmeldet
* deckt nur das Menü ab, das auf dem Desktop zu sehen ist -> nicht in VR
* bisher nicht vorgesehen dass man die Lektion noch einmal aufrufen kann

Lektion 2: Start in VR
siehe Folien
* wiederholtes aufrufen und durchführen in dem Guides/Tutorial Fenster des Menüs moglich

Lektion 3: Durchführung der Simulation
siehe Folien
* wiederholtes aufrufen und durchführen in dem Guides/Tutorial Fenster des Menüs moglich

Lektion 4: Erklärung der Statistiken
siehe Folien
* wiederholtes aufrufen und durchführen in dem Guides/Tutorial Fenster des Menüs moglich
* Absprache mit Post Surgery Team, welche Aspekte sie erklären wollen

## Nutzerführung durch das Tutorial
Der Nutzer kann entweder nach jedem Learning Step der Lektionen des Tutorials auf "Weiter" klicken und so direkt den nächsten Learning Step starten oder über die Navigationsleiste des Guides/Tutorial Fenster des Menüs einen beliebigen anderen Learning Step aussuchen bzw beenden. So können Nutzer mit mehr Erfahrungen für sie irrelevante Lernschritte auslassen. Außerdem kann man bei bedarf noch mal einzelne Learning Steps wiederholen, ohne das gesamte Tutorial wiederholen zu müssen.