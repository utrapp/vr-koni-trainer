## Persona:

### 1: 
Name: Nina Schuck <br>
Alter: 26 <br>
Beruf: Student <br><br>
Nina ist eine 26-jahre alte Medizinstudentin. Sie liest gerne in der Freizeit, treibt viel Sport, auch aufgrund ihres Medizinstudiums. Ansonsten ist sie viel wie ihre Freundinnen und Freunde auf Social-Media wie Instagram und BeReal unterwegs. Nina ist außerdem eine Person, die viel neues probiert. Sei es ein kurzfristiger Trip nach Spanien, eine coole Challenge von Instagram oder andere technische Neuerungen. Wenn sie ihre Freizeit nicht gerade mit den genannten Tätigkeiten beginnt, ist sie meist damit beschäftigt, für ihr Studium zu lernen. Dazu nutzt sie überwiegend Bücher, die die Professoren in den Veranstaltungen oder andere Studenten ihr empfohlen haben. Da sie viel Zeit in ihr Studium investiert, ist sie dort auch ziemlich erfolgreich. Ihr aktueller Notenschnitt beträgt aktuell 1,3. 

### 2: 
Name: Rudolf Müller <br>
Alter: 58 <br>
Beruf: Oberarzt <br><br>
Rudolf ist ein 58-jähriger Oberarzt im Universitätsklinikum Frankfurt. Dort leitet er verschiedenste OPs. Weil sein Tag immer wirklich voll ist, ist er während seiner Arbeit kaum zu erreichen. Wenn überhaupt telefonisch, schließlich ist ihm die ganze Technik viel zu kompliziert. Zeit hatte er ja noch nicht wirklich, sich da genauer einzuarbeiten. Kommt er von seiner Arbeit nach Hause, geht er erst einmal eine Runde joggen. Damit hält er sich nicht nur fit, sondern kann auch seinen Kopf „durchlüften“. Ansonsten kocht Rudolf gerne gesund und liest viel. Abends schaut er mit seiner Frau Tageschau um 20:15 und danach einen Krimi. Rudolf reist gerne durch die Welt. Muss er nur kleine Strecken zurücklegen, fährt er die lieber mit dem Auto, anstatt zu fliegen. Aber wehe, jemand benutzt Google Maps! Mit seiner Karte hat er nämlich sowieso einen besseren Weg gefunden als dieses neuartige Ding.

### 3: 
Name: Luk Bienert <br>
Alter: 40 <br>
Beruf: Facharzt <br><br>
Luk ist ein Facharzt im Bereich Gynäkologie. Er ist also spezialisiert auf das Gebiet. Zur Arbeit fährt er täglich mit dem Fahrrad. Luk liest gerne viel Zeitung. Er trifft sich auch jeden Samstag mit seinen Freunden zum Stammtisch, isst oder trinkt dort etwas und unterhält sich mit ihnen oft über Politik. Gleichzeitig ist er immer wieder auf medizinischen Konferenzen unterwegs, um immer über die interessantesten Entwicklungen in seinem Bereich informiert zu sein. Dort hat er unter anderem einen Vortrag zum Thema „VR in der Medizin“ besucht, eine Technologie, die großes für die Zukunft verspricht. Dort konnte Luk sogar eine VR-Brille ausprobieren! Viel verstanden hat er davon nicht, trotzdem wirkte es beeindruckend auf ihn. Mit seiner Frau ging er dann danach, wie so oft, spazieren. Beide sind begeistert von der Natur und den Tieren, die sie manchmal während dem Spazieren sehen.   

### 4: 
Name: Sophie Limbach <br>
Alter: 29 <br>
Beruf: Krankenschwester <br><br>
Sophie ist Krankenschwester. Sie übt diesen Beruf leidenschaftlich aus und hilft wo sie kann. Nach der Arbeit ist sie meist am Computer oder Handy. Wenn sie gerade nicht am Handy ist oder Freunde trifft, zockt sie mit ihrer neuen VR-Brille auf ihrem Gaming PC die neuesten Games. Eigentlich das selbe Erlebnis immer und immer wieder und trotzdem begeistert sie das Erlebnis jedes Mal wieder. Sogar ihr Essen isst sie während sie die VR-Brille auf hat! Das macht sie dann meist in einer Lobby mit anderen Freunden, entweder während sie ihren Raum streamt oder indem sie ihren Avatar zeigt. Sophie schaut außerdem viel Serien und Filme, hauptsächlich über Netflix. Immer mal wieder geht sie aber auch mit ihrem Hund spazieren, das ihr natürlich wichtiger als das Zocken oder Serien schauen.