Shader "Unlit/V3"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color1 ("Color1", Color) = (1.,1.,1.,1.)
        _Color2 ("Color2", Color) = (1.,1.,1.,1.)
        _Color3 ("Color2", Color) = (1.,1.,1.,1.)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 _Color1;
            fixed4 _Color2;
            fixed4 _Color3;
            
            #define ls 1
            #define cp 4
            #define sc 3.3
            #define aa 4.5
            #define ma 2.0
            #define d 0.02
            #define sp 1.0
            #define dt 0.0075
            #define st 320.0
            #define c 0.5
            #define b 0.575

            fixed4 frag(v2f i) : COLOR {
                float ad = sc, qu = aa;
                float2 p = (i.uv * _ScreenParams.xy) * dt + -_Time.x * d;
                for (int i = 1; i < cp; i += ls) {
                    float k = ma / qu;
                    float s = _Time.x * sp * (sin(float(i)*0.0001) + 1.0);
                    p.x += k * sin(ad * p.y - s) + st;
                    p.y += k * sin(ad * p.x + s) + st;
                    qu += aa;
                    ad += sc;
                }

                fixed3 col = fixed3(sin(p.y * ma), sin(p.x * ma), cos(p.y + p.x));
                fixed4 final = fixed4(b - col * c, 1.0);
    
                fixed4 colorR = final.x * _Color1;// fixed4(0.012, 0.506, 1.0, 1.0);
                fixed4 colorG = final.y * _Color2;// fixed4(0.545,0.757,0.969, 1.0)*0.4;
                fixed4 colorB = final.z * _Color3;// fixed4(0.388, 0.388, 0.388, 1.0);
                
                return colorR+colorG+colorB;
            }
            ENDCG
        }
    }
}
