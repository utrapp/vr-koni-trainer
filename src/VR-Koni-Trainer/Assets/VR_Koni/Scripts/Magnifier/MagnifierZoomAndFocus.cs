/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class MagnifierZoomAndFocus : MonoBehaviour
{
    [SerializeField] private Interactable interactable;
    [SerializeField] private SteamVR_Action_Vector2 controllerMovementInput;
    private Camera magnifierCam;

    [SerializeField] private float zoomFactor = 10;
    [SerializeField] private float focusFactor = 1;

    [SerializeField] private float maxFoV = 90;
    [SerializeField] private float minFov = 3;

    private void Reset()
    {
        Debug.LogWarning("Reset was called.");
        /*
        interactable = GetComponent<Interactable>();
        controllerMovementInput = SteamVR_Input.GetAction<SteamVR_Action_Vector2>("Magnifier", "Zoom_and_Focus");
        */
        /*
    }

    private void Start()
    {
        interactable.onDetachedFromHand += OnDetach;
        magnifierCam = GetComponent<Camera>();
    }

    private void OnDestroy()
    {
        interactable.onDetachedFromHand -= OnDetach;
    }

    private void OnDetach(Hand _)
    {
    }

    private void Update()
    {
        if (!SteamVR.active || !SteamVR.enabled)
            return;

        if (!interactable.attachedToHand)
            return;

        SteamVR_Input_Sources handType = interactable.attachedToHand.handType;
        Vector2 movement = controllerMovementInput.GetAxis(handType);

        if (Mathf.Abs(movement.x) > 0.4)
            magnifierCam.fieldOfView += movement.x * zoomFactor * Time.deltaTime;
        if (Mathf.Abs(movement.y) > 0.4)
            magnifierCam.focusDistance += movement.y * focusFactor * Time.deltaTime; //Currently does not do anything, might need to change Unity Settings

        magnifierCam.fieldOfView = Mathf.Clamp(magnifierCam.fieldOfView, minFov, maxFoV);
    }
}
*/
