using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace VR_Koni.UserData.Export
{
    public static class ExportCSV
    {
        public static bool Export(List<Surgery> surgeries, string filename)
        {
            string docPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            string exportDirectory = Path.Combine(docPath, "Surgery Data");
            Directory.CreateDirectory(exportDirectory);

            Dictionary<string, string> nameMap = ExportUtils.CreateEmojis(surgeries);
            string data = Surgery.SQLHeader() + "\n";
            data += string.Join("\n", surgeries.Select(x => x.ToCSV(nameMap[x.Username])));

            using (StreamWriter outputFile = new StreamWriter(Path.Combine(exportDirectory, filename + ".csv")))
            {
                outputFile.Write(data);
            }

            return true;
        }
    }
}
