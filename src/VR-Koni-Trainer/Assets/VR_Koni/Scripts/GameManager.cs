using System;
using UnityEngine;
using VR_Koni.ElectroSnare;
using VR_Koni.UserData;

namespace VR_Koni
{
    /// <summary>
    /// GameManger is a singleton
    /// </summary>
    public class GameManager : MonoBehaviour
    {
        public enum GameState
        {
            SelectScreen,
            Operation,
            Victory,
            Loose
        }

        private static GameManager instance;

        public static GameState CurrentGameState;
        public static event Action<GameState> OnGameStateChanged;

        private void Awake()
        {
            if (instance)
            {
                DestroyImmediate(this);
                return;
            }

            instance = this;
        }

        private void OnEnable()
        {
            // TODO: Change to Select Screen when further in development
            UpdateGameState(GameState.Operation);

            CuttingAndGeneration.OnVictory += UpdateGameState;

            ElectroSnareVR.onChangingState += ReceiveCuttingCondition;

            Timer.OnTimeChange += ReceiveCuttingTime;

            _ = DataManager.Instance; // Creates singleton
        }

        private void OnDisable()
        {
            CuttingAndGeneration.OnVictory -= UpdateGameState;

            ElectroSnareVR.onChangingState -= ReceiveCuttingCondition;

            Timer.OnTimeChange -= ReceiveCuttingTime;
        }

        private static void UpdateGameState(GameState newState)
        {
            OnGameStateChanged?.Invoke(newState);
            CurrentGameState = newState;
            DataManager.Instance.WriteCurrentUser();
        }

        // TODO: extend functionality or conditions for loosing state and reenable
        /// <summary>
        /// Touching the speculum or vagina with the activated (i.e., energized) wire loop.
        /// </summary>
        /// <param name="cuttingState"></param>
        private static void ReceiveCuttingCondition(ElectroSnareVR.CuttingState cuttingState)
        {
            /* if (cuttingState == ElectroSnareVR.CuttingState.CancerTissue)
                UpdateGameState(GameState.Loose); */
        }

        /// <summary>
        /// Activated wire loop remains in the tissue of the cervix for longer than 8 seconds
        /// (this way we want to avoid that participants guide the wire loop too slowly and thus cause the before mentioned problem).
        /// </summary>
        /// <param name="time"></param>
        private static void ReceiveCuttingTime(float time)
        {
            //if (time > 8.0f)
            //UpdateGameState(GameState.Loose);
        }

        public static void RestartOperation()
        {
            //TODO: Check if game state is properly reset (not only the scene)
            UpdateGameState(GameState.Operation);
            instance.StartCoroutine(SceneHelper.ReloadScene("VR_OperationRoom"));
        }
    }
}
