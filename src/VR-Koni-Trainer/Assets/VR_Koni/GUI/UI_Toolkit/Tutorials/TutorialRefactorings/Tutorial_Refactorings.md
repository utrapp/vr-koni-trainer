# Tutorial Refactoring

## Overview
VR_Koni implements a tutorial system where the users can walk through the basic concepts on how the do a surgery and how to work with the VR set that we have.
The system includes Tutorials, Lessons, and Units, which guide users through different sections of the application. The current implementation of tutorials is not 
structured properly, and we need to refactor the system of tutorials to ensure maintainability, scalability, proper functioning and easier bug fixes.

## Objectives
    1. Store Tutorial Progress in the Database:
        As users interact with the tutorials and make progress, their progress must be stored in the database.
        This allows users to continue further tutorials in the future from where they previously left off.
        Currently we have a rought sketch of how the database might look like. You can find the database
        structure in Assets/VR_Koni/Resources/crebas.sql.txt. This database might not be perfect since its just a 
        rough estimation of how the storing can be done, so you see changes are required, feel free to discuss it 
        with Miss Trapp to modify the structure.
       
    2. Refactor Current Tutorial Implementation:
        The current code for managing tutorials is not well-optimized and needs restructuring.
        We should aim to create a more modular and organized codebase, improving readability and ease of updates.      

## Refactored Components
    Tutorial Scene:
        - Handles the display and interaction of tutorials.
        - Uses List<Tutorial> to manage a collection of Tutorial objects.

    Tutorial:
        - Has a TutorialElement instance that derives from VisualElement.
        - Has all the other necessary properties that will be needed as development continues.

    TutorialElement:
        - Derives from VisualElement so that we can define our custom attributes for the XML file.
        This XML file will serve as a "master template" which can then be used to dynamically create the
        various tutorial elements during runtime. This way we save ourselves from having to define 10 different
        xml files for 10 Tutorials. We can just use that one template 'Tutorial.uxml' to generate the uxml
        dynamically if possible.
    
    Lesson:
        - Has a LessonElement instance that derives from VisualElement
        - Has all the other necessary properties that will be needed as development continues

    LessonElement:
        - Analog to TutorialElement
    
    Unit:
        - Analog to Lesson
    
    UnitElement:
        - Analog to LessonElement

    UXML Structures:
        We utilize UXML and USS files to define the structure and styling of the tutorials, lessons, and units.
        Files:
            Tutorial.uxml: Defines the structure of the tutorial UI.
            Lesson.uxml: Defines the structure of individual lessons within a tutorial.
            Unit.uxml: Defines the structure of units within a lesson.

## Database Interaction
    User Login:
        When a user logs in, their tutorial progress is retrieved from the database.

    Updating Progress:
        After each interaction with a tutorial, or upon leaving the tutorial scene, the progress should be updated and stored back into the database.
        Preferably, this storing should happen after each unit of task completed, whatever that task may be.

    Optional UI Updates:
        After every successful interaction, refresh the UI using the current Tutorial object.

    Scriptable Object or Database:
        There were 2 main points on how to store the data for the tutorials:
        - One possible way that we discussed was using scriptable objects to save the data for the tutorial progress. This hides the abstraction from the c#
        part but introduces new complexity since we are now relying on scriptable objects from unity. Migration to a new system might also mean that the tutorial
        progress is lost, if the scriptable objects cannot be migrated.

        - Another possibility would be to serialize the whole Tutorial class before storing it in the database. This introduces abstraction on the c# side of things
        but makes it a lot easier to understand whats going on since abstraction on the side of unity is hidden and its easier to debug programatic code than an
        automation. Migration will not be a problem since we are storing the tutorial progress entirely in the database.

## Class Structure
    class Tutorial:
        Contains fundamental information about the tutorial.
        Nested classes:
            class Lesson as ICollection<Lesson>: Manages individual lessons within a tutorial.
            class TutorialElement: For the custom visual elements and attributes
    
    class Lesson:
        Contains the fundamental information about the lesson.
        Nested classes:
            class Unit as ICollection<Unit>: Manages individual units within a lesson.
            class LessonElement: For the custom visual elements and attributes

    class Unit:
        Contains the fundamental information about the Unit.
        Nested classes:
            class UnitElement: For the custom visual elements and attributes

## Notes for Development
    Use custom UXML attributes for flexibility in UI definition.
    Keep the design modular to facilitate future expansions or changes in the tutorial system.
    Keep in mind that when breaking changes are done to the database and migrated to main, everyone that bases their branch off of this new main branch must delete the old database file. Otherwise the application will break.
    As a rule of thumb, if you're not sure that your change to the database will glitch the application, inform everyone that database has been migrated and they might need to delete their old database file if they face strange issues.
