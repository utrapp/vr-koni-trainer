using System.Collections;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class FruitResetter : MonoBehaviour
{
    private Vector3 originalPosition;
    private Quaternion originalRotation;
    public float returnDuration = 1.0f; // Duration in seconds to return to the original position

    void Start()
    {
        // Save the original position and rotation
        originalPosition = transform.position;
        originalRotation = transform.rotation;
    }

    public void OnFruitReleased()
    {
        StartCoroutine(ReturnToOriginalPosition());
    }
    
    private IEnumerator ReturnToOriginalPosition()
    {
        float elapsedTime = 0;
        Vector3 startPosition = transform.position;
        Quaternion startRotation = transform.rotation;

        while (elapsedTime < returnDuration)
        {
            transform.position = Vector3.Lerp(startPosition, originalPosition, elapsedTime / returnDuration);
            transform.rotation = Quaternion.Lerp(startRotation, originalRotation, elapsedTime / returnDuration);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.position = originalPosition;
        transform.rotation = originalRotation;
    }
}
