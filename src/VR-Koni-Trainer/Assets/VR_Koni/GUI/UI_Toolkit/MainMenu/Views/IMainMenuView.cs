using UnityEngine.Localization;
using VR_Koni.GUI.UI_Toolkit.Helper;

namespace VR_Koni.GUI.UI_Toolkit.MainMenu.Views
{
    public interface IMainMenuView : IUIView
    {
        public LocalizedString ViewName { get; }
    }
}
