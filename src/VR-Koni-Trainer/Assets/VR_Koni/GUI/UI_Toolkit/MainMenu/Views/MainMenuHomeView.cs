using UnityEngine.Localization;
using UnityEngine.UIElements;

namespace VR_Koni.GUI.UI_Toolkit.MainMenu.Views
{
    public class MainMenuHomeView : IMainMenuView
    {
        public LocalizedString ViewName => new LocalizedString { TableReference =  ProjectConstants.UiToolKitLocalizationTableName, TableEntryReference = "MainMenuHomeViewName" };
        public VisualElement RootElement { get; private set; }

        public void SetupVisualElements(VisualElement rootElement) => RootElement = rootElement;

        public void SubscribeToEvents() {}

        public void UnsubscribeFromEvents() {}

        public void ResetViewSelections() {}
    }
}
