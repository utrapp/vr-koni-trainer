using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.Interactables;

namespace VR_Koni.ElectroSnare
{
    public class MoveToPoint : MonoBehaviour
    {
        [SerializeField] private Transform snapToTransform;
        [SerializeField] private bool snapToInitialPosition;
        [SerializeField] private float snapTime = 2;
        [SerializeField] private float speed = 2;
        [SerializeField] private bool applyTranslation = true;
        [SerializeField] private bool applyRotation = true;
        
        private float dropTimer;
        private Vector3 snapPosition;
        private Quaternion snapRotation;

        private bool isHovered = false;
        private bool isSelected = false;
    
        private void Start()
        {
            if (snapToInitialPosition)
            {
                snapPosition = transform.position;
                snapRotation = transform.rotation;
            }
            else
            {
                snapPosition = snapToTransform.position;
                snapRotation = snapToTransform.rotation;
            }
        }

        public void SetHovered(bool state)
        {
            isHovered = state;
            if (state == true)
            {
                ResetTimer();
            }
        }

        public void SetSelected(bool state)
        {
            isSelected = state;
            if (state == true)
            {
                ResetTimer();
            }
        }

        private void ResetTimer()
        {
            dropTimer = 0;
        }
        
        private void FixedUpdate()
        {
            bool isAtTargetPosition = (applyTranslation == false || transform.position == snapPosition);
            bool isAtTargetRotation = (applyRotation == false || transform.rotation == snapRotation);
            
            if (isSelected == false && isHovered == false)
            {
                dropTimer += Time.fixedDeltaTime;

                if (dropTimer >= snapTime)
                {
                    float step = speed * Time.fixedDeltaTime;
                    
                    if (isAtTargetPosition == false)
                    {
                        transform.position = Vector3.MoveTowards(transform.position, snapPosition, step / 4);
                    }

                    if (isAtTargetRotation == false)
                    {
                        transform.rotation = Quaternion.RotateTowards(transform.rotation, snapRotation, step * 50);
                    }

                    dropTimer = snapTime;
                }
            }
        }
    }
}
