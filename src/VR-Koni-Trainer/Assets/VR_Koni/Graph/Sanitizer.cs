#nullable enable //The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.

using System;
using System.Collections.Generic;
using System.Linq;

namespace VR_Koni.Graph
{
    /// <summary>
    /// Provides methods to sanitize data collections by removing excessive consecutive duplicates.
    /// Handles specific data types such as lists of tuples and vectors.
    /// </summary>
    public class Sanitizer<T>
    {
        /// <summary>
        /// specifies the count of tolerance till which consecutive duplicates wont be removed.
        /// When set to X for instance, it will ignore up to X consecutive duplicates but then not add
        /// the duplicates from the (X+1)th instance if it is consecutive.
        /// </summary>
        private readonly int duplicateCountTolerance;

        /// <summary>
        /// Initializes a new instance of the Sanitizer class with a specified tolerance for consecutive duplicates.
        /// </summary>
        /// <param name="duplicateTolerance">The maximum number of consecutive duplicates allowed.</param>
        public Sanitizer(int duplicateTolerance = 3)
        {
            duplicateCountTolerance = duplicateTolerance;
        }

        /// <summary>
        /// Main sanitization method that routes the sanitization process to type-specific implementations.
        /// Params <paramref name="tupleSanitizer"/> and <paramref name="vectorSanitizer"/> are only if you want to define/pass
        /// in your own implementations of how the sanitizing should happen.
        /// </summary>
        /// <typeparam name="T">The type of collection to sanitize.</typeparam>
        /// <param name="data">The data collection to sanitize, passed by reference.</param>
        /// <param name="vectorSanitizer"> custom anon func for sanitizing (optional)</param>
        /// <param name="tupleSanitizer"> custom anon func for sanitizing (optional)</param>
        public void SanitizeData(
            ref List<T> data,
            Func<ICollection<System.Numerics.Vector2>, List<System.Numerics.Vector2>>? vectorSanitizer = null,
            Func<ICollection<Tuple<float, float>>, List<Tuple<float, float>>>? tupleSanitizer = null)
        {
            switch (data)
            {
                case List<Tuple<float, float>> tupleList:
                    List<Tuple<float, float>> sanitizedTuples = tupleSanitizer != null
                        ? tupleSanitizer(tupleList) //if the user passed in their custom func to sanitize, use it
                        : SanitizeTupleData(tupleList).ToList();
                    tupleList.Clear();
                    tupleList.AddRange(sanitizedTuples);
                    break;

                case List<System.Numerics.Vector2> vectorList:
                    List<System.Numerics.Vector2> sanitizedVectors = vectorSanitizer != null
                        ? vectorSanitizer(vectorList) //if the user passed in their custom func to sanitize, use it
                        : SanitizeVectorData(vectorList).ToList();
                    vectorList.Clear();
                    vectorList.AddRange(sanitizedVectors);
                    break;

                default:
                    throw new ArgumentException($"{nameof(SanitizeData)}: Unsupported type '{typeof(T)}' passed for sanitization.");
            }
        }

        /// <summary>
        /// Sanitizes <see cref="System.Numerics.Vector2"/> data by limiting the number of consecutive duplicate Y values.
        /// </summary>
        /// <param name="data">The list of <see cref="System.Numerics.Vector2"/> to sanitize</param>
        /// <returns>Sanitized collection with limited consecutive duplicates</returns>
        private IEnumerable<System.Numerics.Vector2> SanitizeVectorData(IEnumerable<System.Numerics.Vector2> data)
        {
            return data
                // Store the original position of each item to preserve order
                .Select((item, index) => (Item: item, Index: index))
                // Group items by their Y value to find duplicates
                .GroupBy(x => x.Item.Y)
                // For each group of same Y values, take only up to _duplicateCountTolerance items
                .SelectMany(grp => grp.Take(duplicateCountTolerance))
                // Restore the original order using the stored indices
                .OrderBy(x => x.Index)
                // Get back just the System.Numerics.Vector2 items without the indices
                .Select(x => x.Item);
        }

        /// <summary>
        /// Sanitizes <see cref="Tuple"/> data by limiting the number of consecutive duplicate Item2 (Y) values.
        /// Works exactly the same as <see cref="System.Numerics.Vector2"/> sanitization but for <see cref="Tuple{T1, T2}"/>
        /// </summary>
        /// <param name="data">The list of Tuples to sanitize</param>
        /// <returns>Sanitized collection with limited consecutive duplicates</returns>
        private IEnumerable<Tuple<float, float>> SanitizeTupleData(IEnumerable<Tuple<float, float>> data)
        {
            return data
                // Store the original position of each item to preserve order
                .Select((item, index) => (Item: item, Index: index))
                // Group items by their Item2 (Y) value to find duplicates
                .GroupBy(x => x.Item.Item2)
                // For each group of same Y values, take only up to _duplicateCountTolerance items
                .SelectMany(grp => grp.Take(duplicateCountTolerance))
                // Restore the original order using the stored indices
                .OrderBy(x => x.Index)
                // Get back just the Tuple items without the indices
                .Select(x => x.Item);
        }
    }
}
