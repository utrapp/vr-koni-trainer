using System;
using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.EventSystems;

namespace VR_Koni.GUI.HeightAdjustment
{
    public class HeightAdjustmentButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private string tagToMove;
        [SerializeField] private float speed = 0.01f;
        [SerializeField] private string direction;

        private bool isButtonHeld;
        private GameObject[] gameObjectsToMove = Array.Empty<GameObject>();

        private void Update()
        {
            if (!isButtonHeld) return;

            foreach (GameObject obj in gameObjectsToMove)
            {
                Vector3 dirVec = direction == "up" ? Vector3.up : Vector3.down;
                if (tagToMove == "Player")
                {
                    CharacterController charController = obj.GetComponent<CharacterController>();
                    GameObject cameraOffset = obj.GetNamedChild("Camera Offset");

                    charController.height += (speed * Time.deltaTime * 1);
                    charController.center = new Vector3(charController.center.x, charController.center.y - charController.height / 2, charController.center.z);
                    cameraOffset.transform.Translate(speed * Time.deltaTime * dirVec, Space.World);
                }
                else
                {
                    obj.transform.Translate(speed * Time.deltaTime * dirVec, Space.World);
                }
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            isButtonHeld = true;
            gameObjectsToMove = GameObject.FindGameObjectsWithTag(tagToMove);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            isButtonHeld = false;
        }
    }
}
