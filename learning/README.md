# Learning

Collect sample code here to familiarize with or try out individual technologies/libraries. Add helpful tutorials, snippets, etc. to this document.

## Assets
Inside [/resources](https://gitlab.com/utrapp/vr-koni-trainer/-/tree/main/learning/resources?ref_type=heads) one can find useful links to resources and other sources that may help when creating.

## Presentations
Inside [/presentations](https://gitlab.com/utrapp/vr-koni-trainer/-/tree/main/learning/presentations?ref_type=heads) one can find the .pdf-files of the students presentations. 