## Nutzer mit VR Erfahrung ##
Annahme:
Nutzer mit VR Vorerfahrung -> keine generelle VR Einweisung notwendig

In der Simulation kann nur ein Schnitt ausgeführt werden.

### Ablauf: ###
Der Nutzer startet die Anwendung und es öffnet sich ein Desktop-Fenster mit einem User Login Screen. Der Nutzer gibt seine Anmeldedaten ein und landet in einem Menü mit den Möglichkeiten, die VR Simulation zu starten, seine bisherigen Ergebnisse und Statistiken einzusehen, oder sich wieder aus zu loggen.

Er startet die Anwendung und setzt die VR Brille auf. Hier sieht er eine Willkommensnachricht und kann mit einem beliebigen Button ins Menü wechseln.

Das Menü bietet die Möglichkeiten eine VR Einführung, eine medizinische Einweisung oder die Trainingssimulation zu starten, sowie die Steuerung oder die bisherigen Ergebnisse anzuzeigen. Außerdem gibt es die Möglichkeit, zurück zum Desktop zu wechseln oder die Anwendung vollständig zu beenden.

Der Nutzer lässt sich die Steuerung/Tastenbelegung des Controllers anzeigen und startet anschließend die Trainings-Simulation.

Das Menü verschwindet und der Nutzer kann sich frei im OP-Raum bewegen. Die Elektroschlinge liegt auf einem Tisch bereit, auf dem sich außerdem ein Screen befindet, der eine Option zum vorzeitigen Beenden enthält.

Der Nutzer nimmt die Schlinge auf, tritt an die Lupe heran und führt einen Schnitt aus.

Eine Nachricht wird angezeigt, ob die Auswertung angezeigt werden soll, mit den Möglichkeiten ja, Simulation wiederholen, Exit.

Der Nutzer wählt die Anzeige der Ergebnisse und den Vergleich mit früheren Versuchen aus. Danach wechselt er zurück ins Hauptmenü und beendet die Anwendung.

### User Stories ###
Als Mediziner <br>
möchte ich meinen Lernfortschritt bezüglich der LLETZ-score einsehen
damit ich weiß ob ich mich verbessert habe

Als erfahrener VR Nutzer <br>
möchte ich eine Tutorial überspringen können,
damit ich direkt in Simulation starten/Zeit sparen kann.

Als Mediziner <br>
möchte ich meine Scores auch am PC einsehen können
damit ich keine Zeit durch Auf und Absetzen der Brille verliere.

Fragen: <br>
Soll das Training am Desktop beachtet werden können?

Flowchart <br>
![flowchart of user with vr experience](./flowchart_user_with_vr_experience.png)
