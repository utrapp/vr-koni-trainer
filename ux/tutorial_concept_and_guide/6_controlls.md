# Controls

For the Training Simulation only the right controller is needed.

## Interacting with the menu

To select an entry in the menu point at it and press the trigger with your index finger. Unless you hold something, a ray will indicate the direction you're pointing.
While not holding the loop electrode you can use the thumbstick to change the direction you're facing

## Using the loop electrode

While holding the loop electrode pressing and holding either the trigger or B activates it. You will hear a continuous high sound, indicating that the loop electrode is active.

To adjust the loop's position in your hand you can use the thumbstick to rotate it.

Pressing on the thumbstick will open a pause menu that allows you to end the current surgery, adjust the volume, recenter your position in the VR scene or resume the simulation.

## Moving the excised cone

The excised cone displayed in the statistics section can be rotated using the thumbstick.

## Fast Exit

If the application needs to be excited fast while in VR-Mode, just take off the headset and press ESC on the keyboard. If a user is logged in all data will be saved and they will be logged out.
If the headset is taken off, but ESC is not pressed, a logged in user will be automatically logged out after ten minutes of inactivity.
After five minutes of inactivity a message will be displayed on the desktop: ‘Automatic logout in 5 min. Put on the headset to continue.’

