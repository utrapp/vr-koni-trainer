using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using VR_Koni.Audio;

namespace VR_Koni.GUI
{
    public class SoundSettingsSlider : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private Slider volumeSlider;
        [SerializeField] private AudioSourceFade audioSource;

        private static event Action<float> onSliderUpdate;

        private void OnEnable() => onSliderUpdate += SetVolumeSlider;

        private void OnDisable() => onSliderUpdate -= SetVolumeSlider;

        private void SetVolumeSlider(float value)
        {
            if (Math.Abs(volumeSlider.value - value) > 0.01f) // Only update if change is visible to the user
                volumeSlider.value = value;
        }

        private void Start()
        {
            volumeSlider.value = AudioListener.volume;
            volumeSlider.onValueChanged.AddListener(SetVolumeSetting);
        }

        private static void SetVolumeSetting(float value)
        {
            AudioListener.volume = value;
            onSliderUpdate?.Invoke(value);
        }


        public void OnPointerDown(PointerEventData eventData) => audioSource.Play();

        public void OnPointerUp(PointerEventData eventData) => audioSource.Stop();
    }
}
