using UnityEngine;
using UnityEngine.UIElements;

public class VideoUIManager : MonoBehaviour
{
    public UIDocument uiDocument;
    public RenderTexture videoRenderTexture;

    void Start()
    {
        if (uiDocument == null)
        {
            Debug.LogError("UI Document is not assigned.");
            return;
        }

        var rootVisualElement = uiDocument.rootVisualElement;
        var videoImage = rootVisualElement.Q<VisualElement>("videoImage");

        if (videoImage == null)
        {
            Debug.LogError("Could not find the videoImage element in the UXML.");
            return;
        }

        if (videoRenderTexture == null)
        {
            Debug.LogError("RenderTexture is not assigned.");
            return;
        }

        videoImage.style.backgroundImage = Background.FromRenderTexture(videoRenderTexture);
    }
}

