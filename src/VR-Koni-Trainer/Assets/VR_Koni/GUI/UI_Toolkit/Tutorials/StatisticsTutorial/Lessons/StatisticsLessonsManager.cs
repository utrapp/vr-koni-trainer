using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using VR_Koni;
using VR_Koni.GUI;
using VR_Koni.GUI.UI_Toolkit.Helper;

public class StatisticsLessonsManager : MonoBehaviour
{
    [SerializeField] private UIDocument uiDoc;
    private VisualElement rootElement;
    private Button prevLessonButton;
    private Button nextLessonButton;
    private Button endTutorialButton;
    private Button lessonsOverviewButton;
    private List<VisualElement> lessons;
    private int currentLessonIndex;

    private void OnEnable()
    {
        uiDoc = GetComponent<UIDocument>();
        rootElement = uiDoc.rootVisualElement;

        // Initialise Lessons
        lessons = new List<VisualElement>();
        for (int i = 1; i <= 7; i++)
        {
            lessons.Add(rootElement.Q($"Lesson{i}"));
        }

        SetCurrentLessonIndexFromPrefs();
        lessons[currentLessonIndex].style.display = DisplayStyle.Flex;

        // Initialise the buttons
        prevLessonButton = rootElement.RegisterButtonCallback("PrevLesson", OnPrevLessonButtonClick);
        nextLessonButton = rootElement.RegisterButtonCallback("NextLesson", OnNextLessonButtonClick);
        endTutorialButton = rootElement.RegisterButtonCallback("EndTutorial", OnEndTutorialButtonClick);
        lessonsOverviewButton = rootElement.RegisterButtonCallback("LessonsOverviewButton", OnLessonsOverviewButtonClick);

        // Update the visibility of the buttons
        UpdateButtonVisibility();
    }

    private void OnDisable()
    {
        // Deregister the ClickEvent callbacks for the buttons
        prevLessonButton?.UnregisterCallback<ClickEvent>(OnPrevLessonButtonClick);
        nextLessonButton?.UnregisterCallback<ClickEvent>(OnNextLessonButtonClick);
        endTutorialButton?.UnregisterCallback<ClickEvent>(OnEndTutorialButtonClick);
        lessonsOverviewButton?.UnregisterCallback<ClickEvent>(OnLessonsOverviewButtonClick);
    }

    private void SetCurrentLessonIndexFromPrefs()
    {
        if (PlayerPrefs.HasKey("lesson"))
        {
            int lessonNumber = PlayerPrefs.GetInt("lesson") - 1; // Convert to 0-based index;
            currentLessonIndex = lessonNumber;
        }
        else
        {
            currentLessonIndex = 0;
        }
    }

    private void OnPrevLessonButtonClick(ClickEvent evt)
    {
        // Hide the current lesson and go to the previous lesson
        lessons[currentLessonIndex].style.display = DisplayStyle.None;
        currentLessonIndex--;
        lessons[currentLessonIndex].style.display = DisplayStyle.Flex;

        UpdateButtonVisibility();
    }

    private void OnNextLessonButtonClick(ClickEvent evt)
    {
        // Hide the current lesson and go to the next lesson
        lessons[currentLessonIndex].style.display = DisplayStyle.None;
        currentLessonIndex++;
        lessons[currentLessonIndex].style.display = DisplayStyle.Flex;

        UpdateButtonVisibility();
    }

    private void UpdateButtonVisibility()
    {
        // Hide the "PrevLesson" Button if the current lesson is the first lesson
        prevLessonButton.style.visibility = (currentLessonIndex == 0) ? Visibility.Hidden : Visibility.Visible;

        // Hide the "NextLesson" Button if the current lesson is the last lesson
        if (currentLessonIndex == lessons.Count - 1)
        {
            nextLessonButton.style.display = DisplayStyle.None;
            endTutorialButton.style.display = DisplayStyle.Flex;
        }
        else
        {
            nextLessonButton.style.display = DisplayStyle.Flex;
            endTutorialButton.style.display = DisplayStyle.None;
        }
    }

    private void OnLessonsOverviewButtonClick(ClickEvent evt)
    {
        string currentSceneName = SceneManager.GetActiveScene().name;
        PlayerPrefs.SetString("PreviousScene", currentSceneName);
        ApplicationManager.SwitchToTutorials();
    }

    private void OnEndTutorialButtonClick(ClickEvent evt)
    {
        string currentSceneName = SceneManager.GetActiveScene().name;
        PlayerPrefs.SetString("PreviousScene", currentSceneName);
        ApplicationManager.SwitchToTutorials();
    }
}
