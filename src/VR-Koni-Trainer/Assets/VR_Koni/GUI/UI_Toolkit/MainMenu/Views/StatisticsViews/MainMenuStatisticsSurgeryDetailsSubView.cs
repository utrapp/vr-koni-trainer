using System;
using System.Globalization;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.UIElements;
using VR_Koni.UserData;

namespace VR_Koni.GUI.UI_Toolkit.MainMenu.Views.StatisticsViews
{
    public class MainMenuStatisticsSurgeryDetailsSubView : IMainMenuView
    {
        public LocalizedString ViewName => new LocalizedString();
        private readonly LocalizedString noSurgeryText = new LocalizedString { TableReference =  ProjectConstants.UiToolKitLocalizationTableName, TableEntryReference = "MainMenuStatisticsViewSurgeryDetailsSubViewNoSurgeryText" };
        private const string DEMO_IMAGE_PATH = "Assets/VR_Koni/GUI/UI_Toolkit/MainMenu/Images/Cone Image.png";

        public VisualElement RootElement { get; private set; }
        private TextElement summaryRightSide;
        private VisualElement frontImageFullscreen;
        private VisualElement backImageFullscreen;
        private VisualElement depthGraphFullscreen;
        private VisualElement velocityGraphFullscreen;
        private Button frontImageFullscreenButton;
        private Button backImageFullscreenButton;
        private Button depthGraphFullscreenButton;
        private Button velocityGraphFullscreenButton;

        public void SetupVisualElements(VisualElement rootElement)
        {
            RootElement = rootElement;

            summaryRightSide = rootElement.Q<TextElement>("SummaryRightSide");
            frontImageFullscreen = rootElement.Q("CutoutFrontImage");
            backImageFullscreen = rootElement.Q("CutoutBackImage");
            depthGraphFullscreen = rootElement.Q("CutoutDepthGraph");
            velocityGraphFullscreen = rootElement.Q("CutoutVelocityGraph");
            frontImageFullscreenButton = rootElement.Q<Button>("CutoutImageFrontFullscreenButton");
            backImageFullscreenButton = rootElement.Q<Button>("CutoutImageBackFullscreenButton");
            
            depthGraphFullscreenButton = rootElement.Q<Button>("DepthGraphFullscreenButton");
            velocityGraphFullscreenButton = rootElement.Q<Button>("VelocityGraphFullscreenButton");
            
            if (DataManager.Instance == null)
            {
                Debug.LogError("Instance is null");
                return;
            }
            
            Surgery lastSurgery = DataManager.Instance?.CurrentUser?.Surgeries?.LastOrDefault();
            if (lastSurgery != null)
            {
                FillSurgeryContent(DataManager.Instance.CurrentUser.Surgeries.Last());
            }
            else
            {
                summaryRightSide.text = noSurgeryText.GetLocalizedString();
                AddImage(frontImageFullscreen,DEMO_IMAGE_PATH);
                AddImage(backImageFullscreen,DEMO_IMAGE_PATH);
            }
        }

        /// <summary>
        /// Populates the surgery content with summary details of surgery and iamges of cutouts.
        /// Currently, image paths are not available in the database.
        /// </summary>
        /// <param name="surgery">The surgery object containing details to be displayed.</param>
        private void FillSurgeryContent(Surgery surgery)
        {
            FillSummary(surgery);

            // no image paths yet in DB
            //AddImage(frontImageFullscreen, surgery.ImagePaths[0]);
            //AddImage(backImageFullscreen, surgery.ImagePaths[1]);
            Debug.Log("No surgery Paths available yet");
        }

        public void SubscribeToEvents()
        {
            frontImageFullscreenButton.RegisterCallback<ClickEvent, VisualElement>(OpenImageFullscreen, frontImageFullscreen);
            backImageFullscreenButton.RegisterCallback<ClickEvent, VisualElement>(OpenImageFullscreen, backImageFullscreen);
            depthGraphFullscreenButton.RegisterCallback<ClickEvent, VisualElement>(OpenImageFullscreen, depthGraphFullscreen);
            velocityGraphFullscreenButton.RegisterCallback<ClickEvent, VisualElement>(OpenImageFullscreen, velocityGraphFullscreen);
        }

        public void UnsubscribeFromEvents()
        {
            frontImageFullscreenButton.UnregisterCallback<ClickEvent, VisualElement>(OpenImageFullscreen);
            backImageFullscreenButton.UnregisterCallback<ClickEvent, VisualElement>(OpenImageFullscreen);
            depthGraphFullscreenButton.UnregisterCallback<ClickEvent, VisualElement>(OpenImageFullscreen);
            velocityGraphFullscreenButton.UnregisterCallback<ClickEvent, VisualElement>(OpenImageFullscreen);
        }

        public void ResetViewSelections() {}

        private void FillSummary(Surgery surgery)
        {
            StringBuilder sb = new();
            sb.AppendLine($"<size=120%><b>{surgery.Date.ToString(CultureInfo.CurrentCulture)}</b></size>");
            sb.AppendLine(surgery.LLETZ.ToString());
            sb.AppendLine(surgery.MaxDepth.ToString("F3") + " mm");
            sb.AppendLine(surgery.ContactTime.ToString("F3") + " s");
            sb.AppendLine(surgery.Volume.ToString("F3") + " mm³");
            sb.AppendLine(surgery.OperationTime.ToString("F3") + " s"); //TODO: not correct time
            summaryRightSide.text = sb.ToString();
        }

        private void OpenImageFullscreen(ClickEvent evt, VisualElement imageNode)
        {
            StyleBackground image = imageNode.style.backgroundImage;
            //TODO: Display image in full scale
        }

        /// <summary>
        /// Updates the LAstTrySubView with the provided surgery data. 
        /// Displays surgery-specific details in the summary and updates related visuals like images and graphs.
        /// If no surgery is provided, displays a default message indicating no data is available.
        /// </summary>
        /// <param name="surgery">The surgery data to display. If null, a default message is shown.</param>
        public void DisplaySelectedSurgery(Surgery surgery)
        {
            if (surgery == null)
            {
                summaryRightSide.text = noSurgeryText.GetLocalizedString();
                AddImage(frontImageFullscreen,DEMO_IMAGE_PATH);
                AddImage(backImageFullscreen,DEMO_IMAGE_PATH);
                return;
            }
            FillSurgeryContent(surgery);
        }

        /// <summary>
        /// Sets the background image for Image fullscreen.
        /// If the texture is null, it clears the background and logs an error with the specified image path.
        /// </summary>
        /// <param name="imageScreen">the element where image is added</param>
        /// <param name="imagePath">The path of the image file, used for error logging if the texture is null.</param>
        private void AddImage(VisualElement imageScreen, string imagePath)
        {
            Texture2D imageTexture = LoadImage(imagePath);
            if (imageTexture == null)
            {
                Debug.LogError($"Failed to load image at path: {imagePath}");
                // Clear background if loading fails
                imageScreen.style.backgroundImage = new StyleBackground((Texture2D)null);
                return;
            }
            imageScreen.style.backgroundImage = new StyleBackground(imageTexture);
        }
        
        /// <summary>
        /// Loads an image from the specified file path into a Texture2D object.
        /// If the path is null, empty, or the file does not exist, returns null.
        /// </summary>
        /// <param name="path">The file path of the image to load.</param>
        /// <returns>
        /// A Texture2D object containing the image data if the file is successfully loaded; 
        /// otherwise, null.
        /// </returns>
        private static Texture2D LoadImage(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                Debug.LogError("Image path is null or empty.");
                return null;
            }

            // Check if the file exists
            if (!System.IO.File.Exists(path))
            {
                Debug.LogError($"Image file does not exist at path: {path}");
                return null;
            }

            // Load image data
            Texture2D texture = new (2, 2);
            try
            {
                byte[] imageData = System.IO.File.ReadAllBytes(path);
                if (imageData.Length > 0)
                {
                    texture.LoadImage(imageData);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError($"Failed to load image from path: {path}. Exception: {ex.Message}");
                return null;
            }
            return texture;
        }
    }
}
