using System.Threading.Tasks;
using JetBrains.Annotations;
using SimpleFileBrowser;
using UnityEngine;

namespace VR_Koni.UserData.Export
{
    /// <summary>
    /// A pure C# class that works with <see cref="FileBrowser"/>
    /// </summary>
    //don't know if it makes sense to remove the static keyword and inherit this from monobehavior, but for now leaving it as static
    public static class CustomDialogBox
    {
        /// <summary>
        /// <see cref="OpenSqLiteFileSelectionDialogAsync"/> Opens a Dialog box for the user to select the sqlite
        /// file in case the sqlite file is not found in <see cref="Application.persistentDataPath"/>.
        /// </summary>
        /// <returns>The selected file as a <see langword="string"/> or <see langword="null"/> if the selection was cancelled.</returns>
        [ItemCanBeNull]
        public static async Task<string> OpenSqLiteFileSelectionDialogAsync()
        {
            FileBrowser.SetFilters(false, new FileBrowser.Filter("SQLite Files", ".sqlite"));

            TaskCompletionSource<string> tcs = new();
            FileBrowser.ShowLoadDialog(onSuccess: (paths) =>
                                       {
                                           if (paths.Length > 0)
                                           {
                                               tcs.TrySetResult(paths[0]); //return only the first file since we don't support multi load here
                                           }
                                           else
                                           {
                                               tcs.TrySetResult(null);
                                           }
                                       },
                                       onCancel: () => tcs.TrySetResult(null),
                                       pickMode: FileBrowser.PickMode.Files,
                                       allowMultiSelection: false, //correlates to the comment above
                                       title: "Load: Select a SQLite File",
                                       loadButtonText: "Choose"
                                      );

            //returns the selected file or null if cancelled
            return await tcs.Task;
        }

        /// <summary>
        /// Opens a dialog to select a folder.
        /// Serves as a general purpose method, meaning it can be used in other places as well.
        /// </summary>
        /// <returns>The selected folder as a <see langword="string"/> or <see langword="null"/> if the selection was cancelled.</returns>
        [ItemCanBeNull]
        public static async Task<string> OpenSaveFolderDialogAsync()
        {
            TaskCompletionSource<string> tcs = new();
            FileBrowser.ShowSaveDialog(
                                       onSuccess: (paths) =>
                                       {
                                           if (!string.IsNullOrEmpty(paths?[0]))
                                           {
                                               tcs.TrySetResult(paths?[0]);
                                           }
                                           else
                                           {
                                               tcs.TrySetResult(null);
                                           }
                                       },
                                       onCancel: () => tcs.TrySetResult(null),
                                       pickMode: FileBrowser.PickMode.Folders,
                                       allowMultiSelection: false,
                                       title: "Save: Choose a Save directory",
                                       saveButtonText: "Save"
                                      );
            //same as above, returns the selected folder path or null if cancelled
            return await tcs.Task;
        }
    }
}
