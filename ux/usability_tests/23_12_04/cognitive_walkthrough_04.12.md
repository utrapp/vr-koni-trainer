# Cognitive Walkthrough

_by Shawkat Abu Elnaser_

## Einleitung

Der Cognitive Walkthrough konzentriert sich darauf, die Lernfähigkeit des Systems für neue oder unerfahrene Nutzer zu verstehen. Deshalb war es für uns wichtig, ihn durchzuführen, indem wir uns in die Rolle von Personen versetzten, die zuvor nicht mit dem Menü gearbeitet haben.

Dieses Cognitive Walkthrough wurde am Freitag, den 01.12.2023, mit der folgenden Version des Koni-Menüs durchgeführt:

[Link zum Figma-Design](https://www.figma.com/file/ZrTOGKjPNXq4eNDrOdxfjD/PSE-Statistics?type=design&mode=design&t=oaQyMO2ONWO5VAmA-1)

Ein weiterer Zweck des Cognitive Walkthroughs ist es, die Usability-Tests und den Prototyp mindestens einmal vor den eigentlichen Usability-Tests am Montag, den 04.12.2023, durchzugehen, um eventuelle Fehler in den Testaufgaben oder im Design des Prototyps selbst zu erkennen.

## Task 1 - Register

Ich starte das Programm und bekomme die Login-Seite angezeigt. Als neuer User möchte ich ein neues Konto anlegen. Ich sehe das Wort "Register" neben "Login", deshalb drücke ich darauf und erhalte die Seite "Register".
Ich fülle die erforderlichen leeren Felder aus und drücke auf die Taste "Register", woraufhin ich sofort zum Hauptmenü weitergeleitet werde. Diese Aktion überrascht mich ein wenig (nicht unbedingt im negativen Sinne), denn ich bin es gewohnt, dass Programme und Webseiten mich nach der Registrierung zum Login auffordern.

## Task 2 - Guide

Um mich mit der Steuerung vertraut zu machen, drücke ich auf "Guide". Für einen Erstbenutzer ist es vielleicht etwas unklar, wie man den Guide benutzt.
Fragen, die ich als neuer Benutzer haben könnte:

- Ist die Anleitung interaktiv oder handelt es sich um Informationen zum Lesen?
- Sind zum Beispiel "Sling aktivieren" und "Sling drehen" Tasten?
- Was ist der Unterschied zwischen einem Sling und einem Controller?

## Task 3 - Settings

Wenn ich die Hintergrundmusik leiser stellen oder die Standardsprache ändern möchte, drücke ich intuitiv auf "Settings" an der Seite.
Die Einstellungen sind einfach zu bedienen. Wenn ich die Sprache ändern möchte, würde ich auf die deutsche Flagge unten drücken. Allerdings ist mir zunächst unklar, was mit "Animation" gemeint ist. Vielleicht entdecke ich es nach dem Drücken.

- Vielleicht sollte es "Background Animation" heißen?
  Es wäre auch interessant zu wissen, ob Musik und Soundbar einfach zu drücken und zu ändern sind.

## Task 4 - Training Starten

Nachdem ich die Einstellungen geändert habe, möchte ich das Training starten. Ich drücke auf "Home". Die Änderung der Taste zu "blau" zeigt mir, dass ich erfolgreich zum Homescreen gewechselt bin.

- Was ist, wenn ich versehentlich etwas an den Einstellungen geändert habe? Sollte es eine Möglichkeit zum Speichern der Einstellungen geben? Dann finde ich die Taste grün unten und drücke sie, um die Simulation zu starten.
  Ich bekomme jedoch zwei Tasten angeboten: Start Tutorial und Start Operation
- Könnte man sich den extra Schritt sparen, indem man beide Schaltflächen am Anfang schon vorher anzeigt?

## Task 5 - Training Beenden

Ich befinde mich in der Simulation und erhalte einen wichtigen Notruf, aber ich weiß nicht, wie ich die Simulation beenden kann. Es wird keine Taste angezeigt, mit der ich die Simulation sofort beenden kann.
Ich sehe "This is in game Click to continue", also klicke ich und ein Fenster öffnet sich. Ich sehe das auffällige rote "Exit VR" und drücke darauf und komme zurück zum Home Screen,
Fragen:

- Kann die Simulation direkt beendet werden? Wenn ja, wird mein Verlauf gespeichert?
- Was ist der Unterschied zwischen "Finish Operation" und "Exit VR"?

## Task 6 - Statistik ansehen

Nachdem ich zum Startbildschirm zurück gekommen bin, möchte ich die Ergebnisse meines letzten Trainings einsehen. Ich drücke auf "Statistics".

- Die Informationen über mein letztes Training sind gut und leicht zu verstehen
- Das untere Diagramm verstehe ich nicht intuitiv. Zeigt es die Tiefe des zugeschnittenen Stücks im Verhältnis zur Zeit an?

## Task 7 + 8 - Ausschnitt ansehen und Ältere Statistiken

Ich drücke auf "All Stats" und sehe die Tabelle. Es ist nicht sehr intuitiv, auf "Inspect" zu drücken, weil es Teil der Tabelle ist und ich instinktiv denke, dass es nur zum Lesen ist.
Ich drücke dann auf LLETZ, es wäre schön gewesen, wenn auf dieser Seite mehr Informationen zu finden sind
Der Aufbau der Statistik-Seite ist jedoch gut gegliedert und die Navigation zu den älteren Statistiken ist klar

Fragen von Erstbenutzer:

- Können andere Benutzer meine Statistiken sehen? Kann ich die Statistiken anderer Benutzer sehen?
- Werden meine Statistiken gespeichert, wenn ich das Programm als Gast starte?
