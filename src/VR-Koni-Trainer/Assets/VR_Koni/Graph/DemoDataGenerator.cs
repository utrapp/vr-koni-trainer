#nullable enable //The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.

using System;
using System.Collections.Generic;

namespace VR_Koni.Graph
{
    /// <summary>
    /// Shapes for which the demo data points are to be generated
    /// </summary>
    public enum Shapes
    {
        Linear,
        Parabola,
        Sine,
        Exponential
    }

    /// <summary>
    /// Generates demo shapes and packs them into a <see cref="List{T}"/>
    /// </summary>
    public static class DemoDataGenerator
    {
        public static List<T> GenerateShapeData<T>(
            Shapes shape = Shapes.Parabola,
            float totalTime = 8.0f,
            float maxDepth = 8.0f,
            int numberOfPoints = 100)
        {
            return shape switch
            {
                Shapes.Parabola => GenerateParabola<T>(totalTime, maxDepth, numberOfPoints),
                Shapes.Linear => GenerateLinear<T>(totalTime, maxDepth, numberOfPoints),
                Shapes.Sine => GenerateSine<T>(totalTime, maxDepth, numberOfPoints),
                Shapes.Exponential => GenerateExponential<T>(totalTime, maxDepth, numberOfPoints),
                _ => GenerateParabola<T>(totalTime, maxDepth, numberOfPoints),
            };
        }

        private static List<T> GenerateParabola<T>(
            float totalTime,
            float maxDepth,
            int numberOfPoints)
        {
            List<T> dataPoints = new();
            float timeStep = totalTime / numberOfPoints;
            float a = maxDepth / (totalTime * totalTime / 4);
            float h = totalTime / 2;

            for (float t = 0; t <= totalTime; t += timeStep)
            {
                float depth = -a * (t - h) * (t - h) + maxDepth;
                dataPoints.Add(GenerateDataPoint<T>(t, depth));
            }
            return dataPoints;
        }

        private static List<T> GenerateLinear<T>(
            float totalTime,
            float maxDepth,
            int numberOfPoints) 
        {
            List<T> dataPoints = new();
            float timeStep = totalTime / numberOfPoints;

            for (float t = 0; t <= totalTime; t += timeStep)
            {
                float depth = maxDepth - (t / totalTime) * maxDepth;
                dataPoints.Add(GenerateDataPoint<T>(t, depth));
            }
            return dataPoints;
        }

        private static List<T> GenerateSine<T>(
            float totalTime,
            float maxDepth,
            int numberOfPoints)
        {
            List<T> dataPoints = new();
            float timeStep = totalTime / numberOfPoints;

            for (float t = 0; t <= totalTime; t += timeStep)
            {
                float depth = maxDepth - ((float)((Math.Sin(t * 2 * Math.PI / totalTime) + 1) * maxDepth / 2));
                dataPoints.Add(GenerateDataPoint<T>(t, depth));
            }
            return dataPoints;
        }

        private static List<T> GenerateExponential<T>(
            float totalTime,
            float maxDepth,
            int numberOfPoints) 
        {
            List<T> dataPoints = new();
            float timeStep = totalTime / numberOfPoints;

            for (float t = 0; t <= totalTime; t += timeStep)
            {
                float depth = (float)(maxDepth * Math.Exp(-2 * t / totalTime));
                dataPoints.Add(GenerateDataPoint<T>(t, depth));
            }
            return dataPoints;
        }

        private static T GenerateDataPoint<T>(float t, float depth)
        {
            if (typeof(T) == typeof(Tuple<float, float>))
            {
                return (T)(object)new Tuple<float, float>(t, depth);
            }
            else if (typeof(T) == typeof(System.Numerics.Vector2))
            {
                return (T)(object)new System.Numerics.Vector2(t, depth);
            }
            else
            {
                throw new InvalidOperationException("Unsupported type");
            }
        }
    }
}
