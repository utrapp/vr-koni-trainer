using System;
using UnityEngine;

namespace VR_Koni.ElectroSnare
{
    public class Timer : MonoBehaviour
    {
        public float ContactTime { get; private set; }
        private bool timerRunning;

        public static event Action<float> OnTimeChange;

        private void Start()
        {
            ElectroSnareVR.onFirstTimeCutChanges += SetTimer;
        }

        private void OnDisable()
        {
            ElectroSnareVR.onFirstTimeCutChanges -= SetTimer;
        }

        private void Update()
        {
            if (timerRunning)
            {
                ContactTime += Time.deltaTime;
                OnTimeChange?.Invoke(ContactTime);
            }
        }

        private void SetTimer(ElectroSnareVR.CuttingState cuttingState)
        {
            // fist time cutting => start timer
            if (cuttingState == ElectroSnareVR.CuttingState.HealthyTissue)
                timerRunning = true;

            // else possible but in case of the need of differentiation we leave it as if
            if (cuttingState != ElectroSnareVR.CuttingState.HealthyTissue)
                timerRunning = false;
        }
    }
}
