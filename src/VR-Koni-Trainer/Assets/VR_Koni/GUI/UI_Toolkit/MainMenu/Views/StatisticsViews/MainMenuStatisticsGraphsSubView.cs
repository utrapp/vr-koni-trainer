using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.UIElements;
using VR_Koni.GUI.UI_Toolkit.Components.Charts;
using VR_Koni.UserData;

namespace VR_Koni.GUI.UI_Toolkit.MainMenu.Views.StatisticsViews
{
    public class MainMenuStatisticsGraphsSubView : IMainMenuView
    {
        public LocalizedString ViewName => new LocalizedString();
        private VisualElement scrollViewElement;
        public VisualElement RootElement { get; private set; }
        private LineChart depthLineChart;
        private PieChart winLossPieChart;
        private PieChart lletzScorePieChart;
        
        private void AddDataToPieChart(PieChart chart, string legend, float value, Color color)
        {
            chart.AddData(legend, value, color);
        }
        public void SetupVisualElements(VisualElement rootElement)
        {
            RootElement = rootElement;
            scrollViewElement = rootElement.Query<ScrollView>("graphScrollView");
            
            depthLineChart = scrollViewElement.Q<LineChart>("DepthLineChart");
            winLossPieChart = scrollViewElement.Q<PieChart>("WinLossPieChart");
            lletzScorePieChart = scrollViewElement.Q<PieChart>("LLETZScorePieChart");
            
            AddDataToLineCharts();
            AddDataToPieCharts();
        }

        private void AddDataToPieCharts()
        {
            const string TESTWERTE = "Testwerte";
            if (DataManager.Instance.CurrentUser != null && DataManager.Instance.CurrentUser.Surgeries.Count != 0)
            {
                List<Surgery> surgeries = DataManager.Instance.CurrentUser.Surgeries;
                
                    int LLETZScore1 = surgeries.Count(s => s.LLETZ >= 0 && s.LLETZ < 4);
                    int LLETZScore2 = surgeries.Count(s => s.LLETZ >= 4 && s.LLETZ < 6);
                    int LLETZScore3 = surgeries.Count(s => s.LLETZ >= 6 && s.LLETZ < 8);
                    int LLETZScore4 = surgeries.Count(s => s.LLETZ >= 8);

                    //Count how often a certain score was reached and then add the data to the corresponding PieChart
                    AddDataToPieChart(lletzScorePieChart, LLETZScore1 + "x " + "LLETZScore(0-4)", LLETZScore1, Color.red);
                    AddDataToPieChart(lletzScorePieChart, LLETZScore2 + "x " + "LLETZScore(4-6)", LLETZScore2, Color.blue);
                    AddDataToPieChart(lletzScorePieChart, LLETZScore3 + "x " + "LLETZScore(6-8)", LLETZScore3, Color.green);
                    AddDataToPieChart(lletzScorePieChart, LLETZScore4 + "x " + "LLETZScore(>8)", LLETZScore4, Color.yellow);
               
                    //This part has yet to be implemented since we currently don't have access to the information on
                    //whether the surgery was a win or loss. Therefore we still use fake data
                    AddDataToPieChart(winLossPieChart, "3x " + TESTWERTE, 3, Color.green);
                    AddDataToPieChart(winLossPieChart, "5x " + TESTWERTE, 5, Color.red);
                
            }
            else
            {
                AddDataToPieChart(lletzScorePieChart, "3x " + TESTWERTE, 3, Color.red);
                AddDataToPieChart(lletzScorePieChart, "7x " + TESTWERTE, 7, Color.blue);
                AddDataToPieChart(lletzScorePieChart, "2x " + TESTWERTE, 2, Color.green);
                AddDataToPieChart(lletzScorePieChart, "5x " + TESTWERTE, 5, Color.yellow);
                
                AddDataToPieChart(winLossPieChart, "3x " + TESTWERTE, 3, Color.green);
                AddDataToPieChart(winLossPieChart, "5x " + TESTWERTE, 5, Color.red);
            }
        }

        private void AddDataToLineCharts()
        {
            if (DataManager.Instance.CurrentUser != null && DataManager.Instance.CurrentUser.Surgeries.Count != 0)
            {
                // the first surgery is the most recent one
                List<Tuple<float, float>>  cuttingData = DataManager.Instance.CurrentUser.Surgeries.First().FetchCuttingDataFromDB();
                List<Vector2> points = new ();
                // prepare the data: scale to mm, remove the data before and after the real cut
                int indexStartCutting = cuttingData.FindIndex(0, t => t.Item2 > 0);
                Tuple<float, float> lastCuttingPoint = cuttingData.LastOrDefault(t => t.Item2 > 0);
                int indexFinishCutting = lastCuttingPoint == null ? 
                    cuttingData.Count - 1 : cuttingData.IndexOf(lastCuttingPoint);
                float offset = cuttingData[indexStartCutting].Item1;
                for (int i = indexStartCutting; i <= indexFinishCutting; i++)
                {
                    Tuple<float, float> tuple = cuttingData[i];
                    points.Add(new Vector2(tuple.Item1 - offset, tuple.Item2*1000));//scale to mm
                }
                depthLineChart.AddData(points, "Cutting Data", Color.blue);
            }
            //Added this part in case the User isn't logged in but still wants to see a graph
            else
            {
                List<Vector2> points = new ();
                depthLineChart.AddData(points, "no data", Color.red);
                
            }
        }

        public void SubscribeToEvents() {}
        public void UnsubscribeFromEvents() {}
        public void ResetViewSelections() {}
    }
}
