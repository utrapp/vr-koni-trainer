# ☯ Teamwork

- We don't know the solution either, but we surely have an idea. Use our experience and discuss alternatives with us, ...
- Team != Great, someone else does it (literally 'Toll Ein Anderer Machts')
  - 'I have done my job' is not enough!
- Use the time on site effectively
- Work effectively -- you will be more effective if you do not spend hours with trial and error, but e.g. ask for help after 30 min. At night and on weekends it is more difficult to find support ... Effectively means, results in time.
- Keep commitments. Communicate delays/problems/obstacles as early as possible.
- [Tuckman Phase Model](https://teamentwicklung-lab.de/tuckman-phasenmodell/) (in German)
- [Belbin team roles](https://karrierebibel.de/belbin-teamrollen/) (in German)

## Communication

The team meets on Wednesday at the h_da and an additional day during the week without the lecturers (Fri if possible). Communication with the physicians will only occur after consultation with the lecturers.

Discord:

- Address conflicts early and in an appreciative manner and resolve asap.
- Do retros

### Sources for better communication

- [Systemic Questions](https://www.business-wissen.de/hb/beispiele-fuer-systemische-fragen/) (in German)
- [Feedforward](https://arras-consulting.de/feedback-oder-feedforward/) (in German)
- [Transactional Analysis](https://karrierebibel.de/transaktionsanalyse/) (in German)
- [Communication Square](https://www.schulz-von-thun.de/die-modelle/das-kommunikationsquadrat) (in German)

## Stakeholders

- Medical expertise: Dr. Anne Cathrine Quenzer and Dr. Matthias Kiesel
- Blender expert: Adam Kalisz
- Graphics/Unity: Benjamin Meyer
- C#/application context/coaching Scrum Master: Ute Trapp

## Scrum Team

You work as a cross-functional team.

- [Scrumguide](https://scrumguide.de/)
- Scrum Master (<50% of working time) moderates and prepares meetings, ensures issues are estimable and prioritized, clarifies problems, documents processes appropriately.
- Product Owner (<40% of working time) formulates the feature issues and prioritizes them in consultation with the lecturers.
- Scrum Master and Product Owner ensure regular grooming.
- The team formulates non-feature issues, works on the issues, contributes ideas and addresses problems independently. At review, the team presents the increment and discusses alternatives in meetings.

## General Learning Outcome of [BPSE](https://obs.fbi.h-da.de/mhb/modul.php?id=-200008953)
You can develop a complex application in an agile, cross-functional team by:
- Independently familiarizing yourself with the technologies to be used
    - Recognizing what you know and what knowledge and skills you still need
- Defining milestones
    - Breaking down problems into simple sub-problems
    - Clearly formulating sub-problems using INVEST criteria and acceptance criteria, and regularly prioritizing and estimating them
    - Carefully planning each milestone (dependencies, what can be done in parallel, available resources (people, time, knowledge, materials such as VR headsets))
- Addressing the sub-problems/issues in a testable, structured, **goal-oriented**, customer-focused, and efficient manner using best practices and SOLID principles
    - Examining solutions for similar sub-problems and deriving best practices for your problem
- Merging/integrating partial results into the overall project weekly
- Conducting at least 2 team retrospectives
- Reflecting on, discussing, and planning project progress weekly
- Communicating proactively
- Acting in various team roles
- Presenting your project work appropriately and reflectively
- Documenting the project organization and development suitably for subsequent teams

This approach will enable you to later develop good solutions for real problems in a diverse and agile team, potentially improving people's work methods or quality of life.

### This BPSE
You can develop an initial version in the open-source project VR-Koni-Trainer that we can deliver to the Women's Clinic in Würzburg by:

- Independently familiarizing yourself with the technologies used:
  - GitLab (branches, merge, issues and board, wiki)
  - Scrum (roles, review, retro, planning, communication)
  - Unity (GameObject, ScriptableObject, RigidBody, Prefab, Scene, Collider/Trigger, Mesh, Raycast, Localization)
  - C# (event, interface, ...)
  - Design Patterns (Singleton, State, ...)
  - UI Toolkit (USS with variables and BEM, UXML with binding)
  - XR Interaction Toolkit (setup, interactors, interactable, interaction manager, XR Device Simulator)
- Understanding the project created so far and the requirements
- As Product Owner:
  - Define milestones and clearly formulate user stories for your assigned milestone
  - Critically and motivationally evaluate sprint results
- As Scrum Master:
  - Define and refine issues (INVEST) with PO, team members and stake holders
  - Plan, moderate, and document team meetings
  - Plan and moderate a retrospective after the sprint
  - Reflect on and evaluate project progress together
  - Develop sprint goals and meaningful weekly schedules
- As Developer:
  - Break down user stories into sub-problems and estimate them
  - Formulate issues clearly with acceptance criteria
  - Work on issues continuously and transparently
  - Define spikes early if knowledge is lacking or uncertainty exists
  - Address issues using best practices and SOLID principles
    - Examine solutions for similar sub-problems and derive best practices
    - Merge/integrate partial results into the overall project weekly
  - Proactively communicate progress, problems, approaches, and dependencies -- yours and overall project
  - As 2D developer: familiarize yourself with Unity Toolkit
  - As VR developer: familiarize yourself with VR
- As QM/GitLab Head:
  - Update commit, branching, and merge rules
  - Perform quality-assured and timely merges
  - build exe regularly

This approach will enable you to develop an initial version of the VR-Koni-Trainer for the Women's Clinic in Würzburg while working in an agile, cross-functional team.

#### Evaluation criteria

- Communication skills
- Team ability
- Qualitative processing of story points, use weight in issues and planning poker to determine the weight
- Active advancement of the project
- Ability to learn and critical faculties
- Concentrated, planed and effective way of working
- Professional use of gitlab etc.

#### Grading
- 1.0: Self-organized, on schedule, customer-oriented, collaborative, best practice, reflective, sustainable, efficient, excellent clear communication, various roles assumed, project actively and effectively advanced 
- 3.0: All customer requirements considered, collaborative, timely, functional 
- 4.0: Parts implemented functionally