using UnityEngine;
using UnityEngine.UI;

namespace VR_Koni.GUI.Popups
{
    public class PauseMenu : FadingMenu
    {
        [SerializeField] private Button buttonResume;
        [SerializeField] private Button buttonResetPosition;
        [SerializeField] private Button buttonBackToMainMenu;
        [SerializeField] private Slider sliderVolume;

        //private readonly SteamVR_Action_Boolean openMenuButton = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("default", "OpenPauseMenu");
        private bool openMenuButton = false;

        private void Start()
        {
            HideMenu(true);

            //TODO: needs to resume the operation, maybe hide all screens
            buttonResume.onClick.AddListener(() => HideMenu());

            //TODO: needs to reset the position of the user, not reset the operation. Is also broken when used in 2D
            //buttonResetPosition.onClick.AddListener(() => GUIManager.Instance.RestartGameScene());
            buttonBackToMainMenu.onClick.AddListener(ApplicationManager.SwitchToMainMenu);
            sliderVolume.onValueChanged.AddListener(value => AudioListener.volume = value);

        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape) || openMenuButton)
                ShowMenu();
        }
    }
}
