using System;
using UnityEngine;
using UnityEngine.UI;

namespace VR_Koni.Tutorials
{
    public class ElectroSnareChecker : MonoBehaviour
    {
        public GameObject electroSnare;
        public Button nextButtonTask2_1;
        public Button nextButtonTask2_2;
        public GameObject task2_1;
        public GameObject task2_2;

        private void Start()
        {
            Debug.Log("Electro Snare Checker Start");
            nextButtonTask2_1.gameObject.SetActive(false);
            nextButtonTask2_2.gameObject.SetActive(false);
        }

        public void SetSelectedState(bool state)
        {
            if (state == true)
            {
                OnSnareGrabbed();
            }
            else
            {
                OnSnareReleased();
            }
        }
        
        private void OnSnareGrabbed()
        {
            if (IsTask2_1Active())
            {
                Debug.Log("Snare grabbed");
                nextButtonTask2_1.gameObject.SetActive(true);
            }
        }
        
        private void OnSnareReleased()
        {
            if (IsTask2_2Active())
            {
                Debug.Log("Snare released");
                nextButtonTask2_2.gameObject.SetActive(true);
            }
        }
        
        private bool IsTask2_1Active()
        {
            return task2_1 != null && task2_1.activeSelf;
        }
        
        private bool IsTask2_2Active()
        {
            return task2_2 != null && task2_2.activeSelf;
        }
    }
}
