# Usability tasks 20.12.23

Fragen bzgl Orga: 
Haben wir bereits das VR Tutorial bis zum 20.12 integriert? Wie ruft man es dann auf?
Inwiefern nochmal auf schon mit Herr Kiesel besprochene Punkte eingehen?


## Task 1
**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und wollen nun zum ersten mal den VR Koni Trainer starten. Wie gehen Sie vor?

**Inputs**
- War der Startprozess für Sie klar?


## Task 2

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und wollen sich nun einen Account für den Koni Trainer anlegen. Wie gehen Sie vor?

**Inputs**
- Sind die geforderten Eingaben für Sie klar?
- Ist der Registrierungsprozess für Sie angenehm und einfach gestaltet?


## Task 3

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und haben noch nie eine Konisation durchgeführt. Sie wissen also nicht genau was auf Sie zukommt. Wie gehen Sie vor, um sich mit dem Verfahren vertraut zu machen?

**Inputs**
- Fühlen Sie sich nun gut mit dem Verfahren vertraut?
- Würden Sie sich weitere Informationen wünschen? Wenn ja, welche?
- Ist die Darstellung der Informationen für Sie hilfreich?
- Was könnte verbessert werden?


## Task 4

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und benutzen den Koni Trainer zum ersten mal. Sie wissen nicht wie die Steuerung des Spiels funktioniert und wollen sich nun mit ihr vertraut machen. Wie gehen Sie vor?

**Inputs**
- Fühlen Sie sich gut vorbereitet um die Controller auch im SPiel zu benutzen?
- Ist die Erklärung für Sie ausreichend?
- Könnte man die Steuerung Ihrer meinung nach noch besser Erklären/Darstellen?


## Task 5

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und wollen, bevor Sie das erste Training starten, die Lautstärke im Spiel auf ihre Präferenzen anpassen. Wie gehen Sie dabei vor?

**Inputs**
- Waren die Einstelungen für Sie gut zu erreichen?
- Sind alle nötigen Einstellungen vorhanden?


## Task 6
**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und haben noch keine Erfahrung mit VR. Wie gehen Sie vor um sich damit vertraut zu machen?

**Inputs**
- FÜhlen Sie sich nun ausreichend mit der VR-Umgebung vetraut?
- Fehlen Ihnen noch Informationen oder Übungen?


## Task 7

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und wollen nun ihr erstes Training starten. Wie gehen Sie dabei vor?

**Inputs**
- War der Weg bis in das Training klar?
- Waren alle Anweisungen gut verständlich?

_________________________________IM_SPIEL____________________________________________________________________________________________

## Task 8

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und wollen zum ersten mal die Schlinge greifen und die Benutzung testen. Wie gehen sie dabei vor?

**Tasks**
- Greifen Sie die Schlinge und schalten Sie sie an und wieder aus
- Legen Sie die Schlinge wieder hin

**Inputs**
- War die Benutzung der Schlinge für Sie leicht?
- Konnten Sie die Schlinge gut greifen?
- Konnten Sie sich noch an die Steuerung erinnern?


## Task 9

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und wollen sich vor der Trainings-OP erstmal das zu entfernende Gewebe genauer anschauen. Wie gehen Sie dabei vor?

**Inputs**
- Ist das auszuschneidende Gewebe für Sie gut erkenntlich?
- War die Benutzung der vergrößerten Ansicht (Lupe) für Sie angenehm?
- Verlassen Sie die vergößerte Ansicht 


## Task 10

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und wollen nun Ihre erste Trainings-OP durchführen, wie gehen Sie dabei vor?

**Inputs**
- Wie ist ihr erster Eindruck nach der OP?
- Waren alle nötigen Informationen währen der OP vorhanden?
- WÜrden Sie sich weitere Informationen während der OP wünschen?
- Wie fühlt sich die Handhabung und Steuerung während der OP für Sie an?
- Starten Sie eine weitere OP (repeat Funktion).
- War es leicht für Sie eine weiter OP zu starten?
- Ist ihnen noch irgendwas zu den Fragen vorher aufgefallen?

_________________________________IM_SPIEL____________________________________________________________________________________________

## Task 11

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und wollen sich nach dem Training mit Ihren Ergebnissen vertraut machen. Wie gehen Sie dabei vor?

**Inputs**
- Sind alle dargestellten Werte für Sie klar? Und auch klar erkennbar (gut zu lesen etc.)
- Wie finden Sie die Navigation zwischen den Bildschirmen?
- Fehlen Werte?
- Sind die angegebenen Werte für Sie angenehm dargestellt? (3D Ausschnitt der Konisation)


## Task 12

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und wollen sich Entwicklung ihrer Ergebnisse über die letzte Zeit ansehen. Wie gehen Sie dabei vor?

**Inputs**
- Sind alle dargestellten Werte für Sie klar? Und auch klar erkennbar (gut zu lesen etc.)
- Sind die angegebenen Werte für Sie angenehm dargestellt? (Graphen)


## Task 13

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und werden während einer Trainings-OP zu einem Notfall gerufen. Wie gehen Sie vor?
- Hier frage ich mich ob das getestet werden soll?

oder:

Stellen Sie sich vor, Sie sind angehende Ärztin und wollen sich Ihre Statistiken nun nochmal am normalen Computerbildschirm ansehen. Wie gehen Sie dabei vor?



## Task 14

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und haben alles abgeschlossen was Sie am Koni Trainer machen wollten. Sie wollen das Programm nun beenden. Wie gehen Sie vor?

**Inputs**
- Ist es Ihnen leicht/schwer gefallen das Programm zu beenden?
- War die Aktion hinter den Buttons die, die Sie erwartet haben?