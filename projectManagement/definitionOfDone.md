# Definition of Done

1. Acceptance criteria met
2. Code runs without errors
3. Tests implemented
4. Code inspection shows no relevant warnings
5. Code review performed (see 6., below at gitlab - workflow)
6. Before the merge request the master branch was merged
7. Merge request made & accepted by git head, comments implemented
8. CI runs without warnings
9. Readme added

## Goals of a code review

- Improve code
- Learn from each other
- Implement best practices, SOLID
- Avoid errors
- Consistency in the whole project
- Perform merge requests without problems if possible -- whoever performs the MR hopes for a previously
  well done code review, but will look at the code again, i.e. will not simply trust that everything has already been cleaned up/improved in the code review ...
