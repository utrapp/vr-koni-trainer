using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class TabMenuManger
{
    private VisualElement activeMainPanelTemplate;
    private Button lessonsButton;
    private Button learningObjectivesButton;
    private ScrollViewManager scrollViewManager;

    public void SetActiveMainPanelTemplate(VisualElement mainPanelTemplateName)
    {
        activeMainPanelTemplate = mainPanelTemplateName;
        activeMainPanelTemplate.style.display = DisplayStyle.Flex;
        lessonsButton = InitialiseButton("LessonsButton", OnLessonsButtonClick);
        learningObjectivesButton = InitialiseButton("LearningObjectivesButton", OnLearningObjectivesButtonClick);
        Debug.Log(activeMainPanelTemplate.name);
        
        ShowLessonsOverview();
        
        scrollViewManager = new ScrollViewManager();
        scrollViewManager.SetRootElement(mainPanelTemplateName);
    }

    public void OnLessonsButtonClick(ClickEvent evt)
    {
        Debug.Log("Lessons Button pressed");
        ShowLessonsOverview();
    }

    private void ShowLessonsOverview()
    {
        activeMainPanelTemplate.Q("LessonsTabContentTemplate").style.display = DisplayStyle.Flex;
        lessonsButton.AddToClassList("selected");
        activeMainPanelTemplate.Q("LearningObjectivesTabContentTemplate").style.display = DisplayStyle.None;
        learningObjectivesButton.RemoveFromClassList("selected");
    }

    public void OnLearningObjectivesButtonClick(ClickEvent evt)
    {
        Debug.Log("Learning Objectives Button pressed");
        ShowLearningObjectivesOverview();
    }
    
    private void ShowLearningObjectivesOverview()
    {
        activeMainPanelTemplate.Q("LessonsTabContentTemplate").style.display = DisplayStyle.None;
        lessonsButton.RemoveFromClassList("selected");
        activeMainPanelTemplate.Q("LearningObjectivesTabContentTemplate").style.display = DisplayStyle.Flex;
        learningObjectivesButton.AddToClassList("selected");
    }
    
    private Button InitialiseButton(string buttonName, EventCallback<ClickEvent> callback)
    {
        Button button = activeMainPanelTemplate.Q(buttonName) as Button;
        if (button != null)
        {
            button.RegisterCallback<ClickEvent>(callback);
        }
        else
        {
            Debug.LogError($"{buttonName} not found");
        }

        return button;
    }
}
