using System.Collections.Generic;
using DocumentFormat.OpenXml.Drawing.Charts;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UIElements;

// demonstrate how to use the PieChart component in the scene playground/componenttest
// check/uncheck the component to be tested on the right hand side of the uidocument game object

namespace VR_Koni.GUI.UI_Toolkit.Components.Charts.ChartExamples
{
    [RequireComponent(typeof(UIDocument))]
    public class PieChartExample : MonoBehaviour
    {
        private PieChart pieChart;

        private void Start()
        {
            VisualElement root = GetComponent<UIDocument>().rootVisualElement;
            pieChart = root.Q<PieChart>("ExamplePieChart");
            AddDataToPieChart("LLETZScore(0-4)", 30, Color.red);
            AddDataToPieChart("LLETZScore(4-6)", 50, Color.blue);
            AddDataToPieChart("LLETZScore(6-8)", 15, Color.green);
            AddDataToPieChart("LLETZScore(>8)", 5, Color.yellow);
        }

        private void AddDataToPieChart(string legend, float value, Color color)
        {
            pieChart.AddData(legend, value, color);
        }
    }
}
