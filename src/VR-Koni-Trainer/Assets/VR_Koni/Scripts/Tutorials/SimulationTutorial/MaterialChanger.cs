using UnityEngine;

namespace VR_Koni.Tutorials
{
    public class MaterialChanger : MonoBehaviour
    {
        private Material oldMaterial;
        public Material newMaterial;

        public void ChangeMaterial()
        {
            Renderer renderer = GetComponent<Renderer>();
            if (renderer != null)
            {
                Material[] materials = renderer.materials;
                oldMaterial = materials[0];
                materials[0] = newMaterial;
                renderer.materials = materials;
                Debug.Log("Material changed.");
            }
            else
            {
                Debug.LogWarning("Render component not found.");
            }
        }

        public void ResetMaterial()
        {
            Renderer renderer = GetComponent<Renderer>();
            if (renderer != null)
            {
                Material[] materials = renderer.materials;
                materials[0] = oldMaterial;
                renderer.materials = materials;
                Debug.Log("Material reset.");
            }
            else
            {
                Debug.LogWarning("Render component not found.");
            }
        }
    }
}

