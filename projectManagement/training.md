# Familiarization 

In order for everyone to launch at full speed, a good and solid induction is necessary. 
According to the 7.5 ECTS and a 42 time hours week you have therefore approx. 10.5 time hours per week available for the project -- for averagely talented students with a solid programming education according to the bachelor degree. 

## 1. Week: 2024-10-16 - 2024-10-23
Create an issue, assign yourself and track time spent and progress.
- Installation, more detailed instructions in the [Wiki](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/wikis/home/Setup/Toolchain)  (1h, during first meeting)
- [ ] [Rider](https://www.jetbrains.com/rider/) from Jetbrains (students of the hda get a free version)
- [ ] [Unity](https://unity.com/download), **version to be discussed**, development with C#
- [ ] [Rider - Unity](https://www.jetbrains.com/help/rider/Unity.html) and [tips](https://unity.com/how-to/tips-optimize-jetbrains-rider-cross-platform-c-script-editor-game-developers)
  - [ ] Alternatively [VS Code](https://code.visualstudio.com/download)
    - [Video-Tutorial](https://www.youtube.com/watch?v=ihVAKiJdd40)
- [ ] [Git](https://git-scm.com/), ensure that [Git-LFS](https://git-lfs.com/) is available on the system (check with `git lfs --version`)!  
- Tutorial C# (approx. 3h)
    - [ ] [C# basics](https://net-framework.h-da.io/net-material/theory/basics/)
    - [ ] [async in Unity](https://docs.unity3d.com/2022.3/Documentation/Manual/JobSystem.html)
  - [ ] [Head First C# - Unity Labs, Rider Edition](https://github.com/head-first-csharp/fourth-edition/blob/master/Unity_Labs/Unity_Labs_Rider_Edition.pdf)

- Familiarization with Unity (approx. 3h)
  - [ ] [Unity book](https://resources.unity.com/games/unity-game-dev-field-guide?ungated=true), just read across for orientation
  - [ ] :video_camera: [Your first unity game](https://www.youtube-nocookie.com/embed/XtQMytORBmM)
  - [ ] [Unity Essentials](https://learn.unity.com/pathway/unity-essentials)
  
- Explore Examples (approx. 2h)
  - [ ] [QuizU](https://assetstore.unity.com/packages/essentials/tutorial-projects/quizu-a-ui-toolkit-sample-268492) and :video_camera: [Walkthrough](https://www.youtube.com/watch?v=XlDdBUyI8fE)
  - [ ] [VR Template](https://docs.unity3d.com/Packages/com.unity.template.vr@9.0/manual/index.html) -- you need to run it using the Valve or the [simulator](https://docs.unity3d.com/Packages/com.unity.xr.interaction.toolkit@3.0/manual/xr-device-simulator-overview.html)

- Present learnings/an own example in Unity of one of the following topics (distribute all topics within the group) on 2024-10-23 (approx. 2h)
  - C# events in Unity 
  - asynchronous programming in Unity
  - debugging in Unity and Rider
  - Structure of Unity Projects, basic concepts like scene, game object, prefab
	- RigidBody, Collider, Trigger
	- Meshes: Vertices, ...
	- Ray cast
  - ScriptableObject and MonoBehaviour
  - Gitlab conventions of the project (KMI, pair)
  - Coding Conventions of the project (KMI, pair)

## 2. Week: 2024-10-23 - 2024-10-30
Create an issue, assign yourself and track time spent and progress.
- Team VR
  - all: Get started with VR (approx. 5h)
    - [ ] [create with vr](https://learn.unity.com/course/create-with-vr)  1.3, 2.2, 2.3, 2.4
  - PO & Scrum Master (approx. 2,5h)
    - [ ] Check issues, docs including nextcloud
    - [ ] Understand your role
    - [ ] Discuss and define milestones, send them as email to Trapp (Monday 2024-10-28, 11 am)
  - Developer (approx. 2,5h)
    -  Attempt to reproduce the crash/freeze occurring in Unity during operations related to the snare functionality. Document any patterns or specific conditions that lead to a crash.
        - Investigate if the crash occurs when the snare is set to a particularly deep level.
        - Determine if the crash happens when switching off the snare while cutting operations are ongoing.
        - Explore other potential scenarios that might trigger a crash.
        - Add your findings and any new observations directly to [issue #374](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/issues/374)
    - Try to model a colposcope in Blender.
    - Consider how a cylinder or similar shape could model the vagina, so that the sling can still be inserted and the object to be operated on does not float freely, allowing for more controlled manipulation.

- Team 2D
  - all: Get started with Unity UI Toolkit (approx. 5h)
    - [ ] [Working with UI Toolkit](https://www.youtube-nocookie.com/embed/6DcwHPxCE54)
    - [ ] [Playlist Unity UI Toolkit](https://www.youtube.com/playlist?list=PLosiZFS_rnz5pjWsiFKPmf-n_hbCW2m8B)
    - [ ] Understand 
      - [ ] [Layout](https://docs.unity3d.com/6000.0/Documentation/Manual/UIE-LayoutEngine.html)
      - [ ] [USS Variables/Custom Properties](https://docs.unity3d.com/2023.2/Documentation/Manual/UIE-USS-CustomProperties.html)
      - [ ] [BEM](https://docs.unity3d.com/2023.2/Documentation/Manual/UIE-USS-WritingStyleSheets.html)
      - [ ] [MVP](https://discussions.unity.com/t/quizu-the-model-view-presenter-pattern/311043) and [Data Binding](https://docs.unity3d.com/2023.2/Documentation/Manual/UIE-data-binding.html) -- unfortunately there is no command binding, i.e. you need to register events
        - [ ] [Serialized Object Data Binding](https://docs.unity3d.com/6000.0/Documentation/Manual/UIE-Binding.html)
      - [ ] [Custom Controls](https://docs.unity3d.com/6000.0/Documentation/Manual/UIE-create-custom-controls.html)
      - [ ] [State Maschine](https://docs.unity3d.com/2023.2/Documentation/Manual/StateMachineBasics.html)
  - PO & Scrum Master (approx. 2,5h)
    - [ ] Check issues, docs including nextcloud
    - [ ] Understand your role
    - [ ] Discuss and define milestones, send them as email to Trapp (Monday 2024-10-28, 11 am)
  - Developer (approx. 2,5h)
    - Look at other libraries, for example 
    [UIToolkit charts & plotting](https://github.com/noirb/UIT_Charts) or [Chart.js](https://www.chartjs.org/docs/latest/), and consider 
    which methods and properties we need.  We will not use Java Script!
        - Try to draw a coordinate system with subdivisions and labels. 
        - Try to draw a parabola. 
        - You can, but don't have to, work in the project itself. 
        - Look at the values stored in the database after a cut, for example 
        using the debug scene. 
        - In the end, we need multiple operations represented as line charts in a graph.
        - Work collaboratively, preferably in pairs, and discuss with each other.

### Further Reading/Links
- [Unity best practices](https://unity.com/how-to)
- VR
  - [Unity Manual](https://docs.unity3d.com/Manual/VROverview.html)
  - Setup VR with OpenXR
      - [VR Setup](https://www.youtube.com/watch?v=F4gDjCLYX88&list=PLwz27aQG0IIK88An7Gd16An9RrdCXKBAB&index=13)
      - [VR Interactables](https://www.youtube.com/watch?v=furfe8E7SOA&list=PLwz27aQG0IIK88An7Gd16An9RrdCXKBAB&index=11)
      - [Playlist VR Basics](https://www.youtube.com/playlist?list=PLX8u1QKl_yPD4IQhcPlkqxMt35X2COvm0)
  - Setup VR with SteamVR
      - [Valve Index in Unity](https://valvesoftware.github.io/steamvr_unity_plugin/)
      - [Quickstart / Download](https://valvesoftware.github.io/steamvr_unity_plugin/articles/Quickstart.html)
      - [First Steps / Getting Started](https://valvesoftware.github.io/steamvr_unity_plugin/articles/Interaction-System.html)
- 2D: Unity Toolkit 
  - [Welcome to the new UI Toolkit sample project QuizU](https://discussions.unity.com/t/welcome-to-the-new-ui-toolkit-sample-project-quizu/308607)

