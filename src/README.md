# VR-Coni-Trainer

- [Rider](https://www.jetbrains.com/rider/) by Jetbrains (students of the hda get a free version)
- [Unity](https://unity.com/download), current [LTS Tech Stream](https://unity.com/releases/lts-vs-tech-stream) version, development with C# 
- [Configuring your Unity Project for XR](https://docs.unity3d.com/2022.2/Documentation/Manual/configuring-project-for-xr.html) or use the given template
- prepare for gitlab, see https://learn.unity.com/tutorial/unity-tips#5c7f8528edbc2a002053b47d


### Possible Options
- use [DataBinding](https://docs.unity3d.com/2022.2/Documentation/Manual/UIE-Binding.html)
- use [OpenXR](https://docs.unity3d.com/Packages/com.unity.xr.openxr@1.7/manual/index.html)
- [Easy Save](https://assetstore.unity.com/packages/tools/utilities/easy-save-the-complete-save-data-serializer-system-768)
- IoC???
  - [reflex](https://github.com/gustavopsantos/reflex) 
  - [VContainer](https://github.com/hadashiA/VContainer)

---


## Code and Project Conventions

### Folder Structure

- Our assets: `VR_Koni/Feature/AssetType` (eg. `/src/VR_Koni/OperationRoom/Materials/brown.mat`)
- Testing/Learning assets: `Learning` (eg. `/src/Assets/Learning/...`)
- Third party learning assets: `ExampleAssets` (eg. `/src/Assets/ExampleAssets/...`)
- Third party: `Packages/PackageName` (eg. `/src/Packages/TextMesh Pro/...`)
    - **Attention:** Some third party packages are path sensitive (e.g. SteamVR)

### General rules

- Specify access level (public, private, etc.) every time
- Use readonly where possible
- 4 Spaces, no Tabs
- Use nameof() where possible
- Specify Fields on top of file
- Abbreviations should be limited to 2, with exception to 3
- Use LINQ where possible, avoid using GetEnumerator and/or traversing multiple times if possible
- Use lambda functions for events you don't need to remove later
- Always use string interpolation
- Use object initializers where possible
- Comments: Double Slash + Space + Message starting with Uppercase letter + period

---

### Naming

PublicFields: PascalCase `public int MyField;`\
PrivateFields: camelCase `private int myField;`\
Const: ALL_UPPER `public const string USER_ID;`\
LocalFields (inside a method): camelCase `int myField;`

---

Namespaces: Plain PascalCase

```csharp
using System;

namespace MyNamespace.SubNamespace
{
    public class MyClass
    {
        public void DisplayMessage()
        {
            Console.WriteLine("Hello from MyClass in MyNamespace.SubNamespace!");
        }
    }
}
```

---

Enums: PascalCase

```csharp
# Normal Singular
enum Color
{
    Red,
    Green,
    Blue,
    orange
}

# Flags Plural
[Flags]
enum Days
{
    None      = 0
    Monday    = 1
    Tuesday   = 2
    Wednesday = 4
    Thursday  = 8
    Friday    = 16
    Saturday  = 32
    Sunday    = 64
    Weekend   = Saturday | Sunday
}
```

---

Classes/Structs: PascalCase

```csharp
public class ProgramManagerFactory
{
    public struct ManagerItems
    {
        // Your code.
    }
}
```

---

Interfaces: Prefix with "I"

```csharp
interface ISampleInterface
{
    void SampleMethod();
}

public class ImplementationClass : ISampleInterface
{
    // Explicit interface member implementation:
    void ISampleInterface.SampleMethod()
    {
        // Method implementation.
    }
}
```

---

Methods: PascalCase\
Parameters: camelCase

```csharp
public class Program
{
  public void MyMethod(int myMethodInteger) 
  {
    // Method code
  }
}
```

---

Event-Field: Prefix with "On" (OnClicked).\
Time is important: OnSpacePressed != OnSpacePressing != OnSpacePress

```csharp
public class EventManager : MonoBehaviour 
{
    public delegate void ClickAction();
    public static event ClickAction OnSpacePressed;

    void Update()
    {
        if(Input.GetKeyDown("space"))
        {
            OnSpacePressed?.Invoke();
        }
    }
}
```

---

### Coding Styles

LINQ: method/lamdas

```csharp
IEnumerable<int> highScoresQuery = scores.Where(score => score > 80)
                                         .OrderByDescending(score => score);
```

---

Usage of Var: **Never use var**. Types can be omitted on object creation if it's clear from the same line

```csharp
ExampleClass instance = new ("MyString", 10.0f);
ExampleClass instance2 = new ChildExampleClass(11.0f, "MyString2");

// Code ...

instance = new ExampleClass("MyString2", 20.0f);
```

---

No single line if-Statements

```csharp
private class MyClass
{
  public static void Main()
  {
    int myAge = 18;
    bool isBirthday = true;
    if (isBirthday) { Console.WriteLine("Happy Birthday"); myAge++; } ❌
    if (myAge > 30) { Console.WriteLine("You look older now"); } ❌
  }
}
  
# How it should be done:
private class MyClass
{
  public static void Main()
  {
    int myAge = 18;
    bool isBirthday = true;
    if (isBirthday)
    {
        Console.WriteLine("Happy Birthday");  ✅
        myAge++;
    }
    
    if (myAge < 18)
    {
      Console.WriteLine("What's up champ?");  ✅
    }

    if (myAge > 30)
      Console.WriteLine("You look older now");  ✅
}
```
