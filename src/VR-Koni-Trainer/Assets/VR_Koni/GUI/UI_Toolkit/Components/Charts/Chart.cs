using Unity.Properties;
using UnityEngine.UIElements;
using UnityEngine;

namespace VR_Koni.GUI.UI_Toolkit.Components.Charts
{
    [UxmlElement]
    public abstract partial class Chart : VisualElement
    {
        // These are USS class names for the control overall and the label.
        private const string USS_CLASS_NAME = "chart";
        private const string USS_TITEL_CLASS_NAME = "chart__title";
        private const string USS_LEGEND_CLASS_NAME = "chart__legend";
        private const string USS_CANVAS_CLASS_NAME = "chart__canvas";
        private static readonly CustomStyleProperty<Color> chartColorStyleProperty = new ("--chart-color");
        protected VisualElement Canvas;
        protected Color ChartColor = Color.white;
        protected VisualElement LegendLabel;

        protected Label TitleLabel;

        protected Chart()
        {
            AddVisualElements();
            RegisterCallback<CustomStyleResolvedEvent>(OnStylesResolved); // handles the custom styles
            RegisterCallback<GeometryChangedEvent>(OnGeometryChanged); // handles the geometry changes, like resizing
        }


        [UxmlAttribute]
        [CreateProperty]
        public string Title
        {
            get => TitleLabel.text;
            set
            {
                TitleLabel.text = value;
            }
        }

        protected void OnGeometryChanged(GeometryChangedEvent evt)
        {
            MarkDirtyRepaint();
        }

        /// <summary>
        /// Add the title and legend label and an empty VisualElement for the canvas. The canvas uses the remaining space.
        /// </summary>
        protected void AddVisualElements()
        {
            // Create a Label, add a USS class name, and add it to this visual tree.
            TitleLabel = new Label();
            TitleLabel.AddToClassList(USS_TITEL_CLASS_NAME);
            TitleLabel.text = Title;
            Add(TitleLabel);
            Canvas = new VisualElement();
            Canvas.style.flexGrow = 1;
            Canvas.AddToClassList(USS_CANVAS_CLASS_NAME);
            Add(Canvas);
            LegendLabel = new VisualElement();
            LegendLabel.AddToClassList(USS_LEGEND_CLASS_NAME);
            Add(LegendLabel);
            // Add the USS class name for the overall control.
            AddToClassList(USS_CLASS_NAME);
        }

        private void OnStylesResolved(CustomStyleResolvedEvent evt)
        {
            evt.customStyle.TryGetValue(chartColorStyleProperty, out ChartColor);
        }

        protected void AddData(string label, Color color)
        {
            AddDataEntryToLegend(label, color);
        }

        protected void AddDataEntryToLegend(string label, Color color)
        {
            // Create the colored square (representing the dataset)
            VisualElement colorBox = new();
            colorBox.style.width = 10;
            colorBox.style.height = 10;
            colorBox.style.backgroundColor = color;

            // Add the legend item to the legend container
            LegendLabel.Add(colorBox);
            LegendLabel.Add(new Label(label));
        }

        protected abstract class DataEntry
        {
            public Color Color;
            public string Label;

            protected DataEntry(string label, Color color)
            {
                Label = label;
                Color = color;
            }
        }
    }
}
