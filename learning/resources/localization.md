Unity: UI Toolkit - Localization

- https://docs.unity3d.com/6000.0/Documentation/Manual/com.unity.localization.html

Follow instructions of 

1.	https://docs.unity3d.com/Packages/com.unity.localization@1.5/manual/Installation.html 
2.	https://docs.unity3d.com/Packages/com.unity.localization@1.5/manual/QuickStartGuideWithVariants.html
3.	https://docs.unity3d.com/Packages/com.unity.localization@1.5/manual/QuickStartGuideWithVariants.html#localize-strings
    - Name the collection, e.g. “StringTableCollection”
    - Add some keys, use a naming convention, e.g. <UXML><Name><Valuetype>
        - e.g. for the view *RegisterView.uxml*
            - for the Button with the name *RegisterButton*
                - attribute text: RegisterViewRegisterButtonText
                - attribute tooltip: RegisterViewRegisterButtonTooltip
            - for the Label with the name *AlreadyRegisteredLabel*
                - attribute text: RegisterViewAlreadyRegisteredLabelText
            - for the TextField mit name *UsernameTextField*
                - for the label: RegisterViewUsernameTextFieldLabel
                - for the tooltip: RegisterViewUsernameTooltip
        - general components, like Ok-Button, simply use *Ok*
    - Example Key: Welcome
4.	Use in [UXML](https://docs.unity3d.com/Packages/com.unity.localization@1.5/manual/UIToolkit.html#uxml-authoring)

```xml
xmlns:localize="UnityEngine.Localization"
    ...
<ui:Label text="???" name="WelcomeLabel" class="title">
    <Bindings>
        <localize:LocalizedString property="text" table="StringTableCollection" entry="Welcome" />
    </Bindings>
</ui:Label>
```
    - This will bind the property *text* of the label to the localized string of the key *Welcome*.
5.	Use in Code
```cs
using UnityEngine.Localization;
...
var localizedString = new LocalizedString("StringTableCollection", "Welcome");
```

6.	Change the local with
```cs
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
...
Locale germanLocale = LocalizationSettings.AvailableLocales.GetLocale("de");
LocalizationSettings.SelectedLocale = germanLocale;
```


## VR-Koni-Trainer
Localization was already added, package has latest version. 
- Localization folder: src\VR-Koni-Trainer\Assets\VR_Koni\Localization
- Created a new string localization collection for UI Toolkit stuff in 
	- src\VR-Koni-Trainer\Assets\VR_Koni\Localization\UIToolkit\
	- name: UIToolkitStringTableCollection

- Example in Assets/VR_Koni/GUI/UI_Toolkit/MainMenu/Views/MainMenuSettingsView.uxml