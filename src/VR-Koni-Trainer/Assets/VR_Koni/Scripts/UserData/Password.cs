using System;

namespace VR_Koni.UserData
{
    /// <summary>
    /// Class <c>Password</c> models a password, used to authenticate the user.
    /// </summary>
    [Serializable]
    public class Password
    {
        public string HashedPassword { get; private set; }

        // set => hashedPassword = value; // probably not a good idea, right?
        /// <summary>
        /// Constructor of the class Password.
        /// </summary>
        /// <param name="passwordString">Password string which can eiter be hashed or not.</param>
        /// <param name="isHashed">Flag which states, if the provided passwordString is hashed or not. Defaults to false.</param>
        public Password(string passwordString, bool isHashed = false)
        {
            HashedPassword = isHashed ? passwordString : Hashing.ToSHA256(passwordString);
        }

        /// <summary>
        /// Check the stored hashed Password against a provided clear text password.
        /// </summary>
        /// <param name="password">Clear text password which needs to be checked</param>
        /// <returns>A bool representing if the provided passwords hash is equal to the hashed password, stored by this class.</returns>
        public bool CheckPassword(string password)
        {
            // this implementation assumes, that SHA256 is collision safe
            return string.Equals(Hashing.ToSHA256(password), HashedPassword);
        }
    }
}
