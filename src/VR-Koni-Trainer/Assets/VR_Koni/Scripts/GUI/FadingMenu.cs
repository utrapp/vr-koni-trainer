using System;
using EditorCools;
using UnityEditor;
using UnityEngine;

namespace VR_Koni.GUI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class FadingMenu : MonoBehaviour
    {
        [SerializeField] private CanvasGroup canvasGroup;

        private const float FADE_SPEED = 4f;
        private float lerpTime;
        private float targetAlpha;

        private void Reset()
        {
            canvasGroup = GetComponent<CanvasGroup>();
        }

        [Button]
        public void ShowMenu()
        {
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;

            targetAlpha = 1;
            lerpTime = 0;

#if UNITY_EDITOR
            if (!EditorApplication.isPlaying) //When clicking the inspector button LateUpdate isn't being called
                canvasGroup.alpha = 1;
#endif
        }

        [Button(parameters: false)]
        public void HideMenu(bool skipFading = false)
        {
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;

            targetAlpha = 0;
            lerpTime = 0;

            if (skipFading)
                canvasGroup.alpha = 0;

#if UNITY_EDITOR
            if (!EditorApplication.isPlaying) //When clicking the inspector button LateUpdate isn't being called
                canvasGroup.alpha = 0;
#endif
        }

        // We are using LateUpdate as Update is used in inheriting classes
        protected void LateUpdate()
        {
            if (Math.Abs(canvasGroup.alpha - targetAlpha) > 0.001f)
            {
                lerpTime += Time.deltaTime;
                canvasGroup.alpha = Mathf.Lerp(Math.Abs(targetAlpha - 1f), targetAlpha, lerpTime * FADE_SPEED);
            }
        }
    }
}
