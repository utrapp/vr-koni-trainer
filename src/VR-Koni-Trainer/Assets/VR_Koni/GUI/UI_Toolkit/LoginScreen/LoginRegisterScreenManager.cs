using UnityEngine.UIElements;
using VR_Koni.GUI.UI_Toolkit.Helper;
using System;

namespace VR_Koni.GUI.UI_Toolkit.LoginScreen
{
    public class LoginRegisterScreenManager : UIManager
    {
        private readonly LoginView loginView = new();
        private readonly RegisterView registerView = new();
        private Button quitButton;
        public static Action<bool> LoginScreenSelected;

        public override void SetupVisualElements(VisualElement rootElement)
        {
            loginView.SetupVisualElements(rootElement.Q("LoginView"));
            registerView.SetupVisualElements(rootElement.Q("RegisterView"));
            quitButton = rootElement.Q<Button>("QuitButton");
            SwitchToLoginView();
        }

        public override void SubscribeToEvents()
        {
            LoginScreenSelected += ToggleScreens;
            quitButton.clicked += ApplicationManager.AskToQuit;
            loginView.SubscribeToEvents();
            registerView.SubscribeToEvents();
        }

        public override void UnsubscribeFromEvents()
        {
            quitButton.clicked -= ApplicationManager.AskToQuit;
            loginView.UnsubscribeFromEvents();
            registerView.UnsubscribeFromEvents();
            LoginScreenSelected -= ToggleScreens;
        }

        public override void ResetViewSelections()
        {
            loginView.ResetViewSelections();
            registerView.ResetViewSelections();
        }

        private void SwitchToLoginView()
        {
            registerView.RootElement.SetIsVisible(false);
            loginView.RootElement.SetIsVisible(true);
        }

        private void SwitchToRegisterView()
        {
            registerView.RootElement.SetIsVisible(true);
            loginView.RootElement.SetIsVisible(false);
        }

        
        private void ToggleScreens(bool showLoginScreen)
        {
            if (showLoginScreen)
            {
                SwitchToLoginView();
                
                //exit if showLoginScreen is true
                return;
            }
            SwitchToRegisterView();
        }
    }
}
