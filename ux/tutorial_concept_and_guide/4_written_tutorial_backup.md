# Interactive Tutorial

## Needs to be turned into text!

### Chapter 1

Explanation of the main menu.

- Statistics: ‘Here you find the results of all your training simulations’
- Guide: ‘Here you find information about conization, the simulation and controls’
- Settings: ‘Here you can adjust the sound volume’
- Close Simulation: ‘logs out user and closes the program’

-> ‘Click ‘start VR’

### Chapter 2

Explanation of the menu in VR and how to interact.

- ‘Removing the headset without leaving VR will lead to logout after 10 min’
-> ‘Aim here with the controller and press the trigger to continue’
- To 2D Menu: ‘Returns to Desktop’
- Logout: ’Returns to User Login on the Desktop’
- (Optional: Aufgaben zur Erklärung der
Steuerung, wenn weitere Optionen vorhanden)
- Start Operation: Starts training simulation

### Chapter 3

Detailed explanation how to interact with the KoniTrainer specifically.

- Picking up the loop electrode
- Rotate the sling by using the thumbstick
- Activate the sling by pressing B or the trigger
- This sound signals that the sling is active’
- let go of the sling: unless held the sling will always return to the tray
- ‘press the thumbstick: Open pause menu
- Reset Position: ‘resets your position to the middle of the VR room’
- Back to Menu:
- ‘resum operation’
- look through the magnifier: this color means normal, this one unnormal tisue
- make a cut to remove the white area

### Chapter 4

Explaines the diffrent statistics screens.

- The Order of the metriks implicates their overall importance for the conization
- the ideal values for [...] are an approximation based on [...]

- this histogram shows [...]
- ‘switch to excised cone
- ‘move cone by doing [...]
- ‘return to main menu
- ‘go to statistics’
- ‘select all statistics’
- open details of one attempt
- maximize excised cone
- maximize depth chart
