# Valve Index Headset

The fit of the VR Headset can be adjusted in the following wayes:

1. Adjust the Head Strap:  
To tighten the head strap turn the wheel on the back clockwise
2. Adjust the Top Strap:  
Pull or release the top strap to bring the screnn to the right height in front of your eyes and achive a better weight distribution

![Valve_Headset_Back](Headset_Scematics_Hinten.png)

3. Adjust the eye relief:  
Turn the wheel on the right side of the headset to move the display inside closer or further away from your eyes

4. Adjust the IPD:  
Use the slider on the underside of the headset until to adjust the distance between the lenses. If you do not know your IPD move the slider until you find the clearest image

![Valve_Headset_Underside](Headset_Scematics_Unterseite.png)


A more detailed guide and videos can found on the Stem Support Website:
https://help.steampowered.com/en/faqs/view/699A-ECD2-F839-760C