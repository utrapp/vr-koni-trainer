# Ergebnisse des Usability Probetests - 19.12.2023

Die Ergebnisse des Probetests wurden am 20.12.2023 zusammengefasst und analysiert. Das Ergebnis ist eine Sammlung von Issues, die weiter umformuliert und zusammengefasst werden kann.

### Dringendste Punkte & Issues

---

- **Pause Menu erstellen (Design)** [#160](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/issues/160)
  - Plan B: es ist möglich, den OP über das Pausenmenü zu beenden
  - Bug Fixing UI ->
- **Ray should not be displayed while operation** [#155](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/issues/155)
- **Notifications & Pop-ups** [#175](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/issues/175)

  - **add a notification when “Start VR” is clicked:** Bitte setzen Sie die VR Brille auf
  - **add a notification when back to 2D menu:** Bitte setzen sie die Brille ab
  - **add pop-up windows (or any way to notify):** Annika und Duc
    - When username is already used
    - When Password is repeated incorrectly
    - When login/registration successful
    - When logging in with the wrong username or password
    - → Identifizieren, wo noch Feedback für Eingaben nötig ist

- **Room Design Refactor** [183](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/issues/183) -> Shawkat
  - Alles ist zu hoch für eine sitzende Operation
  - Magnifier should be 30 cm away from cone
- **Fix endless Snare-Bug** [#159](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/issues/159) -> Philipp

  - Schlinge bleibt bestehen, wenn noch in der Hand und restart/back to menue (sollte sich klären, wenn Schlinge über Trigger aktivierbar wird)

  - chlinge bleibt bestehen, wenn losgelassen, aber noch in der Luft und restart/ back to menue

- **Bei Logout bleiben Strings in Name und Passwort im Anmeldefeld bestehen** [#168](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/issues/168) -> Philipp
- **(All Statistics --> Wird nicht gefüllt während man angemeldet ist, sondern erst bei logout login oder hängt das mit rausgehen aus 3D zusammen? Nach der OP sind sogar wieder alle weg.** --> Richard hat Idee woher der Anzeige-Bug kommt
  - -> Aber letzte OP wird korrekt angezeigt ) → Philipp und Richard?
- **Merging mit Wilpert Branch -> damit beenden von OP möglich wird**
- **Show statistics Menu when meshes created** → Philipp

### Punkte & Issues mit mittlerer Dringlichkeit

---

- **Rand schwarz färben, damit mein optisches Feedback zum Schnitt ist → bekommen wir Daten dafür während des Schnittes?**
- **when clicking “back to menu” → Menu goes left**
  Default orientation of Menu and Simulation scene needs to match
  - the surgery area is defined as ‘in front of the user’
  - all Menus need to appear in front of the user
  - ‘in front of the user’ is the default direction
- **Table in All Statistics is wrong formatted. Columns should be corrected** [#159](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/issues/159) -> Pierre
- **Interactive and non-interactive elements need to be better distinguishable -> Marc und Flo**
- **create a more appealing (and maybe natural) design for the gloves. Like blueish latex, etc…. Currently, it looks like coming from riding a motorbike.** -> Shawkat

### Andere Punkte & Issues

---

- **Flesh cutting sound should be less loud → Anne**
- **add notification: Did you calibrate the Headset?**
  - explain what problems could be caused (z.B Spawn am falschen Punkt)
- **All Users are currently created as admins**
- **Ray should be visible on both eyes?** [#156](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/issues/156)
- **Guide soll erkennen und zeigen, welcher Knopf gerade gedrückt ist**
  → Controller Erklärung sollte mit Stand übereinstimmen.

#### Needs Discussion

- Sollte die Schlinge automatisch in der Hand positioniert werden? (Snap in)
- Ist bisher kein Snap-in, weil sonst Zeigefinger und Daumen Position nicht mehr korrekt, müssten individuell anders funktionieren
- Sollte ein Hinweis eingefügt werden, wie die Schlinge gegriffen werden kann bzw. sollte?
- Neigen der Schlinge kann sonst zu sehr seltsamen Winkeln führen

- **Untere Optionsleiste für Start von VR nicht direkt ersichtlich**
  - Wie könnte man die Funktionen unten deutlicher machen?
- **A oder B ergonomischer für Loop Aktivierung?**
- **Wording für Wireloop/Electro Snare**

#### Fragen an Würzburger

- Sollen Versuche aus all Statistik Ansicht gelöscht werden können, wenn Sie keine richtigen waren.
- Abgebrochene OPs: Speichern oder nicht?
- Größe der Schlinge ?? → Vielleicht sind die Schlinge und die Koni im Verhältnis zu klein?
  - **Antwort:** Sollte passen. Weil Schlinge 2x2 cm.
