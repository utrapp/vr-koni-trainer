using TMPro;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.Interactables;
using VR_Koni.UserData;

namespace VR_Koni.GUI.Results
{
    public class ResultVictoryMenu : FadingMenu
    {
        [SerializeField] private TextMeshProUGUI labelLletzScore;
        [SerializeField] private TextMeshProUGUI labelMaxDepth;
        [SerializeField] private TextMeshProUGUI labelContactDuration;
        [SerializeField] private float cutoutScale;

        private GameObject cutoutObject;

        private void OnEnable() => DataManager.OnSurgeryEnded += SetResults;

        private void OnDisable() => DataManager.OnSurgeryEnded -= SetResults;

        private void SetResults(Surgery s)
        {
            cutoutObject = GameObject.Find("CancerCutoutPrep");
            cutoutObject.transform.eulerAngles = new Vector3(0f, 0f, 0f);
            cutoutObject.transform.localScale = new Vector3(cutoutScale, cutoutScale, cutoutScale);
            cutoutObject.GetComponent<XRGrabInteractable>().enabled = true;

            labelLletzScore.text = s.LLETZ.ToString();
            labelMaxDepth.text = s.MaxDepth.ToString("F3") + " mm";
            labelContactDuration.text = s.ContactTime.ToString("F3") + " s";
        }

        private void Update()
        {
            if (cutoutObject)
                cutoutObject.transform.Rotate(Vector3.up, 15 * Time.deltaTime);
            //cutoutObject.transform.RotateAround(cutoutObject.GetComponent<Renderer>().bounds.center, Vector3.up, 15 * Time.deltaTime);
        }
    }
}
