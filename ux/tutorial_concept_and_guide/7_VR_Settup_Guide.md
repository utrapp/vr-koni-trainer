# VR Setup Guide:

The following guide will lead you through all steps necessary to set up the Valve Index VR-Headset. The guide containes some tips specific to the KoniTrainer.

But you can also follow the Steam Setup Guide: https://help.steampowered.com/en/faqs/view/7F7D-77FB-8CAA-4329

Pick an Area for your setup:

For the Simulation you will assume a sitting position. Therefore you don't need much space to move around.
But make sure you have nothing in arms reach around you, that you could hit or knock over.

### Set up the two Index Base stations
![Valve_BaseStation](BaseStation_Scematics.png)

They need to be plugged into a power outlet and face towards the center of your play area, meaning if they are placed above you, tilt them down so they face the spot you will sit in.

### Connect the Headset to the PC.
The Headset headset cord splits into three ports. 

A DisplayPort that needs to be plunged into a matching port of your graphics card. (In case you are using a Laptop you may need an adapter)
A USB 3.0 port that you can connect to any USB3 Port of your computer. 
A Power adapter to connect the headset to an power outlet
	
### Turn on the right Index Controller

![Valve_BaseStation](Controller_Scematics_Power_on.png)

### Open Steam VR on the computer.

You should see a window with four green icons, resembling the Valve Headset, the right Controller and the two base Stations. Only the icon for the left Controller should be gray.

![Valve_BaseStation](T_Steam_VR_Ready.jpg)

If any other icon is also gray, check your set-up before continuing. If one of the control stations is shown as inactive, this might be because it has no direct line of sight to the headset, e.g. when the headset is placed behind it or an object is placed between the two.
Complete the SteamVR Room setup
	a. Open the menu on the top left and select 'Room Setup'
	b. select 'Standing only'
	c. follow the set-up instructions
	
![Valve_BaseStation](Steam_VR_room_setup.png)
	
Now your set-up is complete and you can start the KoniTrainer application.
Every time you change the position of a base station you need to repeat the room set-up in Steam VR.
