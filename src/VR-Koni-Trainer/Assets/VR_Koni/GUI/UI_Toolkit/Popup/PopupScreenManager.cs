using System;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using UnityEngine.UIElements;
using VR_Koni.GUI.UI_Toolkit.Helper;

namespace VR_Koni.GUI.UI_Toolkit.Popup
{
    [RequireComponent(typeof(UIDocument))]
    public class PopupScreenManager : MonoBehaviour
    {
        private static PopupScreenManager instance;
        private VisualElement rootElement;
        private VisualElement popupContainer;
        private Label headerLabel;
        private Label descriptionLabel;
        private Button leftButton;
        private Button rightButton;
        private Action currentActionLeft;
        private Action currentActionRight;

        private static LocalizedString currentHeaderLocalized;
        private static LocalizedString currentDescriptionLocalized;
        private static LocalizedString currentLeftButtonLocalized;
        private static LocalizedString currentRightButtonLocalized;

        private const int DEFAULT_HEADER_FONT_SIZE = 32;
        private const int DEFAULT_DESCRIPTION_FONT_SIZE = 20;

        //left button is probably always gonna be a cancel button or smth negative so make it red
        internal static readonly Color defaultLeftButtonColor = new(0.7f, 0.35f, 0.1f);
        internal static readonly Color defaultRightButtonColor = new(0.2f, 0.6f, 0.8f);

        private void UpdatePopupText()
        {
            if (!rootElement.visible) return;

            if (currentHeaderLocalized != null)
                headerLabel.text = currentHeaderLocalized.GetLocalizedString();
            if (currentDescriptionLocalized != null)
                descriptionLabel.text = currentDescriptionLocalized.GetLocalizedString();
            if (currentLeftButtonLocalized != null)
                leftButton.text = currentLeftButtonLocalized.GetLocalizedString();
            if (currentRightButtonLocalized != null)
                rightButton.text = currentRightButtonLocalized.GetLocalizedString();
        }

        private void OnEnable()
        {
            if (instance && instance != this)
            {
                Debug.LogError($"Two instances of {nameof(PopupScreenManager)} were instantiated in the same scene! Errors should be expected");
                return;
            }

            instance = this;
            UIDocument uiDoc = GetComponent<UIDocument>();
            uiDoc.enabled = true;
            rootElement = uiDoc.rootVisualElement;
            rootElement.SetIsVisible(false);

            popupContainer = rootElement.Q<VisualElement>("Popup");
            headerLabel = rootElement.Q<Label>("Header");
            descriptionLabel = rootElement.Q<Label>("Description");
            leftButton = rootElement.Q<Button>("LeftButton");
            rightButton = rootElement.Q<Button>("RightButton");

            popupContainer.style.alignItems = Align.Center;
            popupContainer.style.justifyContent = Justify.Center;

            headerLabel.style.unityTextAlign = TextAnchor.UpperCenter;
            descriptionLabel.style.unityTextAlign = TextAnchor.UpperCenter;

            leftButton.style.unityTextAlign = TextAnchor.MiddleCenter;
            leftButton.style.marginRight = 50; //space between the buttons
            rightButton.style.unityTextAlign = TextAnchor.MiddleCenter;

            TextOverflowHandler.AdjustTextToContainer(descriptionLabel, popupContainer);

            leftButton.clicked += OnLeftButtonClick;
            rightButton.clicked += OnRightButtonClick;

            LocalizationSettings.SelectedLocaleChanged += OnLocaleChanged;
        }

        /// <summary>
        /// <see cref="OnLocaleChanged"/> updates the locale settings for the popup when its active
        /// </summary>
        /// <remarks>
        /// <param name="locale"> is not used but is required to match the signature
        /// of <see cref="LocalizationSettings.SelectedLocaleChanged"/>
        /// </param></remarks>
        /// <param name="locale"></param>
        private void OnLocaleChanged(Locale locale)
        {
            UpdatePopupText();
        }

        private void OnDisable()
        {
            leftButton.clicked -= OnLeftButtonClick;
            rightButton.clicked -= OnRightButtonClick;
            instance = null;
        }

        private void OnLeftButtonClick()
        {
            rootElement.SetIsVisible(false);
            currentActionLeft?.Invoke();
        }

        private void OnRightButtonClick()
        {
            rootElement.SetIsVisible(false);
            currentActionRight?.Invoke();
        }

        /// <summary>
        /// Standard method to display a popup
        /// </summary>
        /// <param name="headerLocalized"></param>
        /// <param name="descriptionLocalized"></param>
        /// <param name="leftButtonTextLocalized"></param>
        /// <param name="actionLeft"></param>
        /// <param name="rightButtonTextLocalized"></param>
        /// <param name="actionRight"></param>
        /// <param name="headerFontSize"></param>
        /// <param name="descriptionFontSize"></param>
        /// <param name="leftButtonColor"></param>
        /// <param name="rightButtonColor"></param>
        public static void Show(
            LocalizedString headerLocalized,
            LocalizedString descriptionLocalized,
            LocalizedString leftButtonTextLocalized,
            Action actionLeft,
            LocalizedString rightButtonTextLocalized = null, //right button is optional 
            Action actionRight = null,
            int headerFontSize = DEFAULT_HEADER_FONT_SIZE,
            int descriptionFontSize = DEFAULT_DESCRIPTION_FONT_SIZE,
            Color? leftButtonColor = null,
            Color? rightButtonColor = null)
        {
            if (!instance)
            {
                Debug.LogError("PopupScreenManager.Show was called without it being initialised or enabled");
                return;
            }

            currentHeaderLocalized = headerLocalized;
            currentDescriptionLocalized = descriptionLocalized;
            currentLeftButtonLocalized = leftButtonTextLocalized;
            currentRightButtonLocalized = rightButtonTextLocalized;

            instance.rightButton.SetIsVisible(rightButtonTextLocalized != null);

            instance.headerLabel.text = headerLocalized.GetLocalizedString();
            instance.descriptionLabel.text = descriptionLocalized.GetLocalizedString();
            instance.leftButton.text = leftButtonTextLocalized.GetLocalizedString();
            //since right button is optional
            if (rightButtonTextLocalized != null)
            {
                instance.rightButton.text = rightButtonTextLocalized.GetLocalizedString();
            }

            instance.headerLabel.style.fontSize = headerFontSize;
            instance.descriptionLabel.style.fontSize = descriptionFontSize;

            instance.leftButton.style.backgroundColor = leftButtonColor ?? defaultLeftButtonColor;
            instance.rightButton.style.backgroundColor = rightButtonColor ?? defaultRightButtonColor;

            instance.currentActionLeft = actionLeft;
            instance.currentActionRight = actionRight;

            instance.rootElement.SetIsVisible(true);
        }


        /// <summary>
        /// use this as a temporary measure till the localization is not complete.
        /// Once localization is complete, <see cref="Show(string,string,string,System.Action,string,System.Action,int,int,UnityEngine.Color?,UnityEngine.Color?)"/>
        /// is obsolete and should be removed to force everyone to use <see cref="Show(string,string,string,System.Action,string,System.Action,int,int,UnityEngine.Color?,UnityEngine.Color?)"/>
        /// </summary>
        /// <param name="header"></param>
        /// <param name="description"></param>
        /// <param name="leftButtonText"></param>
        /// <param name="actionLeft"></param>
        /// <param name="rightButtonText"></param>
        /// <param name="actionRight"></param>
        /// <param name="headerFontSize"></param>
        /// <param name="descriptionFontSize"></param>
        /// <param name="leftButtonColor"></param>
        /// <param name="rightButtonColor"></param>
        [Obsolete(message:"Please use the localized variant instead. This method should be removed in a future version.", error: true)]
        public static void Show(
            string header,
            string description,
            string leftButtonText,
            Action actionLeft,
            string rightButtonText = null,
            Action actionRight = null,
            int headerFontSize = DEFAULT_HEADER_FONT_SIZE,
            int descriptionFontSize = DEFAULT_DESCRIPTION_FONT_SIZE,
            Color? leftButtonColor = null,
            Color? rightButtonColor = null)
        {
            if (!instance)
            {
                Debug.LogError("PopupScreenManager.Show was called without it being initialised or enabled");
                return;
            }


            instance.rightButton.SetIsVisible(!string.IsNullOrEmpty(rightButtonText));

            instance.headerLabel.text = header;
            instance.descriptionLabel.text = description;

            instance.leftButton.text = leftButtonText;
            instance.rightButton.text = rightButtonText;

            instance.headerLabel.style.fontSize = headerFontSize;
            instance.descriptionLabel.style.fontSize = descriptionFontSize;

            instance.leftButton.style.backgroundColor = leftButtonColor ?? defaultLeftButtonColor;
            instance.rightButton.style.backgroundColor = rightButtonColor ?? defaultRightButtonColor;

            instance.currentActionLeft = actionLeft;
            instance.currentActionRight = actionRight;

            instance.rootElement.SetIsVisible(true);
        }

        public static void Show(string translationKey, string translationKeyLeftButton, Action actionLeft,
                                string translationKeyRightButton, Action actionRight = null)
        {
            //TODO: get text with translationKey + suffix (e.g. -header -description etc)
            throw new NotImplementedException();
        }
    }
}
