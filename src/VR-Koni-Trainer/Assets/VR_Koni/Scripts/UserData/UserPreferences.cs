using System;

namespace VR_Koni.UserData
{
    /// <summary>
    /// Class <c>UserPreferences</c> models the preferences of a user.
    /// </summary>
    [Serializable]
    public class UserPreferences
    {
        /// <summary>
        /// <see cref="PreferredVolume"/> gets or sets the desired volume of the user.
        /// </summary>
        public double PreferredVolume { get; set; }

        /// <summary>
        /// <see cref="PreferredLanguage"/> gets or sets the local language of the user.
        /// </summary>
        public string PreferredLanguage { get; set; }

        /// <summary>
        /// <see cref="UserPreferences"/> initializes a new instance of the <see cref="UserPreferences"/> class with default values.
        /// </summary>
        public UserPreferences()
        {
            PreferredVolume = 100.0; 
            PreferredLanguage = "en"; 
        }

        /// <summary>
        /// <see cref="UserPreferences"/> initializes a new instance of the <see cref="UserPreferences"/> class with specific values.
        /// </summary>
        /// <param name="preferredVolume">The desired volume of the user.</param>
        /// <param name="preferredLanguage">The local language of the user.</param>
        public UserPreferences(double preferredVolume, string preferredLanguage)
        {
            PreferredVolume = preferredVolume;
            PreferredLanguage = preferredLanguage;
        }
    }
}
