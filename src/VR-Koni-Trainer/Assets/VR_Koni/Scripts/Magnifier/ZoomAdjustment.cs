using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace VR_Koni.Magnifier
{
    public class ZoomAdjustment : MonoBehaviour
    {
        public enum ZoomLevel
        {
            ONE,
            TWO,
            THREE
        }

        [SerializeField] private InputActionReference zoomUpInput;
        [SerializeField] private InputActionReference zoomDownInput;
        
        [SerializeField] private float firstZoomLevel = 1.0f;
        [SerializeField] private float secondZoomLevel = 2.0f;
        [SerializeField] private float thirdZoomLevel = 3.0f;
        
        private const float IPD_IN_MILLIMETERS = 22.0f;
        private Camera colposcopeCamera;
        private ZoomLevel currentZoomLevel;

        private void Start()
        {
            colposcopeCamera = GetComponent<Camera>();

            currentZoomLevel = ZoomLevel.ONE;
            
            float ipdInMeters = IPD_IN_MILLIMETERS / 1000.0f;
            ApplyCustomIPD(ipdInMeters);

            zoomUpInput.action.performed += _ => IncreaseZoomLevel();
            zoomDownInput.action.performed += _ => DecreaseZoomLevel();
            Camera.onPostRender += InitialSetup; //Required to ensure proper Projection Matrices exist
        }

        private void InitialSetup(Camera cam)
        {
            if (cam == colposcopeCamera)
            {
                ApplyZoomLevel(currentZoomLevel);

                Camera.onPostRender -= InitialSetup;
            }
        }

        public void PrepareForSwitch()
        {
            float ipdInMeters = IPD_IN_MILLIMETERS / 1000.0f;
            ApplyCustomIPD(ipdInMeters);
        }

        public void IncreaseZoomLevel()
        {
            if (currentZoomLevel < ZoomLevel.THREE)
            {
                ApplyZoomLevel(currentZoomLevel + 1);
            }
        }
        
        private void DecreaseZoomLevel()
        {
            if (currentZoomLevel > ZoomLevel.ONE)
            {
                ApplyZoomLevel(currentZoomLevel - 1);
            }
        }
        
        public void ApplyZoomLevel(ZoomLevel newZoomLevel)
        {
            if (!colposcopeCamera)
            {
                return;
            }
            
            float zoomFactor;
            switch (newZoomLevel)
            {
                case ZoomLevel.ONE:
                    zoomFactor = firstZoomLevel;
                    break;
                case ZoomLevel.TWO:
                    zoomFactor = secondZoomLevel;
                    break;
                case ZoomLevel.THREE:
                    zoomFactor = thirdZoomLevel;
                    break;
                default:
                    zoomFactor = firstZoomLevel;
                    break;
            }
            
            colposcopeCamera.ResetStereoProjectionMatrices();
            Matrix4x4 newLProjectionMatrix = colposcopeCamera.GetStereoProjectionMatrix(Camera.StereoscopicEye.Left);
            Matrix4x4 newRProjectionMatrix = colposcopeCamera.GetStereoProjectionMatrix(Camera.StereoscopicEye.Right);
            newLProjectionMatrix = AdjustFovOnMatrix(newLProjectionMatrix, colposcopeCamera.fieldOfView / zoomFactor, colposcopeCamera.aspect);
            newRProjectionMatrix = AdjustFovOnMatrix(newRProjectionMatrix, colposcopeCamera.fieldOfView / zoomFactor, colposcopeCamera.aspect);
            colposcopeCamera.SetStereoProjectionMatrix(Camera.StereoscopicEye.Left, newLProjectionMatrix);
            colposcopeCamera.SetStereoProjectionMatrix(Camera.StereoscopicEye.Right, newRProjectionMatrix);

            currentZoomLevel = newZoomLevel;
        }
        
        private Matrix4x4 AdjustFovOnMatrix(Matrix4x4 originalMatrix, float newFoV, float aspect)
        {
            // Convert FoV from degrees to radians
            float tanHalfFov = Mathf.Tan(newFoV * Mathf.Deg2Rad / 2.0f);
            
            Matrix4x4 adjustedMatrix = originalMatrix;
            adjustedMatrix[1, 1] = 1.0f / tanHalfFov; // Update vertical FoV
            adjustedMatrix[0, 0] = 1.0f / (tanHalfFov * aspect); // Update horizontal scaling (aspect ratio)

            return adjustedMatrix;
        }
        
        private void ApplyCustomIPD(float ipd)
        {
            if (!colposcopeCamera)
            {
                return;
            }
            
            // Calculate half-IPD for left and right eye offsets
            float halfIPD = ipd / 2.0f;
            
            Matrix4x4 baseViewMatrix = colposcopeCamera.worldToCameraMatrix;

            // Create translation vectors in local space for left and right eyes
            Vector3 leftEyeOffset = colposcopeCamera.transform.right * halfIPD;
            Vector3 rightEyeOffset = colposcopeCamera.transform.right * -halfIPD;

            // Adjust both view matrices by applying the offsets
            Matrix4x4 leftViewMatrix = baseViewMatrix * Matrix4x4.Translate(leftEyeOffset);
            Matrix4x4 rightViewMatrix = baseViewMatrix * Matrix4x4.Translate(rightEyeOffset);

            // Set both custom view matrices for the camera
            colposcopeCamera.SetStereoViewMatrix(Camera.StereoscopicEye.Left, leftViewMatrix);
            colposcopeCamera.SetStereoViewMatrix(Camera.StereoscopicEye.Right, rightViewMatrix);
        }
    }
}
