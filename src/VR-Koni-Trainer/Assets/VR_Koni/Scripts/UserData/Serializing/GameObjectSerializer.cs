using UnityEngine;
using Newtonsoft.Json;
using Unity.XR.CoreUtils;

namespace VR_Koni.UserData
{
    /// <summary>
    /// This class offers static methods to serialize a gameobject (tested only for the cancer cutout gameobject) to a JSON String and also deserialize back from JSON to Gameobject.
    /// DO NOT USE FOR OBJECTS OTHER THAN THE CANCER CUTOUT, IF YOU DO, TEST THOROUGHLY AND MAKE NECESSARY CHANGES BUT WITHOUT BREAKING THE CODE FOR THE CANCER CUTOUT
    /// </summary>
    public static class GameObjectSerializer
    {
        
        private static JsonSerializerSettings settings = new JsonSerializerSettings
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Error,
            TypeNameHandling = TypeNameHandling.Auto
        };
        /// <summary>
        /// Serializes a Gameobject to JSON
        /// </summary>
        /// <param name="gameObject">The Gameobject to be serialized (Intended to be the Cancer Cutout)</param>
        /// <returns>A JSON String which only contains the data necessary to store the visual Components of the Object</returns>
        public static string SerializeGameObject(GameObject gameObject)
        {
            SerializableGameObject serializableGameObject = new SerializableGameObject(gameObject);
            
            return JsonConvert.SerializeObject(serializableGameObject, settings);
        }

        /// <summary>
        /// Creates a GameObect from JSON
        /// </summary>
        /// <param name="serializedGameObject">The JSON String which contains the Information about the Gameobject</param>
        /// <returns>A Gameobject (Cancer Cutout)</returns>
        public static GameObject DeserializeGameObject(string serializedGameObject)
        {
            SerializableGameObject deserializedObject = JsonConvert.DeserializeObject<SerializableGameObject>(serializedGameObject, settings);
            GameObject restoredCancerCutout = deserializedObject.ToUnityGameObject;
            restoredCancerCutout.SetActive(false);
            return restoredCancerCutout;
        }
    }
}
