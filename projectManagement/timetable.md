# 📆 Schedule WiSe 24/25 (24-10-16 - 25-???)

## 2024-10-16
- planned and moderated by Ute Trapp
- 10:15 Introduction and Motivation Project
- 10:30 Short Intro Technologies
- 10:45 Explore Project (3*30 min + 15 min break)
    - Round 1: 11:00
    - Round 2: 11:30
    - break: 12:00
    - Round 3: 12:15
- 12:45 Input/Discuss Learning Outcome and Grading, see [team.md](team.md)
- 13:00 Discuss Homework, see [training.md](training.md)
13:30 end

## 2024-10-23
- planned and moderated by Ute Trapp
- 10:15 Your presentations
- 12:00 break
- 12:15 Input Roles, Team work
- 12:30 Game
- 12:50 Build 2 teams (2d/VR)
    - Discuss Roles
- 13:15 Discuss Homework, see [training.md](training.md)
13:30 end

## 2024-10-30
- 10:15 Status and feedback, questions
- 10:45 gitlab merge and test of MR 
    - 2 students still need to do a VR surgery
- 11:30 gitlab workflow in general
- 11:45 break
- 12:00 Preparation meeting tomorrow with law students (licensing)
    - parallel UX test
    - parallel check issues 
        - [#359](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/issues/359)
        - [#365](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/issues/365) 
        - [#371](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/issues/371),  
- 12:45 Questions UI Toolkit
- 13:15 Everyone briefly says what they will do until next Wednesday.
- 13:30 End
- Homework
    - Everyone works on issues, committing regularly.
    - Merge changes as soon as possible.

## 2024-11-06
- merge, disucssions, testing

## 2024-11-13
- 10:15 Prepare Sprint Review
    - setup VR
    - pull latest changes of main
    - start application
- 10:25 Sprint-Review of both teams
    - SM: opening (5 min)
    - PO: Overview, what was achieved/not achieved (5 min)
    - only issues in "in sprint review" are demonstrated, i.e. only merged issues + UX docs + Blender models
        - each team ca. 20 min
        - dev people show their contributions
- 11:15 Input Ute on Team Work, Scrum and Retrospective
- 11:30 Break
- 11:40 Retrospective
- 12:40 Break
- 12:55 Planning next week
    - after meeting on friday create good issues and milestones
- 13:30 End

## 2024-11-20
- 10:15 Discussions/Questions
- 10:45 Sprint-Planning
- 11:45 Break
- 12:00 Merge, work on issues
- 13:30 End

## 2024-12-10 Visit Würzburg

## 2024-12-11
- 10:15 Report Würzburg
- 10:45 Presentation Localization Chatchai
- 11:10 Retro
- 12:10 Break
- 12:25 Sprint Planning