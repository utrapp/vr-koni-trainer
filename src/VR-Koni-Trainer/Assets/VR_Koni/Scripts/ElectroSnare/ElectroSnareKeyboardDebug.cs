using UnityEngine;

namespace VR_Koni.ElectroSnare
{
    public class ElectroSnareKeyboardDebug : MonoBehaviour
    {
        private const float WIRE_LENGTH = 0.0186f;

        [SerializeField]
        private float moveSpeed = 1.0f;

        [SerializeField, Tooltip("CuttingDepth(LengthOfWire)")]
        private float rayLength = 0.0186f;

        private Vector3 moveDirection;
        private Vector2 rotationDirection;

        private Rigidbody mRigidbody;
        private Collider mWireCollider;
        private Transform mainRayCast;

        private void Awake()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        private void Start()
        {
            //Fetch the Rigidbody from the GameObject with this script attached
            mRigidbody = GetComponent<Rigidbody>();
            mainRayCast = transform.Find("RayCastOrigin");
        }

        private void Update()
        {
            HandleMovementInput();
            HandleRotationInput();

            ApplyFinalMovements();
        }

        private void FixedUpdate()
        {
            // https://docs.unity3d.com/ScriptReference/Physics.Raycast.html
            // get raycast hit of middle point of the instrument. Use the angle between the normal and the forward vector of 
            // the instrument and the length of the raycast to determine, how deep the wire cuts into the tissue
            if (Physics.Raycast(mainRayCast.position, mainRayCast.TransformDirection(Vector3.forward),
                                out RaycastHit hit, rayLength))
            {
                // get normal of hit
                Vector3 hitNormal = hit.normal;

                // get angle between vectors
                float angle = Vector3.Angle((transform.forward * -1), hitNormal);

                // get distance of hitpoint
                float hitpointDistance = hit.distance;

                // calc actual depth of wire
                float wireDepth = WIRE_LENGTH - hitpointDistance;

                Debug.Log("Tiefe: " + (wireDepth * 100) + " mm");
                Debug.Log("Winkelabweichung: " + angle + " Grad");

                Debug.DrawRay(hit.point, hit.normal * 100, Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f), 10f);
                Debug.DrawRay(mainRayCast.position, transform.TransformDirection(Vector3.forward) * hit.distance,
                              Color.yellow);

                LeaveTrail(hit.point, 0.001f);
            }
        }

        private void HandleMovementInput()
        {
            float movementX = Input.GetAxis("Horizontal");
            float movementY = Input.GetAxis("UpDown");
            float movementZ = Input.GetAxis("Vertical");

            moveDirection = new Vector3(movementX, movementZ, movementY);
            moveDirection = Vector3.ClampMagnitude(moveDirection, 1);
        }

        private void HandleRotationInput()
        {
            float rotationX = Input.GetAxis("Mouse Y");
            float rotationZ = Input.GetAxis("Mouse X");

            Vector3 axis = new Vector3(rotationX, 0, rotationZ);
            Quaternion deltaRotation = Quaternion.Euler(axis * Time.deltaTime);
            mRigidbody.MoveRotation(mRigidbody.rotation * deltaRotation);
        }

        private void ApplyFinalMovements()
        {
            mRigidbody.MovePosition(mRigidbody.position + moveSpeed * Time.deltaTime * moveDirection);
        }

        // Unnecessary. I can always stop the velocity and only allow the input. 
        // drawing happens on koni side
        private void OnCollisionEnter(Collision _)
        {
            mRigidbody.MovePosition(mRigidbody.position);
            mRigidbody.MoveRotation(mRigidbody.rotation);


            mRigidbody.isKinematic = true;
        }

        private void OnCollisionExit(Collision _)
        {
            mRigidbody.isKinematic = false;
            mRigidbody.MovePosition(mRigidbody.position);
            mRigidbody.MoveRotation(mRigidbody.rotation);
        }

        /// <summary>
        /// Places a single sphere at a specific point in space, and sets it to auto-destroy
        /// </summary>
        /// <param name="point">The world point at which to spawn the sphere</param>
        /// <param name="scale">The local scale of the sphere</param>
        private void LeaveTrail(Vector3 point, float scale)
        {
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.localScale = Vector3.one * scale;
            sphere.transform.position = point;
            sphere.transform.parent = transform.parent;
            sphere.GetComponent<Collider>().enabled = false;
            Destroy(sphere, 3f);
        }
    }
}
