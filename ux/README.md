# UX-Documentation

This file serves the purpose of documenting the UI/UX aspect of the VR-Coni-Trainer application. Our design team utilized various tools and methods to outline and document the mood board, style guide, which helped us reach the current prototype we have today.

Here is the general documentation of our application, along with a small introduction in the different concepts and tools we implemented and other additional helpful links.

## Mood-Board

A [moodboard](https://www.nngroup.com/articles/mood-boards/)
is a collage of images, video frames, patterns, or text that communicate the brand identity of an application, and decide on the product's visual direction.
In our moodboard, we tried to convey the tone of VR-Koni-Trainer, which gives a sense of professionalism and seriousness, which fits the medical purpose of the application.

[Link to our current Mood-Board in Miro](https://miro.com/app/board/uXjVNSb7rmY=/?share_link_id=495664323790)

## Style Guide

### Main Menu

Our team used Figma to plan the style guide for the main menu, and design the different UI prototypes.

[Link to our chosen style guide](https://www.figma.com/file/3ksJ5l532aSPVeeE9OtPg3/finalizedPrototype?type=design&node-id=2797-14871&mode=design&t=mTGCnvgDP1htpsY5-0)

We initially attempted to manually collect the assets for the style guide, but found this process to be inefficient and tedious, especially over the long run. As our prototype evolved, so did our Figma assets for colors and fonts. To address this, we decided to streamline the process by using the Figma plugin [Automatic Style Guides](https://www.figma.com/community/plugin/838622388628773312), allowing us to automatically generate a style guide based on our designed prototype.

- [Figma Tutorial: Creating Styles](https://www.youtube.com/watch?v=gtQ_A3imzsg&list=PLXNfBiURZZ0PsM9zgZqB_8iOoqBEWevnc&index=7)

#### Prototypes

[Link to our main prototype in Figma](https://www.figma.com/file/3ksJ5l532aSPVeeE9OtPg3/finalizedPrototype?type=design&node-id=1813-5949&mode=design&t=YOmvt0ZGp6WPL0zw-0)

**links to previous prototypes**

- [first prototype](https://www.figma.com/file/ZrTOGKjPNXq4eNDrOdxfjD/PSE-Statistics?type=design&node-id=0%3A1&mode=design&t=B8Nj7FqSGHaCSNYI-1)
- [second prototype](https://www.figma.com/file/Be1H4lZkMq2gWBGZZ6otbN/Prototype?type=design&node-id=0-1&mode=design&t=ZbrPydSCNnoZR3Hf-0)

### Room Design

...

## Usability Tests

Usability testing is a method used to evaluate how user-friendly and efficient an application or system is by observing users as they interact with it. The goal is to identify any usability issues, gather feedback, and make improvements to enhance the overall user experience.

**Important:** It is important to always run the tests with new people, who are unfamiliar to your application, because it is easier for them to spot important usability problems, which can be invisible to a regular user or a developer of the application.

In the context of a VR-application like VR-Koni-Trainer, usability testing becomes challenging due to several factors:

    * Limited Accessibility: VR technology often requires specialized hardware such as VR headsets and controllers. This limits the accessibility of the application to users who have access to this equipment, making it challenging to run the tests.
    This is why it was important for our team members to organize the access to the available headsets.

    * Complex Interaction: Interacting in a virtual environment involves different gestures, movements, and interactions compared to traditional 2D interfaces. Usability testing for VR applications needs to account for these unique interaction patterns, which may require additional expertise and resources.

**helpful links**

- [The UX of VR](https://www.creativebloq.com/ux/the-user-experience-of-virtual-reality-31619635)
- [uxofvr](https://www.uxofvr.com/)
- [Medium](https://medium.com/uxxr/the-user-experience-of-virtual-reality-c464762deb8e)

### Test Tasks

The tasks in a usability test are realistic activities that the participants perform under the supervision of the test runners. They can be very specific or very open-ended, depending on the research questions and the type of usability testing.
The wording of the tasks is very important, since it's a key factor to help the test participants understand what they need to do, without giving too much information. It is the key to receiving transparent results from the tests.

- [A template fot usability tasks](https://www.usertesting.com/blog/usability-testing-template)

### Thinking Aloud

This was one of the most effiecient and easiest concepts we used in our usability tests. It is also critical to understand what exactly is going through the user's mind during the tests.

The Method is basically achieved by making sure that the user is able to express their thoughts freely and honestly during the tests. This can happen by asking the user in the beginning of the test to speak outloud everything they are thinking, or also by reminding them during the tests if necessary.

It is important to take good notes of what the test participant is saying and his behaviours, analyze those notes and then create new issues on the basis of the analysis, to further improve the application.

- [Our Thinking Aloud](https://gitlab.com/darmstadt-university-of-applied-sciences-trapp/vr-koni-trainer/-/blob/DesignDocumentation-%23140-SAE/ux/usability_tests/2024/thinking_aloud.md?ref_type=heads)

**helpful links**

- [Usability Cafe](https://www.youtube.com/watch?v=0YL0xoSmyZI)
- [NN/g](https://www.nngroup.com/articles/thinking-aloud-the-1-usability-tool/)

### Cognitive Walkthrough

A Cognitive Walkthrough is a usability evaluation method where evaluators systematically work through the usability tasks and execute them from the perspective of the user. It helps identify potential issues and estimate the user's thought process when interacting with the system.
In our case, we always ran the Cognitive Walkthroughs as internal sample tests (Probetests) with the different team members and professors, to identify any bugs or problems before running the actual usability tests.

**helpful links**

- [NN/g](https://www.nngroup.com/articles/cognitive-walkthrough-workshop/)
- [ACM](https://dl.acm.org/doi/abs/10.1145/3535782.3535814)

### A/B Testing

An A/B test is a simple experiment where users are shown two or more variants of a design to find out which one performs better.

**TBD: Why was it helpful in out case**

**helpful links**

- [UEQ](https://www.ueq-online.org)
- [A/B-Test](https://www.uxpin.com/studio/blog/ab-testing-in-ux-design-when-and-why/)
