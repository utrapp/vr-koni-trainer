using System.Collections.Generic;
using System.Linq;

namespace VR_Koni.UserData.Export
{
    public static class ExportUtils
    {
        public static Dictionary<string,string> CreateEmojis(List<Surgery> surgeries)
        {
            string[] emojiAnimal = {
                "🐉", "🐱", "🐳", "🦀", "🦖", "🦋", "🐛", "🐜",
                "🐝", "🐢", "🐶", "🦎", "🐍", "🦈", "🐚", "🦭",
                "🦜", "🐧", "🦉", "🐦", "🐙", "🐙", "🦗", "🐞",
            };

            string[] emojiPlant = {
                "🌸", "💮", "🪷", "🏵️", "🌵", "🪻", "🌺", "🏝️",
                "🗻", "🫛", "🍒", "🥥", "🥝", "🫐", "🍈", "🍍",
                "🍊", "🫑", "🌽", "🥕", "🍏", "🌶️", "🍇", "🥑",
            };

            System.Random random = new();
            List<int> choices = new ();
            Dictionary<string, string> nameMap = new();

            foreach(string n in surgeries.Select(x => x.Username).Distinct().ToList())
            {
                int animal, plant;
                do
                {
                    animal = random.Next(0, 24);
                    plant = random.Next(0, 24);
                } while (choices.Contains(animal + plant * 2));
                choices.Add(animal + plant * 2);

                nameMap.Add(n, emojiPlant[plant] + emojiAnimal[animal]);
            }

            return nameMap;
        }
    }
}
