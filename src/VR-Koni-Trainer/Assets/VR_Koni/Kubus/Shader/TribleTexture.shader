Shader "Custom/TribleTexture"
{
    Properties
    {
        _Color ("Color", Color) = (1, 1, 1, 1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _SecondTex ("Second Texture", 2D) = "white" {}
        _ThirdTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0, 1)) = 0.5
        _Metallic ("Metallic", Range(0, 1)) = 0.0
        //custom property to allow input of multiple uv coordinates via a c# script
        _CustomUVs ("Custom UV Coordinates", Vector) = (-1, -1, -1, -1)
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows

        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _SecondTex;
        sampler2D _ThirdTex;

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_SecondTex;
            float2 uv_ThirdTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        //float4 _CustomUVs; // Custom UV coordinates

        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)
   
        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            //fixed4 mainColor = tex2D(_MainTex, IN.uv_MainTex) * _Color;
            fixed4 secondColor = tex2D(_SecondTex, IN.uv_SecondTex);
            fixed4 thirdColor = tex2D(_ThirdTex, IN.uv_ThirdTex); //* _Color;
            
            // Use the opacity from the second texture to determine if the pixel is opaque
            if (secondColor.a > 0)
            {
                thirdColor = secondColor;
            }
            
            o.Albedo = thirdColor.rgb;
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = thirdColor.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
