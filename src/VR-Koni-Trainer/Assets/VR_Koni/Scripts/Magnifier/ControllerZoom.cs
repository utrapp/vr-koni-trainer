using UnityEngine;
using UnityEngine.InputSystem;

namespace VR_Koni.Magnifier
{
    public class ControllerZoom : MonoBehaviour
    {
        public InputActionReference xButtonAction; 
        public InputActionReference yButtonAction; 
        public Camera cameraToControl;
        public GameObject viewbox;
        public GameObject ersteplane;
        public GameObject zweiteplane;
        private int[] fovValues = { 10, 15, 30 }; 
        private int currentFOVIndex; 
        private string currentActiveIcon;

        private void Getcurrentfov()
        {
            float currentFOV = cameraToControl.fieldOfView;

            if (Mathf.Approximately(currentFOV, 10f))
                currentFOVIndex = 0;
            else if (Mathf.Approximately(currentFOV, 15f))
                currentFOVIndex = 1;
            else if (Mathf.Approximately(currentFOV, 30f))
                currentFOVIndex = 2;
        }
            
        private void OnEnable()
        {
            xButtonAction.action.performed += OnXButtonPressed;
            xButtonAction.action.Enable();

      
            yButtonAction.action.performed += OnYButtonPressed;
            yButtonAction.action.Enable();
        }

        private void OnDisable()
        {
     
            xButtonAction.action.performed -= OnXButtonPressed;
            xButtonAction.action.Disable();

            yButtonAction.action.performed -= OnYButtonPressed;
            yButtonAction.action.Disable();
        }

        private void OnXButtonPressed(InputAction.CallbackContext context)
        {
            Getcurrentfov();
            if (viewbox.activeSelf)
            {
                currentFOVIndex = Mathf.Max(currentFOVIndex - 1, 0);
                UpdateCameraFOV();
            }
       
        }

        private void OnYButtonPressed(InputAction.CallbackContext context)
        {
            Getcurrentfov();
            if (viewbox.activeSelf)
            {
                currentFOVIndex = Mathf.Min(currentFOVIndex + 1, fovValues.Length - 1);
                UpdateCameraFOV();
            }
        }
    
        private void UpdateCameraFOV()
        {
            cameraToControl.fieldOfView = fovValues[currentFOVIndex];
            currentActiveIcon = GetTargetIconName();
            Adjustzoom(ersteplane,currentActiveIcon);
            Adjustzoom(zweiteplane,currentActiveIcon);
        }

        private string GetTargetIconName()
        {
            switch (cameraToControl.fieldOfView)
            {
                case 10:
                    return "Zoom6";
                case 15:
                    return "Zoom4";
                case 30:
                    return "Zoom2";
                default:
                    return null;
            }
        }

        private void Adjustzoom(GameObject parent, string targetName)
        {
            if (parent == null || string.IsNullOrEmpty(targetName))
                return;

            foreach (Transform child in parent.transform)
            {
           
                child.gameObject.SetActive(child.name == targetName);
            }
        }
    }
}
