using System;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using UnityEngine.UIElements;
using VR_Koni.Audio;
using VR_Koni.UserData;
using Object = UnityEngine.Object;

namespace VR_Koni.GUI.UI_Toolkit.MainMenu.Views
{
    public class MainMenuSettingsView : IMainMenuView
    {
        public LocalizedString ViewName => new LocalizedString { TableReference =  ProjectConstants.UiToolKitLocalizationTableName, TableEntryReference = "MainMenuSettingsViewName" };
        public VisualElement RootElement { get; private set; }
        private Slider volumeSlider;
        private AudioSourceFade volumeTestPlayer;
        private DropdownField languageSelector;

        public void SetupVisualElements(VisualElement rootElement)
        {
            RootElement = rootElement;
            volumeTestPlayer = Object.FindObjectOfType<AudioSourceFade>();

            volumeSlider = rootElement.Q<Slider>("VolumeSlider");
            languageSelector = rootElement.Q<DropdownField>("LanguageDropdownSelector");
            languageSelector.choices = LocalizationSettings.AvailableLocales.Locales.Select(x => x.LocaleName).ToList();
            languageSelector.value = LocalizationSettings.SelectedLocale.LocaleName;
        }

        public void SubscribeToEvents()
        {
            volumeSlider.RegisterValueChangedCallback(VolumeSliderChanged);
            volumeSlider.RegisterCallback<MouseDownEvent>(VolumeSliderMouseDown);  //TODO: Currently this is no longer working, reason unknown, it used to work like a day ago

            // This is a workaround for a unity bug, see https://discussions.unity.com/t/slider-cant-manage-the-mouseupevent/870583
            // TODO: Check if this is still needed after unity version upgrade
            volumeSlider.Q("unity-drag-container").RegisterCallback<MouseUpEvent>(VolumeSliderMouseUp);

            languageSelector.RegisterValueChangedCallback(LanguageSelectorChanged);
        }

        public void UnsubscribeFromEvents()
        {
            volumeSlider.UnregisterValueChangedCallback(VolumeSliderChanged);
            volumeSlider.UnregisterCallback<MouseDownEvent>(VolumeSliderMouseDown);
            volumeSlider.Q("unity-drag-container").UnregisterCallback<MouseUpEvent>(VolumeSliderMouseUp);

            languageSelector.UnregisterValueChangedCallback(LanguageSelectorChanged);
        }

        public void ResetViewSelections()
        {
            volumeSlider.value = AudioListener.volume;
            languageSelector.index = LocalizationSettings.AvailableLocales.Locales.IndexOf(LocalizationSettings.SelectedLocale);
        }

        private void VolumeSliderChanged(ChangeEvent<float> evt)
        {
            if (Math.Abs(AudioListener.volume - evt.newValue) <= 0.01f) // Only update if change is visible to the user
            {
                evt.StopPropagation();
                return;
            }
            AudioListener.volume = volumeSlider.value;

            if (DataManager.Instance.CurrentUser == null
                || DataManager.Instance.CurrentUser.Privileges == User.UserPrivileges.Guest)
            {
                Debug.Log("Failed to update DB. No User preference update for guests");
                return;
            }
            DataManager.Instance.CurrentUser.Preferences.PreferredVolume = AudioListener.volume;
            DataManager.Instance.WriteOrUpdateUserPreferences();
        }

        private void VolumeSliderMouseDown(MouseDownEvent _) => volumeTestPlayer.Play();

        private void VolumeSliderMouseUp(MouseUpEvent _) => volumeTestPlayer.Stop();

        private void LanguageSelectorChanged(ChangeEvent<string> _)
        {
            Locale culture = LocalizationSettings.AvailableLocales.Locales[languageSelector.index];
            //CultureInfo.CurrentCulture = culture.Identifier.CultureInfo; //this line is dangerous sine changing current culture can break other parts of the application
            LocalizationSettings.SelectedLocale = culture;

            if (DataManager.Instance.CurrentUser == null
                || DataManager.Instance.CurrentUser.Privileges == User.UserPrivileges.Guest)
            {
                Debug.Log("Failed to update DB. No User preference update for guests");
                return;
            }
            DataManager.Instance.CurrentUser.Preferences.PreferredLanguage = culture.Identifier.Code;
            DataManager.Instance.WriteOrUpdateUserPreferences();
        }
    }
}
