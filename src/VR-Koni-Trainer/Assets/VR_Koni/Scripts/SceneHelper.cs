using System.Collections;
using DocumentFormat.OpenXml.Drawing.Charts;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace VR_Koni
{
    public static class SceneHelper
    {
        public static IEnumerator LoadScene(string loadScene, LoadSceneMode mode = LoadSceneMode.Additive)
        {
            GameObject pv = GameObject.Find("PlayerVariant");
            if (pv != null)
            {
                Object.Destroy(pv);
            }
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(loadScene, mode);
            while (asyncLoad is { isDone: false })
            {
                yield return null;
            }

            SceneManager.SetActiveScene(SceneManager.GetSceneByName(loadScene));
        }

        public static IEnumerator UnloadScene(string unloadScene)
        {
            if (!SceneManager.GetSceneByName(unloadScene).isLoaded) yield break;
            
            AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(unloadScene);
            while (asyncUnload is { isDone: false })
            {
                yield return null;
            }
        }

        public static IEnumerator ReloadScene(string reloadScene)
        {
            yield return UnloadScene(reloadScene);
            yield return LoadScene(reloadScene);
        }
    }
}
