# User Feedback Summary

## User 1
### Menu Overlay
| Nr | Task Description | Result |
|----|-------------------|--------|
| 1  | Read through the explanations for the buttons. | Successful |
| 2  | Navigate through the explanations. | Successful |
| 3  | Skip the tutorial at any point of the tutorial. | Successful |

**Observation:**
  1. In Statistic Panel, the buttons “Last Attempt”, “All Statistics” and “Charts” were too dark to notice. (Could be due to screen brightness)

**Feedback:**
  1. UI looks too simple, apply better font for the text.

### Start in VR Results

| Nr | Task Description | Result |
|----|-------------------|--------|
| 1  | After log in, go to the tutorial | Successful |
| 2  | On the tutorial's lesson overview, click the tutorial "Start in VR" and choose Lesson 1 | Successful |
| 3  | Go through all the lessons. Navigate forwards and backwards through the tutorial | Successful |
| 4  | Return to the lesson overview at any point during the tutorial | Successful |
| 5  | Leave the lesson at any point during the tutorial | Successful |
| 6  | From the lesson overview, navigate to the last lesson on the lesson overview and select it | Successful |
| 7  | Return to the main menu at any point during the tutorial | Successful |

**Feedback:**
  1. Button “Lektionübersicht” in the tutorial is too dark, did not notice the button.

### Simulation Results

| Nr | Task Description | Result |
|----|-------------------|--------|
| 1  | On the tutorial's lesson overview, click the tutorial "Simulation" and choose Lesson 1 | Successful |
| 2  | Navigate through the explanations | Successful |
| 3  | Skip the tutorial at any point of the tutorial | Successful |
| 4  | Go through all the lessons. Navigate forwards and backwards through the tutorial | Successful |
| 5  | Return to the lesson overview at any point during the tutorial | Successful |
| 6  | Leave the lesson at any point during the tutorial | Successful |
| 7  | Return to the main menu | Successful |

**Feedback:**
  1. The 3D Menu appears on the right side of the user instead of directly in front.
  2. Need to mention that users can utilize the thumbstick to change their viewing angle and perspective within the VR environment, allowing for better orientation and interaction in the virtual space. (helpful if the room is small or the user has limited space).
  3. Must be mentioned to do activity within the VR sensors’ field.
  4. Cable of the VR headset can cause the user to trip while the user is moving.
  5. The possibility to adjust the headset to make it easier to wear for people who wear glasses.

### Operation
**Task Results:**

| Nr | Task Description | Result |
|----|-------------------|--------|
| 1  | Adjust the height of the surgery chair and player | Successful |
| 2  | Hold the surgical loop and adjust the angle.| Successful |
| 3  | Carry out the operation. | Successful after 5 tries |

**Observation:**
  1. The user does not know that the operation should be carried out with a minimum number of cuts. (Currently the tutorial "Statistik" is not implemented completely)

## User 2
### Menu Overlay
| Nr | Task Description | Result |
|----|-------------------|--------|
| 1  | Read through the explanations for the buttons. | Successful |
| 2  | Navigate through the explanations. | Successful |
| 3  | Skip the tutorial at any point of the tutorial. | Successful |

**Observation:**
  1. The user presses on other buttons such as “Statistic”, “Home” instead, not knowing that the buttons are not interactable.

**Feedback:**
  1. Do not know what Conization is.
  2. Darken the buttons or clarify that the buttons are not interactable.

### Start in VR Results

| Nr | Task Description | Result |
|----|-------------------|--------|
| 1  | After log in, go to the tutorial | Successful |
| 2  | On the tutorial's lesson overview, click the tutorial "Start in VR" and choose Lesson 1 | Successful |
| 3  | Go through all the lessons. Navigate forwards and backwards through the tutorial | Successful |
| 4  | Return to the lesson overview at any point during the tutorial | Successful |
| 5  | Leave the lesson at any point during the tutorial | Successful |
| 6  | From the lesson overview, navigate to the last lesson on the lesson overview and select it | Successful |
| 7  | Return to the main menu at any point during the tutorial | Successful |

**Feedback:**
1. The Lesson “Absetzen des Headsets” was too late, because the user tried the headset on and took it off himself before reaching the lesson.

2. An explanation of the On/Off button for both the controller and the VR headset.

### Simulation Results

| Nr | Task Description | Result |
|----|-------------------|--------|
| 1  | On the tutorial's lesson overview, click the tutorial "Simulation" and choose Lesson 1 | Successful |
| 2  | Navigate through the explanations | Successful |
| 3  | Skip the tutorial at any point of the tutorial | Successful |
| 4  | Go through all the lessons. Navigate forwards and backwards through the tutorial | Successful |
| 5  | Return to the lesson overview at any point during the tutorial | Successful |
| 6  | Leave the lesson at any point during the tutorial | Successful |
| 7  | Return to the main menu | Successful |

**Observation:**
1. It took awhile for the user to set the focus, the user realized after tightening the headset with the strap, he was able to see clearer. (Eye level is center of the VR Headset)

2.During the lesson “Handhabung der Schlinge”, user did not notice the Schlinge on the table, and proceeded with the next task.

3. User did not grab all the fruit. The  fruits moved upon contact before the user was able to grab it, so the task was not completed. The user thought the tutorial ended as there was no more navigation button to click.

4. User grabbed the small apple before noticing the magnifying glass. Was unsure about the magnifying glass until he put his hand in front of it.

5.. From Simulation back to menu, user did not know he was supposed to remove his VR headset. (Maybe a pop up??)


**Feedback:**
1. The surgical loop is white and placed on a white tray. Not obvious.

2. Need to clearly state where the magnifying glass is. It looked like something pink was floating. 

3. During the lesson “Höhenverstellbarkeit”, the user tried to press on the image. 


### Operation
**Task Results:**

| Nr | Task Description | Result |
|----|-------------------|--------|
| 1  | Adjust the height of the surgery chair and player | Successful |
| 2  | Hold the surgical loop and adjust the angle.| Successful |
| 3  | Carry out the operation. | Not Successful |

**Observation:**
1. The user kept pressing down the joystick.

2. The user thought the magnified panel is the cervix.

3. The user does not know that the operation should be carried out with a minimum number of cuts. (The tutorial "Statistik" is not implemented completely)

4. The user does not know when the experiment is considered complete. He did not notice the statistic panel appeared as it was out of his field of vision in VR.

5. The user could not find the way from VR to main menu, button “zum 2D Menu” is not intuitive enough.

## User 3
### Menu Overlay
| Nr | Task Description | Result |
|----|-------------------|--------|
| 1  | Read through the explanations for the buttons. | Successful |
| 2  | Navigate through the explanations. | Successful |
| 3  | Skip the tutorial at any point of the tutorial. | Successful |

**Observation:**
  1. The user presses on other buttons such as “Statistic”, “Home” instead, not knowing that the buttons are not interactable.

**Feedback:**
  1. Do not know what Conization is.
  2. Darken the buttons or clarify that the buttons are not interactable.

### Start in VR Results

| Nr | Task Description | Result |
|----|-------------------|--------|
| 1  | After log in, go to the tutorial | Successful |
| 2  | On the tutorial's lesson overview, click the tutorial "Start in VR" and choose Lesson 1 | Successful |
| 3  | Go through all the lessons. Navigate forwards and backwards through the tutorial | Successful |
| 4  | Return to the lesson overview at any point during the tutorial | Successful |
| 5  | Leave the lesson at any point during the tutorial | Successful |
| 6  | From the lesson overview, navigate to the last lesson on the lesson overview and select it | Successful |
| 7  | Return to the main menu at any point during the tutorial | Successful |

**Feedback:**
1. The text is too long for some lessons.

### Simulation Results

| Nr | Task Description | Result |
|----|-------------------|--------|
| 1  | On the tutorial's lesson overview, click the tutorial "Simulation" and choose Lesson 1 | Successful |
| 2  | Navigate through the explanations | Successful |
| 3  | Skip the tutorial at any point of the tutorial | Successful |
| 4  | Go through all the lessons. Navigate forwards and backwards through the tutorial | Successful |
| 5  | Return to the lesson overview at any point during the tutorial | Successful |
| 6  | Leave the lesson at any point during the tutorial | Successful |
| 7  | Return to the main menu | Successful |

**Observation:**
1.During the lesson “Handhabung der Schlinge”, the user did not notice the Schlinge on the table, and proceeded with the next task.

2. During the lesson “Höhenverstellbarkeit”, the user tried to press on the image. 

3. Upon contact with the hand, the fruits moved and the user was unable to grab the fruit. The user does not know he has to grab all the fruits to proceed to the next lesson.

4. From Simulation back to lesson overview on PC, user did not know he was supposed to remove his VR headset. (Maybe a pop up??)


**Feedback:**
1. Too much text

2. Should mention the thumbstick feature.

2. Need to clearly state where the magnifying glass is.


### Operation
**Task Results:**

| Nr | Task Description | Result |
|----|-------------------|--------|
| 1  | Adjust the height of the surgery chair and player | Successful |
| 2  | Hold the surgical loop and adjust the angle.| Successful |
| 3  | Carry out the operation. | Not Successful |

**Observation:**
1. The user do not understand the purpose of the button “Start VR” 

2. The user thought the magnified panel is the cervix.

3. The user does not know that the operation should be carried out with a minimum number of cuts. The user also does not know when the operation is considered finished. He did not notice the Statistic panel.

**Feedback:**
1. Is there a max height for the height adjustment of the chair? 

2. Do not know when the operation is considered completed.

3. Which is the cancer cell?







