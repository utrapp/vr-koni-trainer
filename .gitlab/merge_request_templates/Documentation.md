_Title analogous to commits, [feat/fix/docs/style/refactor/learn/orga/test]: Summary, eg feat: Add search endpoint to server_

## What does this MR do?

_Briefly describe what this MR is about._

## Related issues

_Link related issues below. Insert the issue link or reference after the word "Closes" if merging this should automatically close it._

## Author's checklist

- [ ] Only one specific issue is fixed or one specific feature is implemented. Do not combine things; send separate merge requests for each issue or feature.
- [ ] Changelog entry added, if necessary.
- [ ] Acceptance criteria as described in the related issue are fulfilled.
- [ ] meaningful tests implemented
- [ ] best practices considered
  - [ ] logging instead of print
  - [ ] commented code deleted etc.
  - [ ] meaningful identifiers
  - [ ] design patterns where useful, no blob class,
  - [ ] one method does one thing
  - [ ] DRY
  - [ ] code commented with /// where useful
- [ ] main branch merged
- [ ] everything runs without errors
- [ ] code inspection shows no relevant warnings
- [ ] Code review carried out with another person
- [ ] ReadMe updated

## Reviewer's checklist

- [ ] Ensure docs are present and up-to-date.
- [ ] Ensure the appropriate labels are added to this MR.
- [ ] You have tested this MR properly (functionality, side effects).
- [ ] You run the tests, all passed and logical parts are tested reasonably.
- [ ] You have carried out the code inspection and did not see any relevant warnings.
- [ ] You have looked at all changes properly, if necessary you improved the code with the author (pair programming), with focus on
  - [ ] general architecture
  - [ ] readability
  - [ ] DRY
  - [ ] separation of concerns
  - [ ] Consistency also in comparison to existing code

## VR's checklist

- [ ] the project is runnable in VR
- [ ] all functionality that existed before the MR works as before
- [ ] all added functionality works as expected
- [ ] the tests are repeated once
- [ ] the tests are repeated twice!
- [ ] you're sure that everything will work (the team will blame you if your MR breaks the project!) 


Thanks for your MR, you're awesome! :+1:
