# UI Toolkit
Some further reading after the training.md

- UXML: https://docs.unity3d.com/Manual/UIE-WritingUXMLTemplate.html 
- Namespace runtime: UnityEngine.UIElements
- Add xmlns="UnityEngine.UIElements" to use Button etc. without prefix in UXML – only possible, if only default controls, i.e. no custom controls are used!!
- schema definition validates UXML: xsi:noNamespaceSchemaLocation XOR xsi:schemaLocation
- Reuse UXML files: https://docs.unity3d.com/Manual/UIE-reuse-uxml-files.html 
    - Template: <ui:Template src="/Assets/Portrait.uxml" name="Portrait"/>
    - Instance: <ui:Instance template="Portrait" name="player2"/>
- View- and Manager-Classes: Uquery: https://docs.unity3d.com/Manual/UIE-UQuery.html 
    - Q is the shorthand for Query<T>.First()
USS: Best Practices https://docs.unity3d.com/Manual/UIE-USS-WritingStyleSheets.html 
    - Avoid inline styles
    - BEM class="menu__item menu__item--disabled"
- Panel Settings: https://docs.unity3d.com/6000.0/Documentation/Manual/UIE-Runtime-Panel-Settings.html 
- Mac: Unity -> settings -> external tools -> regenerate project files
