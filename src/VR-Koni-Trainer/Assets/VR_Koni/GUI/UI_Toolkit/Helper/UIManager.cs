using UnityEngine;
using UnityEngine.UIElements;

namespace VR_Koni.GUI.UI_Toolkit.Helper
{
    [RequireComponent(typeof(UIDocument))]
    public abstract class UIManager : MonoBehaviour, IUIView
    {
        public VisualElement RootElement { get; private set; }

        private UIDocument uiDoc;

        private void OnEnable()
        {
            uiDoc = GetComponent<UIDocument>();
            RootElement = uiDoc.rootVisualElement;
            SetupVisualElements(RootElement);
            SubscribeToEvents();
        }

        private void OnDisable() => UnsubscribeFromEvents();

        public abstract void SetupVisualElements(VisualElement rootElement);
        public abstract void SubscribeToEvents();
        public abstract void UnsubscribeFromEvents();
        public abstract void ResetViewSelections();
    }
}
