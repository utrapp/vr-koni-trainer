﻿#nullable enable //The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.

using System;
using System.Globalization;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using Newtonsoft.Json;

namespace VR_Koni.Graph
{
    public class FileHandler<T>
    {
        /// <summary>
        /// Writes the provided data to a JSON file at the specified path.
        /// </summary>
        /// <param name="data">The data to write as JSON.</param>
        /// <param name="outputFilePath">The path to the output JSON file.</param>
         public void WriteDataToJsonFile(ICollection<T>? data, string outputFilePath = "output.json")
         {
             Container<T> container = new() { CuttingData = data };
             string jsonString = JsonConvert.SerializeObject(container, Formatting.Indented);
             File.WriteAllText(outputFilePath, jsonString);

             UnityEngine.Debug.Log($"{nameof(WriteDataToJsonFile)}: Data successfully written to JSON file: {outputFilePath}");
         }

        /// <summary>
        /// Writes the provided data to a CSV file at the specified path.
        /// </summary>
        /// <param name="data">The data to write as CSV.</param>
        /// <param name="outputFilePath">The path to the output CSV file.</param>
        public void WriteDataToCsvFile(ICollection<T>? data, string outputFilePath = "output.csv")
        {
            if (data == null || data.Count == 0)
            {
                UnityEngine.Debug.LogWarning($"{nameof(WriteDataToCsvFile)}: No data available to write to CSV. DataCount: {data?.Count}");
                return;
            }

            try
            {
                StringBuilder csvBuilder = new();
                csvBuilder.AppendLine("X, Y" + (typeof(T) == typeof(System.Numerics.Vector3) || typeof(T) == typeof(Tuple<float, float, float>) ? ", Z" : ""));

                foreach (T? item in data)
                {
                    csvBuilder.AppendLine(FormatCsvData(item));
                }

                File.WriteAllText(outputFilePath, csvBuilder.ToString());
                UnityEngine.Debug.Log($"{nameof(WriteDataToCsvFile)}: Data successfully written to CSV file: {outputFilePath}");
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.LogWarning($"{nameof(WriteDataToCsvFile)}: {ex.Message}");
            }
        }

        /// <summary>
        /// Writes two collections to a CSV file, appending data from both collections side by side.
        /// The primary Data must be the <see cref="ICollection{T}"/> containing the depth and time info.
        /// Pads shorter collections with zeros to match lengths.
        /// </summary>
        /// <param name="primaryData">The first collection of data to write.</param>
        /// <param name="secondaryData">The second collection of data to write.</param>
        /// <param name="outputFilePath">The path to the output CSV file.</param>
        private void WriteDataToCsvFileWithSecondaryCollection(ICollection<T>? primaryData, ICollection<T>? secondaryData, string outputFilePath = "output_with_velocity.csv")
        {
            if (primaryData == null || secondaryData == null || primaryData.Count == 0 || secondaryData.Count == 0)
            {
                UnityEngine.Debug.LogWarning($"{nameof(WriteToCsvFileWithVelocityData)}: One or both data collections are empty or null.");
                return;
            }

            int maxCount = Math.Max(primaryData.Count, secondaryData.Count);
            List<T> data1List = primaryData.ToList();
            List<T> data2List = secondaryData.ToList();

            try
            {
                StringBuilder csvBuilder = new StringBuilder();
                csvBuilder.AppendLine("X, Y, DX, DY" + (typeof(T) == typeof(System.Numerics.Vector3) || typeof(T) == typeof(Tuple<float, float, float>) ? ", Z, DZ" : ""));

                for (int i = 0; i < maxCount; i++)
                {
                    string line = GenerateCsvLineWithVelocityData(data1List, data2List, i);
                    csvBuilder.AppendLine(line);
                }

                File.WriteAllText(outputFilePath, csvBuilder.ToString());
                Console.WriteLine($"{nameof(WriteToCsvFileWithVelocityData)}: Data successfully written to CSV file: {outputFilePath}");
            }
            catch (Exception e)
            {
                Console.WriteLine($"{nameof(WriteToCsvFileWithVelocityData)}: {e.Message}");
            }
        }

        /// <summary>
        /// Writes two collections to a CSV file, appending data from both collections side by side.
        /// The primary Data must be the <see cref="ICollection{T}"/> containing the depth and time info.
        /// Pads shorter collections with zeros to match lengths.
        /// </summary>
        /// <param name="primaryData">The first collection of data to write.</param>
        /// <param name="velocityData">The second collection of data to write.</param>
        /// <param name="outputFilePath">The path to the output CSV file.</param>
        public void WriteToCsvFileWithVelocityData(ICollection<T>? primaryData, ICollection<T>? velocityData, string outputFilePath = "output_with_velocity.csv")
        {
            WriteDataToCsvFileWithSecondaryCollection(primaryData, velocityData, outputFilePath);
        }

        /// <summary>
        /// Writes a collections to a CSV file, appending data one by one.
        /// The primary Data must be the <see cref="ICollection{T}"/> containing the depth and time info.
        /// Using the values in the <paramref name="primaryData"/>, it calculates how the perfect parabola
        /// might look like for the given <see cref="ICollection{T}"/> <paramref name="primaryData"/>.
        /// </summary>
        /// <param name="primaryData">The first collection of data to write.</param>
        /// <param name="outputFilePath">The path to the output CSV file.</param>
        public void WriteToCsvFileWithPerfectParabolaData(ICollection<T>? primaryData, string outputFilePath = "output_with_perfect_parabola.csv")
        {
            if (primaryData == null || primaryData.Count == 0)
            {
                UnityEngine.Debug.LogWarning($"{nameof(WriteToCsvFileWithPerfectParabolaData)}: No primary data available.");
                return;
            }
            
            //since for now our "optimal" depth is considered to be 8.0f, we can hard code it
            //float maxDepth = primaryData.Max(item => DataPoints<T>.GetDepth(item)); 
            
            float totalTime = primaryData.Max(item => DataPoints<T>.GetTime(item));

            ICollection<T> parabola = DemoDataGenerator.GenerateShapeData<T>(
                Shapes.Parabola,
                totalTime: totalTime,
                maxDepth: 8.0f,
                numberOfPoints: primaryData.Count);

            WriteDataToCsvFileWithSecondaryCollection(primaryData, parabola, outputFilePath);
        }


        /// <summary>
        /// Generates a line of CSV data by formatting entries from two lists to encorportae the velocity data as well as the depth and time data.
        /// </summary>
        /// <param name="data1List">The first list of data.</param>
        /// <param name="data2List">The second list of data.</param>
        /// <param name="index">The current index for both lists.</param>
        /// <returns>A formatted CSV line as a string.</returns>
        private string GenerateCsvLineWithVelocityData(List<T> data1List, List<T> data2List, int index)
        {
            T? data1Item = index < data1List.Count ? data1List[index] : default(T?);
            T? data2Item = index < data2List.Count ? data2List[index] : default(T?);

            string data1String = FormatCsvData(data1Item);
            string data2String = FormatCsvData(data2Item);

            return $"{data1String}, {data2String}";
        }

        /// <summary>
        /// Formats a data item as a CSV-compatible string based on type.
        /// </summary>
        /// <param name="data">The data item to format.</param>
        /// <returns>A string formatted for CSV output.</returns>
        private string FormatCsvData(T? data)
        {
            return data switch
            {
                System.Numerics.Vector2 v2 => $"{v2.X}, {v2.Y}",
                System.Numerics.Vector3 v3 => $"{v3.X}, {v3.Y}, {v3.Z}",
                Tuple<float, float> tuple2 => $"{tuple2.Item1}, {tuple2.Item2}",
                Tuple<float, float, float> tuple3 => $"{tuple3.Item1}, {tuple3.Item2}, {tuple3.Item3}",
                _ => "0, 0" + (typeof(T) == typeof(System.Numerics.Vector3) || typeof(T) == typeof(Tuple<float, float, float>) ? ", 0" : "")
            };
        }

        /// <summary>
        /// Reads data from a CSV file and populates a list of type <typeparamref name="T"/>.
        /// </summary>
        /// <param name="inputFilePath">The path to the input CSV file.</param>
        /// <param name="delimiter">The delimiter used in the csv file.</param>
        /// <returns>A populated list of <typeparamref name="T"/> with data read from the CSV file.</returns>
        public List<T> ReadDataFromCsvFile(string inputFilePath, char delimiter = ',')
        {
            List<T> data = new();

            try
            {
                if (!File.Exists(inputFilePath))
                {
                    throw new FileNotFoundException($"{nameof(ReadDataFromCsvFile)}: File {inputFilePath} does not exist!");
                }

                using StreamReader reader = new(inputFilePath);
                //can be ignored since we don't want the headers anyway: Value assigned is not used in any execution path
                string? line = reader.ReadLine();

                while ((line = reader.ReadLine()) != null)
                {
                    string[] parts = line.Split(delimiter);
                    if (parts.Length == 1)
                    {
                        throw new FormatException($"{nameof(ReadDataFromCsvFile)}: Split failed, please check if all the rows in the file use '{delimiter}' as delimiter.");
                    }

                    T item = (T)ConvertToType(parts);
                    data.Add(item);
                }

                return data;
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.LogError($"{nameof(ReadDataFromCsvFile)}: {ex.Message}");
            }
            return new List<T>();
        }

        /// <summary>
        /// Converts a string array of values into the specified type <typeparamref name="T"/>.
        /// </summary>
        /// <param name="line">An array of string values from a CSV line.</param>
        /// <returns>An object of type T with the parsed values.</returns>
        /// <exception cref="InvalidCastException">Thrown when <typeparamref name="T"/> is not supported.</exception>
        /// <exception cref="FormatException">Thrown when the format of the lines in the CSV file is invalid.</exception>
        private object ConvertToType(string[] line)
        {
            try
            {
                return Activator.CreateInstance(typeof(T)) switch
                {
                    System.Numerics.Vector2 => new System.Numerics.Vector2(
                                                                           float.Parse(line[0], CultureInfo.InvariantCulture),
                                                                           float.Parse(line[1], CultureInfo.InvariantCulture)),

                    System.Numerics.Vector3 => new System.Numerics.Vector3(
                                                                           float.Parse(line[0], CultureInfo.InvariantCulture),
                                                                           float.Parse(line[1], CultureInfo.InvariantCulture),
                                                                           float.Parse(line[2], CultureInfo.InvariantCulture)),

                    Tuple<float, float> => Tuple.Create(
                                                        float.Parse(line[0], CultureInfo.InvariantCulture),
                                                        float.Parse(line[1], CultureInfo.InvariantCulture)),

                    Tuple<float, float, float> => Tuple.Create(
                                                               float.Parse(line[0], CultureInfo.InvariantCulture),
                                                               float.Parse(line[1], CultureInfo.InvariantCulture),
                                                               float.Parse(line[2], CultureInfo.InvariantCulture)),

                    _ => throw new InvalidCastException($"{nameof(ConvertToType)}: Unsupported type {typeof(T)}."),
                };
            }
            catch (Exception ex)
            {
                throw new FormatException($"{nameof(ConvertToType)}: Failed to parse following CSV values: {string.Join(", ", line)}", ex);
            }
        }


        /// <summary>
        /// Reads data from a CSV file where two data collections primary data with depth and time; and velocity data with delta time and delta depth; are stored side by side.
        /// element at index 0 will be the primary collection, followed by the secondary collection being at index 1.
        /// </summary>
        /// <param name="inputFilePath">The path to the input CSV file.</param>
        /// <param name="delimiter">The character used to separate values in the CSV file.</param>
        /// <returns>A tuple containing two lists of <typeparamref name="T"/> with the primary and velocity data.</returns>
        public Tuple<List<T>, List<T>> ReadDataFromCsvFileWithVelocityData(string inputFilePath, char delimiter = ',')
        {
            List<T> primaryData = new(); //primary meaning our time (x) and depth (y) data
            List<T> velocityData = new(); //our delta x for change in time and delta y for change in depth

            try
            {
                if (!File.Exists(inputFilePath))
                {
                    throw new FileNotFoundException($"{nameof(ReadDataFromCsvFileWithVelocityData)}: File {inputFilePath} does not exist!");
                }

                using StreamReader reader = new(inputFilePath);
                //skipping the header because its useless info: Value assigned is not used in any execution path
                string? line = reader.ReadLine(); 

                while ((line = reader.ReadLine()) != null)
                {
                    string[] parts = line.Split(delimiter);
                    if (parts.Length < 4)
                    {
                        throw new FormatException($"{nameof(ReadDataFromCsvFileWithVelocityData)}: Expected at least 4 columns for two data sets, but got {parts.Length} columns.");
                    }

                    //splitting the CSV data into two sets to return 2 separate list entities
                    string[] primaryDataParts = parts.Take(parts.Length / 2).ToArray();
                    string[] velocityDataParts = parts.Skip(parts.Length / 2).ToArray();

                    //converting the CSV data parts into type T and adding to the respective lists
                    primaryData.Add((T)ConvertToType(primaryDataParts));
                    velocityData.Add((T)ConvertToType(velocityDataParts));
                }
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.LogError($"{nameof(ReadDataFromCsvFileWithVelocityData)}: {ex.Message}");
                throw;
            }

            return Tuple.Create(primaryData, velocityData);
        }
    }

    /// <summary>
    /// A container class for storing a collection of data items.
    /// </summary>
    /// <typeparam name="T">The type of data to store.</typeparam>
    public class Container<T>
    {
        public ICollection<T>? CuttingData { get; set; }
    }
}
