using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using VR_Koni.Graph;

namespace VR_Koni.Tests
{
    /// <summary>
    /// static methods are rigid, so it makes sense to have their own dedicated class to test their behavior.
    /// This class <see cref="DataPointsStaticMethodsTests"/> is for all the static methods for <see cref="DataPoints{T}"/>.
    /// </summary>
    [TestFixture]
    public class DataPointsStaticMethodsTests
    {
        [Test]
        public void StartXFromZero_NullContainer_ThrowsArgumentNullException()
        {
            ICollection<System.Numerics.Vector2> container = null;

            ArgumentNullException ex = Assert.Throws<ArgumentNullException>(() => StartXFromZero(ref container));
            Assert.That(ex.ParamName, Is.EqualTo($"{nameof(container)}"));
        }

        [Test]
        public void StartXFromZero_NullContainer_LogsArgumentNullError()
        {
            ICollection<System.Numerics.Vector2> container = new List<System.Numerics.Vector2>();

            ArgumentNullException ex = Assert.Throws<ArgumentNullException>(() => StartXFromZero(ref container));
            Assert.That(ex.Message.ToLowerInvariant(), Does.Contain("count:0"));
        }


        [Test]
        public void StartXFromZero_EmptyContainer_ThrowsArgumentNullException()
        {
            ICollection<System.Numerics.Vector2> container = new List<System.Numerics.Vector2>();

            ArgumentNullException ex = Assert.Throws<ArgumentNullException>(() => StartXFromZero(ref container));
            Assert.That(ex.ParamName, Is.EqualTo("container"));
        }

        [Test]
        public void StartXFromZero_UnsupportedType_ThrowsNotSupportedException()
        {
            ICollection<object> container = new List<object>
            {
                "Unsupported Type"
            };

            NotSupportedException ex = Assert.Throws<NotSupportedException>(() => StartXFromZero(ref container));
            Assert.That(ex.Message, Does.Contain("not supported for time extraction currently."));
        }


        [Test]
        public void StartXFromZero_ValidContainer_TranslatesCorrectly()
        {
            ICollection<System.Numerics.Vector2> container = new List<System.Numerics.Vector2>
            {
                new System.Numerics.Vector2(15, 25),
                new System.Numerics.Vector2(10, 20),
                new System.Numerics.Vector2(30, 40)
            };

            StartXFromZero(ref container);

            List<System.Numerics.Vector2> expected = new List<System.Numerics.Vector2>
            {
                new System.Numerics.Vector2(0, 20),
                new System.Numerics.Vector2(5, 25),
                new System.Numerics.Vector2(20, 40)
            };

            CollectionAssert.AreEqual(expected, container);
        }

        [Test]
        public void StartXFromZero_WorksWithTuples()
        {
            ICollection<Tuple<float, float>> container = new List<Tuple<float, float>>
            {
                Tuple.Create(15f, 25f),
                Tuple.Create(10f, 20f),
                Tuple.Create(30f, 40f)
            };

            StartXFromZero(ref container);

            List<Tuple<float,float>> expected = new()
            {
                Tuple.Create(0f, 20f),
                Tuple.Create(5f, 25f),
                Tuple.Create(20f, 40f)
            };

            CollectionAssert.AreEqual(expected, container);
        }

        [Test]
        public void StartXFromZero_WorksWithVector3()
        {
            ICollection<System.Numerics.Vector3> container = new List<System.Numerics.Vector3>
            {
                new System.Numerics.Vector3(15, 25, 5),
                new System.Numerics.Vector3(10, 20, 3),
                new System.Numerics.Vector3(30, 40, 10)
            };

            StartXFromZero(ref container);

            List<System.Numerics.Vector3> expected = new()
            {
                new System.Numerics.Vector3(0, 20, 3),
                new System.Numerics.Vector3(5, 25, 5),
                new System.Numerics.Vector3(20, 40, 10)
            };

            CollectionAssert.AreEqual(expected, container);
        }

        [Test]
        public void StartXFromZero_WorksWithTupleFloat3()
        {
            ICollection<Tuple<float, float, float>> container = new List<Tuple<float, float, float>>
            {
                Tuple.Create(15f, 25f, 5f),
                Tuple.Create(10f, 20f, 3f),
                Tuple.Create(30f, 40f, 10f)
            };

            StartXFromZero(ref container);

            List<Tuple<float, float, float>> expected = new()
            {
                Tuple.Create(0f, 20f, 3f),
                Tuple.Create(5f, 25f, 5f),
                Tuple.Create(20f, 40f, 10f)
            };

            CollectionAssert.AreEqual(expected, container);
        }
        
        [Test]
        public void StartXFromZero_AllElementsSameXValue_TranslatesCorrectly()
        {
            ICollection<System.Numerics.Vector2> container = new List<System.Numerics.Vector2>
            {
                new System.Numerics.Vector2(10, 20),
                new System.Numerics.Vector2(10, 25),
                new System.Numerics.Vector2(10, 30)
            };

            StartXFromZero(ref container);

            List<System.Numerics.Vector2> expected = new()
            {
                new System.Numerics.Vector2(0, 20),
                new System.Numerics.Vector2(0, 25),
                new System.Numerics.Vector2(0, 30)
            };

            CollectionAssert.AreEqual(expected, container);
        }

        [Test]
        public void StartXFromZero_LargeNumbers_TranslatesCorrectly()
        {
            ICollection<System.Numerics.Vector2> container = new List<System.Numerics.Vector2>
            {
                new System.Numerics.Vector2(1000000, 2000000),
                new System.Numerics.Vector2(999995, 2000005),
                new System.Numerics.Vector2(1000010, 2000010)
            };

            StartXFromZero(ref container);

            List<System.Numerics.Vector2> expected = new()
            {
                new System.Numerics.Vector2(0, 2000005),
                new System.Numerics.Vector2(5, 2000000),
                new System.Numerics.Vector2(15, 2000010)
            };

            CollectionAssert.AreEqual(expected, container);
        }

        [Test]
        public void StartXFromZero_MixedLargeAndSmallNumbers_TranslatesCorrectly()
        {
            ICollection<System.Numerics.Vector2> container = new List<System.Numerics.Vector2>
            {
                new System.Numerics.Vector2(1000000, 2000000),
                new System.Numerics.Vector2(10, 20),
                new System.Numerics.Vector2(1000010, 2000010)
            };

            StartXFromZero(ref container);

            List<System.Numerics.Vector2> expected = new()
            {
                new System.Numerics.Vector2(0, 20),
                new System.Numerics.Vector2(999990, 2000000),
                new System.Numerics.Vector2(1000000, 2000010)
            };

            CollectionAssert.AreEqual(expected, container);
        }

        [Test]
        public void StartXFromZero_ContainerWithNullElements_FiltersNullAndWarns()
        {
            ICollection<System.Numerics.Vector2?> container = new List<System.Numerics.Vector2?>
            {
                new System.Numerics.Vector2(10, 20),
                null,
                new System.Numerics.Vector2(30, 40)
            };

            LogAssert.Expect(LogType.Warning, "StartXFromZero: 1 point(s) were removed.");
            StartXFromZero(ref container);

            Assert.AreEqual(2, container.Count);
            Assert.IsFalse(container.Any(point => point == null));
        }
        
        private static void StartXFromZero<T>(ref ICollection<T> container)
        {
            DataPoints<T>.StartXFromZero(ref container);
        }
    }
}
