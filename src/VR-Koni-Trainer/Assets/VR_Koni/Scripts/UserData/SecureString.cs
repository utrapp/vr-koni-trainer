using System;

namespace VR_Koni.UserData
{
    /// <summary>
    /// Class <c>Password</c> models a password, used to authenticate the user.
    /// </summary>
    [Serializable]
    public class SecureString
    {
        private string hashedString;

        /// <summary>
        /// Constructor of the class Password.
        /// </summary>
        /// <param name="text">Password string which can eiter be hashed or not.</param>
        /// <param name="isHashed">Flag which states, if the provided passwordString is hashed or not. Defaults to false.</param>
        public SecureString(string text, bool isHashed = false)
        {
            hashedString = isHashed ? text : Hashing.ToSHA256(text);
        }

        /// <summary>
        /// Check the stored hashed Password against a provided clear text password.
        /// </summary>
        /// <param name="text">Clear text password which needs to be checked</param>
        /// <returns>A bool representing if the provided passwords hash is equal to the hashed password, stored by this class.</returns>
        public bool CheckString(string text)
        {
            // this implementation assumes, that SHA256 is collision safe
            return string.Equals(Hashing.ToSHA256(text), hashedString);
        }

        public override string ToString()
        {
            return hashedString;
        }
    }
}
