using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace VR_Koni.PlayerHelper
{
    public class HeadsetOffTimer : MonoBehaviour
    {
        private const float MAX_HEADSET_OFF_DURATION = 10.0f;
        private const float HEADSET_POLL_RATE = 0.5f;

        private float headsetOffTime;

        private InputAction userPresence;

        private void OnEnable()
        {
            userPresence = InputSystem.actions.FindAction("User Presence");
            userPresence.started += OnUserPresenceChanged;
            //Player.OnHeadsetRemoved += RemovedHeadset;
            //Player.OnHeadsetPutOn += PutOnHeadset;
        }

        private void OnUserPresenceChanged(InputAction.CallbackContext callbackContext)
        {
            if (callbackContext.ReadValueAsButton() == true)
            {
                PutOnHeadset();
            }
            else
            {
                RemovedHeadset();
            }
        }

        private void OnDisable()
        {
            userPresence.started -= OnUserPresenceChanged;
            //Player.OnHeadsetRemoved -= RemovedHeadset;
            //Player.OnHeadsetPutOn -= PutOnHeadset;
        }

        private void PutOnHeadset()
        {
            if (SceneManager.GetSceneByName("VR_Koni_OperationRoom").isLoaded ||
                SceneManager.GetSceneByName("VR_Koni_EmptyRoom").isLoaded)
                CancelInvoke(nameof(UpdateHeadsetOffTimer));
        }

        private void RemovedHeadset()
        {
            if (SceneManager.GetSceneByName("VR_Koni_OperationRoom").isLoaded ||
                SceneManager.GetSceneByName("VR_Koni_EmptyRoom").isLoaded)
            {
                headsetOffTime = 0.0f;
                InvokeRepeating(nameof(UpdateHeadsetOffTimer), 0.0f, HEADSET_POLL_RATE);
            }
        }

        private void UpdateHeadsetOffTimer()
        {
            if (headsetOffTime >= MAX_HEADSET_OFF_DURATION)
            {
                CancelInvoke(nameof(UpdateHeadsetOffTimer));
                ApplicationManager.SwitchToLoginRegister();
            }

            headsetOffTime += HEADSET_POLL_RATE;
        }
    }
}
