#nullable enable //The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.

using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace VR_Koni.Graph
{
    public static class DataPointsHelper
    {
#region DebugHelper

        /// <summary>
        /// Returns a detailed string representation of all data points
        /// </summary>
        /// <returns>A formatted string containing all data points and collection information</returns>
        public static string ToString<T>(ICollection<T> dataPoints)
        {
            StringBuilder sb = new();

            _ = sb.AppendLine($"\nDataPoints<{typeof(T).Name}>");
            _ = sb.AppendLine($"Count: {dataPoints?.Count ?? 0}");
            _ = sb.AppendLine("----------------------------------------");

            if (dataPoints == null || dataPoints.Count == 0)
            {
                _ = sb.AppendLine("Collection was either empty or null");
                return sb.ToString();
            }

            // Add each data point with index
            int index = 0;
            foreach (T point in dataPoints)
            {
                _ = sb.AppendLine($"{index++} {FormatDataPoint(point)}");
            }

            if (typeof(T) == typeof(System.Numerics.Vector2) ||
                typeof(T) == typeof(System.Numerics.Vector3) ||
                typeof(T) == typeof(Tuple<float, float>) ||
                typeof(T) == typeof(Tuple<float, float, float>))
            {
                _ = sb.AppendLine("----------------------------------------");
                _ = sb.AppendLine("Info:");
                AppendStatistics(sb, dataPoints);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Formats a single data point based on its type
        /// </summary>
        private static string FormatDataPoint<T>(T point)
        {
            if (point == null)
            {
                //return string.Empty;
                return "PARAM_POINT_NULL";
            }

            return point switch
            {
                System.Numerics.Vector2 v2 => $"(x: {v2.X:F7}, y: {v2.Y:F7})",
                System.Numerics.Vector3 v3 => $"(x: {v3.X:F7}, y: {v3.Y:F7}, z: {v3.Z:F7})",
                Tuple<float, float> tuple => $"(x: {tuple.Item1:F7}, y: {tuple.Item2:F7})",
                Tuple<float, float, float> tuple => $"(x: {tuple.Item1:F7}, y: {tuple.Item2:F7}, z: {tuple.Item3:F7})",
                _ => throw new
                    NotImplementedException($"{nameof(FormatDataPoint)}: Could not format point of type {typeof(T)}")
            };
        }

        /// <summary>
        /// Appends statistical information for numeric data types
        /// </summary>
        private static void AppendStatistics<T>(StringBuilder sb, ICollection<T> dataPoints)
        {
            void AppendRange(string dimensionName, Tuple<float, float> stats) =>
                _ = sb.AppendLine($"{dimensionName} Range: {stats.Item1:F7} to {stats.Item2:F7}");

            try
            {
                if (typeof(T) == typeof(System.Numerics.Vector2))
                {
                    foreach (T? p in dataPoints)
                    {
                        if (p is System.Numerics.Vector2 vector2)
                        {
                            Tuple<float, float> xDimensionStats = GetMinMax(dataPoints, _ => vector2.X);
                            Tuple<float, float> yDimensionStats = GetMinMax(dataPoints, _ => vector2.Y);
                            AppendRange("X", xDimensionStats);
                            AppendRange("Y", yDimensionStats);
                        }
                    }
                }
                else if (typeof(T) == typeof(System.Numerics.Vector3))
                {
                    foreach (T? p in dataPoints)
                    {
                        if (p is System.Numerics.Vector3 vector3)
                        {
                            Tuple<float, float> xDimensionStats = GetMinMax(dataPoints, _ => vector3.X);
                            Tuple<float, float> yDimensionStats = GetMinMax(dataPoints, _ => vector3.Y);
                            Tuple<float, float> zDimensionStats = GetMinMax(dataPoints, _ => vector3.Z);
                            AppendRange("X", xDimensionStats);
                            AppendRange("Y", yDimensionStats);
                            AppendRange("Z", zDimensionStats);
                        }
                    }
                }
                else if (typeof(T) == typeof(Tuple<float, float>))
                {
                    foreach (T? p in dataPoints)
                    {
                        if (p is Tuple<float, float> tuple)
                        {
                            Tuple<float, float> xDimensionStats = GetMinMax(dataPoints, _ => tuple.Item1);
                            Tuple<float, float> yDimensionStats = GetMinMax(dataPoints, _ => tuple.Item2);
                            AppendRange("Time", xDimensionStats);
                            AppendRange("Depth", yDimensionStats);
                        }
                    }
                }
                else if (typeof(T) == typeof(Tuple<float, float, float>))
                {
                    foreach (T? p in dataPoints)
                    {
                        if (p is Tuple<float, float, float> tuple)
                        {
                            Tuple<float, float> xDimensionStats = GetMinMax(dataPoints, _ => tuple.Item1);
                            Tuple<float, float> yDimensionStats = GetMinMax(dataPoints, _ => tuple.Item2);
                            Tuple<float, float> zDimensionStats = GetMinMax(dataPoints, _ => tuple.Item3);
                            AppendRange("Time", xDimensionStats);
                            AppendRange("Depth", yDimensionStats);
                            AppendRange("Z", zDimensionStats);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _ = sb.AppendLine($"{nameof(AppendStatistics)}: Error calculating statistics for type {typeof(T).Name}: {ex.Message}");
            }
        }


        //for local use only.
        private static Tuple<float, float> GetMinMax<T>(ICollection<T> dataPoints, Func<T, float> valueSelector)
        {
            return Tuple.Create(dataPoints.Min(valueSelector), dataPoints.Max(valueSelector));
        }

#endregion
    }
}
