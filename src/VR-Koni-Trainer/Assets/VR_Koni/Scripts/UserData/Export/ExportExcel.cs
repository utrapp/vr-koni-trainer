using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using JetBrains.Annotations;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Drawing.Chart;
using UnityEngine;
using VR_Koni.Graph;

namespace VR_Koni.UserData.Export
{
    public static class ExportExcel
    {
        private const int WORK_SHEET_LIMIT = 200;

        public static string CreateExcelFileForAllUsers(
            List<List<Surgery>> groupedSurgeries,
            string exportDirectory = "C:/Koni_Exports")
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            string filePath = string.Empty;

            try
            {
                filePath = InitializeExportDirectory(exportDirectory);
                Dictionary<int, List<Surgery>> userMapping = CreateUserMapping(groupedSurgeries);
                if (!ValidateWorksheetLimit(userMapping))
                {
                    return null;
                }
                using ExcelPackage excel = new();
                ProcessUserData(excel, userMapping);
                SaveExcelFile(excel, filePath);
                return filePath;
            }
            catch (Exception ex)
            {
                HandleExportError(ex);
                return default;
            }
        }

        private static string InitializeExportDirectory(string exportDirectory)
        {
            if (!Directory.Exists(exportDirectory))
            {
                Directory.CreateDirectory(exportDirectory);
                Debug.LogWarning($"Directory {exportDirectory} was missing, created a new directory. " +
                                 $"The exports will be placed at {exportDirectory}.");
            }

            string timestamp = DateTime.Now.ToString("dd_MM_yyyy__HH_mm_ss");
            string fileName = $"KoniExport__{timestamp}.xlsx";
            return Path.Combine(exportDirectory, fileName);
        }

        private static Dictionary<int, List<Surgery>> CreateUserMapping(List<List<Surgery>> groupedSurgeries)
        {
            int userIdTmp = 1;
            return groupedSurgeries
                   .Where(surgeries => surgeries.Count > 0)
                   .Select((surgeries, index) => new
                   {
                       Surgeries = surgeries,
                       UserId = userIdTmp++
                   })
                   .ToDictionary(x => x.UserId, x => x.Surgeries);
        }

        private static bool ValidateWorksheetLimit(Dictionary<int, List<Surgery>> userMapping)
        {
            if (userMapping.Count * 2 > WORK_SHEET_LIMIT)
            {
                Debug.LogError($"Too many users to export in one Excel file. Limit: {WORK_SHEET_LIMIT} worksheets.");
                return false;
            }

            return true;
        }

        private static void ProcessUserData(ExcelPackage excel, Dictionary<int, List<Surgery>> userMapping)
        {
            foreach (KeyValuePair<int, List<Surgery>> entry in userMapping)
            {
                GenerateStatsWorksheet(excel, entry.Key, entry.Value);
                GenerateGraphWorksheet(excel, entry.Key, entry.Value);
            }
        }

        private static void GenerateStatsWorksheet(ExcelPackage excel, int localUniqueId, List<Surgery> surgeries)
        {
            string statsSheetName = $"{localUniqueId:00}_DataStatistics";
            Debug.Log($"statsSheetName:{statsSheetName}");
            ExcelWorksheet statsSheet = excel.Workbook.Worksheets.Add(statsSheetName);
            InitializeStatsHeaders(statsSheet);
            PopulateStatsData(statsSheet, surgeries);
            statsSheet.Cells[statsSheet.Dimension.Address].AutoFitColumns();
        }

        private static void InitializeStatsHeaders(ExcelWorksheet statsSheet)
        {
            statsSheet.Cells["A1"].Value = "SurgeryID";
            statsSheet.Cells["B1"].Value = "Username";
            statsSheet.Cells["C1"].Value = "Operation Date";
            statsSheet.Cells["D1"].Value = "Depth";
            statsSheet.Cells["E1"].Value = "LLETZ";
            statsSheet.Cells["F1"].Value = "Contact Time";
            statsSheet.Cells["G1"].Value = "Operation Time";
            statsSheet.Cells["H1"].Value = "Mass Volume";
            statsSheet.Cells["I1"].Value = "Number of Cuts";
        }

        private static void PopulateStatsData(ExcelWorksheet statsSheet, List<Surgery> surgeries)
        {
            int row = 2;
            foreach (Surgery surgery in surgeries)
            {
                AddSurgeryRow(statsSheet, surgery, row);
                row++;
            }
        }

        private static void AddSurgeryRow(ExcelWorksheet statsSheet, Surgery surgery, int row)
        {
            statsSheet.Cells[$"A{row}"].Value = surgery.SurgeryId;
            statsSheet.Cells[$"B{row}"].Value = surgery.Username ?? "N/A";
            statsSheet.Cells[$"C{row}"].Value = surgery.Date.ToString("dd.MM.yyyy HH:mm:ss");
            statsSheet.Cells[$"D{row}"].Value = surgery.MaxDepth;
            statsSheet.Cells[$"E{row}"].Value = surgery.LLETZ;
            statsSheet.Cells[$"F{row}"].Value = TimeSpan.FromSeconds(surgery.ContactTime).ToString(@"hh\:mm\:ss");
            statsSheet.Cells[$"G{row}"].Value = TimeSpan.FromSeconds(surgery.OperationTime).ToString(@"hh\:mm\:ss");
            statsSheet.Cells[$"H{row}"].Value = surgery.Volume;
            statsSheet.Cells[$"I{row}"].Value = surgery.NumOfCuts;
        }

        private static void GenerateGraphWorksheet(ExcelPackage excel, int localUniqueId, List<Surgery> surgeries)
        {
            string graphSheetName = $"{localUniqueId:00}_Graphs";
            ExcelWorksheet graphSheet = excel.Workbook.Worksheets.Add(graphSheetName);

            ExcelChart masterChart = CreateMasterChart(graphSheet);
            //temp work around for the perfect chart
            List<List<Tuple<float, float>>> allTranslatedCuttingData = new();
            int dataRow = 45;

            foreach (Surgery surgery in surgeries)
            {
                ProcessSurgeryData(graphSheet, surgery, masterChart, allTranslatedCuttingData, ref dataRow);
            }

            CreatePerfectChart(graphSheet, allTranslatedCuttingData, dataRow, masterChart);
            graphSheet.Cells[graphSheet.Dimension?.Address].AutoFitColumns();
        }

        private static ExcelChart CreateMasterChart(ExcelWorksheet graphSheet)
        {
            ExcelChart masterChart = graphSheet.Drawings.AddChart("OverallChart", eChartType.Line);
            masterChart.SetPosition(1, 0, 1, 0);
            masterChart.SetSize(800, 650);
            masterChart.Title.Text = "Summary Evaluation for all surgeries performed by this user.";
            masterChart.XAxis.Title.Text = "Time(seconds)";
            masterChart.YAxis.Title.Text = "Depth(mm)";
            masterChart.Font.Italic = true;
            masterChart.XAxis.LabelPosition = eTickLabelPosition.Low;
            masterChart.XAxis.MinValue = 0.0;
            masterChart.YAxis.MinValue = 0.0;
            return masterChart;
        }

        private static void ProcessSurgeryData(
            ExcelWorksheet graphSheet,
            Surgery surgery,
            ExcelChart masterChart,
            List<List<Tuple<float, float>>> allTranslatedCuttingData,
            ref int dataRow)
        {
            List<Tuple<float, float>> cuttingData = PrepareCuttingData(surgery, allTranslatedCuttingData);
            AddDataToSheet(graphSheet, cuttingData, dataRow);

            _ = CreateSurgeryChart(graphSheet, surgery, dataRow, cuttingData);

            AddImageToChart(graphSheet, surgery, dataRow);
            AddToMasterChart(masterChart, graphSheet, surgery, cuttingData, dataRow);

            dataRow = CalculateNextDataRow(dataRow, cuttingData.Count);
        }

        private static List<Tuple<float, float>> PrepareCuttingData(
            Surgery surgery,
            List<List<Tuple<float, float>>> allTranslatedCuttingData)
        {
            DataPoints<Tuple<float, float>> currDataPoints = new(surgery.CuttingData);
            List<Tuple<float, float>> cuttingData = currDataPoints.GetDataPointsCopy().ToList();
            allTranslatedCuttingData.Add(cuttingData);
            return cuttingData;
        }

        private static void AddDataToSheet(
            ExcelWorksheet graphSheet,
            List<Tuple<float, float>> cuttingData,
            int dataRow)
        {
            graphSheet.Cells[dataRow, 1].Value = "Time(sec) [X- series]";
            graphSheet.Cells[dataRow, 2].Value = "Depth(mm) [Y- series]";

            for (int i = 0; i < cuttingData.Count; i++)
            {
                graphSheet.Cells[dataRow + i + 1, 1].Value = cuttingData[i].Item1;
                graphSheet.Cells[dataRow + i + 1, 2].Value = cuttingData[i].Item2;
            }
        }

        private static ExcelChart CreateSurgeryChart(
            ExcelWorksheet graphSheet,
            Surgery surgery,
            int dataRow,
            List<Tuple<float, float>> cuttingData)
        {
            ExcelChart chart = graphSheet.Drawings.AddChart($"Chart_Surgery_{surgery.SurgeryId}", eChartType.Line);
            chart.SetPosition(dataRow, 0, 4, 5);
            chart.SetSize(900, 700);
            chart.XAxis.Title.Text = "Time(seconds)";
            chart.YAxis.Title.Text = "Depth(mm)";
            chart.Title.Text = $"Depth v/s Time for SurgeryID: {surgery.SurgeryId}";
            chart.Font.Italic = true;
            chart.XAxis.LabelPosition = eTickLabelPosition.Low;

            ExcelChartSerie series = chart.Series.Add(
                                                      graphSheet.Cells[dataRow + 1, 2, dataRow + cuttingData.Count, 2],
                                                      graphSheet.Cells[dataRow + 1, 1, dataRow + cuttingData.Count, 1]
                                                     );
            series.Header = $"Depth(mm) for Surgery {surgery.SurgeryId}";

            return chart;
        }

        private static void AddImageToChart(ExcelWorksheet graphSheet, Surgery surgery, int dataRow)
        {
            int yOffSet = 200; 
            const int IMAGE_WIDTH = 500;
            const int IMAGE_HEIGHT = 500;
            const int INCREMENT = IMAGE_HEIGHT + 100; //so 100 px gap between images since height is 500

            foreach (string path in surgery.ImagePaths)
            {
                if (File.Exists(path))
                {
                    ExcelPicture picture = graphSheet.Drawings.AddPicture(
                                                                          $"Image_Surgery_{surgery.SurgeryId}_{Path.GetFileNameWithoutExtension(path)}",
                                                                          new FileInfo(path));

                    picture.SetPosition(dataRow, yOffSet, 19, 0);
                    picture.SetSize(IMAGE_WIDTH, IMAGE_HEIGHT);
                    yOffSet += INCREMENT;
                }
            }
        }

        private static void AddToMasterChart(
            ExcelChart masterChart,
            ExcelWorksheet graphSheet,
            Surgery surgery,
            List<Tuple<float, float>> cuttingData,
            int dataRow)
        {
            ExcelChartSerie masterSeries = masterChart.Series.Add(
                                                                  graphSheet.Cells[dataRow + 1, 2,
                                                                                   dataRow + cuttingData.Count, 2],
                                                                  graphSheet.Cells[dataRow + 1, 1,
                                                                                   dataRow + cuttingData.Count, 1]
                                                                 );
            masterSeries.Header = $"Surgery {surgery.SurgeryId}";
        }

        private static int CalculateNextDataRow(int currentDataRow, int dataCount)
        {
            return Math.Max((currentDataRow + dataCount + 15), (currentDataRow + 45));
        }

        private static void CreatePerfectChart(
            ExcelWorksheet graphSheet,
            List<List<Tuple<float, float>>> allTranslatedCuttingData,
            int dataRow,
            ExcelChart masterChart)
        {
            float maxTime = allTranslatedCuttingData
                            .SelectMany(data => data.Select(point => point.Item1))
                            .Max();

            List<Tuple<float, float>> perfectChartData = DemoDataGenerator.GenerateShapeData<Tuple<float, float>>(
                                                                                                                  shape:
                                                                                                                  Shapes
                                                                                                                      .Parabola,
                                                                                                                  maxDepth
                                                                                                                  : 8.0f,
                                                                                                                  totalTime
                                                                                                                  : maxTime
                                                                                                                 );

            AddPerfectChartData(graphSheet, perfectChartData, dataRow);

            _ = CreatePerfectChartVisual(graphSheet, dataRow, perfectChartData);

            AddPerfectChartToMaster(masterChart, graphSheet, dataRow, perfectChartData);
        }

        private static void AddPerfectChartData(
            ExcelWorksheet graphSheet,
            List<Tuple<float, float>> perfectChartData,
            int dataRow)
        {
            int perfectChartRow = dataRow + 6;
            graphSheet.Cells[perfectChartRow, 1].Value = "Perfect X";
            graphSheet.Cells[perfectChartRow, 2].Value = "Perfect Y";

            for (int i = 0; i < perfectChartData.Count; i++)
            {
                graphSheet.Cells[perfectChartRow + i + 1, 1].Value = perfectChartData[i].Item1;
                graphSheet.Cells[perfectChartRow + i + 1, 2].Value = perfectChartData[i].Item2;
            }
        }

        private static ExcelChart CreatePerfectChartVisual(
            ExcelWorksheet graphSheet,
            int dataRow,
            List<Tuple<float, float>> perfectChartData)
        {
            int perfectChartRow = dataRow + 6;
            ExcelChart perfectChart = graphSheet.Drawings.AddChart("PerfectChart", eChartType.Line);
            perfectChart.SetPosition(perfectChartRow + 1, 0, 5, 5);
            perfectChart.SetSize(800, 800);
            perfectChart.Title.Text = "Perfect Relation: Depth v/s Time (max Depth = 8.0, " +
                                      "max Time = max Time taken from all the surgeries performed " +
                                      "by this user.)";
            perfectChart.XAxis.Title.Text = "Time(seconds)";
            perfectChart.YAxis.Title.Text = "Depth(mm)";
            perfectChart.Font.Bold = true;
            perfectChart.Font.Italic = true;

            ExcelChartSerie perfectSeries = perfectChart.Series.Add(
                                                                    graphSheet.Cells[perfectChartRow + 1, 2,
                                                                                     perfectChartRow +
                                                                                     perfectChartData.Count, 2],
                                                                    graphSheet.Cells[perfectChartRow + 1, 1,
                                                                                     perfectChartRow +
                                                                                     perfectChartData.Count, 1]
                                                                   );
            perfectSeries.Header = "Perfect Data Series";

            return perfectChart;
        }

        private static void AddPerfectChartToMaster(
            ExcelChart masterChart,
            ExcelWorksheet graphSheet,
            int dataRow,
            List<Tuple<float, float>> perfectChartData)
        {
            int perfectChartRow = dataRow + 6;
            ExcelChartSerie perfectMasterSeries = masterChart.Series.Add(
                                                                         graphSheet.Cells[perfectChartRow + 1, 2,
                                                                                          perfectChartRow +
                                                                                          perfectChartData.Count,
                                                                                          2],
                                                                         graphSheet.Cells[perfectChartRow + 1, 1,
                                                                                          perfectChartRow +
                                                                                          perfectChartData.Count,
                                                                                          1]
                                                                        );
            perfectMasterSeries.Header = "Perfect Data Series";
        }

        private static void SaveExcelFile(ExcelPackage excel, string filePath)
        {
            excel.SaveAs(new FileInfo(filePath));
            Debug.Log($"Excel file created successfully at: {filePath}");
        }

        private static void HandleExportError(Exception ex)
        {
            Debug.LogError($"Error creating Excel file: {ex.Message}");
        }

        /// <summary>
        /// <see cref="ProcessAndExportDataAsync"/> processes the SQLite file and exports the statistics.
        /// It is assumed that the sqlite file always resides in <see cref="Application.persistentDataPath"/>.
        /// If the sqlite is not found, then a load dialog box is opened to let the user select the file.
        /// </summary>
        public static async Task ProcessAndExportDataAsync()
        {
#region LoadingFile

            string filePath = await ValidateRequiredFileExistence();
            filePath = filePath.Replace("\\", "/");
            if (string.IsNullOrEmpty(filePath))
            {
                Debug.LogError($"Export Operation was cancelled because a valid file was not found/provided by the user.");
                return;
            }

            Debug.Log($"Selected file path: {filePath}");

            List<Surgery> flattenedSurgeries = Surgery.ExtractSurgeryFromDatabase(filePath);
            List<List<Surgery>> surgeriesGrouped = DataManager.Instance.GetSurgeriesGroupedByUser(flattenedSurgeries);
            Debug.Log($"Total {surgeriesGrouped?.Count} grouped surgeries are present.");

#endregion

#region SavingFile

            string folderPath = await CustomDialogBox.OpenSaveFolderDialogAsync();
            if (string.IsNullOrEmpty(folderPath))
            {
                Debug.LogError("Save directory is invalid. Export cancelled.");
                return;
            }

            ExportExcel.CreateExcelFileForAllUsers(groupedSurgeries: surgeriesGrouped,
                                                   exportDirectory: folderPath);

            Debug.Log($"Data successfully exported to: {folderPath}");

#endregion
        }

        /// <summary>
        /// <see cref="ValidateRequiredFileExistence"/> uses recursion to validate the existence of the required file.
        /// If a valid file was not found, then it opens the dialog box to select a valid file, if that selected
        /// file was not valid or somehow did not exist because of multithreaded operation on that file
        /// or deletion of the file by another process, then it opens the dialog box again unless explicitly
        /// told not to do so when the user presses the cancel button on the Dialog Box UI.
        /// </summary>
        private static async Task<string> ValidateRequiredFileExistence([CanBeNull] string filePath = null)
        {
            filePath ??= Path.Combine(Application.persistentDataPath, "koni.sqlite");
            //serves as a double layer of security since its crucial that we get a valid sqlite file in form we expect
            if (File.Exists(filePath) && Path.GetExtension(filePath).ToLowerInvariant().Equals(".sqlite"))
            {
                return filePath;
            }

            Debug.LogError($"File {filePath} does not exist.");
            filePath = await CustomDialogBox.OpenSqLiteFileSelectionDialogAsync();

            //handles cases for if the user presses "Cancel"
            if (string.IsNullOrEmpty(filePath))
            {
                return string.Empty;
            }

            //if we did not find a valid file in the system, nor did the user provide a valid file,
            //then call yourself again to request for a valid file
            return await ValidateRequiredFileExistence(filePath);
        }

        public static bool Export(List<Surgery> surgeries, string filename)
        {
            // Pfad zum Ordner "Surgery Data" auf dem Desktop
            string folderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                                             "Surgery Data");

            // Überprüfen, ob der Ordner existiert, und falls nicht, den Ordner erstellen
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            // Vollständigen Dateipfad zusammensetzen
            string fileName = Path.Combine(folderPath, filename + ".xlsx");

            try
            {
                // Create a spreadsheet document by supplying the fileName.
                using SpreadsheetDocument spreadsheetDocument =
                    SpreadsheetDocument.Create(fileName, SpreadsheetDocumentType.Workbook);
                // Add a WorkbookPart to the document.
                WorkbookPart workbookPart = spreadsheetDocument.AddWorkbookPart();
                workbookPart.Workbook = new Workbook();

                // Add a WorksheetPart to the WorkbookPart.
                WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet(new SheetData());

                // Add Sheets to the Workbook.
                Sheets sheets = spreadsheetDocument.WorkbookPart.Workbook.AppendChild(new Sheets());

                // Append a new worksheet and associate it with the workbook.
                Sheet sheet = new()
                {
                    Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart),
                    SheetId = 1,
                    Name = "VR Koni"
                };

                sheets.Append(sheet);
                Worksheet worksheet = new Worksheet();
                SheetData sheetData = new SheetData();

                Row row = new Row();

                row.AppendChild(AddCellWithText("username"));
                row.AppendChild(AddCellWithText("depth"));
                row.AppendChild(AddCellWithText("lletz"));
                row.AppendChild(AddCellWithText("contacttime"));
                row.AppendChild(AddCellWithText("volume"));
                row.AppendChild(AddCellWithText("operationtime"));
                row.AppendChild(AddCellWithText("operationdate"));
                row.AppendChild(AddCellWithText("numofcuts"));

                sheetData.AppendChild(row);

                Dictionary<string, string> nameMap = ExportUtils.CreateEmojis(surgeries);
                foreach (Row r in surgeries.Select(x => x.ToExcel(nameMap[x.Username])))
                {
                    sheetData.AppendChild(r);
                }

                worksheet.Append(sheetData);
                worksheetPart.Worksheet = worksheet;
                workbookPart.Workbook.Save();
            }
            catch (Exception e)
            {
                Console.WriteLine("Fehler beim Erstellen oder Speichern der Excel-Datei: " + e.Message);
                return false;
            }

            return true;
        }

        public static Cell AddCellWithDate(DateTime date)
        {
            Cell cell = new()
            {
                CellValue = new CellValue(date.ToString("yyyy-MM-dd")),
                DataType = CellValues.Date
            };

            return cell;
        }

        public static Cell AddCellWithNumber(IFormattable number)
        {
            CultureInfo ci = new("en-us");
            Cell cell = new()
            {
                CellValue = new CellValue(number.ToString("F3", ci)),
                DataType = CellValues.Number
            };

            return cell;
        }

        public static Cell AddCellWithText(string text)
        {
            Cell cell = new()
            {
                DataType = CellValues.InlineString
            };

            Text t = new()
            {
                Text = text
            };

            InlineString inlineString = new InlineString();
            inlineString.AppendChild(t);

            cell.AppendChild(inlineString);

            return cell;
        }
    }
}
