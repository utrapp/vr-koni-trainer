# Kolposkopie und Konisation im Rahmen der Gebärmutterhalskrebs-Früherkennung

Die medizinischen Informationen in diesem Bereich erheben keinen Anspruch auf Vollständigkeit oder Aktualität.
Sie soll den Nutzer*innen des Koni Trainers einen allgemeinen Überblick geben, wie sich die Konisation in die Gebärmutterhalskrebs-Früherkennung eingegliedert, wie sie in Form einer LEEP/LLETZ durchgeführt wird und wie sie in dieser Simulation geübt werden kann.


## Kolposkopie und Biopsie

### Kolposkopie

Die Kolposkopie ist eine Gynäkologische Untersuchung, die infolge eines auffälligen Pap-Befunds (Pap IIID2 oder höher) im Rahmen der Gebärmutterhalskrebs Früherkennung, durchgeführt wird.[1]
Bei der Kolposkopie wird die Schleimhaut der Zervix mit Hilfe eines Koloskops, einer Art Lupe mit Lichtquelle, begutachtet. Zur besseren Erkennung veränderter Zellen kann der Gebärmutterhals außerdem mit einer verdünnten Essiglösung oder Jod betupft werden, wodurch sich kranke Zellen eher verfärben, als gesunde. [1]

### Biopsie
Werden im Rahmen der Kolposkopie Veränderungen festgestellt, wird zunächst mit einer kleinen Zange ein Gewebestück aus dem betroffenen Areal entnommen. Diese Entnahme wird auch als Knipsbiopsie bezeichnet. [1]

Der Gebärmutterhals ist sehr unempfindlich und die Biopsie kaum schmerzhaft. Auf Wunsch ist eine örtliche Betäubung trotzdem möglich. [1]

Das entnommene Gewebe wird anschließend in einem Labor auf die Form der vorliegenden Zellveränderung untersucht. [1]

Bei Veränderungen, die nicht über die Schleimhaut hinaus reichen, unterscheidet man:
- CIN 1: leichte Zellveränderungen, die sich bei 50% der Betroffenen von allein zurückbildet
- CIN 2: mittelschwere Zellveränderungen, die sich bei etwas mehr als einem Drittel der Frauen zurückbildet.
- CIN 3: weit fortgeschrittene Zellveränderungen - Krebsvorstufe im Übergang zum Karzinom. Hierbei handelt es sich um Tumore, die noch auf die oberen Gewebeschichten begrenzt sind, aber unbehandelt mit hoher Wahrscheinlichkeit zu Gebärmutterhalskrebs werden.
Bei tiefergehenden Gewebeveränderungen gilt der Befund als invasives Zervixkarzinom und somit im engeren Sinne als Gebärmutterhalskrebs [1]

Bei CIN 1 und 2 Befunden werden zunächst weitere Untersuchungen in Abständen von etwa 6 Monaten durchgeführt.
Bei CIN 3-Befunden wird besonders bei Patient*innen über 25 zu einer operativen Entfernung geraten. [1]

## Konisation

Die Konisation dient der Entfernung von verändertem Gewebe aus dem Gebärmutterhals.
Dazu wird das erkrankte Gewebe in einem Kegel aus dem Gebärmutterhals geschnitten. Dabei wird darauf geachtet, einen Rand von gesundem Gewebe mit zu entfernen, um die Wahrscheinlichkeit eines Rückfalls so gering wie möglich zu halten. [1]

Häufig wird die Konisation mittels einer "Schlingenkonisation" durchgeführt. Dabei kommt eine Elektroschlinge zum Einsatz. Alternativ kann das Gewebe mit einem Laserstrahl oder einem Skalpell entfernt werden. [1]

Je nach Situation wird im Anschluss an die Konisation noch eine Ausschabung des Gebärmutterhalses durchgeführt, um zu verhindern, dass Zellveränderungen im Zervikalkanal übersehen werden. [1]

## Kontrolle nach der Konisation
Durch die Untersuchung des entfernten Gewebes im Labor wird festgestellt, ob alles an verändertem Gewebe bei der Konisation entfernt wurde. [1]
Ist dies der Fall, ist keine weitere Behandlung nötig. Nach 6, 12 und 24 Monaten sollte dennoch eine Nachkontrolle stattfinden. [1]

## Worauf Patient*innen nach der OP achten müssen
### (Sollen diese Informationen mit in den Guide aufgenommen werden?)

### Kolposkopie und Konisation in dieser Simulation

Die Kolposkopie, sowie die Entnahme einer Biopsie sind nicht Teil dieser Trainings-Simulation.
Die Simulation dient derzeit ausschließlich dem Erlernen und Üben den Schlingenführung im Rahmen einer "Schlingenkonisation", auch bekannt als Loop Electrosurgical Excision Procedure (LEEP)

## Quellen:
[1] https://www.krebsinformationsdienst.de/tumorarten/gebaermutterhalskrebs/vorstufen.php (zuletzt abgerufen am 17.01.2024)
