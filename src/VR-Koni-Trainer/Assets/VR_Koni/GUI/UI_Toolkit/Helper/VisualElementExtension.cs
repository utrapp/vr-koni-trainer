using UnityEngine;
using UnityEngine.UIElements;

namespace VR_Koni.GUI.UI_Toolkit.Helper
{
    public static class VisualElementExtension
    {
        public static Button RegisterButtonCallback(this VisualElement rootElement, string buttonName, EventCallback<ClickEvent> callback)
        {
            if (rootElement.Q(buttonName) is not Button button)
            {
                Debug.LogError($"Button {buttonName} not found");
                return null;
            }

            button.RegisterCallback(callback);
            return button;
        }

        public static void SetIsVisible(this VisualElement element, bool display)
        {
            if (element == null) return;
            element.style.display = display ? DisplayStyle.Flex : DisplayStyle.None;
        }

        public static bool GetIsVisible(this VisualElement element)
        {
            if (element == null) return false;
            return (element.style.display == DisplayStyle.Flex);
        }

        public static void TogglePasswordVisibility(this TextField passwordTextField, Button eyeToggleButton) =>
            SetPasswordVisibility(passwordTextField, eyeToggleButton, passwordTextField.isPasswordField);

        public static void SetPasswordVisibility(this TextField passwordTextField, Button eyeToggleButton, bool visibility)
        {
            if (visibility)
            {
                passwordTextField.isPasswordField = false;
                eyeToggleButton.RemoveFromClassList("eye-icon");
                eyeToggleButton.AddToClassList("eye-icon--slashed");
            }
            else
            {
                passwordTextField.isPasswordField = true;
                eyeToggleButton.RemoveFromClassList("eye-icon--slashed");
                eyeToggleButton.AddToClassList("eye-icon");
            }
        }
    }
}
