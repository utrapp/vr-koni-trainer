# LEEP-Score

The LLEP-Score was developed by Takacs et al. [2,3] to measure the learning effects of participants in a study practicing LEEP surgery with a simulator.
The score allows simultaneous evaluation of cone depth and specimen fragmentation resulting from a LEEP.

It is calculated using the deviation from a target cone depth of 8-10 mm and the number of excisions performed minus the first one.
LEEP-Score: Total Cone depth - target range + number of cuts -1

For Example:
An excision accomplished by performing two resections with a resulting total cone depth of 12 mm results in a LEEP-Score of 3: 
12 mm – 10 mm + 2 cuts - 1 cut = 3

References:

2. Takacs FZ, Gerlinger C, Hamza A, et al. A standardized simulation training program to type 1 loop electrosurgical excision of the transformation zone: a prospective observational study. Arch Gynecol Obstet. 2020;301(2):611–618. doi: 10.1007/s00404-019-05416-1

3. Takacs FZ, Radosa JC, Gerlinger C, et al. Introduction of a learning model for type 1 loop excision of the transformation zone of the uterine cervix in undergraduate medical students: a prospective cohort study. Arch Gynecol Obstet. 2019;299(3):817–824. doi: 10.1007/s00404-018-5019-7
