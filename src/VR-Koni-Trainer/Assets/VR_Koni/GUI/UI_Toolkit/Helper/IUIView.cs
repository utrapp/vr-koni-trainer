using UnityEngine.UIElements;

namespace VR_Koni.GUI.UI_Toolkit.Helper
{
    public interface IUIView
    {
        VisualElement RootElement { get; }
        void SubscribeToEvents();
        void UnsubscribeFromEvents();

        /// <summary>
        /// Resets the view layout and subviews to its original state.
        /// </summary>
        void ResetViewSelections();
    }
}
