using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using DocumentFormat.OpenXml.Spreadsheet;
using JetBrains.Annotations;
using Unity.Mathematics;
using UnityEngine;
using VR_Koni.UserData.Export;
using Mono.Data.Sqlite;
using VR_Koni.Graph;

namespace VR_Koni.UserData
{
    /// <summary>
    /// Class <c>Surgery</c> models a training attempt (regarding of it was successful or a failure) of a certain User.
    /// </summary>
    [Serializable]
    public class Surgery
    {
        // (Unity) timestamp , depth in mm
        public List<Tuple<float, float>> CuttingData { get; set; } = new();
        public int SurgeryId { get; set; }
        public int NumOfCuts { get; }

        [CanBeNull]
        public string Username { get; set; }

        public DateTime Date { get; }
        public int LLETZ { get; }
        public float OperationTime { get; }
        public float ContactTime { get; }
        public float MaxDepth { get; }
        public double Volume { get; }
        public string SerializedCancerCutoutString { get; set; }
        public bool NeedsUpdate { get; set; }
        
        public ICollection<string> ImagePaths  = new List<string>();
        
        /// <summary>
        /// Constructor of the class Surgery, could either be used manual or by creating an instance from the DB.
        /// </summary>
        /// <param name="surgeryId">ID of the Surgery, set when inserting into DB.
        /// If created manually, use the default argument.</param>
        /// <param name="username">username of the Users that's responsible for the Surgery.
        ///     This should be set when assigning an Surgery to an User and could be used to write LINQ-Queries.
        ///     If the User doesn't exists at the moment of creation, use the default argument.</param>
        /// <param name="depth"></param>
        /// <param name="lletz"></param>
        /// <param name="contactTime"></param>
        /// <param name="volume"></param>
        /// <param name="operationTime"></param>
        /// <param name="date"></param>
        /// <param name="numOfCuts"></param>
        /// <param name="serializedGameObject">The Gameobject representing the Cancer Cutout of that surgery</param>
        /// <param name="imagePaths"> path to the images for this surgery</param>
        public Surgery(string username, 
                       float depth, 
                       int lletz, 
                       float contactTime,
                       float volume, 
                       float operationTime,
                       DateTime date, 
                       int numOfCuts, 
                       string serializedGameObject, 
                       int surgeryId = -1,
                       params string[] imagePaths)// imagePath is empty so that we don't have to adjust the current codebase
        {
            SurgeryId = surgeryId;
            NeedsUpdate = false;
            MaxDepth = depth;
            Username = username;
            LLETZ = lletz;
            Volume = volume;
            ContactTime = contactTime;
            OperationTime = operationTime;
            Date = date;
            NumOfCuts = numOfCuts;
            SerializedCancerCutoutString = serializedGameObject;
            foreach (string path in imagePaths)
            {
                this.ImagePaths.Add(path);
            }
        }

        public Surgery(User user, float operationTime, float contactTime, float maxDepth, double volume,
                       List<Tuple<float, float>> data, string serializedCancerCutoutString, params string[] imagePaths) // imagePath is empty so that we dont have to adjust the current codebase
        {
            SurgeryId = -1;
            Username = user.Clearname;
            NeedsUpdate = true;
            MaxDepth = maxDepth;
            OperationTime = operationTime;
            Volume = volume;
            ContactTime = contactTime;
            Date = DateTime.Now;

            CuttingData = data.Select(x => new Tuple<float, float>(x.Item1, x.Item2)).ToList();
            
            foreach (string path in imagePaths)
            {
                this.ImagePaths.Add(path);
            }

            NumOfCuts = 0;
            for (int i = 0; i < CuttingData.Count; i++)
            {
                if (CuttingData[i].Item2 < 0.001f)
                {
                    NumOfCuts++;
                    for (++i; i < CuttingData.Count && CuttingData[i].Item2 < 0.001f; i++) {}
                }
            }

            LLETZ = maxDepth switch
            {
                < 8 => NumOfCuts + math.abs(8 - (int)maxDepth),
                > 10 => NumOfCuts + math.abs(10 - ((int)maxDepth + 1)),
                _ => NumOfCuts
            };
            LLETZ--;
            SerializedCancerCutoutString = serializedCancerCutoutString;
        }
        
        /// <summary>
        /// Prepares the current objects data as a collection of SQL parameters for database insertion.
        /// <para>
        /// - Formats <see cref="CuttingData"/> into a string using the classic C# tuple format: "{Item1,Item2},{Item3,Item4},...".
        /// - Ensures numeric values (<see cref="MaxDepth"/>, <see cref="LLETZ"/>, etc.) are represented with 7 decimal places for consistency.
        /// - Handles date formatting (<see cref="Date"/>) using DE culture ifno, outputting date in "dd.MM.yyyy HH:mm:ss" format.
        /// - Includes additional fields like <see cref="Username"/>, <see cref="NumOfCuts"/>, and <see cref="SerializedCancerCutoutString"/>.
        /// - Returns the complete collection of <see cref="SqliteParameter"/> objects, which are ready to be added to a SQL command.
        /// </para>
        /// </summary>
        public ICollection<IDbDataParameter> GetSqlParameters()
        {
            ICollection<IDbDataParameter> parameters = new List<IDbDataParameter>();

            CultureInfo germanCultureInfo = new CultureInfo("de-DE");
            CultureInfo usCultureInfo = new CultureInfo("en-US");

            parameters.Add(new SqliteParameter("@username", Username));
            parameters.Add(new SqliteParameter("@operationdate", Date.ToString("dd.MM.yyyy HH:mm:ss", germanCultureInfo)));
            parameters.Add(new SqliteParameter("@depth", MaxDepth.ToString("F7", usCultureInfo)));
            parameters.Add(new SqliteParameter("@lletz", LLETZ));
            parameters.Add(new SqliteParameter("@contacttime", ContactTime.ToString("F7", usCultureInfo)));
            parameters.Add(new SqliteParameter("@operationtime", OperationTime.ToString("F7", usCultureInfo)));
            parameters.Add(new SqliteParameter("@volume", Volume.ToString("F7", usCultureInfo)));
            parameters.Add(new SqliteParameter("@numofcuts", NumOfCuts));

            //use the classic c# tuple formatting "<x1,y1>,<x2,y2>,..." 
            string formattedCuttingData = string.Join(",", CuttingData.Select(tuple => $"<{tuple.Item1.ToString("F7", usCultureInfo)},{tuple.Item2.ToString("F7", usCultureInfo)}>"));
            parameters.Add(new SqliteParameter("@depthdata", formattedCuttingData));
            parameters.Add(new SqliteParameter("@meshdata", SerializedCancerCutoutString));
            parameters.Add(new SqliteParameter("@imagePath", string.Join(",", ImagePaths)));

            return parameters;
        }
        
        public List<Tuple<float, float>> FetchCuttingDataFromDB()
        {
            string result = DataManager.Instance.FetchCuttingData(SurgeryId);
            if (string.IsNullOrEmpty(result))
            {
                Debug.LogError($"{nameof(FetchCuttingDataFromDB)}: No cutting data found for surgeryId: {SurgeryId}.");
                return new List<Tuple<float, float>>();
            }

            List<float> parsedValues = result.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                             .Select(item => item.Trim('<', '>'))
                                             .Select(item =>
                                             {
                                                 try
                                                 {
                                                     return float.Parse(item, new CultureInfo("en-US"));
                                                 }
                                                 catch (Exception ex)
                                                 {
                                                     Debug.LogError($"{nameof(FetchCuttingDataFromDB)}: Error parsing '{item}': {ex.Message}");
                                                     return float.NaN;
                                                 }
                                             })
                                             .Where(value => !float.IsNaN(value))
                                             .ToList();

            List<Tuple<float, float>> tuples = new();
            for (int i = 0; i < parsedValues.Count - 1; i += 2)
            {
                tuples.Add(new Tuple<float, float>(parsedValues[i], parsedValues[i + 1]));
            }

            return tuples;
        }


        
        public string ToCSV(string emoji = null)
        {
            CultureInfo ci = new("en-us");
            return @$"{emoji ?? Username}, {MaxDepth.ToString("F3", ci)}, " +
                   @$"{LLETZ.ToString("F3", ci)}, {ContactTime.ToString("F3", ci)}, " +
                   @$"{Volume.ToString("F3", ci)}, {OperationTime.ToString("F3", ci)}, {Date:yyyy-MM-dd}, {NumOfCuts}";
        }

        /// <summary>
        /// <c>SQLHeader</c> headers the SQL
        /// </summary>
        /// <returns>username, depth, lletz, contacttime, volume, operationtime, operationdate</returns>
        public static string SQLHeader()
        {
            return "username, depth, lletz, contacttime, volume, operationtime, operationdate, numofcuts, meshdata, imagepath";
        }

        public static string SQLDepthHeader()
        {
            return
                "username, depth, lletz, contacttime, volume, operationtime, operationdate, numofcuts, depthdata, meshdata, imagePath";
        }

        public Row ToExcel(string emoji = null)
        {
            Row row = new();

            row.AppendChild(ExportExcel.AddCellWithText(emoji ?? Username));
            row.AppendChild(ExportExcel.AddCellWithNumber(MaxDepth));
            row.AppendChild(ExportExcel.AddCellWithNumber(LLETZ));
            row.AppendChild(ExportExcel.AddCellWithNumber(ContactTime));
            row.AppendChild(ExportExcel.AddCellWithNumber(Volume));
            row.AppendChild(ExportExcel.AddCellWithNumber(OperationTime));
            row.AppendChild(ExportExcel.AddCellWithDate(Date));
            row.AppendChild(ExportExcel.AddCellWithNumber(NumOfCuts));

            return row;
        }
        
        /// <summary>
        /// <see cref="ExtractSurgeryFromDatabase"/> is supposed to extract all the data from the table surgery
        /// from the sqlite file.
        /// We expect the sqlite file to always have a table called "surgery" so we can safely call
        /// <c>select * from surgery</c>, get the data in the form stored in the database, then convert it to c# types
        /// so that we can get a collection of collection of <see cref="VR_Koni.UserData.Surgery"/>.
        /// </summary>
        /// <param name="sqliteFilePath"></param>
        /// <exception cref="FileNotFoundException"></exception>
        /// <exception cref="SqliteException"></exception>
        public static List<Surgery> ExtractSurgeryFromDatabase(string sqliteFilePath)
        {
            if (!File.Exists(sqliteFilePath))
            {
                throw new
                    FileNotFoundException($"{nameof(ExtractSurgeryFromDatabase)}: " +
                                          $"The file '{sqliteFilePath}' does not exist.");
            }

            List<Surgery> surgeries = new();
            const string QUERY = "SELECT * FROM surgery";

            try
            {
                using SqliteConnection connection = new($"URI=file:{sqliteFilePath}");
                connection.Open();

                using SqliteCommand command = new(QUERY, connection);
                using SqliteDataReader reader = command.ExecuteReader();
                
                //remove the duplicate entries for the depth value so we won't have something like 10 consecutive
                //rows with 0 as Y value.
                Sanitizer<Tuple<float,float>> sanitizer = new(2);
                
                while (reader.Read())
                {
                    int surgeryId = reader.GetInt32(0);
                    string? username = reader.IsDBNull(1) ? null : Hashing.ToSHA256(reader.GetString(1));
                    DateTime operationDate = DateTime.ParseExact( reader.GetString(2), "dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    float depth = (float)reader.GetDouble(3);
                    int lletz = reader.GetInt32(4);
                    float contactTime = (float)reader.GetDouble(5);
                    float operationTime = (float)reader.GetDouble(6);
                    double volume = reader.GetDouble(7);
                    int numOfCuts = reader.GetInt32(8);
                    //will be retrieved in the surgery class itself
                    //string depthDataBase64 = reader.GetString(9);
                    string? meshData = reader.IsDBNull(10) ? null : reader.GetString(10);
                    string[] imagePaths = reader.GetString(11).Split(',');
                    Surgery surgery = new(username:username, 
                                          depth:depth, 
                                          lletz:lletz, 
                                          contactTime:contactTime,
                                          volume:(float)volume, 
                                          operationTime:operationTime,
                                          date: operationDate, 
                                          numOfCuts:numOfCuts, 
                                          serializedGameObject:meshData, 
                                          surgeryId:surgeryId,
                                          imagePaths:imagePaths);
                    
                    List<Tuple<float, float>> cuttingData = surgery.FetchCuttingDataFromDB();
                    sanitizer.SanitizeData(ref cuttingData);
                    
                    surgery.CuttingData = cuttingData;
                    
                    surgeries.Add(surgery);
                }
            }
            catch (SqliteException ex)
            {
                Debug.LogError($"SQLite Error: {ex.Message}\n{ex.StackTrace}");
                throw;
            }
            catch (Exception ex)
            {
                Debug.LogError($"Error: {ex.Message}\n{ex.StackTrace}");
                throw;
            }

            return surgeries;
        }
    }
}
