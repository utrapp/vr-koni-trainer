# Usability Test - Probetest Durchführung

Testperson: Anne

### Pr-Menu

#### Task 1

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und wollen nun zum ersten mal den VR Koni Trainer starten. Wie gehen Sie vor?

**Inputs**

- War der Startprozess für Sie klar?

**Prozess**

- Anne: Geht nicht war schon gestartet
- Derzeit ist es nur möglich, die Anwendung mit Unity auszuführen

#### Task 2

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und wollen sich nun einen Account für den Koni Trainer anlegen. Wie gehen Sie vor?

**Inputs**

- Sind die geforderten Eingaben für Sie klar?
- Ist der Registrierungsprozess für Sie angenehm und einfach gestaltet?

**Prozess**

- Herr Meyer hat sich soch bei einem Pre-Test regeestiert
- Anne: Sehe die Register makierung, den Reiter wähle ich aus

**Notizen**

- War im Allgemeinen einfach
- Frage: Ist es möglich, einen benutzten Benutzernamen wiederzuverwenden (eindeutiger Benutzername)?
- Pop-up-Fenster hinzufügen (oder eine andere Möglichkeit der Benachrichtigung)
  - Wenn der Benutzername bereits verwendet wird
  - Wenn das Passwort bei der Registrierung falsch wiederholt wird
  - Wenn man sich mit einem falschen Benutzernamen oder Passwort anmeldet

# Kein Login Task

zusammensfassen mit registrierung

#### Task 3 - noch nicht testbar

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und haben noch nie eine Konisation durchgeführt. Sie wissen also nicht genau was auf Sie zukommt. Wie gehen Sie vor, um sich mit dem Verfahren vertraut zu machen?

**Inputs**

- Fühlen Sie sich nun gut mit dem Verfahren vertraut?
- Würden Sie sich weitere Informationen wünschen? Wenn ja, welche?
- Ist die Darstellung der Informationen für Sie hilfreich?
- Was könnte verbessert werden?

#### Task 4

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und benutzen den Koni Trainer zum ersten mal. Sie wissen nicht wie die Steuerung des Spiels funktioniert und wollen sich nun mit ihr vertraut machen. Wie gehen Sie vor?

**Inputs**

- Fühlen Sie sich gut vorbereitet um die Controller auch im Spiel zu benutzen?
- Ist die Erklärung für Sie ausreichend?
- Könnte man die Steuerung Ihrer Meinung nach noch besser Erklären/Darstellen?

**Notizen**

#### Task 5

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und wollen, bevor Sie das erste Training starten, die Lautstärke im Spiel auf ihre Präferenzen anpassen. Wie gehen Sie dabei vor?

**Inputs**

- Waren die Einstellungen für Sie gut zu erreichen?
- Sind alle nötigen Einstellungen vorhanden?
- Keine ahnung, habs ja noch nie angewendet

Lautstärke gefunden und eingestellt
leicht zu erreichen, setting symbol bekannt und verständlich

#### Task 6 - retundand

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und haben noch keine Erfahrung mit VR. Wie gehen Sie vor um sich damit vertraut zu machen?

**Inputs**

- Fühlen Sie sich nun ausreichend mit der VR-Umgebung vetraut?
- Fehlen Ihnen noch Informationen oder Übungen?

#### Task 7

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und wollen nun ihr erstes Training starten. Wie gehen Sie dabei vor?

würde zurerst auf home gehen, weil alle anderen optionen links stehen
erst wenn da nicht start vr steht schaue ich auf die Leiste

Hinweis Haben sie die brille kalibriert? bevor Vr gestartet wird

Braucht großes Pop Up brille aufsetzen

Frage zu offen gestellt? Was genau ist zu tun?

**Inputs**

- War der Weg bis in das Training klar?
-
- Waren alle Anweisungen gut verständlich?

Handhabung der Schlinge anpassen

#### Task 8

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und wollen zum ersten mal die Schlinge greifen und die Benutzung testen. Wie gehen sie dabei vor?

Frage erklärt bereits die Vorgehensweise

intuitiv würde eher Trigger genutzt werden

Komisch mit B, weil Daumen immer über dem A Knopf liegt

Schlinge kann suboptimal aufgegriffen werden.

Snapp in richtige Position vieleicht besser? damit winkel nicht komisch

**Tasks**

- Greifen Sie die Schlinge und schalten Sie sie an und wieder aus
- Legen Sie die Schlinge wieder hin

**Inputs**

- War die Benutzung der Schlinge für Sie leicht?
- Konnten Sie die Schlinge gut greifen?
- Konnten Sie sich noch an die Steuerung erinnern?
- Wie empfinden Sie das akustische Feedback?

#### Drehen und Kippen der Schlinge als Task

Schlinge an eigene Handhaltung anpassen.

#### Durchführen der OP als Task

Der Sound war gut, Zischen zu laut im verhältnis zum pippen

Back to menue erscheint links statt frontal -> seltsam, lieber wieder frontal
Wenn man schlinge in der Hand hält während man die op nochmal startet hat man zwei schlingen

(Vorschlag: Schlinge sollte im Menu automatisch zu ihrem span punkt resetet werden)
Hinweis Schlinge loslassen um Menu zu bedienen einfügen

#### Task 9 - muss aufgesplittet werden

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und wollen sich vor der Trainings-OP erstmal das zu entfernende Gewebe genauer anschauen. Wie gehen Sie dabei vor?

Geht im prototyp nicht, nur in herr wilperts anwednung

**Inputs**

- Ist das auszuschneidende Gewebe für Sie gut erkenntlich?
- War die Benutzung der vergrößerten Ansicht (Lupe) für Sie angenehm?
- Verlassen Sie die vergößerte Ansicht

#### Task 10

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und wollen nun Ihre erste Trainings-OP durchführen, wie gehen Sie dabei vor?

**Inputs**

- Wie ist ihr erster Eindruck nach der OP?
- Waren alle nötigen Informationen währen der OP vorhanden?
- WÜrden Sie sich weitere Informationen währen der OP wünschen?
- Wie empfinden Sie das akustische Feedback?
- Wie fühlt sich die Handhabung und Steuerung währen der OP für Sie an?
- Sind die akustischen Signale in ihrer Bedeutung klar?
- Starten Sie eine weitere OP (repeat Funktion).
- War es leicht für Sie eine weiter OP zu starten?
- Ist ihnen noch irgendwas zu den Fragen vorher aufgefallen?

#### Task 11

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und wollen sich nach dem Training mit Ihren Ergebnissen vertraut machen. Wie gehen Sie dabei vor?

**Inputs**

- Sind alle dargestellten Werte für Sie klar? Und auch klar erkennbar (gut zu lesen etc.)
- Wie finden Sie die Navigation zwischen den Bildschirmen?
- Fehlen Werte?
- Sind die angegebenen Werte für Sie angenehm dargestellt? (3D Ausschnitt der Konisation)

#### Task 12

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und wollen sich Entwicklung ihrer Ergebnisse über die letzte Zeit ansehen. Wie gehen Sie dabei vor?

**Inputs**

- Sind alle dargestellten Werte für Sie klar? Und auch klar erkennbar (gut zu lesen etc.)
- Sind die angegebenen Werte für Sie angenehm dargestellt? (Graphen)

#### Task 13

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und werden während einer Trainings-OP zu einem Notfall gerufen. Wie gehen Sie vor?

- Hier frage ich mich ob das getestet werden soll?

oder:

Stellen Sie sich vor, Sie sind angehende Ärztin und wollen sich Ihre Statistiken nun nochmal am normalen Computerbildschirm ansehen. Wie gehen Sie dabei vor?

#### Task 14

**Scenario**
Stellen Sie sich vor, Sie sind angehende Ärztin und haben alles abgeschlossen was Sie am Koni Trainer machen wollten. Sie wollen das Programm nun beenden. Wie gehen Sie vor?

**Inputs**

- Ist es Ihnen leicht/schwer gefallen das Programm zu beenden?
- War die Aktion hinter den Buttons die, die Sie erwartet haben?

## Abschließende Fragen:

- Wie gefällt Ihnen das Raumdesign?
- Wie ist Ihr abschließender erster Eindruck des VR-Koni Trainer?
- Wie würden Sie ein einführendes Tutorial für den Koni-Trainer gestalten?
- Wenn Sie drei Sachen ändern könnten, welche wären das?
