using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine.Localization;
using UnityEngine.UIElements;
using VR_Koni.UserData;

namespace VR_Koni.GUI.UI_Toolkit.MainMenu.Views.StatisticsViews
{
    public class MainMenuStatisticsAllTriesSubView : IMainMenuView
    {
        public LocalizedString ViewName => new LocalizedString();
        public VisualElement RootElement { get; private set; }
        private ScrollView scrollView;
        public event Action<Surgery> OnDetailsRequested;

        public void SetupVisualElements(VisualElement rootElement)
        {
            RootElement = rootElement;

            scrollView = rootElement.Q<ScrollView>("AllTriesListView");

            if (DataManager.Instance?.CurrentUser != null)
            {
                FillSurgeries(DataManager.Instance.CurrentUser.Surgeries);
            }
        }

        public void SubscribeToEvents() {}
        public void UnsubscribeFromEvents() {}
        public void ResetViewSelections() {}

        private void FillSurgeries(List<Surgery> surgeries)
        {
            foreach (VisualElement contentEntry in scrollView.contentContainer.Children())
            {
                scrollView.contentContainer.Remove(contentEntry);
            }
            
            // reverse the surgery list so that latest surgery will be shown on the top
            surgeries.Reverse();
            
            foreach (Surgery surgery in surgeries)
            {
                VisualElement entry = new();
                entry.AddToClassList("all-statistics-entry");

                Label dateField = new(surgery.Date.ToString("dd.MM.y"));
                dateField.style.width = new StyleLength(150);

                Label timeField = new(surgery.Date.ToString("HH:mm:ss"));
                timeField.style.width = new StyleLength(75);

                // TO:DO CultureInfo.CurrentCulture might need to be edited when we use Localization
                Label maxDepthField = new(surgery.MaxDepth.ToString(CultureInfo.CurrentCulture));
                maxDepthField.style.width = new StyleLength(150);

                VisualElement spacer = new();
                spacer.AddToClassList("spacer");

                Button detailsButton = new();
                detailsButton.text = "Details";
                detailsButton.SetEnabled(true);  // Make sure the button is enabled
                detailsButton.RegisterCallback<ClickEvent>(_ => SwitchToDetailsView(surgery));
                detailsButton.AddToClassList("button-blue");
                detailsButton.AddToClassList("button");

                entry.Add(dateField);
                entry.Add(timeField);
                entry.Add(maxDepthField);
                entry.Add(spacer);
                entry.Add(detailsButton);
                scrollView.Add(entry);
            }
        }
        
        /// <summary>
        /// Invokes the OnDetailsRequested event with the provided surgery details.
        /// This method is typically called when the "Details" button is clicked,
        /// allowing MainMenuStatisticsView to handle the request
        /// for switching to the detailed view of the specified surgery.
        /// </summary>
        /// <param name="surgery">The surgery data to be passed to the event subscribers.</param>

        private void SwitchToDetailsView(Surgery surgery)
        {
            OnDetailsRequested?.Invoke(surgery); 
        }
    }
}
