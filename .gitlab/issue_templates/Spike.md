# 🔬 Spike

_Short description of what will be tried and why_

## Hypotheses

_We believe that... {describe your hypothesis in one sentence}_

## Alternatives

_Which alternatives should be tried?_

## Criteria/metrics

_Which criteria/metrics are relevant to the decision? Performance...?_

## Modules/Components

_Which modules/components are likely to be affected?_

## :link: Links

_already researched sources/alternatives/blog articles..._

## :white_check_mark: Acceptance criteria
