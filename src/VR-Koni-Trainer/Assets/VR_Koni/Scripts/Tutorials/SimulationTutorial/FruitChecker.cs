using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.Interactables;

public class FruitChecker : MonoBehaviour
{
    public GameObject fruit1;
    public GameObject fruit2;
    public GameObject fruit3;
    public GameObject fruit4;
    public Button nextButton3_2;
    public Button nextButton3_3;

    private bool fruit1Grabbed = false;
    private bool fruit2Grabbed = false;
    private bool fruit3Grabbed = false;
    private bool fruit4Grabbed = false;

    public GameObject task3_2;
    public GameObject task3_3;
    public GameObject magnifier;

    void Start()
    {
        nextButton3_2.gameObject.SetActive(false);
        nextButton3_3.gameObject.SetActive(false);

        // Ensure all fruits are initially inactive
        fruit1.SetActive(false);
        fruit2.SetActive(false);
        fruit3.SetActive(false);
        fruit4.SetActive(false);
    }

    void Update()
    {
        if (IsTask3_2Active())
        {
            fruit1.gameObject.SetActive(true);
            fruit2.gameObject.SetActive(true);
            fruit3.gameObject.SetActive(true);
        }

        if (IsTask3_3Active())
        {
            fruit4.gameObject.SetActive(true);
            magnifier.gameObject.SetActive(true);
        }
    }

    public void OnFruitGrabbed(SelectEnterEventArgs args)
    {
        GameObject grabbedObject = args.interactableObject.transform.gameObject;

        if (grabbedObject == fruit1)
        {
            fruit1Grabbed = true;
            Debug.Log("Fruit 1 grabbed.");
            CheckAllFruitsGrabbed3_2();
        }
        else if (grabbedObject == fruit2)
        {
            fruit2Grabbed = true;
            Debug.Log("Fruit 2 grabbed.");
            CheckAllFruitsGrabbed3_2();
        }
        else if (grabbedObject == fruit3)
        {
            fruit3Grabbed = true;
            Debug.Log("Fruit 3 grabbed.");
            CheckAllFruitsGrabbed3_2();
        }
        else if (grabbedObject == fruit4)
        {
            fruit4Grabbed = true;
            Debug.Log("Fruit 4 grabbed.");
            CheckAllFruitsGrabbed3_3();
        }
        else
        {
            Debug.LogWarning("An unknown object was grabbed.");
        }
    }

    void CheckAllFruitsGrabbed3_2()
    {
        Debug.Log($"Checking if all fruits are grabbed for task 3.2. Status: {fruit1Grabbed}, {fruit2Grabbed}, {fruit3Grabbed}");
        if (fruit1Grabbed && fruit2Grabbed && fruit3Grabbed && IsTask3_2Active())
        {
            Debug.Log("All fruits for task 3.2 grabbed. Showing next button.");
            nextButton3_2.gameObject.SetActive(true);
        }
        else
        {
            nextButton3_2.gameObject.SetActive(false);
        }
    }

    void CheckAllFruitsGrabbed3_3()
    {
        Debug.Log($"Checking if all fruits are grabbed for task 3.3. Status: {fruit4Grabbed}");
        if (fruit4Grabbed && IsTask3_3Active())
        {
            Debug.Log("All fruits for task 3.3 grabbed. Showing next button.");
            nextButton3_3.gameObject.SetActive(true);
        }
        else
        {
            nextButton3_3.gameObject.SetActive(false);
        }
    }

    bool IsTask3_2Active()
    {
        return task3_2 != null && task3_2.activeSelf;
    }

    bool IsTask3_3Active()
    {
        return task3_3 != null && task3_3.activeSelf;
    }
}
