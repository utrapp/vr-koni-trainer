#nullable enable //The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
namespace VR_Koni.Graph
{
    /// <summary>
    /// Generic helper class that represents an organized version of the data points for graph rendering
    /// </summary>
    /// <typeparam name="T">The type of data like System.Numerics.Vector2, System.Numerics.Vector3, System.Numerics.Vector4 or a custom type</typeparam>
    public class DataPoints<T>
    {
        /// <summary>
        /// Container to store the received data.
        /// </summary>
        private readonly ICollection<T> dataPoints; //example: List<Tuple<float,float>>

#region ConstructorsAndTheirHelpers

        /// <summary>
        /// Default constructor initializing an empty collection
        /// </summary>
        public DataPoints()
        {
            dataPoints = new List<T>();
        }

        /// <summary>
        /// Parameterized constructor that takes in a collection regardless of the <typeparamref name="T"/> used for <see cref="DataPoints{T}"/>.
        /// Offers the flexibility of for example passing a <see cref="Tuple{T1, T2}"/> to <see cref="DataPoints{T}"/>, if <typeparamref name="T"/> 
        /// was a <see cref="System.Numerics.Vector2"/>, instead of having to stick to the same type.
        /// </summary>
        /// <param name="collection"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public DataPoints(ICollection collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException(nameof(collection),
                                                $"{nameof(DataPoints<T>)}: Cannot instantiate {nameof(dataPoints)} with a null value");
            }

            //we will be performing a deep copy of the data
            dataPoints = new List<T>();

            if (collection is ICollection<T> typedCollection)
            {
                if (DataPoints<T>.AllYValuesInMeters(typedCollection))
                {
                    typedCollection = DataPoints<T>.ConvertYValuesToMillimeters(typedCollection);
                    Debug.LogWarning($"{nameof(DataPoints<T>)}: Detected values in meters. Converted all Y values to mm.");
                }

                foreach (T? item in typedCollection)
                {
                    dataPoints.Add(item);
                }
            }
            else
            {
                foreach (object? item in collection)
                {
                    AddItemToDataPoints(item);
                }

                if (DataPoints<T>.AllYValuesInMeters(dataPoints))
                {
                    dataPoints = DataPoints<T>.ConvertYValuesToMillimeters(dataPoints);
                    Debug.LogWarning($"{nameof(DataPoints<T>)}: Detected values in meters. Converted all Y values to mm.");
                }
            }

            //finally we have all the points, now let's translate the X values to the origin so the graph starts from origin.
            StartXFromZero(ref dataPoints);
        }
        
        /// <summary>
        /// <see cref="AllYValuesInMeters"/> checks if all the given values in the <paramref name="dataPoints1"/>
        /// are in meter. It does so by checking if all the elements in <paramref name="dataPoints1"/> start with a
        /// 0.0x, if that's the case, it multiplies all the Y values in the <see cref="ICollection{T}"/> with
        /// <see langword="1000"/>
        /// </summary>
        /// <param name="dataPoints1"></param>
        /// <returns></returns>
        internal static bool AllYValuesInMeters(ICollection<T> dataPoints1)
        {
            return dataPoints1.All(item => item switch
            {
                Tuple<float, float> tuple2 => tuple2.Item2 < 0.1f,
                System.Numerics.Vector2 v2 => v2.Y < 0.1f,
                Tuple<float, float, float> tuple3 => tuple3.Item2 < 0.1f,
                System.Numerics.Vector3 v3 => v3.Y < 0.1f,
                _ => false
            });
        }
        
        /// <summary>
        /// <see cref="ConvertYValuesToMillimeters"/> is a helper method intended to be used with
        /// <see cref="AllYValuesInMeters"/>. When used with <see cref="AllYValuesInMeters"/>, it creates a good
        /// check to convert the values from meter to mm.
        /// </summary>
        /// <param name="dataPoints1"></param>
        /// <returns></returns>
        internal static ICollection<T> ConvertYValuesToMillimeters(ICollection<T> dataPoints1)
        {
            const int MILLIMETERS = 1000;
            return dataPoints1.Select(item => item switch
            {
                Tuple<float, float> tuple2 => (T)(object)Tuple.Create(tuple2.Item1, tuple2.Item2 * MILLIMETERS),
                System.Numerics.Vector2 v2 => (T)(object)new System.Numerics.Vector2(v2.X, v2.Y * MILLIMETERS),
                Tuple<float, float, float> tuple3 => (T)(object)Tuple.Create(tuple3.Item1, tuple3.Item2 * MILLIMETERS, tuple3.Item3),
                System.Numerics.Vector3 v3 => (T)(object)new System.Numerics.Vector3(v3.X, v3.Y * MILLIMETERS, v3.Z),
                _ => item
            }).ToList();
        }

        /// <summary>
        /// Adds an item to the data points collection based on its type.
        /// </summary>
        /// <param name="item">The item to add</param>
        /// <exception cref="InvalidOperationException"></exception>
        private void AddItemToDataPoints(object item)
        {
            try
            {
                switch (item)
                {
                    //user passed in a tuple2, but our type T was a System.Numerics.Vector2
                    case Tuple<float, float> tuple2 when typeof(T) == typeof(System.Numerics.Vector2):
                        dataPoints.Add((T)(object)new System.Numerics.Vector2(tuple2.Item1, tuple2.Item2));
                        break;
                    //user passed in a tuple2 but our Type T was a System.Numerics.Vector3
                    case Tuple<float, float> tuple2 when typeof(T) == typeof(System.Numerics.Vector3):
                        dataPoints.Add((T)(object)new System.Numerics.Vector3(tuple2.Item1, tuple2.Item2, 0.0f));
                        break;
                    //user passed in a tuple2 but our Type T was a Tuple3
                    case Tuple<float, float> tuple2 when typeof(T) == typeof(Tuple<float, float, float>):
                        dataPoints.Add((T)(object)Tuple.Create(tuple2.Item1, tuple2.Item2, 0.0f));
                        break;

                    //user passed in a System.Numerics.Vector2, but our type T was a tuple2
                    case System.Numerics.Vector2 v2 when typeof(T) == typeof(Tuple<float, float>):
                        dataPoints.Add((T)(object)Tuple.Create(v2.X, v2.Y));
                        break;
                    //user passed in a System.Numerics.Vector2, but our type T was a tuple3
                    case System.Numerics.Vector2 v2 when typeof(T) == typeof(Tuple<float, float, float>):
                        dataPoints.Add((T)(object)Tuple.Create(v2.X, v2.Y, 0.0f));
                        break;
                    //user passed in a System.Numerics.Vector2, but our type T was a System.Numerics.Vector3
                    case System.Numerics.Vector2 v2 when typeof(T) == typeof(System.Numerics.Vector3):
                        dataPoints.Add((T)(object)new System.Numerics.Vector3(v2.X, v2.Y, 0.0f));
                        break;
                    default:
                        throw new
                            InvalidOperationException($"{nameof(AddItemToDataPoints)}: No logic to convert item of type {item.GetType()} to {typeof(T)}.");
                }
            }
            catch (Exception ex) when (ex is InvalidCastException)
            {
                Debug
                    .LogWarning($"{nameof(AddItemToDataPoints)}: Error casting item for type {typeof(T)}: {ex.Message}");
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"{nameof(AddItemToDataPoints)}: {ex.Message}");
                //throw;
            }
        }

        /// <summary>
        /// <see cref="StartXFromZero"/> cleans and translates the data to the x-origin based on X/Item1 values (time).
        /// Sorts the collection by X values and adjusts X values by subtracting the first X value.
        /// This is done because the time is calculated using <see cref="Time.time"/> in Unity, which may be useful for
        /// other applications, but for us as graph drawers, we want it to look like the time always started from 0.
        /// This translation will not affect the meaning of the original data, because we are just 'shifting' the value
        /// and not really changing the meaning behind it.
        /// </summary>
        /// <param name="container"> The container where the Translation is to be performed</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="NotSupportedException"></exception>
        /// <exception cref="Exception">General unforeseen exceptions.</exception>
        public static void StartXFromZero(ref ICollection<T> container)
        {
            if (container == null || container.Count == 0)
            {
                throw new ArgumentNullException(nameof(container),
                                                $"Container cannot be null or empty Count:{container?.Count}.");
            }

            //sort for safety since time is a continuous value, it should also look like it's continuous in the graph.
            //cant use our GetDataPointsSortedBy since it makes sense to leave this method static
            List<T> pointsList = container.ToList();
            
            //if the first elem is 0,0 then skip it , else don't skip the first item
            List<T> sortedPoints = IsZeroPoint(pointsList[0]) 
                ? pointsList.Skip(1).Where(point => point != null).OrderBy(GetTime).ToList()
                : pointsList.Where(point => point != null).OrderBy(GetTime).ToList();
            
            if (container.Count != sortedPoints.Count)
            {
                Debug.LogWarning($"{nameof(StartXFromZero)}: {container.Count - sortedPoints.Count} point(s) were removed.");
            }

            float originTime = GetTime(sortedPoints.First());
            
            //clear the current contents from the container so we can refresh the contents
            container.Clear();
            foreach (T point in sortedPoints)
            {
                container.Add(TranslatePoint(point, originTime));
            }
        }
        
        private static bool IsZeroPoint(T point)
        {
            return point switch
            {
                Tuple<float,float> t2 => t2.Item1 is 0 && t2.Item2 is 0,
                Tuple<float,float,float> t3 => t3.Item1 is 0 && t3.Item2 is 0 && t3.Item3 is 0,
                System.Numerics.Vector2 vec => vec.X is 0 && vec.Y is 0,
                System.Numerics.Vector3 vec => vec.X is 0 && vec.Y is 0 && vec.Z is 0,
                _ => false
            };
        }


        /// <summary>
        /// Helper for <see cref="StartXFromZero"/>.
        /// Translates a point to the left using <paramref name="originTime"/>
        /// </summary>
        /// <param name="point"></param>
        /// <param name="originTime"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        private static T TranslatePoint(T point, float originTime)
        {
            return point switch
            {
                System.Numerics.Vector2 v2 => (T)(object)new System.Numerics.Vector2(v2.X - originTime, v2.Y),
                System.Numerics.Vector3 v3 => (T)(object)new System.Numerics.Vector3(v3.X - originTime, v3.Y, v3.Z),
                Tuple<float, float> t2 => (T)(object)Tuple.Create(t2.Item1 - originTime, t2.Item2),
                Tuple<float, float, float> t3 => (T)(object)Tuple.Create(t3.Item1 - originTime, t3.Item2, t3.Item3),
                _ => throw new
                    NotSupportedException($"{nameof(StartXFromZero)}: Currently unsupported type: {typeof(T)}."),
            };
        }

#endregion

        /// <summary>
        /// Adds a data point to the collection
        /// </summary>
        /// <param name="point">The point to add</param>
        public void AddDataPoint(T point)
        {
            if (point == null)
            {
                Debug
                    .LogWarning($"{nameof(AddDataPoint)}: Attempted to add null point to {nameof(dataPoints)}");
                return;
            }

            dataPoints.Add(point);
        }

        /// <summary>
        /// Gets a copy of <see cref="dataPoints"/> as a read-only collection
        /// </summary>
        public IReadOnlyCollection<T> GetDataPointsAsReadOnly()
        {
            _ = IsDataValid(throwOnException: true);
            return dataPoints.ToList().AsReadOnly();
        }
        
        /// <summary>
        /// Gets a copy of the <see cref="dataPoints"/> collection
        /// </summary>
        /// <returns></returns>
        public ICollection<T> GetDataPointsCopy()
        {
            _ = IsDataValid(throwOnException: true);
            return dataPoints.ToList();
        }


        /// <summary>
        /// Gets the minimum and maximum values for the specified dimension using a value selector
        /// </summary>
        /// <param name="valueSelector">Function to select the value for the specified dimension</param>
        public Tuple<float, float> GetMinMax(Func<T, float> valueSelector)
        {
            _ = IsDataValid(throwOnException: true);
            return Tuple.Create(dataPoints.Min(valueSelector), dataPoints.Max(valueSelector));
        }

        /// <summary>
        /// Make sure <see cref="dataPoints"/> is not <see langword="null"/> or the count of <see cref="dataPoints"/> is not <see langword="0"/>.
        /// When <paramref name="throwOnException"/> is set to <see langword="true"/>, the program will throw a
        /// <see cref="ArgumentNullException"/> if <see cref="dataPoints"/> is <see langword="null"/>
        /// or count of <see cref="dataPoints"/> is <see langword="0"/>.
        /// </summary>
        /// <param name="throwOnException"> specifies whether the application should throw an exception or silently continue.</param>
        /// <exception cref="ArgumentNullException"></exception>
        private bool IsDataValid(bool throwOnException = false)
        {
            if (dataPoints != null && dataPoints.Count != 0)
            {
                return true; //the data was valid, return true to indicate that the data was valid
            }

            //if we manage to reach here, then data was possibly invalid
            if (!throwOnException)
            {
                Debug
                    .LogWarning($"{nameof(IsDataValid)}: Property {nameof(dataPoints)} was null/empty when it " +
                                $"was probably not intended to be. Count: {dataPoints?.Count}");
                return false;
            }
            else
            {
                throw new
                    ArgumentNullException($"{nameof(IsDataValid)}: Property {nameof(dataPoints)} was null/empty when it " +
                                          $"was probably not intended to be. Count: {dataPoints?.Count}",
                                          nameof(dataPoints));
            }
        }

        /// <summary>
        /// Takes in a method to perform the sorting operation and returns a copy of <see cref="dataPoints"/> as a <see cref="IReadOnlyCollection{T}"/>.
        /// This will make it easier to find/remove duplicates.
        /// </summary>
        /// <param name="sortSelector"></param>
        /// <returns></returns>
        public IReadOnlyCollection<T> GetDataPointsSortedByAsReadOnly(Func<T, float> sortSelector)
        {
            return dataPoints.OrderBy(sortSelector).ToList().AsReadOnly();
        }

        /// <summary>
        /// Takes in a method to perform the sorting operation and returns a copy of <see cref="dataPoints"/> as a <see cref="ICollection{T}"/>.
        /// This will make it easier to find/remove duplicates and give more freedom in terms of mutability since we are returning a copy of
        /// the actual <see cref="dataPoints"/> and not the actual <see cref="dataPoints"/> itself.
        /// </summary>
        /// <param name="sortSelector"></param>
        /// <returns></returns>
        public ICollection<T> GetDataPointsSortedBy(Func<T, float> sortSelector)
        {
            return dataPoints.OrderBy(sortSelector).ToList();
        }

#region VelocityComponents

        /// <summary>
        /// Calculates the velocity components (changes in depth and time) between successive points
        /// Returns a List of Tuples where:
        /// Item1 = change in time  (Δx)
        /// Item2 = change in depth (Δy)
        /// </summary>
        /// <returns>List of velocity components between successive points</returns>
        /// <exception cref="NotSupportedException"></exception>
        /// <exception cref="InvalidCastException"></exception>
        public ICollection<T> CalculateVelocityComponents()
        {
            List<T> velocityComponents = new();
            try
            {
                _ = IsDataValid(throwOnException: true);

                if (dataPoints.Count < 2)
                {
                    return velocityComponents;
                }

                List<T> points = dataPoints.ToList();
                for (int i = 0; i < points.Count - 1; i++)
                {
                    float deltaDepth;
                    float deltaTime;

                    //firstly calculate the deltas
                    switch (points[i])
                    {
                        //using 'when' here offers better type safety 
                        case System.Numerics.Vector2 v2Current when points[i + 1] is System.Numerics.Vector2 v2Next:
                            deltaDepth = v2Next.Y - v2Current.Y;
                            deltaTime = v2Next.X - v2Current.X;
                            velocityComponents.Add((T)(object)new System.Numerics.Vector2(deltaTime, deltaDepth));
                            break;

                        case System.Numerics.Vector3 v3Current when points[i + 1] is System.Numerics.Vector3 v3Next:
                            deltaDepth = v3Next.Y - v3Current.Y;
                            deltaTime = v3Next.X - v3Current.X;
                            velocityComponents.Add((T)(object)new System.Numerics.Vector3(deltaTime, deltaDepth, 0.0f));
                            break;

                        case Tuple<float, float> t2Current when points[i + 1] is Tuple<float, float> t2Next:
                            deltaDepth = t2Next.Item2 - t2Current.Item2;
                            deltaTime = t2Next.Item1 - t2Current.Item1;
                            velocityComponents.Add((T)(object)Tuple.Create(deltaTime, deltaDepth));
                            break;

                        case Tuple<float, float, float> t3Current
                            when points[i + 1] is Tuple<float, float, float> t3Next:
                            deltaDepth = t3Next.Item2 - t3Current.Item2;
                            deltaTime = t3Next.Item1 - t3Current.Item1;
                            velocityComponents.Add((T)(object)Tuple.Create(deltaTime, deltaDepth, 0.0f));
                            break;

                        default:
                            throw new
                                NotSupportedException($"{nameof(CalculateVelocityComponents)}: Type {typeof(T)} is not supported for velocity calculations currently.");
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.LogError($"{nameof(CalculateVelocityComponents)}: {ex.Message}");
                throw;
            }

            return velocityComponents;
        }

        /// <summary>
        /// Get the time from the current <typeparamref name="T"/> instance.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        internal static float GetTime(T point)
        {
            return point switch
            {
                System.Numerics.Vector2 v2 => v2.X,
                System.Numerics.Vector3 v3 => v3.X,
                Tuple<float, float> t2 => t2.Item1,
                Tuple<float, float, float> t3 => t3.Item1,
                _ => throw new
                    NotSupportedException($"{nameof(GetTime)}: Type {typeof(T)} is not supported for time extraction currently.")
            };
        }

        /// <summary>
        /// Get the depth from the current <typeparamref name="T"/> instance.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        internal static float GetDepth(T point)
        {
            return point switch
            {
                System.Numerics.Vector2 v2 => v2.Y,
                System.Numerics.Vector3 v3 => v3.Y,
                Tuple<float, float> t2 => t2.Item2,
                Tuple<float, float, float> t3 => t3.Item2,
                _ => throw new
                    NotSupportedException($"{nameof(GetDepth)}: Type {typeof(T)} is not supported for depth extraction currently.")
            };
        }

#endregion

        /// <summary>
        /// Returns a detailed string representation of all data points <see cref="DataPointsHelper"/> for more info.
        /// </summary>
        /// <returns>A formatted string containing all data points and collection information</returns>
        public override string ToString()
        {
            return DataPointsHelper.ToString(dataPoints);
        }
    }


#region ExtensionHelperMethodsForDataPoints

    /// <summary>
    /// Helper class for extension methods for <see cref="DataPoints{T}"/>.
    /// A Helper class is needed to extend the functionality of the <see cref="DataPoints{T}"/> class because of the 
    /// nature of the class being type instantiated which cannot hold multiple types at a same time.
    /// </summary>
    public static class DataPointsExtensions
    {
#region ReturnTime

        /// <summary>
        /// Returns the min and max value of a particular value, time, from a Collection of properties in <see cref="DataPoints{T}"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataPoints"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        public static Tuple<float, float> GetMinMaxX<T>(this DataPoints<T> dataPoints)
        {
            if (dataPoints == null)
            {
                Debug
                    .LogWarning($"{nameof(GetMinMaxX)}: {nameof(dataPoints)} was null, returning " +
                                $"pair of -1.0f as default value");
                return Tuple.Create(-1.0f, -1.0f);
            }

            try
            {
                return dataPoints switch
                {
                    DataPoints<System.Numerics.Vector2> =>
                        dataPoints.GetMinMax(p => p is System.Numerics.Vector2 vec
                                                 ? vec.X
                                                 : throw new
                                                     InvalidCastException($"{nameof(GetMinMaxX)}:Invalid type in collection.")),
                    DataPoints<System.Numerics.Vector3> =>
                        dataPoints.GetMinMax(p => p is System.Numerics.Vector3 vec
                                                 ? vec.X
                                                 : throw new
                                                     InvalidCastException($"{nameof(GetMinMaxX)}:Invalid type in collection.")),
                    DataPoints<Tuple<float, float>> =>
                        dataPoints.GetMinMax(p => p is Tuple<float, float> tuple
                                                 ? tuple.Item1
                                                 : throw new
                                                     InvalidCastException($"{nameof(GetMinMaxX)}:Invalid type in collection.")),
                    DataPoints<Tuple<float, float, float>> =>
                        dataPoints.GetMinMax(p => p is Tuple<float, float, float> tuple
                                                 ? tuple.Item1
                                                 : throw new
                                                     InvalidCastException($"{nameof(GetMinMaxX)}:Invalid type in collection.")),
                    _ => throw new NotSupportedException($"{nameof(GetMinMaxX)}: Type {typeof(T)} is not supported.")
                };
            }
            catch (Exception ex) when (ex is InvalidCastException)
            {
                Debug.LogError($"{nameof(GetMinMaxX)}: Invalid Cast: {ex.Message}");
                //throw; 
            }
            catch (Exception ex)
            {
                Debug.LogError($"{nameof(GetMinMaxX)}: {ex.Message}");
                //throw;
            }

            return Tuple.Create(-1.0f, -1.0f);
        }

#endregion

#region ReturnDepth

        /// <summary>
        /// Returns the min and max value of a particular value, depth, from a Collection of properties in <see cref="DataPoints{T}"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataPoints"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException"></exception>
        public static Tuple<float, float> GetMinMaxY<T>(this DataPoints<T> dataPoints)
        {
            try
            {
                return dataPoints switch
                {
                    DataPoints<System.Numerics.Vector2> =>
                        dataPoints.GetMinMax(p => p is System.Numerics.Vector2 vec
                                                 ? vec.Y
                                                 : throw new
                                                     InvalidCastException($"{nameof(GetMinMaxY)}: Invalid type in collection.")),
                    DataPoints<System.Numerics.Vector3> =>
                        dataPoints.GetMinMax(p => p is System.Numerics.Vector3 vec
                                                 ? vec.Y
                                                 : throw new
                                                     InvalidCastException($"{nameof(GetMinMaxY)}: Invalid type in collection.")),
                    DataPoints<Tuple<float, float>> =>
                        dataPoints.GetMinMax(p => p is Tuple<float, float> tuple
                                                 ? tuple.Item2
                                                 : throw new
                                                     InvalidCastException($"{nameof(GetMinMaxY)}: Invalid type in collection.")),
                    DataPoints<Tuple<float, float, float>> =>
                        dataPoints.GetMinMax(p => p is Tuple<float, float, float> tuple
                                                 ? tuple.Item2
                                                 : throw new
                                                     InvalidCastException($"{nameof(GetMinMaxY)}: Invalid type in collection.")),
                    _ => throw new NotSupportedException($"{nameof(GetMinMaxY)}: Type {typeof(T)} is not supported.")
                };
            }
            catch (Exception ex)
            {
                Debug.LogError($"{nameof(GetMinMaxY)}: {ex.Message}");
                //throw;
            }

            return Tuple.Create(-1.0f, -1.0f);
        }

#endregion
    }

#endregion
}
