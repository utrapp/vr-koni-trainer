# Usability tasks

## Task 1 - Register

**Research question**
- Wo terten Probleme beim Registrierungsprozess auf?

**Scenario**
- Stellen Sie sich vor, Sie sind angehende Fachärztin und wollen sich auf die Konisation vorbereiten. Sie nutzen die Anmeldung zum ersten Mal, wie gehen Sie vor?

**Task**
- Sie haben noch kein Login. Melden Sie sich an.

**Inputs**
- Eingabe per Leertaste
- Sind die geforderten Eingaben für Sie klar?
- Nun würden wir Sie bitten sich wieder auszuloggen



## Task 2 - Gast Login

**Research question**
- Wo treten Probleme beim Gast Login auf?

**Scenario**
- Stellen Sie sich vor, Sie wollen die Anwendung kurz testen ohne sich einen Account anzulegen, wie gehen Sie vor?

**Task**
- Benutzen Sie die Anwendung ohne sich einen Account anzulegen.

**Inputs**
- Ist der Gast-Login Button ihrer Meinung nach gut zu finden? Sollte er sich eventuel mehr abheben?
- Bitte wieder ausloggen



## Task 3 - Login

**Research question**
- Wo treten Probleme beim Login auf?

**Scenario**
- Stellen Sie sich vor, Sie sind angehende Fachärztin und wollen sich auf die Konisation vorbereiten. Sie wollen sich jetzt mit Ihrem vorher erstellten Account anmelden.

**Task**
- Loggen Sie sich mit Ihrem erstellten Account ein.

**Inputs**
- Kein eintippen möglich in diesem Prototypen
- Finden Sie der Login Bereich ist gut strukturiert? Bzw. übersichtlich?
- Sind auch hier auf dem Bildschirm alle geforderten Eingaben klar?




## Task 4 - Guide

**Research question**
- Treten Probleme beim ereichen des Guides auf?

**Scenario**
- Sie sind angehende Fachärztin und möchten sich auf eine Konisation vorbereiten. Sie benutzen das Programm zum ersten mal und wissen nicht wie die Steuerung funktioniert. Was machen Sie? 

**Task**
- Starten Sie die Anmeldung und versuchen Sie Informationen zur Steuerung zu finden.

**Inputs**
- Ist die Anleitung der Steurung für Sie gut verständlich?
- Wünschen Sie sich ggf. weitere Erklärungen?




## Task 5 - Training Starten

**Research question**
- Treten Probleme bei dem Versuch ein Training zu starten auf?

**Scenario**
- Sie sind angehende Fachärztin und möchten sich auf eine Konisation vorbereiten. Sie wollen nun ihr erstes Training starten. Wie gehen Sie hierbei vor?

**Task**
- Starten Sie ein Training

**Inputs**
- Wurde Ihnen klar, dass Sie jetzt in der VR Welt sind?
- Training können wir hier nicht simulieren, aber mit einem weiteren Klick auf den bildschirm tun wir so als hätten Sie die OP abgeschlossen




## Task 6 - Statistik ansehen

**Research question**
- Gibt es Probleme beim Aufrufen der Statistiken

**Scenario**
- Sie sind angehende Fachärztin und wollen sich nun alle verfügbaren Ergebnisse ihres letzten Versuches ansehen.

**Task**
- Schauen Sie sich die Ergebnisse des letzten Versuches an

**Inputs**
- Sind alle dargestellten Werte für Sie klar?
- Wie finden Sie die Navigation zwischen den Bildschirmen?
- Fehlen Werte?
- Sind die angegebenen Werte für Sie angenehm dargestellt?
- Bitte kehren Sie zurück zum Home-Screen




## Task 7 - Zwei Versuche hintereinander

**Research question**
- Treten Probleme auf, wenn der nutzer einen zweiten Versuch direkt nach dem ersten machen will?

**Scenario**
- Sie sind angehende Fachärztin und möchten sich auf eine Konisation vorbereiten. Sie wollen weiter Trainieren und dieses mal direkt mehrere Versuche hintereinander machen.

**Task**
- Starten Sie nach Beenden des ersten Trainings direkt einen zweiten Versuch.

**Inputs**
- Ist der Repeat-Button für Sie gut zu finden?
- Bitte wieder in das Menü zurückkehren




## Task 8 - Ältere Statistiken

**Research question**
- Treten Probleme auf, wenn der Nutzer sich ältere Statistiken ansehen will?

**Scenario**
- Sie sind angehende Fachärztin und möchten sich gerne einen Überblick über ihre allgePerformance d letzten Versuche machen.

**Task**
- Schauen Sie sich die Statistiken älterer Versuche an und bringen sie die Entwicklung ihres LLETZ-Score in Erfahrung.

**Inputs**
- Sind die gegebenen Daten in der Tabelle 'All Statistics' für sie passend?
- Wie würden Sie vorgehen, wenn Sie sich einen Versuch aus der Tabelle genauer ansehen wollen?
- Wie würden Sie vorgehen, wenn Sie sich den ausgeschnittenen Bereich genauer ansehen würden?
- Was halten Sie von der Aufteilung der Statistik in (Last Attempt, All Statistics und LLETZ-Chart)
- Fänden Sie neben der allgemeinen Entwicklung des LEETZ-Score noch andere Statistiken interessant, die über die Zeit gemessen werden? (Wie z.B die Dauer einer OP)
- Loggen Sie sich bitte aus



Admin: 

## Task 9 - Administrator Login und Datenexport

**Research question**
- Gibt es Probleme beim exportieren der Daten?

**Scenario**
- Sie sind Oberarzt und wollen sich nun die Ergebnisse und Leistungen der angehenden Ärzte ansehen und diese Daten als Excel Datei exportieren

**Task**
- Exportieren Sie alle Ergebnisdaten der angehenden Ärzte

**Inputs**
- Bedenken Sie dass die Funktion nur für Administratoren zu Verfügung steht.
- War die Funktion für Sie gut zu finden?
- Würden Sie sich noch andere Admin Funktionen wünschen?




## Task 10 - Programm beenden

**Research question**
- Gibt es Probleme beim Beendes des Programmes?

**Scenario**
- Sie haben nun alles erledigt und wollen das komplette Programm beenden. Wie gehen Sie dabei vor

**Task**
- Beenden Sie das Programm

**Inputs**
- War die Formulierung Close Simulator für sie klar?
- Hat der Button das gemacht, was sie erwartet haben?




 
