# A real conization vs the KoniTrainer simulation

- The Simulation only covers the excision of tissue using a loop electrode.

- Previous or later diagnostics are not part of the training simulation.

- To make results of the training easier to compare a LLETZ-Score is used. For further information consult the corresponding Guide section.

- The operation area is viewed through a static lense, rather than a colposcope.

