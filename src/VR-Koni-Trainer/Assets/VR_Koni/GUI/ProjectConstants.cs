using UnityEngine.Localization.Tables;

namespace VR_Koni.GUI
{
    /// <summary>
    /// This class is meant to hold all the project constants that will most likely not change
    /// </summary>
    /// <remarks>
    /// <para>
    /// Stick to using <see langword="static"/> <see langword="readonly"/> for all class fields since this offers
    /// greater flexibility than using <see langword="const"/>.
    /// </para>
    /// <para>
    /// Currently, mainly focusing on the GUI aspect, but if constants are required for
    /// other parts of the application, then the directory level of this file must be adjusted
    /// to the main directory.
    /// </para>
    /// </remarks>
    public static class ProjectConstants
    {
        public static readonly TableReference UiToolKitLocalizationTableName = "UIToolkitStringTableCollection";
    }
}
