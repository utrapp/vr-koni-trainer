
# References

### Medical References

[1] https://www.krebsinformationsdienst.de/tumorarten/gebaermutterhalskrebs/vorstufen.php (zuletzt abgerufen am 17.01.2024)

[2] Takacs FZ, Gerlinger C, Hamza A, et al. A standardized simulation training program to type 1 loop electrosurgical excision of the transformation zone: a prospective observational study. Arch Gynecol Obstet. 2020;301(2):611–618. doi: 10.1007/s00404-019-05416-1

[3] Takacs FZ, Radosa JC, Gerlinger C, et al. Introduction of a learning model for type 1 loop excision of the transformation zone of the uterine cervix in undergraduate medical students: a prospective cohort study. Arch Gynecol Obstet. 2019;299(3):817–824. doi: 10.1007/s00404-018-5019-7


### Valve Index setup and Customization

https://help.steampowered.com/en/faqs/view/699A-ECD2-F839-760C