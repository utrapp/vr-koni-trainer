using UnityEngine.Localization;
using UnityEngine.UIElements;
using VR_Koni.UserData;
using VR_Koni.UserData.Export;
using VR_Koni.GUI.UI_Toolkit.Popup;

namespace VR_Koni.GUI.UI_Toolkit.MainMenu.Views
{
    public class MainMenuAdminView : IMainMenuView
    {
        public LocalizedString ViewName => new LocalizedString
        {
            TableReference = "UIToolkitStringTableCollection",
            TableEntryReference = "MainMenuAdminViewName"
        };

        public VisualElement RootElement { get; private set; }
        
        private Button exportStatisticsButton;
        private Toggle adminConfirmToggle;
        private LocalizedString popupHeaderLocalized;
        private LocalizedString popupDescriptionLocalized;
        private LocalizedString popupLeftButtonText;
        private LocalizedString popupRightButtonText;

        public void SetupVisualElements(VisualElement rootElement)
        {
            RootElement = rootElement;

            exportStatisticsButton = rootElement.Q<Button>("ExportStatisticsButton");
            adminConfirmToggle = rootElement.Q<Toggle>("AdminConfirmToggle");

            popupHeaderLocalized = new LocalizedString
            {
                TableReference = "UIToolkitStringTableCollection",
                TableEntryReference = "MainMenuAdminViewExportPopupHeader"
            };
            popupDescriptionLocalized = new LocalizedString
            {
                TableReference = "UIToolkitStringTableCollection",
                TableEntryReference = "MainMenuAdminViewExportPopupWarning"
            };
            popupLeftButtonText = new LocalizedString
            {
                TableReference = "UIToolkitStringTableCollection",
                TableEntryReference = "MainMenuAdminViewExportPopupLeftButtonText"
            };
            popupRightButtonText = new LocalizedString
            {
                TableReference = "UIToolkitStringTableCollection",
                TableEntryReference = "MainMenuAdminViewExportPopupRightButtonText"
            };
        }

        public void SubscribeToEvents()
        {
            exportStatisticsButton.clicked += ShowConfirmationPopup;
        }

        public void UnsubscribeFromEvents()
        {
            exportStatisticsButton.clicked -= ShowConfirmationPopup;
        }

        public void ResetViewSelections() 
        {
        }
        
        private void ShowConfirmationPopup()
        {
            PopupScreenManager.Show(
                                    headerLocalized: popupHeaderLocalized,
                                    descriptionLocalized: popupDescriptionLocalized,
                                    leftButtonTextLocalized: popupLeftButtonText,
                                    actionLeft: () => {},
                                    rightButtonTextLocalized: popupRightButtonText,
                                    actionRight: OnConfirmExport,
                                    headerFontSize:40,
                                    descriptionFontSize:22
                                   );
        }

        private void OnConfirmExport()
        {
            DataManager.Instance.ExportAllStats();
            ExportExcel.ProcessAndExportDataAsync();
        }
    }
}
