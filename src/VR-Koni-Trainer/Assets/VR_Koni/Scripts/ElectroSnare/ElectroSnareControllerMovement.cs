/*
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

namespace VR_Koni.ElectroSnare
{
    public class ElectroSnareControllerMovement : MonoBehaviour
    {
        [SerializeField] private Interactable interactable;
        [SerializeField] private Transform snareBase;
        [SerializeField] private SteamVR_Action_Vector2 controllerMovementInput;

        [SerializeField] private float tiltScale;
        [SerializeField] private float rollScale;

        private Vector2 currentInputOffset;

        private void Reset()
        {
            interactable = GetComponent<Interactable>();
            controllerMovementInput = SteamVR_Input.GetAction<SteamVR_Action_Vector2>("elektroschlinge", "SnareMove");
        }

        private void Start()
        {
            interactable.onDetachedFromHand += OnDetach;
        }

        private void OnDestroy()
        {
            interactable.onDetachedFromHand -= OnDetach;
        }

        private void OnDetach(Hand _)
        {
            currentInputOffset = Vector2.zero;
            snareBase.localRotation = Quaternion.identity;
        }

        private void Update()
        {
            if (!SteamVR.active || !SteamVR.enabled)
                return;

            if (!interactable.attachedToHand)
                return;

            SteamVR_Input_Sources handType = interactable.attachedToHand.handType;
            Vector2 movement = controllerMovementInput.GetAxis(handType);

            if (Mathf.Abs(movement.x) > 0.4)
                currentInputOffset.x += movement.x * rollScale * Time.deltaTime;
            if (Mathf.Abs(movement.y) > 0.4)
                currentInputOffset.y += movement.y * tiltScale * Time.deltaTime;

            snareBase.localRotation = Quaternion.AngleAxis(currentInputOffset.y, Vector3.right) *
                                      Quaternion.AngleAxis(currentInputOffset.x, Vector3.forward);
        }
    }
}
*/
