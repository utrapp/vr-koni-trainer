using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

namespace VR_Koni.GUI.UI_Toolkit.Components.Charts.ChartExamples

    // demonstrate how to use the LineChart component in the scene playground/componenttest
    // check/uncheck the component to be tested on the right hand side of the uidocument game object

{
    /// <summary>
    /// Simple script to be attached to a GameObject with a UIDocument component to add lines to a LineChart.
    /// </summary>
    [RequireComponent(typeof(UIDocument))]
    public class LineChartExample : MonoBehaviour
    {
        private LineChart lineChart;

        private void Start()
        {
            VisualElement root = GetComponent<UIDocument>().rootVisualElement;
            lineChart = root.Q<LineChart>("ExampleLineChart");
            AddLines();
        }

        private void AddLines()
        {
            List<Vector2> points1 = new();
            for (int x = 0; x <= 4; x++)
            {
                points1.Add(new Vector2(x, x));
            }

            lineChart.AddData(points1, "y=x", Color.red);
            List<Vector2> points2 = new();
            for (int x = 0; x <= 10; x++)
            {
                points2.Add(new Vector2(x, x + 1));
            }

            lineChart.AddData(points2, "y=(x-2)*(x+3)", Color.blue);
        }
    }
}
