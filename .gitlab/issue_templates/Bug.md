# 🐞 Bug Report

_Summarize the bug encountered concisely._

## Steps to reproduce

_Describe how one can reproduce the issue - this is very important. Please use an ordered list._ 

## Current _faulty_ behavior

_Describe what actually happens._

## Expected _correct_ behavior

_Describe what you should see instead._

## Logs/Screenshots

_Paste any relevant logs - please use code blocks (```) to format console output, logs, and code
as it's tough to read otherwise._

## Ideas for solutions

_If you can, link to the line of code that might be responsible for the problem. Any hints, how this could be solved are welcome_

/label ~bug