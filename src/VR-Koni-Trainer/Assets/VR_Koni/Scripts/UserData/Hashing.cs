using System;
using System.Security.Cryptography;
using System.Text;

namespace VR_Koni.UserData
{
    /// <summary>
    /// Class <c>Hashing</c> provides a function to generate SHA256 values.
    /// </summary>
    [Serializable]
    public static class Hashing
    {
        /// <summary>
        /// Returns hashed SHA256 value of a provided string.
        /// </summary>
        /// <param name="s">String which needs to be hashed.</param>
        public static string ToSHA256(string s) {
            using SHA256 sha256 = SHA256.Create();
            byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(s));
            StringBuilder sb = new();

            foreach (byte b in bytes)
            {
                sb.Append(b.ToString("x2"));
            }
            return sb.ToString();
        }
    }
}
