# Input Mapping for Valve Index Controller

To make it easier for the user to remove controller and VR-Headset, only the right controller is used.  

| Valve Index Controller | Input               | Menu              | during Simulation         |
|:-----------------------|:--------------------|:------------------|:--------------------------|
| Trigger                | Press               | select            | Activate wire loop        |
| Button B               | Press               | (free)            | Activate wire loop        |
| Button A               | Press               | (free)            | (free)                    |
| Thumbstick             | Horizontal Movement | (free)            | Tilt wire loop            |
| Thumbstick             | Vertical Movement   | (free)            | Rotate wire loop          |
| Thumbstick             | Press               | (free)            | Open pause menu           |
| Grip                   | Hold                | (free)            | Grip or release an object |
| ESC (Keyboard)         |                     |                   | Open pause menu           |

1. User is able to hold and inspect the cut out 3D Mesh after the operation. 
2. User can open the Pause Menu by pressing down the Thumbstick on the controller or pressing ESC key on keyboard. In the Pause Menu, user is able to reset position, adjust volume or return to 3D menu.
3. After conducting the operation, the cut out tissue will be shown rotating on the statistic menu. The user is able to grab and observe the tissue with the controller.
4. The program will automatically return to the main menu on the PC screen within 10 seconds after the headset is removed.

