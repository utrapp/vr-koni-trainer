using UnityEngine.Localization;
using UnityEngine.UIElements;

namespace VR_Koni.GUI.UI_Toolkit.MainMenu.Views
{
    public class MainMenuTutorialsView : IMainMenuView
    {
        public LocalizedString ViewName => new LocalizedString { TableReference =  ProjectConstants.UiToolKitLocalizationTableName, TableEntryReference = "MainMenuTutorialViewName" };
        public VisualElement RootElement { get; private set; }
        private Button openTutorialsScreenButton;

        public void SetupVisualElements(VisualElement rootElement)
        {
            RootElement = rootElement;

            openTutorialsScreenButton = rootElement.Q<Button>("OpenTutorialsScreenButton");
        }

        public void SubscribeToEvents() => openTutorialsScreenButton.clicked += ApplicationManager.SwitchToTutorials;

        public void UnsubscribeFromEvents() => openTutorialsScreenButton.clicked -= ApplicationManager.SwitchToTutorials;

        public void ResetViewSelections() {}
    }
}
