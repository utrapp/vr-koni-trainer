using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Properties;
using UnityEngine;
using UnityEngine.UIElements;
using Color = UnityEngine.Color;

namespace VR_Koni.GUI.UI_Toolkit.Components.Charts
{
    [UxmlElement]
    public partial class LineChart : Chart
    {
        /// <summary>
        ///     Default constructor -- is also called for preview in the unity editor.
        /// </summary>
        public LineChart()
        {
            RegisterCallback<CustomStyleResolvedEvent>(OnStylesResolved); // handles the custom styles
            Canvas.generateVisualContent +=
                DrawLineChart; // handles the drawing of the chart, make sure to use the chart control here to get the geometry right
        }


        private void OnStylesResolved(CustomStyleResolvedEvent evt)
        {
            evt.customStyle.TryGetValue(MarkerFontSize, out markerFontSize);
            Debug.Log("StyleChanged in linechart");
        }


        /// <summary>
        ///     Add a list of points (data series) to the line chart with the given points, legend, and color.
        /// </summary>
        /// <param name="points">List of (x, y) points</param>
        /// <param name="legend">e.g. f(x)=x+3</param>
        /// <param name="color">color to be used for the drawing of this function</param>
        public void AddData(List<Vector2> points, string legend, Color color)
        {
            if (points == null || points.Count == 0)
            {
                Debug.LogWarning("Points list is null or empty.");
                return;
            }

            base.AddData(legend, color);
            LineDataSet lineDataSet = new(points, legend, color);
            dataSets.Add(lineDataSet);
        }

        /// <summary>
        ///     Called, whenever the visual content should be created.
        /// </summary>
        /// <param name="context">needed for paint and geometry (rect)</param>
        private void DrawLineChart(MeshGenerationContext context)
        {
            if (dataSets.Count == 0) return;
            Debug.Log("DrawLineChart");
            CalcAxesRanges();
            // The contentRect changes after the binding and somehow the sizes get mixed up, hence the following workaround
            if (Canvas.resolvedStyle != null)
            {
                float width = Canvas.resolvedStyle.width;
                float height = Canvas.resolvedStyle.height;
                int markerSpaceVertical =
                    MARKER_LENGTH + 2 * PADDING_LABEL_AXIS +
                    2 * markerFontSize; // 2 * markerFontSize one for the marker itself, one for the x-axis label 
                int markerSpaceHorizontal = MARKER_LENGTH + 2 * markerFontSize + PADDING_LABEL_AXIS;
                chartGeometry.Height = (int)height - markerSpaceVertical- 2 * markerFontSize; //to have space for the last marker 
                chartGeometry.Width =
                    (int)width - markerSpaceHorizontal - 2 * markerFontSize; //to have space for the last marker 
                (chartGeometry.FirstMarkerX, chartGeometry.ScaleX) =
                    CalcUnits(chartGeometry.RangeX, chartGeometry.Width);
                (chartGeometry.FirstMarkerY, chartGeometry.ScaleY) =
                    CalcUnits(chartGeometry.RangeY, chartGeometry.Height);
                chartGeometry.Origin =
                    new
                        Vector2(-chartGeometry.ScaleX * chartGeometry.RangeX.Min + markerSpaceHorizontal,
                                chartGeometry.ScaleY * chartGeometry.RangeY.Min + chartGeometry.Height + markerFontSize);
            }

            DrawAxes(context);
            RenderDataSets(context);
            if (ShowAxisMarker)
                DrawMarkers(context);
        }

        private void RenderDataSets(MeshGenerationContext context)
        {
            foreach (LineDataSet dataSet in dataSets)
            {
                for (int i = 0; i < dataSet.Points.Count - 1; i++)
                {
                    DrawLine(context, dataSet.Points[i], dataSet.Points[i + 1], dataSet.Color);
                }
            }
        }

        private void DrawMarkers(MeshGenerationContext context)
        {
            for (int x = chartGeometry.FirstMarkerX; x <= chartGeometry.RangeX.Max; x += chartGeometry.FirstMarkerX)
            {
                Vector2 start = MapDataPointToCanvasPoint(new Vector2(x, 0));
                Vector2 end = new(start.x, start.y + MARKER_LENGTH);
                DrawLine(context, start, end, ChartColor, true);
                context.DrawText(x.ToString(), end, markerFontSize, ChartColor);
            }

            // Draw the negative x-axis markers
            for (int x = -chartGeometry.FirstMarkerX; x >= chartGeometry.RangeX.Min; x -= chartGeometry.FirstMarkerX)
            {
                Vector2 start = MapDataPointToCanvasPoint(new Vector2(x, 0));
                Vector2 end = new(start.x, start.y + MARKER_LENGTH);
                DrawLine(context, start, end, ChartColor, true);
                context.DrawText(x.ToString(), end, markerFontSize, ChartColor);
            }

            // Draw the positive y-axis markers
            for (int y = chartGeometry.FirstMarkerY; y <= chartGeometry.RangeY.Max; y += chartGeometry.FirstMarkerY)
            {
                Vector2 start = MapDataPointToCanvasPoint(new Vector2(0, y));
                Vector2 end = new(start.x - MARKER_LENGTH, start.y);
                DrawLine(context, start, end, ChartColor, true);
                context.DrawText(y.ToString(), end - new Vector2(MARKER_LENGTH + markerFontSize, markerFontSize / 2.0f),
                                 markerFontSize, ChartColor);
            }

            // Draw the negative y-axis markers
            for (int y = -chartGeometry.FirstMarkerY; y >= chartGeometry.RangeY.Min; y -= chartGeometry.FirstMarkerY)
            {
                Vector2 start = MapDataPointToCanvasPoint(new Vector2(0, y));
                Vector2 end = new(start.x - MARKER_LENGTH, start.y);
                DrawLine(context, start, end, ChartColor, true);
                context.DrawText(y.ToString(), end - new Vector2(MARKER_LENGTH + markerFontSize, markerFontSize / 2.0f),
                                 markerFontSize, ChartColor);
            }

            context.DrawText(XAxisLabel,
                             MapDataPointToCanvasPoint(new Vector2(chartGeometry.RangeX.Max, 0)) +
                             new Vector2(-XAxisLabel.Length * markerFontSize / 2.0f,
                                         markerFontSize * 2 + PADDING_LABEL_AXIS), markerFontSize,
                             ChartColor);
            context.DrawText(YAxisLabel,
                             MapDataPointToCanvasPoint(new Vector2(0, chartGeometry.RangeY.Max)) +
                             new Vector2(PADDING_LABEL_AXIS, 0), markerFontSize, ChartColor);
        }

        /// <summary>
        ///     Draws the x and y axes in the canvas.
        /// </summary>
        /// <param name="context"></param>
        private void DrawAxes(MeshGenerationContext context)
        {
            DrawLine(context, new Vector2(chartGeometry.RangeX.Min, 0), new Vector2(chartGeometry.RangeX.Max, 0),
                     ChartColor);
            DrawLine(context, new Vector2(0, chartGeometry.RangeY.Min), new Vector2(0, chartGeometry.RangeY.Max),
                     ChartColor);
        }

        /// <summary>
        ///     Map a given chart data point to a canvas point. Take the Origin and scaling into account and
        ///     that y is upside down.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        private Vector2 MapDataPointToCanvasPoint(Vector2 point)
        {
            return new Vector2(chartGeometry.Origin.x + point.x * chartGeometry.ScaleX,
                               chartGeometry.Origin.y - point.y * chartGeometry.ScaleY); //y is upside down
        }

        /// <summary>
        ///     Draws a line in the canvas matching the given points to the canvas.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="start">starting point of the line</param>
        /// <param name="end">end point of the line</param>
        /// <param name="color">line color</param>
        /// <param name="hasCanvasCoords">true: start and end are given in canvas coords, default is false and data
        /// is given as data points</param>
        private void DrawLine(MeshGenerationContext context, Vector2 start, Vector2 end, Color color,
                              bool hasCanvasCoords = false)
        {
            Painter2D painter = context.painter2D;

            if (!hasCanvasCoords)
            {
                start = MapDataPointToCanvasPoint(start);
                end = MapDataPointToCanvasPoint(end);
            }

            painter.strokeColor = color;
            painter.lineWidth = LINE_WIDTH;
            painter.BeginPath();
            painter.MoveTo(start);
            painter.LineTo(end);
            painter.Stroke();
        }


        /// <summary>
        ///     Calculate the number range for x and y axes.
        ///     The min and max values are calculated based on the min and max values of all points in all lines and are
        ///     multiples of AxisValuesMultipleOf, e.g. multiples of 5.
        /// </summary>
        private void CalcAxesRanges()
        {
            if (dataSets.Count == 0) return;
            chartGeometry.RangeX.Min =
                RoundToNextSignificant((int)Math.Round(dataSets.SelectMany(data => data.Points).Min(point => point.x)+0.5f, 0), -1);
            chartGeometry.RangeX.Max =
                RoundToNextSignificant((int)Math.Round(dataSets.SelectMany(data => data.Points).Max(point => point.x)-0.5f, 0),  1);
            chartGeometry.RangeY.Min =
                RoundToNextSignificant((int)Math.Round(dataSets.SelectMany(data => data.Points).Min(point => point.y)-0.5f, 0), -1);
            chartGeometry.RangeY.Max =
                RoundToNextSignificant((int)Math.Round(dataSets.SelectMany(data => data.Points).Max(point => point.y)+0.5f, 0), 1);
        }

        /// <summary>
        ///     Calculate the scaling and the label of the first marker.
        ///     Taking the chart width and height and min/max values into account.
        ///     e.g. if the chart width is 1000 and the x range is 0-100, then markers every 10 pixel will be too narrow,
        ///     ensure a spacing of and least MIN_SPACING=30 between markers.
        ///     valueRange 0..10, pixels 1000 -> markerLabel 1, scale 100
        ///     valueRange 0..20, pixels 1000 -> markerLabel 1, scale 50
        ///     valueRange 0..50, pixels 1000 -> markerLabel 2, scale 20 as the MIN_SPACING is 30
        ///     try to use multiples of 1,2,5=divisors for marker
        /// </summary>
        /// <param name="valueRange">range of values, e.g. 0..10</param>
        /// <param name="pixels">number of available pixels, i.e. height or width, e.g. 500</param>
        /// <returns>tuple label of the first marker and number of pixels for one unit</returns>
        private (int unitLabel, float scale) CalcUnits(Range valueRange, int pixels)
        {
            int range = (int)(valueRange.Max - valueRange.Min);
            float scale = pixels / (float)range;

            int[] divisors =
            {
                1, 2, 5, 10, 20, 50, 100, 200, 500, 1000
            };
            foreach (int divisor in divisors)
            {
                if (scale * divisor >= MIN_SPACING)
                    return (divisor, scale);
            }

            return (1000, scale);
        }

        /// <summary>
        ///     Calculate the next axis value based on the direction as multiples of 5.
        ///     e.g. if the value is 7 and the direction is 1, the result will be 10
        ///     -5 -> -5, 0->0, 3->5 and for direction -1: -6->-10, -3->-5, 0->0
        /// </summary>
        /// <param name="value">given value</param>
        /// <param name="direction">1 or -1</param>
        /// <returns></returns>
        public static int RoundToNextSignificant(int value, int direction)
        {
            const int MULTIPLE_OF = 5;
            if (value % MULTIPLE_OF == 0)
                return value;

            if (direction * value < 0) return 0; // always show the full first quadrant
            return value + direction * (MULTIPLE_OF - Math.Abs(value % MULTIPLE_OF));
        }

#region helper structs -- records would be better but not supported in Unity

        private class Range
        {
            public Range(float min, float max)
            {
                Min = min;
                Max = max;
            }

            public float Min { get; set; }
            public float Max { get; set; }
        }

        private class LineDataSet : DataEntry
        {
            public LineDataSet(List<Vector2> points, string label, Color color) : base(label, color)
            {
                Points = points;
            }

            public List<Vector2> Points { get; }
        }

        private class ChartGeometry
        {
            public int Width { get; set; } = 200;
            public int Height { get; set; } = 200;
            public Vector2 Origin { get; set; } = new(0, 0);
            public Range RangeX { get; } = new(0, 10);
            public Range RangeY { get; } = new(0, 10);
            public int FirstMarkerX { get; set; } = 1;
            public float ScaleX { get; set; } = 10f;
            public int FirstMarkerY { get; set; } = 1;
            public float ScaleY { get; set; } = 10f;
        }

#endregion

#region UXML & USS

        // CreateProperty is needed for localization, see https://docs.unity3d.com/Packages/com.unity.localization@1.5/manual/UIToolkit.html#bind-to-a-custom-element
        private string xAxisLabel = "X Axis";

        [UxmlAttribute]
        [CreateProperty]
        public string XAxisLabel
        {
            get => xAxisLabel;
            set
            {
                xAxisLabel = value;
                Debug.Log("set xLabel in linchart to " + xAxisLabel);
                Canvas.MarkDirtyRepaint();
            }
        }

        private string yAxisLabel = "Y Axis";

        [UxmlAttribute]
        [CreateProperty]
        public string YAxisLabel
        {
            get => yAxisLabel;
            set
            {
                yAxisLabel = value;
                Canvas.MarkDirtyRepaint();
            }
        }

        private bool showAxisMarker = true;

        [UxmlAttribute]
        public bool ShowAxisMarker
        {
            get => showAxisMarker;
            set
            {
                showAxisMarker = value;
                Canvas.MarkDirtyRepaint();
            }
        }

        private static readonly CustomStyleProperty<int>
            // ReSharper disable once InconsistentNaming -- we have a private fiels with the same name
            MarkerFontSize = new("--marker-font-size-int");

#endregion

#region Members and Consts

        private readonly ChartGeometry chartGeometry = new();
        private int markerFontSize = 10;

        private readonly List<LineDataSet> dataSets = new();
        
        private const int MARKER_LENGTH = 10; // length of the marker
        private const int MIN_SPACING = 30; // minimum spacing between markers
        private const float LINE_WIDTH = 2.0f; // width of the axis lines
        private const int PADDING_LABEL_AXIS = 5; // padding for the axis label

#endregion
    }
}
