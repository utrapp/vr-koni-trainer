# Thinking Aloud

## Einleitung

Herzlich willkommen zum Usability-Test für unsere VR-Anwendung des KONI TRAINER. Wir freuen uns, dass Sie heute Zeit gefunden haben, an unserem Test teilzunehmen und uns bei der Verbesserung der Benutzerfreundlichkeit zu unterstützen.

Während der Aufgaben bitten wir Sie, sich frei zu äußern und uns Einblicke in Ihre Gedanken zu geben.

Bevor wir beginnen, betonen wir, dass Ihr ehrliches und spontanes Feedback entscheidend für die Verbesserung der Anwendung ist. Ermutigen Sie uns während der Aufgaben, laut darüber nachzudenken, was Sie tun, und teilen Sie uns Ihre Eindrücke, Meinungen und Herausforderungen mit.

#### Generell zu beobachten

- die Testperson immer wieder darauf hinweisen, laut zu sprechen! Dies wird oft im Ablauf der Tests vergessen.
- Wann verspüren die Nutzer das Bedürfnis, die VR-Brille abzusetzen?
- Wie gut funktioniert die Steuerung in der VR?

## Usability Test Tasks

[Usability Tasks für einen **unerfahrenen** Benutzer]()
[Usability Tasks für einen **erfahrenen** Benutzer]()

### Abschließende Fragen:

- Wie gefällt Ihnen das Raumdesign?
- Wie ist Ihr abschließender erster Eindruck des VR-Koni Trainer?
- Wie würden Sie ein einführendes Tutorial für den Koni-Trainer gestalten
